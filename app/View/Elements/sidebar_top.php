
            <?php if ($this->Session->read('fe.type') == 2) { ?>
                <div class="thumbs col-lg-12">
                    <div class="row">
                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/syst/profile" class="thmb"><img src="<?php echo $this->webroot; ?>images/user.png"><br/>Edit Profile</a>
                        </div><!--col-lg-3 -->
                        <div class="col-sm-2  tmbbx">
                            <a href="<?php echo $this->base ?>/variations" class="thmb"><img src="<?php echo $this->webroot; ?>images/addvariation_icon.png"><br/>Variations</a>
                        </div><!--col-lg-3 -->
                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/products" class="thmb"><img src="<?php echo $this->webroot; ?>images/addproduct.png"><br/>Products</a>
                        </div><!--col-lg-3 -->

                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/syst/changePassword" class="thmb"><img src="<?php echo $this->webroot; ?>images/changepassword_icon.png"><br/>Change Password</a>
                        </div><!--col-lg-3 -->
                   
                        <div class="col-sm-2 tmbbx">
                            <a href="#" class="thmb"><img src="<?php echo $this->webroot; ?>images/history.png"><br/>Orders</a>
                        </div><!--col-lg-3 -->  
                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/users/logout" class="thmb"><img src="<?php echo $this->webroot; ?>images/loutout_icon.png"><br/>Logout</a>
                        </div><!--col-lg-3 -->
                    </div>
                </div><!--thumbs -->

            <?php } else { ?>
                <div class="thumbs col-lg-12">
                    <div class="row">
                        <div class="col-sm-2  tmbbx">
                            <a href="<?php echo $this->base ?>/syst/profile" class="thmb"><img src="<?php echo $this->webroot; ?>images/user.png"><br/>Edit Profile</a>
                        </div><!--col-lg-3 -->


                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/syst/changePassword" class="thmb"><img src="<?php echo $this->webroot; ?>images/changepassword_icon.png"><br/>Change Password</a>
                        </div><!--col-lg-3 -->


                        <div class="col-sm-2 tmbbx">
                            <a href="#" class="thmb"><img src="<?php echo $this->webroot; ?>images/history.png"><br/>Orders</a>
                        </div><!--col-lg-3 -->  
                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/users/logout" class="thmb"><img src="<?php echo $this->webroot; ?>images/loutout_icon.png"><br/>Logout</a>
                        </div><!--col-lg-3 -->
                    </div><!--row -->
                </div><!--thumbs -->

            <?php } ?>
