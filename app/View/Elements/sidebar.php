<!-- #HEADER -->
<?php
/*
$ctrl = $this->request->params['controller'];
$actn = $this->request->params['action'];
$vtn = ($ctrl == 'variations') ? "active" : "";
$pdt = ($ctrl == 'products') ? "active" : "";
$dbd = ($ctrl == 'syst' AND $actn == 'dashboard') ? "active" : "";
$pfl = ($actn == 'profile') ? "active" : "";
$chp = ($actn == 'changePassword') ? "active" : "";
$odr = ($ctrl == 'orders') ? "active" : "";
$cp = ($actn == 'changePassword' AND $ctrl == 'syst') ? "active" : "";
$pro = ($actn == 'profile' AND $ctrl == 'syst') ? "active" : "";
$dhb = ($actn == 'index' AND $ctrl == "system") ? "active" : "";

?> 
<div class="dshbrdmnu col-xs-12 col-sm-4 col-md-4 col-lg-3">
    <div class="sidebar-nav dashboardnav" style="padding-top:0px;margin-top: 5px;">
        <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <span class="visible-xs navbar-brand">Dashboard Menu</span>
                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                    <?php if ($this->Session->read('fe.type') == 2) { ?>
                        <ul class="nav navbar-nav">
                            <li class="<?php echo $dbd ?>"><a href="<?php echo $this->base; ?>/syst/dashboard"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span>Dashboard</a></li>
                            <li class="<?php echo $vtn ?>"><a href="<?php echo $this->base; ?>/variations"><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>Variations</a></li>
                            <li class="<?php echo $pdt ?>"><a href="<?php echo $this->base; ?>/products"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>Products</a></li>

                            <li><a href="#"><span class="glyphicon glyphicon-time" aria-hidden="true"></span>Orders</a></li>
                            <li class="<?php echo $pro; ?>"><a href="<?php echo $this->base; ?>/syst/profile"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Profile</a></li>
                            <li class="<?php echo $cp; ?>"><a href="<?php echo $this->base; ?>/syst/changePassword"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>Change Password</a></li>
                            <li><a href="<?php echo $this->base; ?>/users/logout"><span class="glyphicon glyphicon-off" aria-hidden="true"></span>Logout</a></li>
                        </ul>
                    <?php } else { ?>
                        <ul class="nav navbar-nav">
                            <li class="<?php echo $dbd ?>"><a href="<?php echo $this->base; ?>/syst/dashboard"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span>Dashboard</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-time" aria-hidden="true"></span>Orders</a></li>
                            <li class="<?php echo $pro; ?>"><a href="<?php echo $this->base; ?>/syst/profile"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Profile</a></li>
                            <li class="<?php echo $cp; ?>"><a href="<?php echo $this->base; ?>/syst/changePassword"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>Change Password</a></li>
                            <li><a href="<?php echo $this->base; ?>/users/logout"><span class="glyphicon glyphicon-off" aria-hidden="true"></span>Logout</a></li>
                        </ul>

                    <?php } ?>
                </div><!--nav-bar collapse -->
            </div><!--navbar-header -->
        </div><!--navbar-default -->
    </div><!--sidebar-nav -->

</div><!--col-lg3 -->

<?php */ ?>


          
            <?php if ($this->Session->read('fe.type') == 2) { ?>
                <div class="thumbs col-lg-12">
                    <div class="row">
                         <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/syst/dashboard" class="thmb"><img src="<?php echo $this->webroot; ?>images/home.png" width="44"><br/>Dashboard</a>
                        </div><!--col-lg-3 -->
                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/syst/profile" class="thmb"><img src="<?php echo $this->webroot; ?>images/user.png"><br/>Edit Profile</a>
                        </div><!--col-lg-3 -->
                        <div class="col-sm-2  tmbbx" style="display: none;">
                            <a href="<?php echo $this->base ?>/variations" class="thmb"><img src="<?php echo $this->webroot; ?>images/addvariation_icon.png"><br/>Variations</a>
                        </div><!--col-lg-3 -->
                          <div class="col-sm-2 tmbbx">
                            <a  href="<?php echo $this->base ?>/orders/vendorHistory" class="thmb"><img src="<?php echo $this->webroot; ?>images/history.png"><br/>Rental History</a>
                        </div><!--col-lg-3 -->  
                        
                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/products" class="thmb"><img src="<?php echo $this->webroot; ?>images/addproduct.png"><br/>Products</a>
                        </div><!--col-lg-3 -->

                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/syst/changePassword" class="thmb"><img src="<?php echo $this->webroot; ?>images/changepassword_icon.png"><br/>Change Password</a>
                        </div><!--col-lg-3 -->
                   
                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/users/logout" class="thmb"><img src="<?php echo $this->webroot; ?>images/loutout_icon.png"><br/>Logout</a>
                        </div><!--col-lg-3 -->  
                      
                    </div>
                </div><!--thumbs -->

            <?php } else { ?>
                <div class="thumbs col-lg-12">
                    <div class="row">
                         <div class="col-sm-2 tmbbx">
                             <a href="<?php echo $this->base ?>/syst/dashboard" class="thmb"><img src="<?php echo $this->webroot; ?>images/home.png" width="44"><br/>Dashboard</a>
                        </div><!--col-lg-3 -->
                        <div class="col-sm-2  tmbbx">
                            <a href="<?php echo $this->base ?>/syst/profile" class="thmb"><img src="<?php echo $this->webroot; ?>images/user.png"><br/>Edit Profile</a>
                        </div><!--col-lg-3 -->


                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/syst/changePassword" class="thmb"><img src="<?php echo $this->webroot; ?>images/changepassword_icon.png"><br/>Change Password</a>
                        </div><!--col-lg-3 -->


                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/syst/billingInfo" class="thmb"><img src="<?php echo $this->webroot; ?>images/billing.png" width="44"><br/>Billing Info</a>
                        </div><!--col-lg-3 -->  
                          <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/orders/history" class="thmb"><img src="<?php echo $this->webroot; ?>images/history.png"><br/>Rental History</a>
                        </div><!--col-lg-3 -->  
                        <div class="col-sm-2 tmbbx">
                            <a href="<?php echo $this->base ?>/users/logout" class="thmb"><img src="<?php echo $this->webroot; ?>images/loutout_icon.png"><br/>Logout</a>
                        </div><!--col-lg-3 -->
                    </div><!--row -->
                </div><!--thumbs -->

            <?php } ?>
