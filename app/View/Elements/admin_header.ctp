<!-- #HEADER -->
<?php $ctrl = $this->request->params['controller'];
$actn = $this->request->params['action'];
$usr = ($ctrl=='users')?"active":"";
$pdt = ($ctrl=='products')?"active":"";
$pkg = ($ctrl=='packages')?"active":"";
$pfl = ($actn=='profile')?"active":"";
$chp = ($actn=='changePassword')?"active":"";
$odr = ($ctrl=='orders')?"active":"";
$pmt = ($ctrl=='payments')?"active":"";
$stg = ($ctrl=='settings')?"active":"";
$dhb = ($actn=='index' AND $ctrl == "system")?"active":"";
?>


                <!-- #NAVIGATION -->
                <!-- Left panel : Navigation area -->
                <!-- Note: This width of the aside area can be adjusted through LESS variables -->
                <aside id="left-panel" style="padding-top: 0px;">

                        <!-- User info -->
                        <div class="login-info">
                                <span> <!-- User image size is adjusted inside CSS, it should stay as it --> 

                                        <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                                                <i class="fa fa-chevron-left"></i>
                                                <span>
                                                        WelCome Admin
                                                </span>
                                                <i class="fa fa-chevron-right"></i>
                                        </a> 

                                </span>
                        </div>
                        <!-- end user info -->

                        <!-- NAVIGATION : This navigation is also responsive

                        To make this navigation dynamic please make sure to link the node
                        (the reference to the nav > ul) after page load. Or the navigation
                        will not initialize.
                        -->
                        <nav>
                                <!-- NOTE: Notice the gaps after each icon usage <i></i>..
                                Please note that these links work a bit different than
                                traditional href="" links. See documentation for details.
                                -->

<ul>
<li class="<?php echo $dhb; ?>">
        <a href="<?php echo $this->base; ?>/system" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
</li>
<li class="<?php echo $pfl; ?>">
        <a href="<?php echo $this->base; ?>/system/profile" title="Profile"><i class="fa fa-pencil"></i> <span class="menu-item-parent">Profile</span></a>
</li>
<li class="<?php echo $pkg; ?>">
        <a href="<?php echo $this->base; ?>/packages/adminPackages" title="Packages"><i class="fa fa-tree"></i> <span class="menu-item-parent">Packages</span></a>
</li>
<li class="<?php echo $pdt; ?>">
        <a href="<?php echo $this->base; ?>/products/adminProducts" title="Products"><i class="fa fa-bank"></i> <span class="menu-item-parent">Products</span></a>
</li>
<li class="<?php echo $odr; ?>">
        <a href="#" title="Orders"><i class="fa fa-truck"></i> <span class="menu-item-parent">Orders</span></a>
</li>

<li class="<?php echo $pmt; ?>">
        <a href="#" title="Payment"><i class="fa fa-money"></i> <span class="menu-item-parent">Payment</span></a>
</li>
<li class="<?php echo $usr; ?>">
   <a href="<?php echo $this->base; ?>/users/adminUsers" title="Users"><i class="fa fa-group"></i> <span class="menu-item-parent">Users</span></a>
</li>
<li class="<?php echo $chp; ?>">
   <a href="<?php echo $this->base; ?>/system/changePassword" title="Change Password"><i class="fa fa-exchange"></i> <span class="menu-item-parent">Change Password</span></a>
</li>

<li class=" pull-right">
   <a href="<?php echo $this->base; ?>/users/logout" title="Logout"><i class="fa fa-sign-out"></i> <span class="menu-item-parent">Logout</span></a>
</li>
<li class="<?php echo $stg; ?> pull-right">
   <a href="<?php echo $this->base; ?>/settings/adminSetting" title="Settings"><i class="fa fa-gear"></i> <span class="menu-item-parent">Settings</span></a>
</li>




</ul>
                        </nav>
                        <span class="minifyme" data-action="minifyMenu"> 
                                <i class="fa fa-arrow-circle-left hit"></i> 
                        </span>

                </aside>
                <!-- END NAVIGATION -->