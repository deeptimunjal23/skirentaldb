<div class="newtoprow">
            <div class="container">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 social">
                    <div class="row">
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_sharing_toolbox"></div>    
                    </div><!--row -->
                </div><!--social -->
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 signin">
                    <div class="row">
                        <ul>
                            <?php if($this->session->read('fe.logedIn')){  ?>
                            <li><a href="<?php echo $this->base; ?>/syst/dashboard">My Account</a></li>
                            <li><a href="<?php echo $this->base; ?>/users/logout">Logout</a></li>
                            <?php }else{ ?>
                            <li id="registenMenu"><a href="javascript:void(0);">Register</a></li> 
                            <li id="loginWord"><a href="<?php echo $this->base; ?>/users/login">Login</a></li>
                            <li role="presentation"><a href="#">Group</a></li>
                            <?php } ?>
                        </ul>
                        <ul id="registenAs" style="display: none;">
                            <li>
                                <a href="<?php echo $this->base; ?>/pages/terms-of-vendor"><strong>Vendor Register</strong></a>
                            </li>
                            <li>
                                <a href="<?php echo $this->base; ?>/users/register/1"><strong>Customer Register</strong></a>
                            </li>

                        </ul>
                    </div><!--row -->
                </div><!--signin -->

            </div><!--container -->

        </div><!--top row -->

<div class="headerwrap">
    <div class="container">
        <header>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 logo">

                <a href="<?php echo $this->webroot ?>"><img src="<?php echo $this->base ?>/images/logo.png"></a>

            </div><!--logo -->

            
        </header>
    </div><!--container -->
</div><!--headerwrap -->
<script type="text/javascript">
            $(document).ready(function () {
                $("#registenMenu").click(function () {
                    $("#registenAs").toggle('slow');

                })
            })
        </script>
