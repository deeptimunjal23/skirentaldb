<div style="height:600px;">&nbsp;</div>
<?php if ($this->params['controller'] === 'home' AND $this->params['action'] === 'index') { ?>
    <div class="container">
        <div class="contentdiv">
            <div class="row">
			<!--
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 categoriesbx gallerybx" style="border: 1px solid #000000;padding-bottom: 20px;float: none;margin: 0px auto">
                    <h4 class="pagetitle centerpagetitle" style="text-align:center">Top Ski Trip Photos and Videos from our Community!</h4>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
                            <img src="<?php echo $this->webroot; ?>img/gal_img1.jpg" width="200" style="border: 1px solid #CCCCCC;padding:1px;">
                        </div><!--galleryimg -->
             <!--           <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
                            <img src="<?php echo $this->webroot; ?>img/gal_img2.jpg" width="200" style="border: 1px solid #CCCCCC;padding:1px;">
                        </div><!--galleryimg -->
            <!--            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
                            <img src="<?php echo $this->webroot; ?>img/gal_img3.jpg" width="200" style="border: 1px solid #CCCCCC;padding:1px;">
                        </div><!--galleryimg -->
            <!--     </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
                            <img src="<?php echo $this->webroot; ?>img/gal_img1.jpg" width="200" style="border: 1px solid #CCCCCC;padding:1px;">
                        </div><!--galleryimg -->
            <!--            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
                            <img src="<?php echo $this->webroot; ?>img/gal_img2.jpg" width="200" style="border: 1px solid #CCCCCC;padding:1px;">
                        </div><!--galleryimg -->
             <!--           <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
                            <img src="<?php echo $this->webroot; ?>img/gal_img3.jpg" width="200" style="border: 1px solid #CCCCCC;padding:1px;">
                        </div><!--galleryimg -->
            <!--        </div><!--gallerybx -->
            <!--    </div>
                <br><br>
				<!--
                <div class="textbx col-xs-8 col-sm-8 col-md-8 col-lg-8" style="border: 1px solid #000000;padding-bottom: 20px;margin-bottom: 20px;margin-top: 20px;margin: 0px auto;float:none">
                    <h4>skirentalDb.com - the world's largest ski and board rental reservation system</h2>
                        <p>skirentalDb's equipment rental reservation system allows users to compare equipment rental prices with just a few clicks from hundreds of specialty ski shops worldwide and save up to 30% off of typical walk-in rental rates. Whether it's a weekend getaway, a family ski trip, or just hitting the slopes for a day, you can search and reserve your equipment in just a few clicks!</p>
                        <br/>
                        <h4>Find the BEST rates on skirentalDb.com</h2>
                            <p>With skirentalDb you can find your ideal ski or board equipment at the BEST rate! Just fol-low the simple step by step process and then let our site do the work for you! SkirentalDb.com makes it easy for you to reserve your ski or board equipment right online. You can search from a wide variety of specialty ski shops and locations all across the U.S.</p>
                            <br/>
                            <h4>How to use the skirentalDb.com reservation system</h2>
                                <p>Simply enter where you want to go, your travel dates, whether you want ski's or snowboard, select your skill level, let us know if you want boots, then Wallah! Your recommended ski package will appear! If the recommended package looks good (or you can change it too!) then click the next button and you'll see a list of ski shops with the best rates on your preferred equipment. Simply pick a shop that's right for your trip, select "View Deal" and you're ready to go!</p>
                                </div>
			-->
			<!-------------------code for slider ----------------------->
			<div class="custom-container default col-xs-8 col-sm-8 col-md-8 col-lg-8">
				 <h2 id="TopSkiArea" class="pagetitle centerpagetitle" style="text-align: center; font-size: 35px; margin-top: 0px; padding-bottom: 26px;">Top Ski Trip Photos from our Community</h2>
				<a href="#" class="prev"><img src="<?php echo $this->webroot; ?>img/Left-arrow.png"></a>
				<div class="carousel responsiveAllWidth">
					<ul>
						<li><img src="<?php echo $this->webroot; ?>img/1.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/2.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/3.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/4.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/5.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/6.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/7.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/8.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/9.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/10.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/11.jpg"></li>
						<li><img src="<?php echo $this->webroot; ?>img/12.jpg"></li>
					</ul>
				</div>
				<a href="#" class="next"><img src="<?php echo $this->webroot; ?>img/Right-arrow.png"></a>
				<div class="clear"></div>
		</div>
			<!---------------------------------------------------------->
                                <br><br>
                             <!-- <div class="textbx col-xs-8 col-sm-8 col-md-8 col-lg-8" style="min-height: 170px; border: 1px solid #000000; padding-bottom: 20px;margin-bottom: 20px;margin: 0px auto;float:none;"> -->
                              <div class="custom-container default col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-bottom: 20px;margin-bottom: 20px;margin: 0px auto;float:none;">
								<!-- new code as exist want to client 30102015-->
									<h2 class="pagetitle centerpagetitle" style="text-align: center; font-size: 35px; padding-bottom: 49px;">Popular Ski Area Destinations</h2>
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
												<img src="<?php echo $this->webroot; ?>img/ski_rental_vail_colorado.png">
											</div><!--galleryimg -->
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
												<img src="<?php echo $this->webroot; ?>img/ski_rental_aspen_colorado.png">
											</div><!--galleryimg -->
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
												<img src="<?php echo $this->webroot; ?>img/ski_rental_vail_colorado.png">
											</div><!--galleryimg -->
										</div>
										</br>
										</br>
										</br>
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
												<img src="<?php echo $this->webroot; ?>img/ski_rental_beaver_creek_colorado.png">
											</div><!--galleryimg -->
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
												<img src="<?php echo $this->webroot; ?>img/ski_rental_copper_mountain_colorado.png">
											</div><!--galleryimg -->
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
												<img src="<?php echo $this->webroot; ?>img/ski_rental_winter_park_colorado.png">
											</div><!--galleryimg -->
										</div>
										</br>
										</br>
										</br>
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
												<img src="<?php echo $this->webroot; ?>img/ski_rental_keystone_colorado.png">
											</div><!--galleryimg -->
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
												<img src="<?php echo $this->webroot; ?>img/ski_rental_telluride_colorado.png">
											</div><!--galleryimg -->
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 galleryimg" style="text-align:center">
												<img src="<?php echo $this->webroot; ?>img/ski_rental_jackson_hole_wyoming.png">
											</div><!--galleryimg -->
										</div>
									
									
								<!-----------------------end of the code------------------------>
                                <!--  <h3>Top Ski and Snowboard Destinations</h3>
                                    <ul class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="list-style: none;">
                                        <?php
                                  //    $toDate = new DateTime('+1 day');
                                  //    $toDate->format('l, d/m/Y');
                                        ?>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Breckenridge&state=CO">Breckenridge, CO</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Vail&state=CO">Vail, CO</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Aspen&state=CO">Aspen, CO</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Steamboat Springs&state=CO">Steamboat Springs, CO</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Copper Mountain&state=CO">Copper Mountain, CO</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Loveland Pass&state=CO">Loveland Pass, CO</a></li>
                                    </ul>

                                    <ul class="col-lg-3 col-md-3 col-sm-4 col-xs-12 " style="list-style: none;margin-bottom: 40px;">
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Keystone&state=CO">Keystone, CO</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Beaver Creek&state=CO">Beaver Creek, CO</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Jackson Hole&state=WY">Jackson Hole, WY</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Winter Park&state=CO">Winter Park, CO</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Arapahoe Basin&state=CO">Arapahoe Basin, CO</a></li>
                                        <li><a href="<?php echo $this->webroot; ?>/search/selectDate?city=Telluride&state=CO">Telluride, CO</a></li>
                                    </ul> -->
                                </div>
                                </div>
                                </div>

                                </div>

<?php } ?>
<br><br>

                            <div class="newfooterouter">
                                <div class="container newfooter">

                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ftrlclm">
                                        <div class="ftrbxinner">
                                            <h1 class="ftrtitle">WHY USE SKIRENTALDB.COM?</h1>
                                            <div style="background-color: #fff;padding: 5px;text-align: center">
                                                <iframe width="290" height="163" src="https://www.youtube.com/embed/xlXk49Mh7go" frameborder="0" allowfullscreen></iframe>
                                            </div>
                                            <div style="padding: 5px;margin-top: 25px;text-align: center"> 
                                                
 <a href="#" target="_blank"><img src="<?php echo $this->webroot;?>images/f.png" border="0" alt="Facebook"/></a>
 <a href="#" target="_blank"><img src="<?php echo $this->webroot;?>images/t.png" border="0" alt="Twitter"/></a>
 <a href="#" target="_blank"><img src="<?php echo $this->webroot;?>images/g.png" border="0" alt="Google Plus"/></a>
 <a href="#" target="_blank"><img src="<?php echo $this->webroot;?>images/y.png" border="0" alt="You Tube"/></a>
<a href="#" target="_blank"><img src="<?php echo $this->webroot;?>images/p.png" border="0" alt="Printerst"/></a>
<a href="#" target="_blank"><img src="<?php echo $this->webroot;?>images/l.png" border="0" alt="Linkedin"/></a>
<a href="#" target="_blank"><img src="<?php echo $this->webroot;?>images/i.png" border="0" alt="Instagram"/></a>
                                               
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ftrlclm">
                                        <div class="ftrbxinner">
                                            <h1 class="ftrtitle">Quick Links</h1>
                                            <ul class="ftrlnks" style="width: 150px;padding: 5px;">
                                                <li><a href="<?php echo $this->webroot; ?>" style="color: #797979;">Home</a></li>
                                               <!-- <li><a href="#" style="color: #797979;">Locations</a></li>-->
                                              <li><a href="<?php echo $this->webroot; ?>pages/about-us" style="color: #797979;">About Us</a></li>
                                                <li><a href="<?php echo $this->webroot; ?>pages/terms-of-use" style="color: #797979;">Terms Of Use</a></li>
                                                <li><a href="<?php echo $this->webroot; ?>pages/privacy-policy" style="color: #797979;">Privacy Policy</a></li>
                                               <!-- <li><a href="<?php echo $this->webroot; ?>/syst/dashboard/" style="color: #797979;">My Account</a></li>-->
                                          <li><a href="<?php echo $this->webroot; ?>users/login/" style="color: #797979;">Login</a></li>
												<li><a href="#" style="color: #797979;">Group Info</a></li>

                                            </ul>
                                        </div>
                                    </div><!--ftrclm -->

                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ftrlclm">
                                        <div class="ftrbxinner">
                                            <h1 class="ftrtitle">Get In Touch</h1>
                                            <p style="width: 250px;padding: 5px;color: #797979;font-weight: bold;">Phone : 913-353-5959<br/> Email : <a href="mailto:info@skirentaldb.com" style="color: #797979;font-size:18px;">info@skirentaldb.com</a></p>
                                            

                                            <a class="vendorlginbtn" href="<?php echo $this->webroot; ?>users/login/">Vendor Login</a>
                                        </div>
                                    </div><!--ftrclm -->

                             </div><!--newfooter -->
                            <div class="container"><div class="copyright"></div></div>
                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="padding: 10px;color: #797979;float: none;margin: 0px auto;text-align: center">© 2015 SRDB, LLC All Rights Reserved</div>
                            </div><!--newfooterouter -->
<!------ code for slider --------->
<link rel="stylesheet" media="all" type="text/css" href="<?php echo $this->webroot; ?>js/style-demo.css">

   <!-- <script src="<?php echo $this->webroot; ?>js/jquery-1.11.1.js"></script> -->
    <script src="<?php echo $this->webroot; ?>js/jquery.easing-1.3.js"></script>
    <script src="<?php echo $this->webroot; ?>js/jquery.mousewheel-3.1.12.js"></script>
    <script src="<?php echo $this->webroot; ?>js/jquery.jcarousellite.js"></script>

	<script type="text/javascript">
        $(function() {
            $(".default .carousel").jCarouselLite({
                btnNext: ".default .next",
                btnPrev: ".default .prev"
            });
        });
    </script>
	<script>
		$(document).ready(function(){
			$(".galleryimg").click(function(){
				$(".galleryimg").removeClass("name");
				$(this).addClass("name");
      
			});
		});	
	</script>
<style>

.custom-container.default { width: 110% !important;}

.carousel img { height: 200px; width: 250px;}
.carousel.default.col-xs-8.col-sm-8.col-md-8.col-lg-8 { width: 100% !important;}
.carousel li {  margin-right: 25px !important; width: 250px !important;  height: 200px !important;}
.carousel {  margin-left: 35px; margin-right: 35px; }
.next { float: right !important; }
.textbx { margin-top: 20% !important;}
.next > img {
    margin-right: 77%;
    margin-top: -200px;
}
.prev > img{ margin-bottom: -19%;} 
.col-xs-12.col-sm-4.col-md-4.col-lg-4.galleryimg > img {width: 70%;}
    .custom-container.default.col-xs-8.col-sm-8.col-md-8.col-lg-8 {
    clear: both;
    padding-top: 39px;
}
@media screen and (max-width: 1100px){
	.carousel {
    width: 810px !important;
}
.prev > img {
    margin-bottom: -23%;
    margin-left: 34px;
}
 }
@media screen and (max-width: 1023px){
	.ftrbxinner iframe {
    width: 210px;
}
}

@media screen and (max-width: 979px){
	.ftrbxinner iframe {
    height: 140px;
    width: 150px !important;
}
}

@media screen and (max-width: 799px){
	.ftrbxinner iframe {
    width: 144px !important;
}
}

@media screen and (max-width: 767px){
	.ftrbxinner iframe {
    height: 150px;
    width: 470px !important;
}
}

@media screen and (max-width: 639px){
	.ftrbxinner iframe {
    width: 100% !important;
}
}

</style>
<!------------------------------->