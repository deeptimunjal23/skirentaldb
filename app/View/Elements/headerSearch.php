<div class="newtoprow">
            <div class="container">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 social">
                    <div class="row">
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_sharing_toolbox"></div>    
                    </div><!--row -->
                </div><!--social -->
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 signin">
                    <div class="row">
                        <ul>
                            <?php if($this->session->read('fe.logedIn')){  ?>
                            <li><a href="<?php echo $this->base; ?>/syst/dashboard">My Account</a></li>
                          <!--  <li><a href="<?php echo $this->base; ?>/carts/view"><?php echo $product_count; ?><img src="<?php echo $this->webroot ?>img/view_cart.png"></a></li>
							-->
						   <li><a href="<?php echo $this->base; ?>/users/logout">Logout</a></li>
                            <?php }else{ ?>
                            <li id="registenMenu"><a href="javascript:void(0);">Register</a></li> 
                            <li id="loginWord"><a href="<?php echo $this->base; ?>/users/login">Login</a></li>
                            <li role="presentation"><a href="#">Group</a></li>
                            <?php } ?>
                        </ul>
                        <ul id="registenAs" style="display: none;">
                            <li>
                                <a href="<?php echo $this->base; ?>/pages/terms-of-vendor"><strong>Vendor Register</strong></a>
                            </li>
                            <li>
                                <a href="<?php echo $this->base; ?>/users/register/1"><strong>Customer Register</strong></a>
                            </li>

                        </ul>
                    </div><!--row -->
                </div><!--signin -->

            </div><!--container -->

        </div><!--top row -->

<div class="headerwrap searcheaderwrap">
	<div class="container">
    	<header>
        	<div class="col-xs-4 logo">
            	 <a href="<?php echo $this->webroot; ?>"><img src="<?php echo $this->base ?>/images/logo.png"></a>
            </div><!--logo -->
            <?php 
            if($this->params['action'] != 'storeDetail' && $this->params['action'] != 'processRent' && $this->params['action'] != 'payment' && $this->params['action'] != 'view'){ ?>
            <div class="col-xs-4 selectdate">
            	<?php echo $this->Form->create('Search',array('id' => 'datePicForm')); ?>
                <div class="location">
                    <div class="form-group headerfrm">
					<!--	<input type="text" class="forOnChange"  id="birds" name="setMyLoc" value="<?php //if(!empty($_REQUEST['resort']) AND !empty($_REQUEST['state'])){ echo $_REQUEST['resort'].','.$_REQUEST['state'];} else if(empty($_REQUEST['resort']) AND !empty($_REQUEST['state'])){ echo ','.$_REQUEST['state'];}?>" style='border:1px solid #2782D1'>      
						-->    
						<input type="text" class="forOnChange"  id="birds" name="setMyLoc" value="<?php if(!empty($_REQUEST['resort']) AND !empty($_REQUEST['state'])){ echo $_REQUEST['resort'].','.$_REQUEST['state'];}?>" style='border:1px solid #2782D1'>      
				   </div>
               </div><!--location -->
               
                <div class="datepickerbox">
                	<div readonly="readonly" class=" checkin input-append date" id="dp3" data-date="12-02-2012" data-date-format="yyyy-mm-dd">
                            <input required="required" class="span2 date-pick forOnChange" id="datepicker" size="16" name="fromDate" value="<?php if(!empty($_REQUEST['fromDate'])){ echo $_REQUEST['fromDate'];} ?>" placeholder="Select From Date">
  						<span class="add-on"><i class="icon-th"></i></span>
					</div>
                	<div readonly="readonly"  class="checkout input-append date " id="dp4" data-date="12-02-2012" data-date-format="yyyy-mm-dd">
                            <input required="required"  class="span2 date-pick forOnChange" id="datepicker2" size="16" name="toDate" value="<?php if(!empty($_REQUEST['toDate'])){ echo $_REQUEST['toDate'];} ?>" placeholder="Select Date">
  						<span class="add-on"><i class="icon-th"></i></span>
					</div>
                </div><!--datepickerbx -->
                </form>
            </div><!--dateselect -->
            <?php } ?>
            
           
        </header>
    </div><!--container -->
</div><!--headerwrap -->

 <script type="text/javascript">
            $(document).ready(function () {
                $("#registenMenu").click(function () {
                    $("#registenAs").toggle('slow');

                })
				$("#birds").on("click", function () {
   $(this).select();
});
            }) 
        </script>
<style>
@media screen and (max-width: 1279px){
	.col-xs-4.selectdate { width: 46%;}
	.clickMe > img {  width: 100%; }
	}

@media screen and (max-width: 767px){
	.col-xs-4.selectdate {float: right; width: 60%;}
}
@media screen and (max-width: 639px){
	.col-xs-4.logo {  margin-bottom: 25px; text-align: center; width: 100%;}
	.col-xs-4.selectdate { width: 100% !important;}
	.col-xs-4.skiforImage {  float: right; margin-bottom: 30px;  width: 90%;}
 }

@media screen and (max-width: 359px){
	.packGesTitle { text-align: left !important;}
}		
		
</style>		
		

