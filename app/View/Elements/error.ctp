
<div class="alert alert-danger fade in">
    <button data-dismiss="alert" class="close" style="cursor: pointer;">
        ×
</button>
<i class="fa-fw fa fa-times"></i>
<strong>Error, </strong> <?php echo $message ?>
</div>

