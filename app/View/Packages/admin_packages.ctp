<?php 

echo $this->Session->flash(); ?>

<!-- Widget ID (each widget will need unique ID)-->
<div data-widget-sortable="false" data-widget-custombutton="false" data-widget-fullscreenbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" id="wid-id-x" class="jarviswidget jarviswidget-color-thistle" role="widget">

    <header>

        <h2>Packages</h2>				
        <h2 class="col col-2 pull-right" style="line-height: 30px;"><a style="padding: 5px;" href="<?php echo $this->base; ?>/packages/adminAdd" class="btn btn-primary btn-sm">Create New Package</a></h2>
    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->
            <input class="form-control" type="text">	
        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body  no-padding">

            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                <thead>			                
                    <tr>
                        <th>ID</th>
                        <th> Pic</th>
                        <th> Title</th>
                        <th> Boot</th>
                        <th> Ski Type</th>
                        <th> Ski For</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($result as $row): 
                       $pic =  $row['Packages']['pic']; 
                        ?>
                    <tr>
                         <td><?php echo $row['Packages']['id'] ?></td>
                        <td><?php if($pic){ ?><img src="<?php echo $this->webroot.'files/packages/'.$pic;?>" width="70"><?php }else{ ?><span class="label label-default padding-10">No Image</span><?php } ?></td> 
                       
                        <td><?php echo $row['Packages']['title'] ?> </td>
                        <td><?php echo ($row['Packages']['boot'])?'Yes':'No'?></td>
                        <td><?php echo $row['ST']['name']?></td>
                        <td><?php echo $row['SF']['name']?></td>
                        <td><a class="btn btn-xs bg-color-<?php echo ($row['Packages']['status'])?"green":"red" ?> txt-color-white" href="javascript:void(0);"><?php  echo ($row['Packages']['status'])?"Activated":"Deactivated" ?></a></td>
                        <td>
                        <a href="<?php echo $this->base; ?>/packages/adminEdit/<?php echo $row['Packages']['id'] ?>" class="btn bg-color-magenta txt-color-white">Edit</a>
                        <a href="<?php echo $this->base; ?>/packages/adminDelete/<?php echo $row['Packages']['id'] ?>" onclick="return confirm('You want to delete this row?')" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                   
                </tbody>
            </table>


        </div>
        <!-- end widget content -->

    </div>
    <!-- end widget div -->

</div>
