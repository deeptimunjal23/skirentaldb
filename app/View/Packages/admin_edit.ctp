   <?php echo $this->Session->flash(); ?>	     
<!-- Widget ID (each widget will need unique ID)-->
<div data-widget-sortable="false" data-widget-custombutton="false" data-widget-fullscreenbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" id="wid-id-x" class="jarviswidget jarviswidget-color-thistle" role="widget">

    <header>

        <h2>Edit Package</h2>				

    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->
            <input class="form-control" type="text">	
        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body  no-padding">

                      <?php echo $this->Form->create('Packages',array('class' => 'smart-form','enctype'=>'multipart/form-data')); ?>

            <fieldset>
                 <div class="row">
                    <section class="col col-6">
                        <label class="label">Ski Type</label>
                        <label class="select">
                            <?php 
                            $optns = array();
          foreach($this->App->getSkiTypes() as $st):
              $optns[$st['skis_types']['id']] = $st['skis_types']['name'];
              endforeach;
         echo $this->Form->select('skitype',$optns,array('label'=>false,'div'=>false,'placeholder' => 'State',"empty"=>"Select Skis Type",'required' => 'required')); ?><i></i> 


                        </label>
                    </section> 
                </div>
                <div class="row">
                    <section class="col col-6">
                        <label class="label">Ski For</label>
                        <label class="select">

                                 <?php 
                            $optns2 = array();
          foreach($this->App->getSkiFor() as $st):
              $optns2[$st['skis_for']['id']] = $st['skis_for']['name'];
              endforeach;
         echo $this->Form->select('skifor',$optns2,array('label'=>false,'div'=>false,'placeholder' => 'State',"empty"=>"Select Skis For",'required' => 'required')); ?><i></i> 

                            <i></i> </label>
                    </section> 
                </div>
                
                 <div class="row">
                    <section class="col col-6">
                        <label class="label">Boot</label>
                        <label class="select">

                                 <?php 
                            $boooot = array('No','Yes');
        
         echo $this->Form->select('boot',$boooot,array('label'=>false,'div'=>false,"empty"=>false,'required' => 'required')); ?><i></i> 

                            <i></i> </label>
                    </section> 
                </div>
               
                <div class="row">
                    <section class="col col-6">
                        <label class="label">Title</label>
                        <label class="input"> <i class="icon-prepend fa fa-text-width"></i>
          <?php echo $this->Form->input('title',array('label'=>false,'div'=>false,'required'=>'required','placeholder' => 'Title')); ?>
                        </label>
                    </section>       
                </div>

                <div class="row">
                    <section class="col col-6">
                        <label class="label">Description</label>
                        <label class="textarea">
          <?php echo $this->Form->textarea('description',array('label'=>false,'div'=>false,'required'=>'required','placeholder' => 'Description')); ?>
                        </label>
                    </section>       
                </div>
                <div class="row">

                </div>
                
                  <div class="row">
<section class="col col-4">
    <div class="input input-file">
        <span class="button">
            <input type="file" onchange="this.parentNode.nextSibling.value = this.value" name="pic" id="file2">Browse</span><input type="text" readonly="" placeholder="Include some files">
    </div>
</section>
                      <section class="col col-2">
                          <?php if(!empty($row2['Packages']['pic'])){ ?><img src="<?php echo $this->webroot; ?>files/packages/<?php echo $row2['Packages']['pic']; ?>" width="100"><?php }else{ ?><span class="label label-default padding-10">No Image</span><?php } ?>
                      </section>
                </div>
               
               
                <div class="row">
                    <div class="col col-4">
                        <label class="label">Status</label>
                        <label class="checkbox">
                            <input type="checkbox" value="1" <?php echo ($row2['Packages']['status'])?"checked='checked'":""; ?> name="data[Packages][status]">
                            <i></i>Active</label>

                    </div>

                </div>

            </fieldset>
            <footer>
                <button class="btn btn-primary" type="submit">
                    Update
                </button>
            </footer>
            </form>

        </div>
        <!-- end widget content -->

    </div>
    <!-- end widget div -->

</div>
<!-- end widget -->
