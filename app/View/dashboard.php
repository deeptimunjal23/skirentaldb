<div class="contentdiv">
    <?php echo $this->Session->flash(); ?>	
    <?php echo $this->element('sidebar'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php if ($this->Session->read('fe.type') == 2) { ?>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">         
                <div class="row">
                    <h1 style="padding-left: 15px;padding-bottom: 15px;">Business Profile Information</h1>
                    <div class="profilinner">
                        <div class="col-xs-2 profileimage">


                            <img src="<?php echo $this->webroot; ?><?php echo empty($row['User']['pic']) ? 'images/default.png' : 'img/avatars/' . $row['User']['pic']; ?>" class="prflpic">
                            <div class="btn-group editpic">
                                <a href="<?php echo $this->base; ?>/syst/profile#editPicture" ><button class="btn btn-success dropdown-toggle" type="button">Edit Picture </button></a>
                            </div><!--btngroup -->
                        </div><!--profileimg -->

                        <div class="col-xs-10 profileinfo">
                            <div>
                                <div style="font-size: 16px;font-weight: bold;width: 75%;float:left;"><?php echo!empty($row['User']['b_name']) ? $row['User']['b_name'] : 'Anonymous' ?></div>
                                <div style="width: 25%;float:right;text-align: right">
                                    <div  style="font-size: 18px;font-weight: bold;">SSN: <?php echo!empty($row['User']['ssn']) ? $row['User']['ssn'] : 'xxxxxxxx' ?>
                                    </div><div class="note" style="">(Shop Identifier Number)</div></div>
                                <div style="font-size: 16px;"><?php echo!empty($row['User']['b_address']) ? $row['User']['b_address'] : 'Anonymous' ?></div>
                                <div style="font-size: 14px;"><strong>Business Phone:</strong> <?php echo!empty($row['User']['b_phone']) ? "+ 1 (" . substr($row['User']['b_phone'], 0, 3) . ") - " . substr($row['User']['b_phone'], 3, -4) . ' - ' . substr($row['User']['b_phone'], 6, 4) : 'Anonymous' ?></div>
                                <div style="font-size: 14px;"><strong>Business FAX:</strong> <?php echo!empty($row['User']['b_fax']) ? $row['User']['b_fax'] : 'Anonymous' ?></div>
                                <div style="font-size: 14px;"><strong>Contact Email:</strong> <?php echo!empty($row['User']['b_email']) ? $row['User']['b_email'] : 'Anonymous' ?></div>
                                <div style="font-size: 14px;"><strong>Ski Shop Owner/Manager Name:</strong> <?php echo!empty($row['User']['firstname']) ? $row['User']['firstname'] : '' ?> <?php echo!empty($row['User']['lastname']) ? $row['User']['lastname'] : '' ?></div>
                                <div style="font-size: 14px;"><strong>Business Website:</strong> <?php echo!empty($row['User']['b_website']) ? $row['User']['b_website'] : 'Anonymous' ?> - <strong>Employee Size:</strong> <?php echo!empty($row['User']['employees']) ? $row['User']['employees'] : '0' ?>  - <strong>Complete Business Hours: </strong><?php echo!empty($row['User']['business_hours']) ? $row['User']['business_hours'] : '0' ?> Hours</div>


                            </div>

                        </div><!--profileinfo -->

                    </div><!--profilinner -->
                    <a href="<?php echo $this->base; ?>/syst/profile" class="btn btn-primary pull-right col-xs-1" style="margin-bottom: 10px;margin-right: 15px">Edit</a>

                </div><!--row -->
            </div> <!--col-lg-12 -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 proouter">
                <div class="row">
                    <div class="proinner">
                        <div class="col-xs-12 ">
                            <div class="col-xs-3 " style="font-size: 16px">Performance Metrics / Reports: </div>
                            <div class="col-xs-3 "><strong>Total Revenue:</strong> <span style='font-size:18px;color: #ed3333'>$455</span> </div>
                            <div class="col-xs-3 "><strong>Total Ski Rental Days:</strong> <span style='font-size:18px;color: #ed3333'>05</span> </div>
                            <div class="col-xs-3 "><strong>Total SRD Clients:</strong> <span style='font-size:18px;color: #ed3333'>15</span></div> 

                        </div><!--col-lg10 -->


                    </div><!--proinner -->
                </div><!--row -->
            </div> <!--proouter -->   
            <div class="statusbar col-lg-12" style="margin-top: 0px;">
                <span class="pagetitle">Products</span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 proouter">

                <?php foreach ($result as $row): ?>
                    <div class="row">
                        <div class="proinner">
                            <div class="col-xs-2">

                                <div class="pull-left proimage">
                                    <div style="margin-bottom: 5px;"><span><img src="<?php echo $this->webroot; ?>images/<?php
                                            switch ($row['SF']['name']) {
                                                case 'Beginners': echo 'be.gif';
                                                    break;
                                                case 'Intermediate':echo 'in.gif';
                                                    break;
                                                case 'Advanced':echo 'ex.gif';
                                            }
                                            ?>" height="18"></span></div>
                                    <img style="border:5px solid #fff" src="<?php echo $this->webroot; ?><?php echo empty($row['PPIC']['pic']) ? 'images/nopic.png' : 'files/products/' . $row['PPIC']['pic']; ?>"> 
                                </div><!--proimg -->

                            </div><!--col-lg-4 -->

                            <div class="col-xs-7 proinfo">
                                <h1 class="protitle"><a href="#"><?php echo $row['Products']['title'] ?></a></h1>
                                <div class="prodescription">
                                    <p style="font-size: 15px;"><?php echo substr($row['PCK']['description'], 0, 150) . '...' ?></p>

                                </div><!--prodescrption -->
                                <div class=" dsplnon skilevl">
                                    <div>Ski Brand: <span><?php echo!empty($row['Products']['ski_brand']) ? $row['Products']['ski_brand'] : "-----"; ?></span></div>
                                    <?php
                                    if (!empty($row['Products']['other_brand']) AND ! empty($row['Products']['other_brand_avi'])) {
                                        $oResult = unserialize($row['Products']['other_brand']);
                                        $brndAvi = unserialize($row['Products']['other_brand_avi']);
                                        foreach ($oResult as $kk => $ob):
                                            if (!empty($brndAvi[$kk]) AND ! empty($ob)) {
                                                ?>
                                                <div><?php echo $ob; ?>: <span><?php echo $brndAvi[$kk]; ?></span></div>
                <?php };
            endforeach;
        }
        ?>
                                </div>

                            </div><!--profileinfo -->
                            <div class="col-xs-3 proinfo">
                                <div class="col-xs-12">
                                    <div class="pull-right"><strong>Walk In Rate:</strong> <span style="font-size: 18px;color: #2167B8">$<?php echo ($row['Products']['walk_price']) ? $row['Products']['walk_price'] : '0'; ?></span></div>
                                    <div class="pull-right"><strong>Discounted Percentage:</strong> <span style="font-size: 18px;color: #00a300"><?php echo!empty($row['Products']['discount']) ? $row['Products']['discount'] : '0%'; ?>%</span></div>
                                    <div class="pull-right"><strong>SRDB Rate:</strong> <span style="font-size: 18px;color: #2167B8">$<?php echo!empty($row['Products']['srdb_price']) ? $row['Products']['srdb_price'] : '0'; ?></span></div><div>&nbsp;</div>
                                </div> 
                                <div>
                                    <a class="btn btn-danger pull-right col-xs-5" onclick="return confirm('You want to delete this product?')" href="<?php echo $this->base ?>/products/delete/<?php echo $row['Products']['id']; ?> " style="margin-left: 3px;margin-top: 10px">Delete</a> 
                                    <a class="btn btn-primary pull-right col-xs-5"  style="margin-top: 10px" href="<?php echo $this->base ?>/products/edit/<?php echo $row['Products']['id']; ?> ">Edit</a> 

                                </div><!--btngroup -->
                            </div>

                        </div><!--proinner -->
                    </div><!--row -->
    <?php endforeach; ?>

            </div>



<?php } else { ?>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">         
                <div class="row">
                    <h1 style=" padding-left: 15px;padding-bottom: 15px;">Basic Profile Information</h1>
                    <div class="profilinner">
                        <div class="col-xs-2 profileimage">


                            <img src="<?php echo $this->webroot; ?><?php echo empty($row['User']['pic']) ? 'images/default.png' : 'img/avatars/' . $row['User']['pic']; ?>" class="prflpic">
                            <div class="btn-group editpic">
                                <a href="<?php echo $this->base; ?>/syst/profile#editPicture" ><button class="btn btn-success dropdown-toggle" type="button">Edit Picture </button></a>
                            </div><!--btngroup -->
                        </div><!--profileimg -->

                        <div class="col-xs-10 profileinfo">

                            <table class="table table1" style="border-collapse: separate; ">

                                <tbody> 
                                    <tr>
                                        <td style="border:none;">
                                            <div><strong>First Name: </strong><?php echo!empty($row['User']['firstname']) ? $row['User']['firstname'] : 'Anonymous' ?></div>
                                            <div><strong>Last Name: </strong><?php echo!empty($row['User']['lastname']) ? $row['User']['lastname'] : 'Anonymous' ?></div>
                                            <div><strong>Email: </strong><?php echo!empty($row['User']['email']) ? $row['User']['email'] : 'Anonymous' ?></div>
                                            <div><strong>Address: </strong><?php echo!empty($row['User']['address']) ? $row['User']['address'] : 'Anonymous' ?></div>
                                            <div><strong>Business Phone:</strong> <?php echo!empty($row['User']['b_phone']) ? "+ 1 (" . substr($row['User']['telephone'], 0, 3) . ") - " . substr($row['User']['telephone'], 3, -4) . ' - ' . substr($row['User']['telephone'], 6, 4) : 'Anonymous' ?></div>
                                            <div><strong>City / State / Postal Code: </strong><?php echo!empty($row['User']['city']) ? $row['User']['city'] : 'Anonymous' ?> / <?php echo!empty($row['User']['state']) ? $this->App->getStateName($row['User']['state']) : 'Anonymous' ?> / <?php echo!empty($row['User']['postalcode']) ? $row['User']['postalcode'] : '-----' ?></div></td>
                                    </tr>
                                </tbody>


                                <tbody>


                                    <tr>
                                        <th style="border-top:none">Facebook Address</th>
                                        <th style="border-top:none">Renter Info</th>
                                        <th style="border-top:none">Previous Packages</th>
                                    </tr>

                                    <tr>
                                        <td style="padding-top:15px;">
                                            <?php if (!empty($row['User']['fb_address']) || !empty($row['User']['fb_id'])) { ?>
                                                <a href="https://facebook.com/<?php echo!empty($row['User']['fb_address']) ? $row['User']['fb_address'] : $row['User']['fb_id']; ?>" target="_blank">https://facebook.com/<?php echo!empty($row['User']['fb_address']) ? $row['User']['fb_address'] : $row['User']['fb_id']; ?></a>
                                                <?php
                                            } else {
                                                echo '---------------';
                                            }
                                            ?>
                                        </td>
                                        <td style="padding-top:15px"><a href="#" style="padding: 3px;background-color: #FF6347;color: #fff;font-weight: bold;font-size: 11px;">Other Family Renters</a></td>
                                        <td style="padding-top:15px"><img src="<?php echo $this->webroot; ?>images/be.gif"> <img src="<?php echo $this->webroot; ?>images/in.gif"> <img src="<?php echo $this->webroot; ?>images/ex.gif"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="">

                            </div><!--btngroup -->
                        </div><!--profileinfo -->


                    </div><!--profilinner -->
                    <a href="<?php echo $this->base; ?>/syst/profile" class="btn btn-primary pull-right" style="margin-bottom: 10px;margin-right: 15px;">Edit</a>
                </div><!--row -->
            </div> <!--col-lg-12 -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">

                <div class="row">
                    <h3 style=" padding-left: 15px;padding-bottom: 15px;">Billing Information</h3>
                    <div class="proouter">

                        <div class="col-xs-4" style="padding-bottom: 20px;">
                            <img src="<?php echo $this->webroot; ?>images/creditcard.png" title="Credit Card" alt="Credit Card">

                        </div><!--col-lg-4 -->
                        <div class="col-xs-2" style="font-size: 18px;font-weight: bold;">OR</div>
                        <div class="col-xs-4">
                            <img src="<?php echo $this->webroot; ?>images/paypal.png" title="Paypal" alt="Paypal">


                        </div><!--col-lg-4 -->

                        <div class="col-xs-2">
                            <a class="btn btn-primary pull-right" href="<?php echo $this->base ?>/syst/billingInfo" style="margin-bottom: 10px;">Edit</a> 
                        </div><!--skilevl -->


                    </div><!--proinner -->
                </div><!--row -->
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">

                <div class="row">

                    <div class="proinner">

                        <h3 style=" padding-left: 15px;padding-bottom: 15px;">Rental History</h3>
                        <table class="table table1">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Package / Product</th>
                                    <th>Store / Shop</th>
                                    <th>Rental Days</th>
                                    <th>Rental Price</th>
                                    <th>Date Time</th>
                                    <th>Payment Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Lorem Dummy / Impsem Test</td>
                                    <td>Lorem Dummy / Impsem Test</td>
                                    <td>3</td>
                                    <td>$300</td>
                                    <td>25-5-2015 08:15:35</td>
                                    <td>Success</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Lorem Dummy / Impsem Test</td>
                                    <td>Lorem Dummy / Impsem Test</td>
                                    <td>5</td>
                                    <td>$550</td>
                                    <td>25-5-2015 08:15:35</td>
                                    <td>Success</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Lorem Dummy / Impsem Test</td>
                                    <td>Lorem Dummy / Impsem Test</td>
                                    <td>2</td>
                                    <td>$210</td>
                                    <td>25-5-2015 08:15:35</td>
                                    <td>Success</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Lorem Dummy / Impsem Test</td>
                                    <td>Lorem Dummy / Impsem Test</td>
                                    <td>7</td>
                                    <td>$350</td>
                                    <td>25-5-2015 08:15:35</td>
                                    <td>Success</td>
                                </tr>
                            </tbody>


                        </table>
                    </div>
                </div><!--row -->
            </div>

<?php } ?>


    </div> <!--col-lg-9 -->

</div><!--contentdiv -->

<style type="text/css">
    .bldtxt1{font-size:18px;}
    .prflpic{clear:both; margin-bottom:15px; display:block; max-width:156px;}
    .profilouter{background: none repeat scroll 0 0 #f1f5f8;border: 1px solid #e0ecf5;
                 clear: both;
                 content: "";
                 display: table;
                 margin-bottom: 10px;

                 width: 100%;
    }
    .profilinner{padding:0px 20px;}
    .table1 tr td{margin-bottom:10px;}
    .editpic button{width:152px;}
    hdetails5 span{color:#ed3333;}

    .proinner{background: none repeat scroll 0 0 #f1f5f8;
              border: 1px solid #e0ecf5;
              clear: both;
              content: "";
              display: table;
              margin-bottom: 10px;
              padding-top: 15px;
              padding-bottom: 10px;
              width: 100%;
    }
    .proimage img{max-width:120px; width:100%;}
    .skilevl div{font-size:12px;color:#004d60}
    .skilevl div span{color:#5fcb08;font-size: 14px;}
    .proinfo .protitle{font-size:16px; margin-top:6px;}
    .proinfo .prodescription{margin-top:10px;}
    @media screen and (max-width: 999px){
        .dsplnon{margin:0px!important;}
    }
</style>