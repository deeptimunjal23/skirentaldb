<div class="contentdiv">
    <div class="row">
   <!-- <div class="col-lg-2">
               <a class="btn btn-default cstmbuttons skibtn snowbtn nextResult" href="javascript:history.back()" style="margin-top: 20px;">Back</a>
            </div>-->
        <?php if(!empty($_REQUEST['skiType']) AND $_REQUEST['skiType'] == 'ski'){ ?>
        
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 responsivewidth" style="float: none;margin: 0px auto;padding-top:25px;">
            <h1 class="pagetitle centerpagetitle" style="text-align: center;">Select Your Ski Level</h1>
            <div class="skiforbx row-center">
                <div class="col-xs-4 skiforImage" style="cursor: pointer">
                    <div class="clickMe" val="beginners" style="float: left;">
                        <img src="<?php echo $this->webroot; ?>images/beginnerimg.jpg">
                    </div><!--begnrsbx -->
                    <div style="float: left;width: 263px;">
                        <h5 class="packGesTitle" style="text-align: center;font-weight:bold">BEGINNER / TYPE I SKIERS</h5>
                    <ul style="text-align: left;padding-left: 0px;"><li>Ski conservatively</li>
  <li>Prefer slower speeds</li>
  <li>Lower release settings - your skis will "unsnap" from your boots easier.</li></ul>
  
                    </div>
                </div><!--begnrswrap -->

                <div class="col-xs-4 skiforImage"  style="cursor: pointer">
                    <div class="clickMe" val="intermediate"  style="float: left;">
                       <img src="<?php echo $this->webroot; ?>images/interimg.jpg">
                    </div><!--begnrsbx -->
                    <div style="float: left;width: 263px;">
                    <h5 class="packGesTitle" style="text-align: center;font-weight:bold">INTERMEDIATE / TYPE II SKIERS</h5>
                    <ul style="text-align: left;padding-left: 0px;"> <li>Ski moderately</li>
<li>Prefer a variety of speeds</li>
<li> Ski on Greens, Blues or Blacks</li></ul>

                   
                    </div>
                </div><!--begnrswrap -->

                <div class="col-xs-4 skiforImage"  style="cursor: pointer;">
                    <div class=" clickMe" val="expert"  style="float: left;">
                        <img src="<?php echo $this->webroot; ?>images/expertimg.jpg">
                    </div><!--begnrsbx -->
                     <div style="float: left;width: 263px;">
                   <h5 class="packGesTitle" style="text-align: center;font-weight:bold">ADVANCED / TYPE III SKIERS</h5>
                    <ul style="text-align: left;padding-left: 0px;"> <li> Ski Aggressively</li>
                        <li> Ski at high speeds</li>
                        <li> Prefer steeper and more challenging
                            terrain and moguls</li></ul>
                       
                </div><!--begnrswrap -->
                </div>
            </div><!--skiforbx -->
        </div><!--categoriesbx -->
        
        
        
        <?php }else{ ?>
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="float: none;margin: 0px auto;;padding-top:25px;">
                    <h1 class="pagetitle centerpagetitle" style="text-align: center;">Select Your Snowboard Level</h1>
            <div class="skiforbx row-center">
                <div class="col-xs-4" style="cursor: pointer">
                    <div class="clickMe" val="beginners" style="float: left;">
                        <img src="<?php echo $this->webroot; ?>images/BbeginnerimgB.jpg">
                    </div><!--begnrsbx -->
                    <div style="float: left;width: 263px;">

                    <h5 class="packGesTitle" style="text-align: center;font-weight:bold">BEGINNER / TYPE I BOARDERS</h5>
                    <ul style="text-align: left;padding-left: 0px;"><li>Snowboard conservatively</li>
  <li>Prefer slower speeds</li>
  <li>Prefer Greens, some easier Blues</li></ul>
  
                    </div>
                </div><!--begnrswrap -->

                <div class="col-xs-4"  style="cursor: pointer">
                    <div class="clickMe" val="intermediate"  style="float: left;">
                       <img src="<?php echo $this->webroot; ?>images/Binterimg.jpg">
                    </div><!--begnrsbx -->
                    <div style="float: left;width: 263px;">
                    <h5 class="packGesTitle" style="text-align: center;font-weight:bold">INTERMEDIATE / TYPE II BOARDERS</h5>
                    <ul style="text-align: left;padding-left: 0px;"> <li>Snowboard  moderately</li>
<li>Prefer a variety of speeds</li>
<li> Board on Greens, Blues or Blacks</li></ul>

                  </div>
                </div><!--begnrswrap -->

                <div class="col-xs-4"  style="cursor: pointer;">
                    <div class=" clickMe" val="expert"  style="float: left;">
                        <img src="<?php echo $this->webroot; ?>images/Bexpertimg.jpg">
                    </div><!--begnrsbx -->
                     <div style="float: left;width: 263px;"> 
                   <h5 class="packGesTitle" style="text-align: center;font-weight:bold">ADVANCED / TYPE III BOARDERS</h5>
                    <ul style="text-align: left;padding-left: 0px;"> <li> Snowboard  Aggressively</li>
                        <li> Snowboard at high speeds</li>
                        <li> Prefer steeper, more challenging         
   terrain, jumps and moguls!</li></ul>
                       
                </div><!--begnrswrap -->
                </div>
            </div><!--skiforbx -->
        </div><!--categoriesbx -->
        <?php } ?>
        
        
        
    </div><!--row -->
</div><!--contentdiv -->
<br><br><br>
<!-- 28/10/2015 -->
<!-- <a class="btn btn-primary" href="javascript:history.back()">
                    Back</a> -->
<a class="btn btn-primary" href="<?php echo $this->webroot; ?>">Back</a>
<script type="text/javascript">

    $(document).ready(function () {
        $(".clickMe").click(function () {
            var goto = $(this).attr('val');
			if(goto == 'beginners'){
				goto = 1;
			}
			else if(goto == 'intermediate'){
				goto = 2;
			}
			else if(goto == 'expert'){
				goto = 3;
			}
			else{
				goto = 3;
			}
            var cs = $("#birds").val().split(',');
            var fD = $("#datepicker").val();
            var tD = $("#datepicker2").val();
		/*	if(cs[0] == ''){
				var qs = "state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=<?php echo ($_REQUEST['skiType'] == "ski")?1:2; ?>"+"&skiFor="+ goto + "&boot=<?php echo $_REQUEST['boot']; ?>";
			}
			else{
		*/
				var qs = "resort=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=<?php echo ($_REQUEST['skiType'] == "ski")?1:2; ?>"+"&skiFor="+ goto + "&boot=<?php echo $_REQUEST['boot']; ?>";
         //  }
			window.location.href = ajaxUrl + "/search/selectPackage?" + qs;
        })
    })
</script>
<style type="text/css">
    .skifrwrap {
    display: inline-block;
    float: none;
}

.packGesTitle{
    font-size: 12px;
    font-weight: bold;
    text-align: left;
}

@media screen and (max-width: 1023px){
	.col-xs-10.col-sm-10.col-md-10.col-lg-10.responsivewidth {
    width: 100% !important;
}
}
@media screen and (max-width: 639px){
	.col-xs-10.col-sm-10.col-md-10.col-lg-10.responsivewidth {
    width: 83.333% !important;
}
}

@media screen and (max-width: 479px){
#package_list > label {
    text-align: center;
    width: 84% !important;
}
.chngpkg {
    margin-left: 37px !important;
}
}
</style>
