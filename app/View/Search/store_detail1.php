<?php

if (isset($_REQUEST['fromDate']) AND ! empty($_REQUEST['fromDate'])) {
    $expF = explode(', ', $_REQUEST['fromDate']);
    $expF2 = explode('/', end($expF));
    $makeFromDate = $expF2[2] . '-' . $expF2[0] . '-' . $expF2[1];
}
if (isset($_REQUEST['toDate']) AND ! empty($_REQUEST['toDate'])) {
    $expT = explode(', ', $_REQUEST['toDate']);
    $expT2 = explode('/', end($expT));
    $makeToDate = $expT2[2] . '-' . $expT2[0] . '-' . $expT2[1];
}

if (!empty($row)) {
    $getOpenTime = unserialize($row['BH']['open_time']);
    $getOpenTimeAmPm = unserialize($row['BH']['open_ampm']);
    $getCloseTime = unserialize($row['BH']['close_time']);
    $getCloseTimeAmPm = unserialize($row['BH']['close_ampm']);
    $weeks = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
}

$address =$row['U']['b_address'].' '.$row['U']['b_city'].' '.$row['U']['b_state'];
$address = str_replace(" ", "+", $address);
$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
$response_a = json_decode($response);
$lat = $response_a->results[0]->geometry->location->lat;
$long = $response_a->results[0]->geometry->location->lng;
?>
 <style>
      #map-canvas {
        width: 100%;
        height: 300px;
      }
    </style>
<script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
          var latitude = parseFloat("<?php echo $lat; ?>"); // Latitude get from above variable
        var longitude = parseFloat("<?php echo $long; ?>");
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(latitude, longitude),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
}
var map = new google.maps.Map(mapCanvas, mapOptions)
var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latitude, longitude),
      map: map,
      title: "<?php echo $row['U']['b_name']; ?>",
  });
        
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>


<div class="contentdiv">
    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 sidebarcheckbx leftbarwrap">
        <div class="leftbar vendorinformation">
            <h1 class="protitle">Store Information</h1>
            <div class="vendordetail">
                <div><span class="storename"><?php echo $row['U']['b_name']; ?></span></div>
                <div><?php echo $row['U']['b_address']; ?></div>
                <div><?php echo $row['U']['b_city'] . ' , ' . $row['U']['b_state'].' '.$row['U']['b_postalcode']; ?></div>
                <div><?php $data= $row['U']['b_phone']; echo $data; ?></div>
                <div><a href="mailto:<?php echo $row['U']['b_email']; ?>"><?php echo $row['U']['b_email']; ?></a></div>

                <div class="resrvtionprice">
                    <span class="resrvtxt">Daily Rate</span>
                    <span class="resrvprice" style="color:#1D9F36">$<?php echo $row['Products']['srdb_price']; ?></span>
                    <span class="wlktxt">Walk In Rate <span style="color:#ED3333">$<?php echo $row['Products']['walk_price']; ?></span></span>
                    <span class="savetxt">You Save <?php echo $row['Products']['discount']; ?>%</span>
                </div><!--resrvationprice -->

                <div class="resrvtionprice" style="float: left;">
                    <p style="font-size: 14px;font-weight: bold;text-align: center;color:#3867a4">Weekly Store Schedule</p>
                    <?php
                    for ($x = 0; $x <= 6; $x++) {
                        $weekTime = ($getCloseTime[$x] == 'closed' || $getOpenTime[$x] == 'closed') ? 'Closed' : $getOpenTime[$x] . $getOpenTimeAmPm[$x] . ' - ' . $getCloseTime[$x] . $getCloseTimeAmPm[$x];
                        ?>
                        <div style="float: left;border-bottom: 1px solid #0066cc;line-height: 25px;">
                            <div class="fixedWeekHour" weekDaysName ="<?php echo $weeks[$x]; ?>" weekDaysStatus ="<?php echo $weekTime; ?>" style="text-align: left;width: 45px;float: left;"><strong><?php echo $weeks[$x] . ': </strong> '; ?></div><div style="float: left;width: 150px;text-align: right;"><?php echo $weekTime; ?></div>
                        </div>
                    <?php } ?>
                </div>
                <br>
                 <div class="resrvtionprice"  id="map-canvas" style="float: left;">
                 
                 </div>
                 <br>
                  <br>
                <a href="#">Affiliate Policies</a>
            </div><!--vendordetail -->
        </div><!--leftbar -->
    </div><!--leftbarwrap -->

    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 centerbar prodetails">

        <div class="row prodcsta paddingtop10">
            <div class="col-xs-7 "><h1 class="protitle"><?php echo $row['U']['b_name']; ?></h1>
 <div class='ratings' style="width:100%;height:27px;padding:0;border:none;">



                        <?php if ($this->Session->read('fe.logedIn') AND $this->App->countRatingByUserId($row['Products']['id'], $this->Session->read('fe.userId')) <= 0) { ?>
                           

                            <div class="col-xs-12 padding0 mrgnbtm" id="rating_2" who ='<?php echo $row['Products']['id']; ?>' >
                                <div rate ='1' class="star_1 ratings_stars"></div>
                                <div rate ='2'  class="star_2 ratings_stars"></div>
                                <div rate ='3' class="star_3 ratings_stars"></div>
                                <div rate ='4' class="star_4 ratings_stars"></div>
                                <div rate ='5' class="star_5 ratings_stars"></div>
                             
                        <?php } else {
                            ?>
                           
                            <div class="col-xs-12 padding0" id="rating_2" who ='<?php echo $row['Products']['id']; ?>' >
                                <?php
                                echo $this->App->getRatingById($row['Products']['id']);
                            }
                            ?>
                        </div> 
                    </div>

<!--protitle --></div>
            <div class="col-xs-5"><div class="row"><h1 class=" protitle">Selected Package</h1></div> </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 prodctgallry">

                <div class="largimage"><img src="<?php echo $this->webroot; ?><?php echo (!empty($row['U']['pic'])) ? 'img/avatars/' . $row['U']['pic'] : 'images/nopic.png'; ?>" class="imagegl"> </div><!--largimg -->
                <div style="margin-top: 25px;">
                    <p><strong>About Store: </strong><?php if($row['U']['about_store']!='') { echo $row['U']['about_store']; } else { echo "No information provided yet."; }?></p>
                </div>
<!-- 9/29/2015-->
<div style="margin-top: 25px;">
                    <p><strong>Why You Should Choose Us: </strong><?php if($row['U']['choose']!='') { echo $row['U']['choose']; } else { echo "No information provided yet."; }?></p>
                </div>
<div style="margin-top: 25px;">
                    <p><strong>Meet Team: </strong><?php if($row['U']['meetteam']!='') { echo $row['U']['meetteam']; } else { echo "No information provided yet."; }?></p>
                </div>
<!---------->


            </div><!--prodctglry -->

            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 prodctdescrption">

                <div class="row">

                    <div>

			<h4 style="font-size: 14px; font-weight: bold;margin-top:0px"><?php echo $row['PCK']['title']; ?></h4>
<div style="float:left;width:100%;">

                        <img src="<?php echo $this->webroot; ?><?php echo (!empty($row['PPIC']['pic'])) ? 'files/products/' . $row['PPIC']['pic'] : 'images/nopic.png'; ?>"  style="width:120px;margin-right:5px;margin-bottom:5px;border:3px solid #ccc" align="left">
                        <?php if (!empty($row['Products']['ski_brand'])) { ?>
                            <p style="text-align: left;line-height:14px;"><strong>Ski Brands : </strong> <?php echo $row['Products']['ski_brand']; ?></p>
                        <?php }if (!empty($row['Products']['boot_brand'])) { ?>
                            <p style="text-align: left;line-height:14px;"><strong>Boots Brands : </strong> <?php echo $row['Products']['boot_brand']; ?></p>
                        <?php } ?>
                        <?php
                        if (!empty($row['Products']['other_brand'])) {
                            foreach (unserialize($row['Products']['other_brand']) as $kk => $ob):
                                $aviB = unserialize($row['Products']['other_brand_avi']);
                                if (!empty($ob)) {
                                    ?>
                                    <p style="text-align: left;line-height:14px;"><strong><?php echo $ob; ?> Brands : </strong> <?php echo $aviB[$kk]; ?></p>

                                    <?php
                                }
                            endforeach;
                        }
                        ?>
</div>
                    </div>
                    <br>
                  
<div style="float:left;width:100%;">
<p><strong>Package Description: </strong><?php echo $row['Products']['description']; ?></p>
</div>


                   
                    <div id="rentButton" class="form-group clearfix">

                    </div>
                </div>

            </div><!--row -->
        </div><!--prodctdescrption -->
    </div><!--row -->
</div><!--prodetails -->
</div><!--contentdiv -->

<script type="text/javascript">
    $(document).ready(function () {

        var fD = "<?php echo isset($makeFromDate) ? $makeFromDate : '';  ?>";
        var tD = "<?php echo isset($makeToDate) ? $makeToDate : '';  ?>";
        var betweenDays = betweenDates(fD, tD);
        myArr2 = [];
        var veryOk = [];
        $(".fixedWeekHour").each(function () {
            if ($(this).attr('weekDaysStatus') == 'Closed') {
                myArr2.push($(this).attr('weekDaysName'));
            }

        });


        //console.log(myArr2);

        if (myArr2.length <= 0) {
            var nextlink = ajaxUrl + "/rental/index/<?php echo $row['Products']['id'] . '?' . $_SERVER['QUERY_STRING'] ?>";
            $("#rentButton").html("<a href=" + nextlink + "><button type='button' class='btn btn-default cstmbuttons skibtn skibtn2' value='Rent Now' name='' style='margin-top:15px;'>Rent Now</button></a>");
            return true;
        } else {

            $.each(myArr2, function (key, val) {
                if (isExistIn(myArr, val)) {
                    var nextlink = ajaxUrl + "/rental/index/<?php echo $row['Products']['id'] . '?' . $_SERVER['QUERY_STRING'] ?>";
                    $("#rentButton").html("<a href=" + nextlink + "><button type='button' class='btn btn-default cstmbuttons skibtn skibtn2' value='Rent Now' name='' style='margin-top:15px;'>Rent Now</button></a>");
                    return true;
                } else {
                    var backlink = ajaxUrl + "/search/getByPackages?<?php echo $_SERVER['QUERY_STRING'] ?>";
                    $("#rentButton").html("<div style='margin-top:20px;'>Store is closed for some days in date range selected by you, <a href=" + backlink + ">click here</a> to select other days or select other Ski Store</div>");
                    return false;
                }


            });
        }



        $('.ratings_stars').hover(
                function () {
                    $(this).prevAll().andSelf().addClass('ratings_over');
                },
                function () {
                    $(this).prevAll().andSelf().removeClass('ratings_over');
                }
        );
//send ajax request to rate.php
        $('.ratings_stars').on('click', function () {

            var id = $(this).parent().attr("who");
            var id2 = $(this).parent().attr("id");
            var num = $(this).attr("rate");
            var poststr = "id=" + id + "&stars=" + num;
            $.ajax({url: ajaxUrl + "/search/getSetRating", cache: 0, data: poststr, success: function (result) {
                    document.getElementById(id2).innerHTML = result;
                }
            });
        });



    })

    function isExistIn(myArr, val2) {
        var okCount = [];
        // console.log(val2);
        // console.log(myArr);
        if ($.inArray(val2, myArr) === -1) {
            okCount.push(val2);
        }
        return okCount.length;
    }
    function betweenDates(fod, tod) {
        var day = 1000 * 60 * 60 * 24;
        date1 = new Date(fod);
        date2 = new Date(tod);
        myArr = [];
        setArr = {};
        weekName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var diff = (date2.getTime() - date1.getTime()) / day;
        for (var i = 0; i <= diff; i++)
        {
            var xx = date1.getTime() + day * i;
            var yy = new Date(xx);
            //setArr[yy.getDay()] = weekName[yy.getDay()];
            myArr.push(weekName[yy.getDay()]);
        }
        return myArr;
    }


</script>
