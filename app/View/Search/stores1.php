<div class="contentdiv">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 sidebarcheckbx leftbarwrap">
            <div class="leftbar">
                <div style="font-size: 16px;font-weight: bold;">CURRENT SELECTIONS</div>
                <div style="font-size: 10px;padding-bottom: 10px;"> Change your current selections by clicking here.</div> 


                <div class="leftPart">

                    <label style="width: 100%;font-size: 18px;">Equipment</label>


                    <div class="checkbox2">
                        <label style="cursor: pointer" <?php echo ($_REQUEST['skiType'] == '1') ? 'class="selectedCheck"' : ''; ?>>
                            <input type="checkbox" class="myCheck" typeOf="equipment" name="packages[]" value="1"  <?php echo ($_REQUEST['skiType'] == '1') ? 'checked' : ''; ?>>
                            Skis
                        </label>
                    </div>
                    <div class="checkbox2">
                        <label style="cursor: pointer" <?php echo ($_REQUEST['skiType'] == '2') ? 'class="selectedCheck"' : ''; ?>>
                            <input type="checkbox" class="myCheck"  typeOf="equipment"  name="packages[]" value="2" <?php echo ($_REQUEST['skiType'] == '2') ? 'checked' : ''; ?>>
                            Snowboard
                        </label>
                    </div>
                </div>
                <div class="leftPart">
                    <label style="width: 100%;font-size: 18px;">Skill Level</label>
                    <?php foreach ($this->App->getSkiFor() as $st):
                        ?>
                        <div class="checkbox2">
                            <label style="cursor: pointer"  <?php echo (strtolower($_REQUEST['skiFor']) === strtolower($st['skis_for']['name'])) ? 'class="selectedCheck"' : '' ?>>
                                <input type="checkbox" typeOf="skillLevel" class="myCheck"  name="packages[]" value="<?php echo $st['skis_for']['id'] ?>" <?php echo (strtolower($_REQUEST['skiFor']) === strtolower($st['skis_for']['name'])) ? 'checked' : '' ?>>
                                <?php echo $st['skis_for']['name'] ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="leftPart">

                    <label style="width: 100%;font-size: 18px;">Boots</label>


                    <div class="checkbox2">
                        <label style="cursor: pointer" <?php echo ($_REQUEST['boot'] == '1') ? 'class="selectedCheck"' : ''; ?>>
                            <input type="checkbox" class="myCheck" typeOf="boot"  name="packages[]" value="1" <?php echo ($_REQUEST['boot'] == '1') ? 'checked' : ''; ?>>
                            Yes
                        </label>
                    </div>
                    <div class="checkbox2">
                        <label style="cursor: pointer" <?php echo ($_REQUEST['boot'] == '0') ? 'class="selectedCheck"' : ''; ?>>
                            <input type="checkbox" class="myCheck"  typeOf="boot"  name="packages[]"  value="0" <?php echo ($_REQUEST['boot'] == '0') ? 'checked' : ''; ?>>
                            No
                        </label>
                    </div>
                </div>
            </div><!--leftbar -->
        </div><!--leftbarwap -->
<?php echo $this->Form->create('Products', array('class' => 'smart-form', 'enctype' => 'multipart/form-data')); ?>

   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 centerbar prolisting">

			<div>
						
							<label>Sort By:</label>
                               
					<div >
<?php $optns2=array('Price','Location','Rating');

						echo $this->Form->select('sortbyy', $optns2, array('class' => 'sortsort','label' => false, 'div' => false, 'placeholder' => '', "empty" => "Select", 'required' => 'required'));
?><i></i> 
					</div></div>
							
					
            <?php
            if ($results) {
                foreach ($results as $pdt):
                    ?>
				<!--	<?php echo $pdt['U']['store_avl_from'];?> -->
                    <div class="prolistbx">
                        <div class="proimg"><img src="<?php echo $this->webroot; ?><?php echo (!empty($pdt['U']['pic'])) ? 'img/avatars/' . $pdt['U']['pic'] : 'images/nopic.png'; ?>" style="height:129px;">
                            
                        </div><!--proimg -->
                        <div class="prodetail">
                            <h1 class="protitle"><a href="<?php echo $this->base; ?>/search/storeDetail/<?php echo $pdt['Products']['id']; ?>?<?php echo $_SERVER['QUERY_STRING']; ?>"><?php echo $pdt['U']['b_name'] ?></a></h1>
 										<div class="rating" style="padding-left: 5px;">
                               	 <?php
                                		echo $this->App->getRatingById($pdt['Products']['id']);
                               	 ?>
                            	</div><!--rating -->
                          	  <div class="prodescription"><p style="text-align:left;border-bottom: 1px solid #ccc; border-top: 1px solid #ccc; max-height:37px;height:37px;padding-left: 5px;"><?php if($pdt['U']['b_address']!=''){ echo $pdt['U']['b_address'].'<br/>'.$pdt['U']['b_city'].','.$pdt['U']['b_state'].' '.$pdt['U']['b_postalcode']; } else { echo "Not mentined"; }  ?></p></div><!--prodescription -->
                            	<div style="text-align: center;float:left;width:50%;border-right:1px solid #ccc;">
												<span><img src="<?php echo $this->webroot; ?>images/<?php
                                            switch ($pdt['SF']['name']) {
                                                case 'Beginner': echo 'be.gif';
                                                    break;
                                                case 'Intermediate':echo 'in.gif';
                                                    break;
                                                default:echo 'ex.gif';
                                            }
                                            ?>" height="18"></span>
                              </div>
										<div style="text-align: center;float:right;width:45%;"><a class="view-all-pack" pid="<?php echo $pdt['Products']['id']; ?>" style="cursor: pointer;padding: 2px;font-weight: bold;color: #333333;">View All Packages</a></div>  
                        </div><!--prodetail -->

                        <div class="rentnow" style="margin-top: 0px;border-left: 1px solid #ccc; padding: 5px;text-align:center;line-height:15px;padding-bottom:0px;">
                     <div><span style="font-size:16px;">Daily RATE</span><br><br><strong style="color: red;font-weight: bold;font-size: 14px;padding-top:10px;"><del>$<?php echo $pdt['Products']['walk_price']; ?></del></strong><br>
<br>
<strong style="color: #00a300;font-weight: bold;font-size: 22px;">$<?php echo $pdt['Products']['srdb_price']; ?></strong>
 </div>

                            <div style="margin-top:11px;" ><a href="<?php echo $this->base; ?>/search/storeDetail/<?php echo $pdt['Products']['id']; ?>?<?php echo $_SERVER['QUERY_STRING']; ?>"><button type="button" class="btn btn-default rentnowbtn">View Deal<span class="glyphicon glyphicon-chevron-right"></span></button></a></div>


                        </div><!--rentnow -->

                        <!--                View Other packages-->

                        <div class="prolistbx view-all-pack-show-<?php echo $pdt['Products']['id']; ?>" style="display: none;width: 100%;padding: 0px;border:none">

                            <?php 
                            $morePro = $this->App->getProductsByUserId($_REQUEST['skiType'],$pdt['Products']['user_id']);
                            if(!empty($morePro)){
                            foreach ($morePro as $pdt2): ?>
                            <div class="prolistbx" style="margin: 0px;padding: 0px;border:none;border-top: 1px solid #00a300;padding-top: 5px;">
                                    <div class="proimg" style="width: 15%;"><img src="<?php echo $this->webroot; ?><?php echo (!empty($pdt2['PPIC']['pic'])) ? 'files/products/' . $pdt2['PPIC']['pic'] : 'images/nopic.png'; ?>" style="height:86px;"></div>
                                    <div class="prodetail" style="line-height: 18px;width: 65%;padding-left:5px;"><strong><?php echo $pdt2['PCK']['title']; ?></strong><br>
                                       <div style="float: left;width: 95%;">Walk In Rate <strong style="color: red;font-weight: bold;"><del>$<?php echo $pdt2['Products']['walk_price']; ?></del></strong> | SRDB Rate <strong style="color: #00a300;font-weight: bold;">$<?php echo $pdt2['Products']['srdb_price']; ?></strong></div>
                                    
                                       <div><p style="line-height: 20px;"><?php echo substr($pdt2['Products']['description'], 0, 80); ?>...</p></div>
                                    </div>
                                    <div class="rentnow" style="margin-top: 30px;margin-right:-5px;">
										<?php echo "hello"; echo $pt['U']['store_avl_from']; ?>
                                        <a href="<?php echo $this->base; ?>/search/storeDetail/<?php echo $pdt2['Products']['id']; ?>?<?php echo $_SERVER['QUERY_STRING']; ?>"><button type="button" class="btn btn-default rentnowbtn" style="margin-top: 20px;width:100%;">View Deal<span class="glyphicon glyphicon-chevron-right"></span></button></a>
										
									</div>
                                </div>
                            <?php endforeach; 
                            }else{
                            ?>
                            <div class="prolistbx" style="margin: 0px;padding: 0px;border:none;border-top: 1px solid #00a300;padding-top: 5px;">
                            No more package found..
                            </div>
                            <?php } ?>

                        </div>


                    </div><!--prolistbx -->











                    <?php
                endforeach;
            }else {
                ?>
                <div class="prolistbx">
                    No result found...
                </div><!--prolistbx -->

            <?php } ?>
        </div><!--prolisting -->
</form>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 rightbarwrap">
            <div class="rightbar">
                <div class="addbx"><img src="<?php echo $this->base; ?>/images/add1.jpg"></div><!--addbx -->
                <div class="addbx"><img src="<?php echo $this->base; ?>/images/add2.jpg"></div><!--addbx -->
                <div class="addbx"><img src="<?php echo $this->base; ?>/images/add3.jpg"></div><!--addbx -->
            </div><!--rightbar -->
        </div><!--rightbarwrap -->
    </div><!--row -->
</div><!--contentdiv -->
<input id="qs" type="hidden" value="<?php echo $_SERVER['QUERY_STRING']; ?>">
 <script src="<?php echo $this->webroot; ?>js/jquery.form.js"></script>
<script type="text/javascript">


    $(document).ready(function () {

<!--7/10/2015-->
 $(".sortsort").on('change', function (){ 
 var sf = $(this).val();
alert(sf);
$users = $this->User->find('sortsort');
    asort($users);
    $this->set('users', $users);
alert("ll");
});



<!------>



        $(".view-all-pack").click(function () {
            var showDiv = $(this).attr('pid');
            $(".view-all-pack-show-" + showDiv).toggle(300);
        });

        $(".myCheck").click(function () {
            var pckg = [];
            var equipment = [];
            var boot = [];
             var group = ":checkbox[typeOf='"+$(this).attr("typeOf")+"']";
            if($(this).is(':checked')){
                $(group).not($(this)).attr("checked",false);
            }
            $(".myCheck").each(function () {
                if ($(this).attr("typeOf") == 'skillLevel') {
                    if ($(this).is(":checked")) {
                        pckg.push($(this).val());
                    }
                }
                if ($(this).attr("typeOf") == 'equipment') {
                    if ($(this).is(":checked")) {
                        equipment.push($(this).val());
                    }
                }
                if ($(this).attr("typeOf") == 'boot') {
                    if ($(this).is(":checked")) {
                        boot.push($(this).val());
                    }
                }
            });
            var cs = $("#birds").val().split(',');
            var fD = $("#datepicker").val();
            var tD = $("#datepicker2").val();
            var qs = "city=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiFor=" + pckg + "&skiType=" + equipment + "&boot=" + boot;
            window.location.href = ajaxUrl + "/search/getByPackages?" + qs;

        })

       /* $("#birds").change(function () {

            var cs = $("#birds").val().split(',');
            var fD = $("#datepicker").val();
            var tD = $("#datepicker2").val();
            var qs = "city=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=<?php echo $_REQUEST['skiType']; ?>" + "&skiFor=<?php echo $_REQUEST['skiFor']; ?>" + "&boot=<?php echo $_REQUEST['boot']; ?>" + "&pid=<?php echo isset($_REQUEST['pid']) ? $_REQUEST['pid'] : 0; ?>";
            window.location.href = ajaxUrl + "/search/stores/<?php echo $id; ?>?" + qs;
        })*/
        $("#birds").autocomplete({
                    minLength: 1,
                    source: "<?php echo $this->base; ?>/home/search",
                    select: function(event, ui) {
                        var cs = $("#birds").val().split(',');
                        var fD = $("#datepicker").val();
                         var tD = $("#datepicker2").val();
                         var qs = "city=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=<?php echo $_REQUEST['skiType']; ?>" + "&skiFor=<?php echo $_REQUEST['skiFor']; ?>" + "&boot=<?php echo $_REQUEST['boot']; ?>" + "&pid=<?php echo isset($_REQUEST['pid']) ? $_REQUEST['pid'] : 0; ?>";
                            window.location.href = ajaxUrl + "/search/stores/<?php echo $id; ?>?" + qs;
                }
                });

      


    })
</script>
<style>
    .selectedCheck{color: #00f;font-size: 16px;font-weight: bold} 
    .leftPart{padding: 10px;margin-bottom: 5px;border:1px solid #ccc;margin-right: 29px;}
</style>
