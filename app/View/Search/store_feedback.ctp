<?php
if (isset($_REQUEST['fromDate']) AND ! empty($_REQUEST['fromDate'])) {
    $expF = explode(', ', $_REQUEST['fromDate']);
    $expF2 = explode('/', end($expF));
    $makeFromDate = $expF2[2] . '-' . $expF2[0] . '-' . $expF2[1];
}
if (isset($_REQUEST['toDate']) AND ! empty($_REQUEST['toDate'])) {
    $expT = explode(', ', $_REQUEST['toDate']);
    $expT2 = explode('/', end($expT));
    $makeToDate = $expT2[2] . '-' . $expT2[0] . '-' . $expT2[1];
}

if (!empty($bh)) {
    $getOpenTime = unserialize($bh['business_hours']['open_time']);
    $getOpenTimeAmPm = unserialize($bh['business_hours']['open_ampm']);
    $getCloseTime = unserialize($bh['business_hours']['close_time']);
    $getCloseTimeAmPm = unserialize($bh['business_hours']['close_ampm']);
    $weeks = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
}
$address =$row['Users']['b_address'].' '.$row['Users']['b_city'].' '.$row['Users']['b_state'];
if($uid)
{
	$userId=$uid;
}
else
{
$userId=$this->Session->read('fe.logedIn');
}
$address = str_replace(" ", "+", $address);
$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
$response_a = json_decode($response);
$lat = $response_a->results[0]->geometry->location->lat;
$long = $response_a->results[0]->geometry->location->lng;
?>
 <style>
      #map-canvas {
        width: 100%;
        height: 300px;
      }
	 .selectdate
	 {
		 display:none;
	 }
    </style>
<script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
          var latitude = parseFloat("<?php echo $lat; ?>"); // Latitude get from above variable
        var longitude = parseFloat("<?php echo $long; ?>");
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(latitude, longitude),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
}
var map = new google.maps.Map(mapCanvas, mapOptions)
var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latitude, longitude),
      map: map,
      title: "<?php echo $row['Users']['b_name']; ?>",
  });
        
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>


<div class="contentdiv">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 sidebarcheckbx leftbarwrap">
        <div class="leftbar vendorinformation">
            <h1 class="protitle">Store Information</h1>
            <div class="vendordetail">
                <div><span class="storename"><?php echo $row['Users']['b_name']; ?></span></div>
                <div><?php echo $row['Users']['b_address']; ?></div>
                <div><?php echo $row['Users']['b_city'] . ' , ' . $row['Users']['b_state'].' '.$row['Users']['b_postalcode']; ?></div>
                <div><?php $data= $row['Users']['b_phone']; echo $data; ?></div>
                <div><a href="mailto:<?php echo $row['Users']['b_email']; ?>"><?php echo $row['Users']['b_email']; ?></a></div>

               <!-- <div class="resrvtionprice">
                    <span class="resrvtxt">Daily Rate</span>
                    <span class="resrvprice" style="color:#1D9F36">$<?php echo $row['Products']['srdb_price']; ?></span>
                    <span class="wlktxt">Walk In Rate <span style="color:#ED3333">$<?php echo $row['Products']['walk_price']; ?></span></span>
                    <span class="savetxt">You Save <?php echo $row['Products']['discount']; ?>%</span>
                </div><!--resrvationprice -->

                <div class="resrvtionprice" style="float: left;">
                    <p style="font-size: 14px;font-weight: bold;text-align: center;color:#3867a4">Weekly Store Schedule</p>
                    <?php
                    for ($x = 0; $x <= 6; $x++) {
                        $weekTime = ($getCloseTime[$x] == 'closed' || $getOpenTime[$x] == 'closed') ? 'Closed' : $getOpenTime[$x] . $getOpenTimeAmPm[$x] . ' - ' . $getCloseTime[$x] . $getCloseTimeAmPm[$x];
                        ?>
                        <div style="float: left;border-bottom: 1px solid #0066cc;line-height: 25px;">
                            <div class="fixedWeekHour" weekDaysName ="<?php echo $weeks[$x]; ?>" weekDaysStatus ="<?php echo $weekTime; ?>" style="text-align: left;width: 45px;float: left;"><strong><?php echo $weeks[$x] . ': </strong> '; ?></div><div style="float: left;width: 150px;text-align: right;"><?php echo $weekTime; ?></div>
                        </div>
                    <?php } ?>
                </div>
                <br>
                 <div class="resrvtionprice"  id="map-canvas" style="float: left;">
                 
                 </div>
                 <br>
                  <br>
                <a href="#">Affiliate Policies</a>
            </div><!--vendordetail -->
        </div><!--leftbar -->
    </div><!--leftbarwrap -->

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 centerbar prodetails">
        <div class="row prodcsta paddingtop10">
			
			<div class="col-xs-7"><img style="width:100%" src="<?php echo $this->webroot; ?><?php echo (!empty($row['Users']['pic'])) ? 'img/avatars/' . $row['Users']['pic'] : 'images/nopic.png'; ?>" class="imagegl"></div>
<div class="col-xs-5"><h1 class="protitle"><?php echo $row['Users']['b_name']; ?></h1></div>
			  <?php if ($userId AND $this->App->countRatingByUserId($row['Users']['id'], $userId) <= 0) { ?><div class="col-xs-12 "><h1 class="protitle">Feedback</h1></div><?php }?>
           		 <div class="col-xs-12">
				<form method="POST" id="ratingsubmit">
				<div class="field">
                     <div class="form-group">
 						<div class='ratings' style="width:100%;height:27px;padding:0;border:none;">	 

                        <?php if ($userId AND $this->App->countRatingByUserId($row['Users']['id'], $userId) <= 0) { ?>
                           

                            <div class="col-xs-12 padding0 mrgnbtm" id="rating_2" who ='<?php echo $row['Users']['id']; ?>' >
                                <div rate ='1' class="star_1 ratings_stars"></div>
                                <div rate ='2'  class="star_2 ratings_stars"></div>
                                <div rate ='3' class="star_3 ratings_stars"></div>
                                <div rate ='4' class="star_4 ratings_stars"></div>
                                <div rate ='5' class="star_5 ratings_stars"></div>
                             
                        <?php } else {
                            ?>
                           
                            <div class="col-xs-12 padding0" id="rating_2" who ='<?php echo $row['Users']['id']; ?>' >
                                <?php
                                echo $this->App->getRatingById($row['Users']['id']);
                            }
                            ?>
                        </div> 
                    </div>
				</div>
			</div>
 <?php if ($userId AND $this->App->countRatingByUserId($row['Users']['id'], $userId) <= 0) { ?>
				<div class="field">
                     <div class="form-group">
                          <label>Comments</label>
						 <textarea name="comments" id="comments"></textarea>
						 <input type="hidden" id="urate"/>               
                       </div><!--formgroup -->
                       <span class="error"></span>
                </div>
				<div class="field">
                     <div class="form-group clearfix">
						 
                         <div class="col-xs-6"><input type="submit" name="" value="Submit" class="cstmbuttons"></div>
						 <div class="col-xs-6" id="ajaxresult"></div>
                      </div>
				</div>
			</form>
<?php } ?>
			</div>
			</div>
		</div><!--row -->
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 rightbarwrap">
            <div class="rightbar">
                <div class="addbx"><img src="<?php echo $this->base; ?>/images/add1.jpg"></div><!--addbx -->
                <div class="addbx"><img src="<?php echo $this->base; ?>/images/add2.jpg"></div><!--addbx -->
                <div class="addbx"><img src="<?php echo $this->base; ?>/images/add3.jpg"></div><!--addbx -->
            </div><!--rightbar -->
		</div>
        </div><!--prodctdescrption -->
    </div><!--row -->

<script type="text/javascript">
    $(document).ready(function () {
		$( "#ratingsubmit" ).submit(function( event ) {
			var id = $("#rating_2").attr("who");
            //var id2 = $(this).parent().attr("id");
            var num = $("#urate").val();
			if(num!='')
			{
			var comments=$("#comments").val();
			var uid=<?php echo $uid; ?>;
            var poststr = "id=" + id + "&stars=" + num+"&uid="+uid +"&review="+comments;
            $.ajax({url: ajaxUrl + "/search/setFeedbackRating", cache: 0, data: poststr, success: function (result) {
                    $("#ajaxresult").html(result);
                }
				   });
			}
			else
			{
				alert("Please give rating.");
			}
			event.preventDefault();
		});
        $('.ratings_stars').on('click', function () {
			//alert('onclick');
			$('.ratings_stars').removeClass('ratings_over');
			$(this).prevAll().andSelf().addClass('ratings_over');
			 var num = $(this).attr("rate");
			$("#urate").val(num);
        });

        var fD = "<?php echo isset($makeFromDate) ? $makeFromDate : '';  ?>";
        var tD = "<?php echo isset($makeToDate) ? $makeToDate : '';  ?>";
        var betweenDays = betweenDates(fD, tD);
        myArr2 = [];
        var veryOk = [];
        $(".fixedWeekHour").each(function () {
            if ($(this).attr('weekDaysStatus') == 'Closed') {
                myArr2.push($(this).attr('weekDaysName'));
            }

        });


        //console.log(myArr2);

        



      



    })

    function isExistIn(myArr, val2) {
        var okCount = [];
        // console.log(val2);
        // console.log(myArr);
        if ($.inArray(val2, myArr) === -1) {
            okCount.push(val2);
        }
        return okCount.length;
    }
    function betweenDates(fod, tod) {
        var day = 1000 * 60 * 60 * 24;
        date1 = new Date(fod);
        date2 = new Date(tod);
        myArr = [];
        setArr = {};
        weekName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var diff = (date2.getTime() - date1.getTime()) / day;
        for (var i = 0; i <= diff; i++)
        {
            var xx = date1.getTime() + day * i;
            var yy = new Date(xx);
            //setArr[yy.getDay()] = weekName[yy.getDay()];
            myArr.push(weekName[yy.getDay()]);
        }
        return myArr;
    }


</script>
