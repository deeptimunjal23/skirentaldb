<?php
$rPkg = $this->App->searchPackage($_REQUEST['skiFor'], $_REQUEST['boot'],$_REQUEST['skiType']);
 $oPck = $this->App->searchOtherPackage($_REQUEST['skiFor'], $_REQUEST['boot'],$_REQUEST['skiType']);
?>

<div class="contentdiv">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 categoriesbx">
            <div class="col-lg-2">
            &nbsp;
              <!-- <a class="btn btn-default cstmbuttons skibtn snowbtn nextResult" href="javascript:history.back()" style="margin-top: 20px;">Back</a>-->
            </div>
            <div class="col-lg-8"><h1 class="pagetitle centerpagetitle">RECOMMENDED PACKAGE</h1></div>
            <div class="col-lg-2">
                <?php if(!empty($rPkg) || !empty($oPck)){ ?>
                <a class="btn btn-default cstmbuttons skibtn snowbtn nextResult"  skiFor="<?php echo !empty($rPkg['SF']['name'])?$rPkg['SF']['name']:'' ?>"  boot="<?php echo !empty($rPkg['Packages']['boot'])?$rPkg['Packages']['boot']:'0'; ?>" skiType ="<?php echo !empty($rPkg['ST']['id'])?$rPkg['ST']['id']:'' ?>" package = "<?php echo !empty($rPkg['Packages']['id'])?$rPkg['Packages']['id']:'' ?>" style="margin-top: 20px;">
                    Next</a>
                <?php } ?>

            </div>
            <div class="recomended row-center">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 40px;cursor: pointer">
<?php if(!empty($rPkg)){ ?>

                    <div class="recomendedbx" style="border: 5px solid #00a300;background-color: #F5FFFA" skiFor="<?php echo $rPkg['SF']['name'] ?>"  boot="<?php echo $rPkg['Packages']['boot']; ?>" skiType ="<?php echo $rPkg['ST']['id'] ?>" pckId = "<?php echo $rPkg['Packages']['id'] ?>">


                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2 recimg">
                            <img src="<?php echo $this->webroot; ?>files/packages/<?php echo $rPkg['Packages']['pic'] ?>" ></div><!--recimg -->
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 recdescrption">
                            <h2><strong><?php echo $rPkg['Packages']['title'] ?></strong></h2>
                            <p><strong>Description : </strong><?php echo $rPkg['Packages']['description'] ?></p>
                        </div><!--recimg -->

                    </div><!--recomendedbx -->
<?php }else{ ?>
       No recomended found...             
<?php } ?>


                </div><!--col-lg-12 -->
            </div><!--recomended -->

        </div><!--categoriesbx -->
        <div class="" style="float: left;width: 100%;">
            <div class="col-xs-10" style="float: none;margin: 0px auto;">
                <h1 style="text-align: center;">Or Choose Another Package</h1>
                
                <?php
               
                if(!empty($oPck)){ 
                foreach ($oPck as $op):
                    ?>

                    <div class="recomendedbx" skiFor="<?php echo $op['SF']['name'] ?>"  boot="<?php echo $op['Packages']['boot']; ?>" skiType ="<?php echo $op['ST']['id'] ?>"   pckId = "<?php echo $op['Packages']['id'] ?>" style="margin-top: 10px;margin-bottom: 10px;cursor: pointer">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2 recimg ">
                            <img src="<?php echo $this->webroot; ?>files/packages/<?php echo $op['Packages']['pic'] ?>"></div><!--recimg -->
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 recdescrption">
                            <h2><strong><?php echo $op['Packages']['title'] ?></strong></h2>
                            <p><strong>Description : </strong><?php echo $op['Packages']['description'] ?></p>
                        </div><!--recimg -->
                    </div><!--recomendedbx -->
                <?php endforeach; }else{ ?>
                    No other packages found..
                <?php } ?>

            </div><!--chooseanother -->
        </div>
    </div><!--row -->
</div><!--contentdiv -->
<a class="btn btn-primary" href="javascript:history.back()">
                    Back</a>
<script type="text/javascript">
    $(document).ready(function () {
        $(".recomendedbx").click(function () {
            $(".recomendedbx").css('border', 'none');
            $(".recomendedbx").css('background-color', '#F1F5F8');
            $(".recomendedbx").removeAttr('package');
			
            $(".nextResult").removeAttr('package');
            $(".nextResult").removeAttr('skiFor');
            $(".nextResult").removeAttr('boot');
            $(".nextResult").removeAttr('skiType');

            var getAt = $(this).attr('pckId');
            var skiType = $(this).attr('skiType');
            var skiFor = $(this).attr('skiFor');
            var boot = $(this).attr('boot');
            $(".nextResult").attr('package', getAt);
            $(".nextResult").attr('skiType', skiType);
            $(".nextResult").attr('skiFor', skiFor);
            $(".nextResult").attr('boot', boot);

            $(this).css('border', '5px solid #00a300');
            $(this).css('background-color', '#F5FFFA');
        });


        $(".nextResult").on('click', function () {
            var pid = $(this).attr('package');
            var skiType = $(this).attr('skiType');
            var skiFor = $(this).attr('skiFor');
            var boot = $(this).attr('boot');
            var cs = $("#birds").val().split(',');
            var fD = $("#datepicker").val();
            var tD = $("#datepicker2").val();
		/*	if(cs[0] == ''){
				var qs = pid + "?state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + skiType + "&skiFor=" + skiFor + "&boot=" + boot;
			}
			else{  
			*/
				var qs = pid + "?resort=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + skiType + "&skiFor=" + skiFor + "&boot=" + boot;
         //   }
			window.location.href = ajaxUrl + "/search/stores/" + qs;
        });
		$(".recomendedbx").on('dblclick', function () {
            var pid = $(".nextResult").attr('package');
            var skiType = $(".nextResult").attr('skiType');
            var skiFor = $(".nextResult").attr('skiFor');
            var boot = $(".nextResult").attr('boot');
            var cs = $("#birds").val().split(',');
            var fD = $("#datepicker").val();
            var tD = $("#datepicker2").val();
		/*	if(cs[0] == ''){
				var qs = pid + "?state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + skiType + "&skiFor=" + skiFor + "&boot=" + boot;
			}
			else{  
			*/
				var qs = pid + "?resort=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + skiType + "&skiFor=" + skiFor + "&boot=" + boot;
         //   }
			window.location.href = ajaxUrl + "/search/stores/" + qs;
        });
    });

</script>

.col-xs-12.col-sm-3.col-md-3.col-lg-2.recimg {
    width: 11% !important;
}

