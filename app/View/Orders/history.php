<div class="contentdiv">
    <div style="margin-top: 5px;">
        <?php echo $this->Session->flash(); ?>
    </div>
    <?php echo $this->element('sidebar'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">
       


                    
<div class="proinner">

                        <h3 style=" padding-left: 15px;padding-bottom: 15px;">Payment History</h3>
                       
                        <table class="table table1">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Store</th>
                                    <th>Pickup Date</th>
                                    <th>Return Date</th>
                                    <th style="text-align:right">Rental Price</th>
                                    <th>Date Time</th>
                                    <th>Payment Status</th>
                                </tr>
                            </thead>
                            <tbody>

<?php
if(!empty($result)){
 foreach($result as $row): ?>
                                <tr>
                                    <td><a href="<?php echo $this->base; ?>/orders/invoice/<?php echo $row['Orders']['id']; ?>"><?php echo $row['Orders']['token'] ?></a></td>
                                    <td><?php if($row['U']['b_name']!='') { echo $row['U']['b_name'].','.$row['U']['b_phone']; } ?></td>
                                    <td><?php echo $row['OrderHistory']['picup_date'] ?></td>
                                    <td><?php echo $row['OrderHistory']['return_date'] ?></td>
                                    <td style="text-align:right">$<?php echo $row['Orders']['amount'] ?></td>
                                    <td><?php echo $row['Orders']['created'] ?></td>
                                    <td><a style="color:white;background-color:green;padding:5px;">Success</a></td>
                                </tr>

<?php endforeach; }else{ ?>
 <tr>
                                    <td colspan='7'>No result found..</td>
                                </tr>
<?php } ?>
                               
                            </tbody>


                        </table>
                    </div>
             


    </div> <!--col-lg-9 -->
</div>
<style>
  .proinner{background: none repeat scroll 0 0 #f1f5f8;
              border: 1px solid #e0ecf5;
              clear: both;
              content: "";
              display: table;
              margin-bottom: 10px;
              padding-top: 15px;
              padding-bottom: 10px;
              width: 100%;
    }
</style>
