<div class="container newsearch">  
    <?php echo $this->Form->create('User', array('class' => 'homsrch', 'id' => 'datePicForm', 'name' => 'myform')); ?>
    <div class="formtopbx">
        <div class="newlogo1"><img src="images/logo1.png"></div>
        <div class="logotxt" style="margin-top: -5px;"><span style="font-size: 22px;">Shop, Compare and Save</span><br/> Ski and Board rental made easy!</div>
    </div>
		<div>
				<div align="left">
					<input type="button" id="sktype" class="skibtn1" name="sktype" value="ski">
					<input type="button" id="sktype1" class="skibtn2" name="sktype" value="snowboard">
				</div>
				<div align="right">
				<!--	Do you want to boot
					<input type="radio" id="boot" name="boot" value="1">Yes
					<input type="radio" id="boot1" name="boot" value="0">No -->
					<input type="button" class="bootbtn" id="bootbtn" value="Boot">
				</div>
			<div>
				<div class="location">
                    <div class="form-group headerfrm">
				<!-- <input type="text" class="forOnChange"  id="birds" name="setMyLoc" value="<?php //if(!empty($_REQUEST['city']) AND !empty($_REQUEST['state'])){ echo $_REQUEST['city'].','.$_REQUEST['state'];} ?>" style='border:1px solid #2782D1'>     
				-->
					<select name="setMyLoc" id="birds" class="forOnChange">
						<option>Select State</option>
						<?php // foreach($mysearch as $row):?>
						<option></option>
						
					</select>
				</div>
               </div><!--location -->
               
                <div class="datepickerbox">
                	<div readonly="readonly" class=" checkin input-append date" id="dp3" data-date="12-02-2012" data-date-format="yyyy-mm-dd">
                            <input required="required" class="span2 date-pick forOnChange" id="datepicker" size="16" name="fromDate" value="<?php if(!empty($_REQUEST['fromDate'])){ echo $_REQUEST['fromDate'];} ?>" placeholder="Select From Date">
  						<span class="add-on"><i class="icon-th"></i></span>
					</div>
                	<div readonly="readonly"  class="checkout input-append date " id="dp4" data-date="12-02-2012" data-date-format="yyyy-mm-dd">
                            <input required="required"  class="span2 date-pick forOnChange" id="datepicker2" size="16" name="toDate" value="<?php if(!empty($_REQUEST['toDate'])){ echo $_REQUEST['toDate'];} ?>" placeholder="Select Date">
  						<span class="add-on"><i class="icon-th"></i></span>
					</div>
						<input type="button" id="btnsubmit" value="Submit">
					<div>
					</div>
                </div><!--search-group -->
				
</form>
</div><!--search -->
<style>
			.blue-box > a{background: #2782d1 !important; color:#fff !important;}
      .green-box > a{background: #5fcb08 !important; color:#fff !important;}
		</style>
<script type="text/javascript">
            $(document).ready(function () {
                $("#registenMenu").click(function () {
                    $("#registenAs").toggle('slow');

                })
				$("#birds").on("click", function () {
   $(this).select();
});
            })
        </script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
                    <script>
                $(document).ready(function () {
					
					$(".skibtn1").click(function(){
							$(this).addClass("intro");
							$(".skibtn2").removeClass("intro");
						
					});
					$(".skibtn2").click(function(){
						$(this).addClass("intro");
						$(".skibtn1").removeClass("intro");
					});
					
					$(".bootbtn").click(function(){
						$(this).toggleClass("add");
					});
                    /*
                     * From Date Settings
                     */
					$("#datepicker").attr("autocomplete", "off");
					$("#datepicker2").attr("autocomplete", "off");
                    $("#datepicker").datepicker({
                        defaultDate: "+1w",
                        dateFormat: 'DD, mm/dd/yy',
                        firstDay: 0,
						minDate: 0,
                        numberOfMonths: 1,
						
                        beforeShowDay: function (date) {
							$('#ui-datepicker-div').removeClass('datepicker2');
							$('#ui-datepicker-div').addClass('datepicker');
                            var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            var date2 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker2").val());
                           var cls = '';
           				 if (date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
              				cls += " dp-highlight ";
            				}

           					 var from_to_dates = {};
            				from_to_dates[$("#datepicker").val()] = $("#datepicker").val();
           				    from_to_dates[$("#datepicker2").val()] = $("#datepicker2").val();
           					 var highlight = from_to_dates[$.datepicker.formatDate('DD, mm/dd/yy', date)];
            				if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker").val())) {
             					 cls += " blue-box ";
           					 }
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker2").val())) {
              cls += " green-box ";
            }
            return [true, cls];
							
                        },
                        onSelect: function (selectedDate, instance) {
                             var fromDate = $('#datepicker').datepicker('getDate', '+1d');
                              fromDate.setDate(fromDate.getDate() + 1);
                             $('#datepicker2').datepicker('setDate', fromDate);                         },
                        onClose: function (selectedDate) {
							var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            $("#datepicker2").datepicker("option", "minDate", selectedDate);
                            $("#datepicker2").datepicker("show");
							
                        },
                    });
					                    $('#datepicker').datepicker('setDate', new Date());
					$("#datepicker").datepicker("show");
					                    //$('#datepicker2').datepicker('getDate', '+1d');


                    /*
                     * From Date Settings
                     */
                    $("#datepicker2").datepicker({
                        defaultDate: "+1w",
                        dateFormat: 'DD, mm/dd/yy',
                        firstDay: 0,
                        numberOfMonths: 1,
                        beforeShowDay: function (date) {
							$('#ui-datepicker-div').removeClass('datepicker');
							$('#ui-datepicker-div').addClass('datepicker2');
                            var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            var date2 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker2").val());
							/*if(date2 && date >= date1 && date <= date2)
							{
								return [true,"dp-highlight",""];
							}*/
							//return [true, date1 && ((date.getTime() === date1.getTime())) ? "dp-highlightOne" : ""];
                            //return [true,  (date2 && date >= date1 && date <= date2) ? "dp-highlight" : ""];
							 var cls = '';
            if (date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
              cls += " dp-highlight ";
            }

            var from_to_dates = {};
            from_to_dates[$("#datepicker").val()] = $("#datepicker").val();
            from_to_dates[$("#datepicker2").val()] = $("#datepicker2").val();
            var highlight = from_to_dates[$.datepicker.formatDate('DD, mm/dd/yy', date)];
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker").val())) {
              cls += " blue-box ";
            }
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker2").val())) {
              cls += " green-box ";
            }
            return [true, cls];
							 
                        },
											onSelect: function (selected) {
               $("#datepicker").datepicker("option", "maxDate", selected);
                $("#btnsubmit").click(function(){
				var cs = $("#birds").val().split(',');
                var fD = $("#datepicker").val();
                var tD = $("#datepicker2").val();
				var ski_type = $(".intro").val();
				var boot = $(".add").val();
                if ($("#birds").val() == '') {
                    alert('Please select city/destination');
                    $("#datepicker2").val('');
                } else if (fD == '') {
                    alert('Please select picup date');
                    $("#datepicker2").val('');
                } else if (tD == '') {
                    alert('Please select return date');
                    $("#datepicker2").val('');
				}
				else if(ski_type == ''){
					alert('Please select ski');
				}else {
					if(ski_type == 'ski'){
						ski_type = 1;
						var cs = $("#birds").val().split(',');
						var fD = $("#datepicker").val();
						var tD = $("#datepicker2").val();
							if(boot == 'Boot'){
								boot = 1;
									var qs = "city=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "/search/selectSkifor?" + qs;
							}
							else{
								boot = 0;
									var qs = "city=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "/search/selectSkifor?" + qs;
							}
					}
					else{
						ski_type = 2;
						var cs = $("#birds").val().split(',');
						var fD = $("#datepicker").val();
						var tD = $("#datepicker2").val();
							if(boot == 'Boot'){
								boot =1;
									var qs = "city=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "/search/selectSkifor?" + qs;
							}
							else{
								boot = 0;
									var qs = "city=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "/search/selectSkifor?" + qs;
							}
					}
                }
				});
            },
							            onClose: function (selectedDate) {
               $("#ui-datepicker-div").removeClass('hasDatepicker');
				 //$("#datepicker").datepicker("option", "maxDate", selectedDate);
            },
                       
                    });
			
                    
                });

            </script>

