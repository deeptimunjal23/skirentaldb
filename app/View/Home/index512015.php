<div class="container newsearch">  
    <?php echo $this->Form->create('User', array('class' => 'homsrch1', 'id' => 'datePicForm', 'name' => 'myform')); ?>
		<div style="width:27%; margin-top:-15%; margin-left:-6%"><img src="images/logo1.png"></div>
			<!-- code for text on home page -->
			<div style="text-align: center; width:100%; margin-top: 11%; font-size: 40px; color: white;"><b>SKI AND BOARD RENTAL MADE EASY!</b></div>
			<div style="text-align: center; width:100%; font-size: 22px; color: white;">Search, Compare and Save from Ski Shops Nationwide!</div>
			
			<!-- Code for how its work button -->
			<div style="align:center;">
				<a href="<?php echo $this->webroot; ?>img/rent_skis_colorado_hiw.png" target="blank"><span style="font-size: 18px; width:14%; margin-top:4%; margin-bottom:6%;" class="btn cstmbuttons howitworks">How it Works</span></a>
			</div>
			<div class="newone">
			<!-- code for SKI , BOARD and BOOT button -->
		<!-- <div class="selecting btn search"> -->
			<div class="allbtn">
				<button style="float: left; width: 15%; type="button" id="sktype" name="sktype" class="btn btn-default custmbuttons skibtn1" value="ski">
					<span style="font-size: 18px; width:40%; margin-top:5%;"><img src="<?php echo $this->webroot; ?>images/ski_icon.png"></span>    SKI
				</button>
				<span type="button" class="" style="font-size: 18px; float: left; width: 5%; color: white; margin-top: 10px;">OR</span>
				<button style="float:left; width:15%; margin-right:28%;" type="button" id="sktype" name="sktype" class="btn btn-default custmbuttons skibtn2 snowbtn" value="snowboard" style="width:20%">
					<span style="font-size: 18px; width:25%; margin-top:10%;"><img src="<?php echo $this->webroot; ?>images/snowboard.png"></span>   BOARD
				</button>
				<button style="float:left; width:15%;" type="button" id="bootbtn" class="btn btn-default cstmbuttons skibtn choice bootYesNo centerpagetitle bootbtn" value="Boot"  style="width: 20%;">+ BOOTS</button>
		</div>	
		<table class="table1">	
			<tr>
				<td><select id="birds" size="1" class="UserState" whereDiv="hereCity3" required="required" name="data[User][b_state]">
                                    <option value="" selected="selected">Select State</option>
                                    <?php
                                    $optns = array();		
									 foreach ($this->App->getStateList() as $st):
                                        ?>
                                       <!-- <option value="<?php //echo $st['states']['state_code']; ?>"><?php //echo $st['states']['state_code']; ?></option> -->
										<option value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state_code']; ?></option>
                                    <?php endforeach; ?>
								
                                </select></td>
				<td colspan="3" class="search-group srchinr1 soption">
					<select id="hereCity3" class="fResort" name="data[User][b_city]" required="required">
                            <option value="">Select your Ski Area!</option>
                    </select>
				</td>
				<td>
					<div readonly="readonly" class=" checkin input-append date datepickerbox" id="dp3" data-date="12-02-2012" data-date-format="yyyy-mm-dd">
                            <input required="required" class="span2 date-pick forOnChange" id="datepicker" size="16" name="fromDate" value="<?php if(!empty($_REQUEST['fromDate'])){ echo $_REQUEST['fromDate'];} ?>" placeholder="Pickup Date">
  						<span class="add-on"><i class="icon-th"></i></span>
					</div>
				</td>
				<td>
					<div readonly="readonly"  class="checkout input-append date datepickerbox" id="dp4" data-date="12-02-2012" data-date-format="yyyy-mm-dd">
                            <input required="required"  class="span2 date-pick forOnChange" id="datepicker2" size="16" name="toDate" value="<?php if(!empty($_REQUEST['toDate'])){ echo $_REQUEST['toDate'];} ?>" placeholder="Return Date">
  						<span class="add-on"><i class="icon-th"></i></span>
					</div>
				</td>
				<td><input type="button" id="btnsubmit" class="btn-default" value="SEARCH"></td>
			</tr>
		</table>
		<div class="divTable" style="display:none;">	
			
				<div class="ddstate"><select id="birds" size="1" class="UserState birds" whereDiv1="hereCity4" required="required" name="data[User][b_state]">
                                    <option value="" selected="selected">Select State</option>
                                    <?php
                                    $optns = array();		
									 foreach ($this->App->getStateList() as $st):
                                        ?>
                                       <!-- <option value="<?php //echo $st['states']['state_code']; ?>"><?php //echo $st['states']['state_code']; ?></option> -->
										<option value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state_code']; ?></option>
                                    <?php endforeach; ?>
								
                                </select>
				</div>
				<div colspan="3" class="search-group srchinr1 soption">
					<select id="hereCity4" class="fResort" name="data[User][b_city]" required="required">
                            <option value="">Select your Ski Area!</option>
                    </select>
				</div>
				<div class="clkdatepicker3" id="clkdatepicker3">
					<div readonly="readonly" class="checkin input-append date datepickerbox" id="dp3" data-date="12-02-2012" data-date-format="yyyy-mm-dd">
                            <input required="required" class="span2 date-pick forOnChange" id="datepicker3" size="16" name="fromDate" value="<?php if(!empty($_REQUEST['fromDate'])){ echo $_REQUEST['fromDate'];} ?>" placeholder="Pickup Date">
  						<span class="add-on"><i class="icon-th"></i></span>
					</div>
				</div>
				<div class="clkdatepicker4" id="clkdatepicker4">
					<div readonly="readonly"  class="checkout input-append date datepickerbox" id="dp4" data-date="12-02-2012" data-date-format="yyyy-mm-dd">
                            <input required="required"  class="span2 date-pick forOnChange" id="datepicker4" size="16" name="toDate" value="<?php if(!empty($_REQUEST['toDate'])){ echo $_REQUEST['toDate'];} ?>" placeholder="Return Date">
  						<span class="add-on"><i class="icon-th"></i></span>
					</div>
				</div>
				<div class="submitbtn1"><input type="button" id="btnsubmit1" class="btn-default" value="SEARCH"></div>		
		</div>
    </div>
	</form>
</div><!--search -->

<style>
			.blue-box > a{background: #2782d1 !important; color:#fff !important;}
      .green-box > a{background: #5fcb08 !important; color:#fff !important;}
		</style>
<script>
	function run(interval, frames) {
    var int = 1;
    
    function func() {
        document.body.id = "b"+int;
        
		int++;
        if(int === frames) { int = 1; }
    }
    
    var swap = window.setInterval(func, interval);
}

run(5000, 4); //milliseconds, frames
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
					<script>	
                $(document).ready(function () {
			// code for resort 16/10/2015	
			$('.UserState').change(function () {
					var full_state=$(this).find(":selected").text();
						$("#fstate").val(full_state);
						var whereIn = $(this).attr('whereDiv');
						$.ajax({
							dataType: 'json',
							type: "POST",
							data: {state: $(this).val(), resort: ''},
							url: window.location + '/users/getResortByState', success: function (response) {
								$("#" + whereIn).html(response.respectiveCities1);
						}});
					})	
					
					$('.UserState').change(function () {
					var full_state=$(this).find(":selected").text();
						$("#fstate").val(full_state);
						var whereIn = $(this).attr('whereDiv1');
						$.ajax({
							dataType: 'json',
							type: "POST",
							data: {state: $(this).val(), resort: ''},
							url: window.location + '/users/getResortByState', success: function (response) {
								$("#" + whereIn).html(response.respectiveCities1);
						}});
					})	
					
					$(".skibtn1").click(function(){
							$(this).addClass("intro");
							$(".skibtn2").removeClass("intro");						
					});
					$(".skibtn2").click(function(){
						$(this).addClass("intro");
						$(".skibtn1").removeClass("intro");
					});
					
					$(".bootbtn").click(function(){
						$(this).toggleClass("add");
					});
                    /*
                     * From Date Settings
                     */
					$("#datepicker").attr("autocomplete", "off");
					$("#datepicker2").attr("autocomplete", "off");
                    $("#datepicker").datepicker({
                        defaultDate: "+1w",
                        dateFormat: 'DD, mm/dd/yy',
                        firstDay: 0,
						minDate: 0,
                        numberOfMonths: 1,
						
                        beforeShowDay: function (date) {
							$('#ui-datepicker-div').removeClass('datepicker2');
							$('#ui-datepicker-div').addClass('datepicker');
                            var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            var date2 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker2").val());
                           var cls = '';
           				 if (date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
              				cls += " dp-highlight ";
            				}

           					 var from_to_dates = {};
            				from_to_dates[$("#datepicker").val()] = $("#datepicker").val();
           				    from_to_dates[$("#datepicker2").val()] = $("#datepicker2").val();
           					 var highlight = from_to_dates[$.datepicker.formatDate('DD, mm/dd/yy', date)];
            				if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker").val())) {
             					 cls += " blue-box ";
           					 }
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker2").val())) {
              cls += " green-box ";
            }
            return [true, cls];
							
                        },
                        onSelect: function (selectedDate, instance) {
                             var fromDate = $('#datepicker').datepicker('getDate', '+1d');
                              fromDate.setDate(fromDate.getDate() + 1);
                             $('#datepicker2').datepicker('setDate', fromDate);                         },
                        onClose: function (selectedDate) {
							var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            $("#datepicker2").datepicker("option", "minDate", selectedDate);
                            $("#datepicker2").datepicker("show");
							
                        },
                    });
					                   // $('#datepicker').datepicker('setDate', new Date());
				// 21/10/2015
			//	$("#datepicker").datepicker("show");
					                    //$('#datepicker2').datepicker('getDate', '+1d');

                    /*
                     * From Date Settings
                     */
                    $("#datepicker2").datepicker({
                        defaultDate: "+1w",
                        dateFormat: 'DD, mm/dd/yy',
                        firstDay: 0,
                        numberOfMonths: 1,
                        beforeShowDay: function (date) {
							$('#ui-datepicker-div').removeClass('datepicker');
							$('#ui-datepicker-div').addClass('datepicker2');
                            var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            var date2 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker2").val());
							/*if(date2 && date >= date1 && date <= date2)
							{
								return [true,"dp-highlight",""];
							}*/
							//return [true, date1 && ((date.getTime() === date1.getTime())) ? "dp-highlightOne" : ""];
                            //return [true,  (date2 && date >= date1 && date <= date2) ? "dp-highlight" : ""];
							 var cls = '';
            if (date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
              cls += " dp-highlight ";
            }

            var from_to_dates = {};
            from_to_dates[$("#datepicker").val()] = $("#datepicker").val();
            from_to_dates[$("#datepicker2").val()] = $("#datepicker2").val();
            var highlight = from_to_dates[$.datepicker.formatDate('DD, mm/dd/yy', date)];
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker").val())) {
              cls += " blue-box ";
            }
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker2").val())) {
              cls += " green-box ";
            }
            return [true, cls];
							 
                        },
											onSelect: function (selected) {
               $("#datepicker").datepicker("option", "maxDate", selected);
					
                
            },
							            onClose: function (selectedDate) {
               $("#ui-datepicker-div").removeClass('hasDatepicker');
				 //$("#datepicker").datepicker("option", "maxDate", selectedDate);
            },
                       
                    });
			
                   // $("#datepicker2").datepicker("setDate", "1");
	/*======================this code for div which is responsive of searching table================================*/
				$("#datepicker3").attr("autocomplete", "off");
				$("#datepicker4").attr("autocomplete", "off");
				 // code for datepicker3
				 
				 $("#datepicker3").datepicker({
                        defaultDate: "+1w",
                        dateFormat: 'DD, mm/dd/yy',
                        firstDay: 0,
						minDate: 0,
                        numberOfMonths: 1,
						
                        beforeShowDay: function (date) {
							$('#ui-datepicker-div').removeClass('datepicker4');
							$('#ui-datepicker-div').addClass('datepicker');
                            var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker3").val());
                            var date2 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker4").val());
                           var cls = '';
           				 if (date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
              				cls += " dp-highlight ";
            				}

           					 var from_to_dates = {};
            				from_to_dates[$("#datepicker3").val()] = $("#datepicker3").val();
           				    from_to_dates[$("#datepicker4").val()] = $("#datepicker4").val();
           					 var highlight = from_to_dates[$.datepicker.formatDate('DD, mm/dd/yy', date)];
            				if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker3").val())) {
             					 cls += " blue-box ";
           					 }
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker4").val())) {
              cls += " green-box ";
            }
            return [true, cls];
							
                        },
                        onSelect: function (selectedDate, instance) {
                             var fromDate = $('#datepicker3').datepicker('getDate', '+1d');
                              fromDate.setDate(fromDate.getDate() + 1);
                             $('#datepicker4').datepicker('setDate', fromDate);                         },
                        onClose: function (selectedDate) {
							var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            $("#datepicker4").datepicker("option", "minDate", selectedDate);
                            $("#datepicker4").datepicker("show");
							
                        },
                    });
					
					 // $('#datepicker3').datepicker('setDate', new Date());
			//	$("#datepicker3").datepicker("show");
					                    //$('#datepicker4').datepicker('getDate', '+1d');

                    /*
                     * From Date Settings
                     */
                    $("#datepicker4").datepicker({
                        defaultDate: "+1w",
                        dateFormat: 'DD, mm/dd/yy',
                        firstDay: 0,
                        numberOfMonths: 1,
                        beforeShowDay: function (date) {
							$('#ui-datepicker-div').removeClass('datepicker3');
							$('#ui-datepicker-div').addClass('datepicker4');
                            var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker3").val());
                            var date2 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker4").val());
							/*if(date2 && date >= date1 && date <= date2)
							{
								return [true,"dp-highlight",""];
							}*/
							//return [true, date1 && ((date.getTime() === date1.getTime())) ? "dp-highlightOne" : ""];
                            //return [true,  (date2 && date >= date1 && date <= date2) ? "dp-highlight" : ""];
							 var cls = '';
            if (date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
              cls += " dp-highlight ";
            }

            var from_to_dates = {};
            from_to_dates[$("#datepicker3").val()] = $("#datepicker3").val();
            from_to_dates[$("#datepicker4").val()] = $("#datepicker4").val();
            var highlight = from_to_dates[$.datepicker.formatDate('DD, mm/dd/yy', date)];
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker3").val())) {
              cls += " blue-box ";
            }
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker4").val())) {
              cls += " green-box ";
            }
            return [true, cls];
							 
                        },
											onSelect: function (selected) {
               $("#datepicker3").datepicker("option", "maxDate", selected);
					
                
            },
							            onClose: function (selectedDate) {
               $("#ui-datepicker-div").removeClass('hasDatepicker');
				 //$("#datepicker3").datepicker("option", "maxDate", selectedDate);
            },
                       
                    });
			
                   // $("#datepicker4").datepicker("setDate", "1");
					
					
	
	/*==============================end of this code=================================================================*/
                });

            </script>
			<script>
				$("#btnsubmit").click(function(){
				//var cs = $("#birds").val().split(',');
				var sts = $("#birds").val();
				var rs = $(".fResort").val();
                var fD = $("#datepicker").val();
                var tD = $("#datepicker2").val();
				var ski_type = $(".intro").val();
				var boot = $(".add").val();
				if(sts == ""){
					alert("Please Select State Name");
				}
				else if($("#hereCity3").val() == ""){
						alert("Please Select Resort");
				}
				else if(ski_type != 'ski' && ski_type != 'snowboard'){
					alert("Please select SKI or Board");
				}
				else {
				if(rs == ''){
							if(boot == 'Boot'){
								boot = 1;
									var qs = "state=" + sts + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "search/selectSkifor?" + qs;
							}
							else{
								boot = 0;
									var qs = "state=" + sts + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "search/selectSkifor?" + qs;
		
							}
				}
				else{
							if(boot == 'Boot'){
								boot = 1;
								
									var qs = "resort=" + rs + "&state=" + sts + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "search/selectSkifor?" + qs;
							}
							else{
								boot = 0;
								
									var qs = "resort=" + rs + "&state=" + sts + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "search/selectSkifor?" + qs;
		
							}
				}	
                }
				});
			</script>
		<!--======================code for submit2 button====================-->
			<script>
				$("#btnsubmit1").click(function(){
				//var cs = $("#birds").val().split(',');
				//var sts = $("#birds").val();
				var sts = $(".birds").val();
				var rs = $("#hereCity4").val();
                var fD = $("#datepicker3").val();
                var tD = $("#datepicker4").val();
				var ski_type = $(".intro").val();
				var boot = $(".add").val();
				if(sts == ""){
					alert("Please Select State Name");
				}
				else if($("#hereCity4").val() == ""){
						alert("Please Select Resort");
				}
				else if(ski_type != 'ski' && ski_type != 'snowboard'){
					alert("Please select SKI or Board");
				}
				else {
				if(rs == ''){
							if(boot == 'Boot'){
								boot = 1;
									var qs = "state=" + sts + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "search/selectSkifor?" + qs;
							}
							else{
								boot = 0;
									var qs = "state=" + sts + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "search/selectSkifor?" + qs;
		
							}
				}
				else{
							if(boot == 'Boot'){
								boot = 1;
								
									var qs = "resort=" + rs + "&state=" + sts + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "search/selectSkifor?" + qs;
							}
							else{
								boot = 0;
								
									var qs = "resort=" + rs + "&state=" + sts + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=" + ski_type + "&boot=" + boot;
									window.location.href = window.location + "search/selectSkifor?" + qs;
		
							}
				}	
                }
				});
			</script>
		<!-------------------------------------end of the code----------------->
			

</head>
		<?php
echo $this->Html->script('oauthpopup');
?>

<style>
#birds {
    height: 47px;
    margin-top: 25px;
    width: 140px;
	color: #787878;
	font-size: 16px;
}
.fResort {
    height: 47px;
    margin-top: 25px;
    width: 98%;
	color: #787878;
	font-size: 16px;
}
#datepicker, #datepicker2 {
   font-weight: 450;
   font-family: arial;
   font-size: 14px;
	color: black;
    height: 47px;
	font-weight:400;
    margin-top: 25px;
    text-align: center;
    width: 200px;
	margin-right:5px;
	background-image: url(app/webroot/img/calendar-icon.png);
            background-repeat: no-repeat;
			    background-position: right;
}

#btnsubmit, #btnsubmit1 {
    height: 47px;
    margin-top: 25px;
    width: 126px;
	font-weight: bold;
}
#btnsubmit1 {
    height: 47px;
    margin-top: 25px;
    width: 160px;
	font-weight: bold;
}

	#datepicker3, #datepicker4 {
   font-weight: 450;
   font-family: arial;
   font-size: 14px;
	color: black;
    height: 47px;
	font-weight:400;
    margin-top: 25px;
    text-align: center;
    width: 200px;
	margin-right:5px;
	background-image: url(app/webroot/img/calendar-icon.png);
            background-repeat: no-repeat;
			    background-position: right;
}

.contentdiv {
    margin-top: -50%;
}
#b1 {background:url("app/webroot/images/slider_image1.jpg") no-repeat center 0px; background-size: 100% 700px;}
#b2 {background:url("app/webroot/images/slider_image2.jpg") no-repeat center 0px; background-size: 100% 700px;}
#b3 {background:url("app/webroot/images/slider_image3.jpg") no-repeat center 0px; background-size: 100% 700px;}
.newone {
    background-color: rgba(0, 0, 0, 0.6);
    padding: 20px 10px;
}
.cstmbuttons {background-color: rgba(255, 255, 255, 0.5);}
 .cstmbuttons:hover { background: rgba(0, 0, 0, 0.35) none repeat scroll 0 0; color: #ffffff;}  
	
#bootbtn {
    border-bottom: 4px solid #1560a2 !important;
}
.btn-default{
    background: #2782d1 none repeat scroll 0 0 !important;
    border-bottom: 5px solid #1560a2 !important;
	    font-size: 22px !important;
    font-weight: 700 !important;
    text-shadow: 0 0 0 !important;
	color: #fff!important;
}
.intro, .add, #btnsubmit, #btnsubmit1 {
    background: #5fcb08 none repeat scroll 0 0!important;
    border: 4px solid #54b108 !important;
    border-radius: 2px;
	}
	
@media screen and (max-width: 1279px){
	.howitworks { width: 17% !important;}
	.carousel {
    margin-left: -56px !important;
    width: 780px !important;
}
.prev > img {
    margin-bottom: -30%;
    margin-left: -85px !important;
}
.next > img {
    margin-right: -53px !important;
    margin-top: -222px;
}
 }   
 @media screen and (max-width: 1920px){
.carousel { width: 1097px;}
}
@media screen and (max-width: 1023px){
	.howitworks { width: 22% !important;}
    .newone {margin-left: -92px; width: 900px!important; height: 240px;}
	.carousel {
    margin-left: -42px !important;
    width: 810px !important;
}
	.col-xs-12.col-sm-4.col-md-4.col-lg-4.galleryimg > img {
    width: 100% !important;
}
.newfooter {
    width: 94%;
}

}

@media screen and (max-width: 979px){
	 #datePicForm > div {margin-left: 2% !important;}
	.table1 {display: none;}
	.divTable { display: block !important;  clear: both;}
	.newone { width: 700px !important; }
	#birds {  width: 180px;	}
	.search-group.srchinr1.soption {  float: left; width: 499px;}	
	.ddstate { float: left;}
	#clkdatepicker3, #clkdatepicker4 {float: left;  width: 250px;  margin-right: 7px;}
	.submitbtn1 {float: left; width: 160px;}
	#btnsubmit {width: 160px !important;}
	.span2.date-pick.forOnChange {  width: 255px !important; }
	#sktype, #bootbtn { width: 22% !important;}
	.carousel {
    margin-left: 96px !important;
    width: 540px !important;
}
.prev > img {
    margin-bottom: -30%!important;
}
}

@media screen and (max-width: 767px){
	.howitworks { width: 27% !important;}
	.newone { width: 595px !important;}
	#birds {float: left; width: 120px; }
	.submitbtn1 {  float: left;  width: 155px; }
	.search-group.srchinr1.soption {  float: left; width: 455px; }
	 .span2.date-pick.forOnChange { width: 200px !important; } 
	#clkdatepicker3, #clkdatepicker4 { float: left;  margin-right: 4px;  width: 204px;}
	#bootbtn { min-width: 22%; }
	.btn.btn-default.custmbuttons.skibtn2.snowbtn {width: 22% !important; }
	.btn.btn-default.custmbuttons.skibtn1 { width: 22% !important;}
	.span2.date-pick.forOnChange {  width: 204px !important;}
	.carousel {
    margin-left: 42px !important;
    width: 530px !important;
}
}

@media screen and (max-width: 639px){
	.howitworks {width: 53% !important;}
	.newone { width: 95% !important;}
	#sktype {  margin-top: 10px; width: 98% !important;}
	#bootbtn {
    margin-top: 10px;
    min-width: 98% !important;
}
	.allbtn > span { padding-left: 45%; }
	.fResort, #birds, #clkdatepicker3, #clkdatepicker4, #datepicker3, #datepicker4, .submitbtn1, #btnsubmit1 {   
		 text-align: center;
		  width: 98% !important;
	   }
	.search-group.srchinr1.soption { width: 100%; }
	.ddstate { width: 100% !important;}
	.carousel { margin-left: 100px !important;  width: 260px !important;}
	.prev > img {
    margin-bottom: -47%;   margin-left: 35px;
}
.next > img {
    margin-right: 35px;
}
 .newone {
    height: 630px;   
   

}

@media screen and (max-width: 479px){
	.carousel {
    margin-left: 43px !important;
}
.prev > img {
    margin-left: 0 !important;
}
.next > img {
    margin-right: 0!important;
}
}

@media screen and (max-width: 359px){
	.carousel {
    margin-left: 32px !important;  
	width: 230px !important;
}
.next > img {
    margin-right: 0!important;

}
	
</style>