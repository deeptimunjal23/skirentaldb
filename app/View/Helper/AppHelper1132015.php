<?php

/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {

    public function getVariationsByUserId($userId, $vType, $productId = 0) {
        if (!empty($productId)) {
            $userVariation = ClassRegistry::init('Variations')->find('all', array(
                'conditions' => array('product_id' => $productId, 'user_id' => $userId)
            ));
            $matchVariation = array();
            foreach ($userVariation as $row2):
                $matchVariation[] = $row2['Variations']['vid'];
            endforeach;
        }

        $result = ClassRegistry::init('VendorVariation')->find('all', array(
            'conditions' => array('VendorVariation.user_id' => $userId, 'VT.id' => $vType),
            //'group' => array('VT.name'), 
            'joins' => array(
                array(
                    'alias' => 'VT',
                    'table' => 'variation_types',
                    'type' => 'LEFT',
                    'conditions' => 'VT.id = VendorVariation.type'
                )
            ), 'fields' => array(
                'VendorVariation.name',
                'VendorVariation.id',
                'VendorVariation.price',
                'VendorVariation.description',
                'VT.name',
            ),
        ));
        $html = "";
        foreach ($result as $row):
            if (!empty($matchVariation) AND in_array($row['VendorVariation']['id'], $matchVariation)) {
                $checked = "checked";
            } else {
                $checked = "";
            }
            $html .= "<div style='float:left;width:130px;margin-bottom:20px;' class=''>
                           
          " . $row['VendorVariation']['name'] . " ( $" . $row['VendorVariation']['price'] . " ) " . " &nbsp;"
                    . "<input type='checkbox' name='variations[]' value='" . $row['VendorVariation']['id'] . "' $checked>
                           
                        </div>";
        endforeach;
        return $html;
    }

    function getRatingById($id) {
        $html = '';
        $countRate = ClassRegistry::init('ratings')->query("SELECT SUM(rate) AS totalRate FROM ratings where pid = '$id'");
        $countRow = ClassRegistry::init('ratings')->query("SELECT COUNT(*) AS totalRow FROM ratings where pid = '$id'");
        if (!empty($countRow[0][0]['totalRow'])) {
            $caluRate = ceil($countRate[0][0]['totalRate'] / $countRow[0][0]['totalRow']);
            $pic = $this->webroot . "css/rating/rate" . $caluRate . ".png";
            return "<img src='$pic' width='100'>";
        } else {
            $pic = $this->webroot . "css/rating/rate0.png";
            return "<img src='$pic' width='100'>";
        }
    }

    function countRatingByUserId($pid, $uid) {
        $countRow = ClassRegistry::init('ratings')->query("SELECT COUNT(*) AS totalRow FROM ratings where pid = '$pid' AND user_id='$uid'");
        return $countRow[0][0]['totalRow'];
    }

    public function getVariationsTypes() {
        return ClassRegistry::init('variation_type')->find('all'
        );
    }

    public function getGoods() {
        return ClassRegistry::init('ski_goods')->find('all', array(
                    'order' => array('order' => 'asc')
        ));
    }

    public function searchPackage($skiFor, $boot = 0, $skiType = 'ski') {
        $boot = ($boot == '1') ? 1 : 0;
        $skiType = ($skiType == 1) ? 1 : 2;

        return ClassRegistry::init('Packages')->find('first', array(
                    'conditions' => array('Packages.boot' => $boot, 'Packages.skifor' => $skiFor, 'Packages.status' => true, 'Packages.skitype' => $skiType),
                    'joins' => array(
                        array(
                            'alias' => 'ST',
                            'table' => 'skis_types',
                            'type' => 'LEFT',
                            'conditions' => 'ST.id = Packages.skitype'
                        ), array(
                            'alias' => 'SF',
                            'table' => 'skis_fors',
                            'type' => 'LEFT',
                            'conditions' => 'SF.id = Packages.skifor'
                        )
                    ),
                    'fields' => array(
                        'SF.name',
                        'ST.id',
                        'Packages.title',
                        'Packages.pic',
                        'Packages.id',
                        'Packages.description',
                        'Packages.status',
                        'Packages.boot',
                    )
        ));
    }

    public function searchOtherPackage($skiFor, $boot = 0, $skiType = 'ski') {
        $boot = ($boot == '1') ? 1 : 0;
        $skiType = ($skiType == 1) ? 1 : 2;

        return ClassRegistry::init('Packages')->find('all', array(
                    'conditions' => array('boot' => $boot, 'Packages.status' => true, 'Packages.skitype' => $skiType, 'NOT' => array('skifor' => $skiFor)),
                    'joins' => array(
                        array(
                            'alias' => 'ST',
                            'table' => 'skis_types',
                            'type' => 'LEFT',
                            'conditions' => 'ST.id = Packages.skitype'
                        ), array(
                            'alias' => 'SF',
                            'table' => 'skis_fors',
                            'type' => 'LEFT',
                            'conditions' => 'SF.id = Packages.skifor'
                        )
                    ),
                    'fields' => array(
                        'SF.name',
                        'ST.id',
                        'Packages.title',
                        'Packages.pic',
                        'Packages.id',
                        'Packages.description',
                        'Packages.status',
                        'Packages.boot',
                    )
        ));
    }

    public function countVariationChild($type, $userId) {
        return ClassRegistry::init('vendor_variations')->find('count', array(
                    'conditions' => array('type' => $type, 'user_id' => $userId)
        ));
    }

    public function countUsersVariation($userId) {
        return ClassRegistry::init('vendor_variations')->find('count', array(
                    'conditions' => array('user_id' => $userId)
        ));
    }

    public function getPackagesTypes() {
        return ClassRegistry::init('PackagesTypes')->find('all', array(
                    'conditions' => array('status' => true)
        ));
    }

    public function getPackages() {
        return ClassRegistry::init('Packages')->find('all', array(
                    'joins' => array(
                        array(
                            'alias' => 'ST',
                            'table' => 'skis_types',
                            'type' => 'LEFT',
                            'conditions' => 'ST.id = Packages.skitype'
                        ), array(
                            'alias' => 'SF',
                            'table' => 'skis_fors',
                            'type' => 'LEFT',
                            'conditions' => 'SF.id = Packages.skifor'
                        ),
                    ),
                    'fields' => array(
                        'SF.name',
                        'ST.name',
                        'Packages.title',
                        'Packages.pic',
                        'Packages.id',
                        'Packages.description',
                        'Packages.status',
                    ))
        );
    }

    public function getStateList() {
        return ClassRegistry::init('states')->find('all');
    }

    public function getStateName($sc) {
        $row = ClassRegistry::init('states')->find('first', array(
            'conditions' => array('state_code' => $sc)
        ));
        return $row['states']['state'];
    }

    public function getPackList() {
        return ClassRegistry::init('Packages')->find('all', array(
                    'conditions' => array('status' => true)
        ));
    }

    public function getSkiFor() {
        return ClassRegistry::init('skis_for')->find('all');
    }

    public function getVariationType() {
        return ClassRegistry::init('variation_types')->find('all');
    }

    public function getSkiTypes() {
        return ClassRegistry::init('skis_types')->find('all', array(
                    'conditions' => array('status' => true)
        ));
    }

    public function getSkiForRow($id) {
        return ClassRegistry::init('skis_for')->find('first', array(
                    'conditions' => array('id' => $id)
        ));
    }

    public function getPicturesByRandNo($pid, $randNo) {
        return ClassRegistry::init('Pictures')->find('all', array(
                    'conditions' => array('product_id' => $pid, 'rand_no' => $randNo)
        ));
    }

    function getForSearch() {
        $results = ClassRegistry::init('User')->find('all', array('group' => array('b_name')));
        $data = '';
        foreach ($results as $row):
            if (!empty($row['User']['b_name'])) {
                $filterData['value'] = $row['User']['b_city'] . ', ' . $row['User']['b_state'];
                $filterData['label'] = $row['User']['b_name'];
                $filterData['desc'] = $row['User']['b_city'] . ', ' . $row['User']['b_state'];
                $data .= json_encode($filterData) . ",";
            }
        endforeach;
        $filename = "img/searchLocation.txt";
        file_put_contents($filename, "[" . substr($data, 0, -1) . "]") or die('cant open file');
    }

    function setForSearch() {
        $filename = "img/searchLocation.txt";
        return file_get_contents($filename);
    }

    function getProductsByUserId($selectedSkiType = ' ',$userId) {
   //print_r($selectedSkiType );
           if (!empty($selectedSkiType)) {
                $expSkiType = explode(',', $selectedSkiType);
                $cond['Products.skitype'] = $expSkiType;
            }else{
               $cond['Products.skitype'] = array(1,2); 
            }
        $arr = array($cond, 'Products.user_id' => $userId, 'Products.status' => true);
        return ClassRegistry::init('Products')->find('all', array(
                    'limit' => 20,
                    'conditions' => $arr,
                    'joins' => array(
                        array(
                            'alias' => 'S',
                            'table' => 'stores',
                            'type' => 'LEFT',
                            'conditions' => 'S.id = Products.store_id'
                        ),
                        array(
                            'alias' => 'U',
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' => 'U.id = Products.user_id'
                        ), array(
                            'alias' => 'ST',
                            'table' => 'skis_types',
                            'type' => 'LEFT',
                            'conditions' => 'ST.id = Products.skitype'
                        ), array(
                            'alias' => 'SF',
                            'table' => 'skis_fors',
                            'type' => 'LEFT',
                            'conditions' => 'SF.id = Products.skifor'
                        ),
                        array(
                            'alias' => 'PCK',
                            'table' => 'packages',
                            'type' => 'LEFT',
                            'conditions' => 'PCK.id = Products.package'
                        ),
                        array(
                            'alias' => 'PPIC',
                            'table' => 'pictures',
                            'type' => 'LEFT',
                            'conditions' => 'PPIC.product_id = Products.id',
                        ),
                    ),
                    'fields' => array(
                        'ST.name',
                        'SF.name',
                        'Products.id',
                        'U.b_name',
                        'Products.pole',
                        'Products.boot_brand',
                        'Products.ski_brand',
                        'Products.discount',
                        'Products.walk_price',
                        'Products.srdb_price',
                        'Products.status',
                        'Products.description',
                        'Products.other_brand',
                        'Products.other_brand_avi',
                        'U.firstname',
                        'U.lastname',
                        'PPIC.pic',
                        'PCK.description',
                        'PCK.title',
                         'PCK.id',
                    ),
                    'order' => array('Products.title' => 'asc'),
                    'group' => array('PPIC.product_id')
        ));
        //return $this->paginate();
    }

    // get product image by product id 
    public function productPicById($product_id) {
        $picture = ClassRegistry::init('Picture')->findByProductId($product_id, array('Picture.pic'));
        return $picture['Picture']['pic'];
    }

}