<?php
extract($_REQUEST);
$productId = $this->params['pass'][0];
//echo  '<pre>'; print_r($row);
//echo $this->Session->read('newId');
if (!empty($row)) {
    $getOpenTime = unserialize($row['BH']['open_time']);
    $getOpenTimeAmPm = unserialize($row['BH']['open_ampm']);
    $getCloseTime = unserialize($row['BH']['close_time']);
    $getCloseTimeAmPm = unserialize($row['BH']['close_ampm']);
    $weeks = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
}

$address = $row['U']['b_address'] . ' ' . $row['U']['b_city'] . ' ' . $row['U']['b_state'];
$address = str_replace(" ", "+", $address);
$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
$response_a = json_decode($response);
$lat = $response_a->results[0]->geometry->location->lat;
$long = $response_a->results[0]->geometry->location->lng;
?>
<style>
    #map-canvas {
        width: 100%;
        height: 300px;
    }
</style>

<script src="https://maps.googleapis.com/maps/api/js"></script>

<script>
    function toggle() {
        window.alert("hi");
        var mydiv = document.getElementById('ski1');
        var mydiv1 = document.getElementById('ski2');
        mydiv.style.display = 'none';
        mydiv1.style.display = 'none';

    }
    function show() {
        window.alert("bye");
        var mydiv = document.getElementById('ski1');
        var mydiv1 = document.getElementById('ski2');
        mydiv.style.display = 'block';
        mydiv1.style.display = 'block';

    }
</script>
<script>
    function initialize() {
        var latitude = parseFloat("<?php echo $lat; ?>"); // Latitude get from above variable
        var longitude = parseFloat("<?php echo $long; ?>");
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
            center: new google.maps.LatLng(latitude, longitude),
            zoom: 15, //7/10/2015
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            map: map,
            title: "<?php echo $row['U']['b_name']; ?>",
        });

    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div class="contentdiv">
    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 sidebarcheckbx leftbarwrap">
        <div class="leftbar vendorinformation">
            <h1>Store Information</h1>

            <div class="vendordetail">
                <div><span class="storename"><?php echo $row['U']['b_name']; ?></span></div>
                <div><?php echo $row['U']['b_address']; ?></div>
                <div><?php echo $row['U']['b_city'] . ' , ' . $row['U']['b_state'] . ' ' . $row['U']['b_postalcode']; ?></div>
                <div><?php $data = $row['U']['b_phone'];
                    echo $data; ?></div>
                <div><a href="mailto:<?php echo $row['U']['b_email']; ?>"><?php echo $row['U']['b_email']; ?></a></div>

                <div class="resrvtionprice">
                    <span class="resrvtxt">Daily Rate</span>
                    <span class="resrvprice" style="color:#1D9F36">$<?php echo $row['Products']['srdb_price']; ?></span>
                    <span class="wlktxt">Walk In Rate <span
                            style="color:#ED3333">$<?php echo $row['Products']['walk_price']; ?></span></span>
                    <span class="savetxt">You Save <?php echo $row['Products']['discount']; ?>%</span>
                </div>
                <!--resrvationprice -->

                <div class="resrvtionprice" style="float: left">
                    <p style="font-size: 14px;font-weight: bold;text-align: center;color:#3867a4">Weekly Store
                        Schedule</p>
                    <?php
                    for ($x = 0; $x <= 6; $x++) {
                        $weekTime = ($getCloseTime[$x] == 'closed' || $getOpenTime[$x] == 'closed') ? 'Closed' : $getOpenTime[$x] . $getOpenTimeAmPm[$x] . ' - ' . $getCloseTime[$x] . $getCloseTimeAmPm[$x];
                        ?>
                        <div style="float: left;border-bottom: 1px solid #0066cc;line-height: 25px;">
                            <div class="fixedWeekHour" weekDaysName="<?php echo $weeks[$x]; ?>"
                                 weekDaysStatus="<?php echo $weekTime; ?>"
                                 style="text-align: left;width: 45px;float: left;">
                                <strong><?php echo $weeks[$x] . ': </strong> '; ?></div>
                            <div style="float: left;width: 150px;text-align: right;"><?php echo $weekTime; ?></div>
                        </div> <?php } ?>
                </div>
                <br>

                <div class="resrvtionprice" id="map-canvas" style="float: left;">

                </div>
                <br>
                <br>
                <a href="<?php echo $this->base; ?>/pages/privacy-policy">Affiliate Policies</a>
            </div>
            <!--vendordetail -->
        </div>
        <!--leftbar -->
    </div>
    <!--leftbarwrap -->


    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 steps step1">
        <h1 class="protitle">New Reservation</h1><!--protitle -->
        <div class="idealsteps-container">
            <nav class="idealsteps-nav"></nav>
            <div class="idealforms">
                <div class="idealsteps-wrap">
                    <section class="idealsteps-step step1">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 stepinerwraper">
                                <h1 class="pagetitle">Returning Customers</h1>
                                <?php if ($this->Session->read('fe.logedIn')) { ?>
                                    <div class="loginform formwrap">
                                        You are already logged In <a class="next" style="cursor: pointer">Click here</a>
                                        for Next Step.
                                    </div>
                                <?php } else { ?>
                                    <div class="loginform formwrap">

                                        <div class="field">
                                            <div class="form-group" style="width: 100%;">
                                                <label>Email</label>
                                                <input type="email" class="" id="uemail" placeholder="Email"
                                                       name="data[User][email]" required='required'>
                                            </div>
                                            <!--formgroup -->
                                            <span class="error"></span>
                                        </div>
                                        <!--field -->

                                        <div class="field">
                                            <div class="form-group" style="width: 100%;">
                                                <label>Password</label>
                                                <input type="password" class="" id="upass" name="data[User][password]"
                                                       placeholder="Password" required='required'>
                                            </div>
                                            <!--formgroup -->
                                            <span class="error"></span>
                                        </div>
                                        <!--field -->

                                        <div class="field">
                                            <div class="form-group- clearfix">
                                                <input type="button" class="cstmbuttons loginUser" value="Login"
                                                       name="">
                                                <input type="button" class="cstmbuttons" value="Login With Facebook"
                                                       id="facebook">
                                                <?php
                                                //  echo $this->Html->image('f2.jpg', array('id' => 'facebook', 'style' => 'cursor:pointer;padding-left:3px;'));
                                                ?>
                                            </div>
                                            <!--formgroup -->
                                        </div>
                                        <!--field -->


                                    </div><!--loginform -->
                                <?php } ?>
                            </div>
                            <!--stepinerwraper -->

                            <div class=" col-xs-12 col-sm-12 col-md-6 col-lg-6 stepinerwraper">

                                <?php if (!$this->Session->read('fe.logedIn')) { ?>
                                    <h1 class="pagetitle">New Customers</h1>
                                    <div class="loginform formwrap">
                                        <div class="field">
                                            <h2 style='margin-top:0px;'>Complete Your Reservation!</h2>

                                            <p>
                                                Register and set up your FREE skirentalDb.com
                                                account to complete your reservation! It’s
                                                SAFE, SECURE, FAST and FREE!</p>

                                            <p>Once you’re setup, you’ll have access to your
                                                skirentalDb.com Customer Dashboard where
                                                you can direct your ski trip experience for this
                                                trip and many more! </p>

                                        </div>
                                        <!--field -->
                                        <div class="field buttons">
                                            <a href="<?php echo $this->request->webroot; ?>users/redirectLogin">
                                                <button type="button" class="cstmbuttons">New User
                                                    Register &raquo;</button>
                                            </a>
                                        </div>

                                    </div><!--loginform -->
                                <?php } ?>
                            </div>
                            <!--stepinerwraper -->
                        </div>
                        <!--row -->
                    </section>

                    <!--  ***************************************** Step 2 *********************************************-->

                    <section class="idealsteps-step step2">
                        <?php if ($this->Session->read('fe.logedIn') AND empty($_REQUEST['tab'])) { ?>
                            <div class="row">
                                <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 stepinerwraper">
                                    <h1 class="pagetitle">RENTAL RESERVATION SUMMARY</h1>

                                    <div class="loginform formwrap frmflt">

                                        <div class="field">
                                            <div class="rowgroup rowgroup1 row_group2">
                                                <div class="form-group col-md-12">
                                                    <label>State / Region *</label>

                                                    <select id="hereState" class="UserState" whereDiv="hereCity"
                                                            required="required" name="" disabled="disabled">
                                                        <option value="">Select State</option>
                                                        <?php
                                                        $optns = array();

                                                        foreach ($this->App->getStateList() as $st):
                                                            if ($st['states']['state_code'] == trim($state)) {
                                                                ?>
                                                                <option
                                                                    value="<?php echo $st['states']['state_code']; ?>"
                                                                    selected='selected'><?php echo $st['states']['state']; ?></option>
                                                            <?php } else { ?>
                                                                <option
                                                                    value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state']; ?></option>
                                                            <?php } endforeach; ?>
                                                    </select>
                                                </div>
                                                <!--form-group -->
                                                <div class="form-group col-md-12">
                                                    <label>City *</label>
                                                    <select id="hereCity" name="data[User][city]" required="required"
                                                            disabled="disabled">
                                                        <option>Select City</option>
                                                    </select>
                                                </div>
                                                <!--form-group -->

                                                <div class="form-group col-md-12">
                                                    <label>Store *</label>
                                                    <select size="1" name="state" id="herestore" disabled="disabled">

                                                    </select>
                                                </div>
                                                <!--formgroup -->

                                                <div class="form-group col-md-12">
                                                    <p><strong>RECOMMENDED PACKAGE SELECTED</strong></p>

                                                    <p id='<!--'>

                                                    </p>-->
<!---------------------- 30/9/2015--->
<p>

<table><tr><td>
                                   <img src="<?php echo $this->webroot; ?><?php echo (!empty($row['PPIC']['pic'])) ? 'files/products/' . $row['PPIC']['pic'] : 'images/nopic.png'; ?>" style="height:129px;">
                            </td><td>&nbsp;&nbsp;&nbsp;</td><td>
                       
<p style=""><strong><?php echo $row['PCK']['title']; ?></strong></p>
<p><strong>Package Description: </strong><?php echo $row['Products']['description']; ?></p>

												<span><img src="<?php echo $this->webroot; ?>images/<?php
                                            switch ($row['SF']['name']) {
                                                case 'Beginner': echo 'be.gif';
                                                    break;
                                                case 'Intermediate':echo 'in.gif';
                                                    break;
                                                default:echo 'ex.gif';
                                            }
                                            ?>" height="18"></span>
                              </td></tr></table>

                        
 
<p>
<!------------------->


                                                </div>


                                                <div class="form-group col-md-12 ">
                                                    <label>Pickup Date *</label>

                                                    <input class="span2" id="datepicker3Remove" size="16"
                                                           readonly="readonly" type="text"
                                                           value="<?php echo !empty($fromDate) ? $fromDate : '' ?>"
                                                           placeholder='Select Pickup Date'>

                                                </div>
                                                <!--formgroup -->

<!--30/9/2015--->
                                                <div class="form-group clearfix ">
<a class="btn btn-primary" href="javascript:history.go(-2)" style="margin-top: 50px;">
        Back</a>
<!---------------------------->
                                                <div class="form-group col-md-12 ">
                                                    <label>Return Date *</label>

                                                    <input class="span2" id="datepicker4Remove" readonly="readonly"
                                                           size="16" type="text"
                                                           value="<?php echo !empty($toDate) ? $toDate : '' ?>"
                                                           placeholder='Select Return Date'>


                                                </div>
                                                <!--formgroup -->

                                                <div class="form-group clearfix ">
                                                    <input type="submit" name="" value="Save And Continue"
                                                           class="secondStep cstmbuttons fltright">
                                                </div>
                                                <!--formgroup -->


                                            </div>
                                            <!--field -->


                                        </div>
                                        <!--formwrap -->

                                    </div>
                                    <!--stepinerwraper -->
                                </div>
                            </div><!--row -->
                        <?php } ?>

                    </section>

                    <!--  ***************************************** Step 3 *********************************************-->

                    <section class="idealsteps-step step3">


                        <?php if ($this->Session->read('fe.logedIn') AND isset($_REQUEST['tab']) AND $_REQUEST['tab'] == 3) { ?>
                            <div id="step3After2" style="display: block;">
                                <h1 class="pagetitle">Reservation Information</h1>

                                <div class="rentrsdetail">
                                    <div class="table-responsive" id="renterInfo">
                                        <table>
                                            <tr>
                                                <th>#</th>
                                                <th>Renter Name</th>
                                                <th>Age</th>
                                                <th>Height</th>
                                                <th>Weight</th>
                                                <th>Skill Level</th>
                                                <th>Gender</th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="8">No result found...</td>

                                            </tr>
                                        </table>
                                    </div>
                                    <!--table-responsive -->
                                    <div class="field1">
                                        <div class="form-group clearfix " id="continueProceedButton"
                                             style="display:none">

                                            <input type="button" name="" value="Add New Renter" id="addNewRenterButton"
                                                   class=" cstmbuttons " style="width:185px;">
                                            <input type="button" name="" value="Checkout" id="continueForInvoice"
                                                   class="cstmbuttons" style="width:185px;float:right">
                                        </div>
                                        <!--formgroup -->
                                    </div>
                                    <!--field -->
                                </div>
                                <!-- rentrsdetail -->

                                <div class="loginform formwrap newrntrfrm" id="renterInputOuter">
                                    <input type="hidden" id="renter_id" name="renter_id" value="">
                                    <div class="field ">
                                        <div class="form-group fields2 col-md-6">
                                            <label>First Name</label>
                                            <input type="text" class="col-md-12 addFirstName" id="fname"
                                                   name="first_name"
                                                   value="<?php echo $this->Session->read('fe.makeFirstName'); ?>">
                                        </div>
                                        <!--formgroup -->
                                        <div class="form-group col-md-6">
                                            <label>Last Name</label>
                                            <input type="text" class="col-md-12 addLastName" id="lname" name="last_name"
                                                   value="<?php echo $this->Session->read('fe.makeLastName'); ?>">
                                        </div>
                                        <!--formgroup -->
                                    </div>
                                    <!--fields1 -->

                                    <div class="field fields2">
                                        <div class="form-group col-md-6">
                                            <label>Age</label>
                                            <select size="1" name="age" id="age">
                                                <option value="5-9">5-9</option>
                                                <option value="10-14">10-14</option>
                                                <option value="15-19">15-19</option>
                                                <option value="20-24">20-24</option>
                                                <option value="25-29">25-29</option>
                                                <option value="30-34">30-34</option>
                                                <option value="35-39">35-39</option>
                                                <option value="40-44">40-44</option>
                                                <option value="45-49">45-49</option>
                                                <option value="50-54">50-54</option>
                                                <option value="55-59">55-59</option>
                                                <option value="60-64">60-64</option>
                                            </select>
                                        </div>
                                        <!--formgroup -->

                                        <div class="form-group col-md-6">
                                            <label>Sex</label>
                                            <select size="1" id="sex" name="sex">

                                                <option value="male">Male</option>
                                                <option value="female">Female</option>

                                            </select>
                                        </div>
                                        <!--formgroup -->

                                        <div class="form-group col-md-6 ">
                                            <label>Height</label>
                                            <select size="1" class="pull-left mrgnrght" name="height" id="height">
                                                <?php
                                                for ($i = 0; $i < 48; ++$i) {
                                                    $x = 4 + floor($i / 12); // starts from 5, increases by 1 every 13 iterations
                                                    $y = $i % 12; // starts from 0, increases to 12 then loops back to 0 etc.

                                                    $optn = $x . ' Feet ' . $y . ' Inches';
                                                    echo "<option value='$optn'>$optn</option>";
                                                }

                                                ?>

                                            </select>

                                            </select>
                                        </div>
                                        <!--formgroup -->

                                        <div class="form-group col-md-6">
                                            <label>Weight</label>
                                            <select size="1" name="weight" id="weight">
                                                <option value="40-50">40-50 lbs</option>
                                                <option value="50-60">50-60 lbs</option>
                                                <option value="60-70">60-70 lbs</option>
                                                <option value="70-80">70-80 lbs</option>
                                                <option value="80-90">80-90 lbs</option>
                                                <option value="90-100">90-100 lbs</option>
                                                <option value="100-110">100-110 lbs</option>
                                                <option value="110-120">110-120 lbs</option>
                                                <option value="120-130">120-130 lbs</option>
                                                <option value="130-140">130-140 lbs</option>
                                                <option value="140-150">140-150 lbs</option>
                                                <option value="150-160">150-160 lbs</option>
                                                <option value="160-170">160-170 lbs</option>
                                                <option value="170-180">170-180 lbs</option>
                                                <option value="180-190">180-190 lbs</option>
                                                <option value="190-200">190-200 lbs</option>
                                                <option value="200-210">200-210 lbs</option>
                                                <option value="210-220">210-220 lbs</option>
                                                <option value="230-240">230-240 lbs</option>
                                                <option value="240-250">240-250 lbs</option>
                                                <option value="250-260">250-260 lbs</option>
                                                <option value="260-270">260-270 lbs</option>
                                                <option value="270-280">270-280 lbs</option>
                                                <option value="280-290">280-290 lbs</option>
                                                <option value="290-300">290-300 lbs</option>
                                                <option value="300+">300+ lbs</option>
                                            </select>
                                        </div>
                                        <!--formgroup -->
                                    </div>
                                    <!--fields2 -->

                                    <div class="fiel">
                                        <div class="form-group col-md-6">
                                            <label>US/shoe size</label>


                                            <select size="1" class="pull-left mrgnrght" id="bootSize" name="boot_size">
                                                <?php
                                                foreach (range(2, 15, 0.5) as $i) {
                                                    $optn = $i;
                                                    echo "<option value='$optn'>$optn</option>";
                                                }


                                                ?>

                                            </select>
                                            <input type="hidden" name="package" value="<?php echo $row['PCK']['id']; ?>"
                                                   id="userPackage"/>
                                            <input type="hidden" class="" id="ability" name="ability"
                                                   value="<?php echo $row['SF']['name']; ?>" readonly="readonly">
                                            <input type="hidden" name="pro_id"
                                                   value="<?php echo $row['Products']['id']; ?>" id="ProductId"/>
                                        </div>
                                        <!--formgroup -->
                                        <div class="col-md-9 clearfix" id="package_list" style="margin-bottom:15px;">
                                            <label>Selected Package</label><span class="chngpkg"
                                                                                 onclick="changePackage();"> (Edit Package)</span>
                                            <div class="col-md-9 clearfix package-common current-package" style="width: 100%; padding: 0">
                                                <div class="prolistbx pkgbxouter activebr slcedtpkgbx"
                                                     style="margin: 0px; padding: 0px; padding-top: 5px;"
                                                     pid="<?php echo $row['PCK']['id']; ?>"
                                                     productId="<?php echo $row['Products']['id']; ?>">
                                                    <div class="proimg"><img
                                                            src="<?php echo $this->webroot; ?><?php echo (!empty($row['PPIC']['pic'])) ? 'files/products/' . $row['PPIC']['pic'] : 'images/nopic.png'; ?>">
                                                    </div>
                                                    <div class="prodetail" style="line-height: 18px; padding-left:5px;">
                                                        <strong><?php echo $row['PCK']['title']; ?></strong>

                                                        <div style="float: left;width: 95%;font-size:10px">Walk In Rate
                                                            <strong style="color: red;font-weight: bold;">
                                                                <del>$<?php echo $row['Products']['walk_price']; ?></del>
                                                            </strong> | SRDB Rate <strong
                                                                style="color: #00a300;font-weight: bold;">$<?php echo $row['Products']['srdb_price']; ?></strong>
                                                        </div>
                                                        <div>
                                                            <p style="line-height: 20px;font-size:11px;"><?php echo substr($row['Products']['description'], 0, 80); ?>
                                                                ...</p></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php

                                            $morePro = $this->App->getProductsByUserId(1, $row['U']['id']);
                                            if (!empty($morePro)) {
                                                //echo '<pre>' ; print_r($morePro);
                                                // echo $row['PCK']['title'];

                                                foreach ($morePro as $pdt2):
                                                    if ($row['PCK']['id'] != $pdt2['PCK']['id']) {

                                                        ?>
                                                        <div class="col-md-9 clearfix package-common auto_hide" style="width: 100%; padding: 0">
                                                            <div class="prolistbx pkgbxouter slcedtpkgbx"
                                                                 style="padding: 0px; padding-top: 5px;"
                                                                 pid="<?php echo $pdt2['PCK']['id']; ?>"
                                                                 productId="<?php echo $pdt2['Products']['id']; ?>">
                                                                <div class="proimg"><img
                                                                        src="<?php echo $this->webroot; ?><?php echo (!empty($pdt2['PPIC']['pic'])) ? 'files/products/' . $pdt2['PPIC']['pic'] : 'images/nopic.png'; ?>">
                                                                </div>
                                                                <div class="prodetail"
                                                                     style="line-height: 18px;padding-left:5px;">
                                                                    <strong><?php echo $pdt2['PCK']['title']; ?></strong>

                                                                    <div style="float: left;width: 95%;font-size:10px">
                                                                        Walk In Rate <strong
                                                                            style="color: red;font-weight: bold;">
                                                                            <del>
                                                                                $<?php echo $pdt2['Products']['walk_price']; ?></del>
                                                                        </strong> | SRDB Rate <strong
                                                                            style="color: #00a300;font-weight: bold;">$<?php echo $pdt2['Products']['srdb_price']; ?></strong>
                                                                    </div>
                                                                    <div>
                                                                        <p style="line-height: 20px;font-size:11px;"><?php echo substr($pdt2['Products']['description'], 0, 80); ?>
                                                                            ...</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } endforeach;
                                            } ?>
                                            <!--------------------------------------------for both skifor------------------------------------------------------------------------------------------------------>

                                            <?php

                                            $morePro1 = $this->App->getProductsByUserId(2, $row['U']['id']);
                                            if (!empty($morePro1)) {
                                                //echo '<pre>' ; print_r($morePro1);
                                                // echo $row['PCK']['title'];

                                                foreach ($morePro1 as $pdt2):
                                                    if ($row['PCK']['id'] != $pdt2['PCK']['id']) {

                                                        ?>
                                                        <div class="col-md-9 clearfix package-common auto_hide" style="width: 100%; padding: 0;">
                                                            <div class="prolistbx pkgbxouter slcedtpkgbx" id="ski2"
                                                                 style="padding: 0px; padding-top: 5px;"
                                                                 pid="<?php echo $pdt2['PCK']['id']; ?>"
                                                                 productId="<?php echo $pdt2['Products']['id']; ?>">
                                                                <div class="proimg"><img
                                                                        src="<?php echo $this->webroot; ?><?php echo (!empty($pdt2['PPIC']['pic'])) ? 'files/products/' . $pdt2['PPIC']['pic'] : 'images/nopic.png'; ?>">
                                                                </div>
                                                                <div class="prodetail"
                                                                     style="line-height: 18px;padding-left:5px;">
                                                                    <strong><?php echo $pdt2['PCK']['title']; ?></strong>

                                                                    <div style="float: left;width: 95%;font-size:10px">
                                                                        Walk In Rate <strong
                                                                            style="color: red;font-weight: bold;">
                                                                            <del>
                                                                                $<?php echo $pdt2['Products']['walk_price']; ?></del>
                                                                        </strong> | SRDB Rate <strong
                                                                            style="color: #00a300;font-weight: bold;">$<?php echo $pdt2['Products']['srdb_price']; ?></strong>
                                                                    </div>
                                                                    <div>
                                                                        <p style="line-height: 20px;font-size:11px;"><?php echo substr($pdt2['Products']['description'], 0, 80); ?>
                                                                            ...</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } endforeach;
                                            } ?>
                                        </div>
                                        <!--formgroup -->
                                        
                                         <div class="field">
                                            <div class="form-group clearfix col-md-12 " style="width:90%;">
                                                <input type="button" id="thirdStep" class="cstmbuttons" value="Save">
     <input type="button" name="" value="Close"  class="cstmbuttons" style="float:right" onclick="closediv();" />
                                            </div><!--formgroup -->
                                        </div>   

                                            <!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->

                                       </div>
                                    </div><!--fields3 -->


                                   




                                </div><!--newrntrfrm -->
<!-- 30/9/2015-->
<div> <a class="btn btn-primary" href="javascript:history.go(-3)" style="margin-top: 50px;">
        Back</a></div>
<!------------->
                            </div>
                        <?php } ?>

                    </section>

                    <!--  ***************************************** Step 4 *********************************************-->

                    <section class="idealsteps-step step4">
                        <?php if ($this->Session->read('fe.logedIn')) { ?>
                        <h1 class="pagetitle">Payment</h1>

                        <div class="paymntdetails rentrsdetail">
                            <div class="table-responsive" id="">
                                <table class="table">
                                    <tr>
                                        <th>#</th>
                                        <th>Renter Name</th>
                                        <th>Age</th>
                                        <th>Days</th>
                                        <th>Package</th>
                                        <th>Gender</th>
                                        <th>Walk In Price</th>
                                        <th>SRDB Price</th>

                                    </tr>
                                    <tbody id="invoicePart">
                                    <tr>
                                        <td colspan="8">No invoice found...</td>
                                    </tr>
                                    </tbody>


                                </table>
                            </div>
                            <!--table-responsive -->

                            <?php } ?>
                    </section>
                    <!--idealsteps-step -->


                    <!--  ***************************************** Step 5 *********************************************-->

                    <section class="idealsteps-step step5 ">

                    </section>
                    <!--idealsteps-step -->

                    <input type="hidden" id="isLogin"
                           value="<?php echo ($this->Session->read('fe.logedIn')) ? 1 : 0; ?>">
                    <input type="hidden" id="isTab"
                           value="<?php echo isset($_REQUEST['tab']) ? $_REQUEST['tab'] : 'blank'; ?>">
                </div>
                <!--idealsteps-wrap -->
            </div>
        </div>
        <!--idealsteps-container -->

    </div>
    <!--steps -->

</div><!--contentdiv -->
<div class="form-for-paypal">
    <div class="col-md-6 "><img style="width:26%" src="<?php echo $this->webroot ?>img/visas.jpeg">
    </div>
    <!--PAyment Dynamic Saab Kitaaab-->
    <!--Script to dynamically choose a seller and buyer account to render on index page-->

    <div class="pull-right" style="width:250px;">

        <form class="form step4" action="<?php echo $this->base . '/payments/paymentProcess' ?>" id="myContainer"
              method="POST">


            <input type="hidden" name="PAYMENTREQUEST_0_NAME" value="<?php echo $row['U']['b_name']; ?>">
            <input type="hidden" name="PAYMENTREQUEST_0_NUMBER" value="Heartbeat-<?php echo $row['Products']['id']; ?>">
            <input type="hidden" name="PAYMENTREQUEST_0_DESC" value="I Love You Rubin">
            <input type="hidden" name="PAYMENTREQUEST_0_QTY" value="1" readonly>
            <input type="hidden" name="PAYMENTREQUEST_0_AMT" value="12.00" id="getSetGTotalP"></input>

            <input type="hidden" name="currencyCodeType" value="USD"></input>
            <input type="hidden" name="paymentType" value="Sale"></input>

            <input type="hidden" name="PAYMENTREQUEST_0_CUSTOM"
                   value="<?php echo $this->Session->read('fe.userId'); ?>-<?php echo $this->Session->read('newId'); ?>-<?php echo $row['Products']['user_id']; ?>">


            <!--Pass additional input parameters based on your shopping cart. For complete list of all the parameters click here -->
        </form>
    </div>
    <div class="col-md-6 "><img style="width:15%;margin-top:20px" src="<?php echo $this->webroot ?>img/ssl.jpeg"></div>
</div>

<script type="text/javascript">
    window.paypalCheckoutReady = function () {
        paypal.checkout.setup('<?php echo($merchantID); ?>', {
            container: 'myContainer',
            environment: '<?php echo($env); ?>'
        });
    };
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="<?php echo $this->webroot; ?>js/jquery.idealforms.js"></script>
<script>
    $(document).ready(function () {

        if ($("#isLogin").val() == true && $("#isTab").val() == '3') {

            $('.next').show();
            $('div.idealforms').idealforms('nextStep');
            $('div.idealforms').idealforms('nextStep');
        }
        if ($("#isLogin").val() == true && $("#isTab").val() == 'blank') {
            $('.next').show();
            $('div.idealforms').idealforms('nextStep');
        }

        $(".loginUser").click(function () {
            $.ajax({
                dataType: 'json',
                type: "POST",
                data: {uemail: $("#uemail").val(), upass: $("#upass").val()},
                url: ajaxUrl + '/rental/login', success: function (response) {
                    if (response.status == true) {
                        window.location.reload();
                    } else {
                        alert('Invalid credentials, please try again');
                    }


                }
            });
        })

        /*Second Step*/
        $(".secondStep").click(function () {
            var stateName = $("#hereState").val();
            var cityName = $("#hereCity").val();
            var storeName = $("#herestore").val();
            var picDate = $("#datepicker3Remove").val();
            var returnDate = $("#datepicker4Remove").val();
            if (stateName == '' || cityName == '' || storeName == '' || picDate == '' || returnDate == '') {
                alert('All the fields are mendatory, please select all fields value.');
            } else {
                $.ajax({
                    dataType: 'json',
                    type: "POST",
                    data: {
                        qs: '<?php echo $_SERVER['QUERY_STRING']; ?>',
                        productId: '<?php echo!empty($productId) ? $productId : ''; ?>',
                        state: stateName,
                        city: cityName,
                        store: storeName,
                        picDate: picDate,
                        returnDate: returnDate
                    },
                    url: ajaxUrl + '/rental/saveStore', success: function (response) {
                        if (response.maashaaAllah == 'accept') {
                            getRenterList();

                            $('div.idealforms').idealforms('nextStep');
                            $("#step3After2").show();

                            window.location.href = window.location.href + "&tab=3";

                        } else {
                            alert('Ops!! there is some technical issue, pls try again');
                        }


                    }
                });
            }
        });

        /*Teeja Step*/
        getRenterList();
        $("#thirdStep").click(function () {
            var fname = $("#fname").val();
            var lname = $("#lname").val();
            var age = $("#age").val();
            var height = $("#height").val();
            var weight = $("#weight").val();
            var sex = $("#sex").val();
            var bootSize = $("#bootSize").val();
            var pro_id = $("#ProductId").val();
            var pckg = $("#userPackage").val();
            var ability = $("#ability").val();
            var renter_id = $("#renter_id").val();

            if (fname == '' || lname == '' || bootSize == '' || ability == '') {
                alert('All the fields are mendatory, please select all fields value.');
            } else {
                $.ajax({
                    dataType: 'json',
                    type: "POST",
                    data: {
                        renter_id: renter_id,
                        weight: weight,
                        fname: fname,
                        lname: lname,
                        age: age,
                        height: height,
                        pro_id: pro_id,
                        sex: sex,
                        bootSize: bootSize,
                        package: pckg,
                        ability: ability
                    },
                    url: ajaxUrl + '/rental/saveRental', success: function (response) {
                        if (response.maashaaAllah == 'accept') {
                            $("#renterInfo").html(response.renterList);
                            $('#renterInputOuter input[type="text"]').val('');
                            $('#renterInputOuter').hide();
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $('#continueProceedButton').show();
                            $().val(response.detailId);
                            getRenterList();

                        } else {
                            alert('Ops!! there is some technical issue, pls try again');
                        }
                    }
                });
            }
        });

        /*Get State city*/
        $('.UserState').change(function () {
            var whereIn = $(this).attr('whereDiv');
            getSetCity($(this).val(), whereIn);
        })
        getSetCity($('.UserState').val(), $('.UserState').attr('whereDiv'));

        $('#hereCity').on('change', function () {
            getStoreByCityState($(this).val());
        })
        $('#addNewRenterButton').on('click', function () {
            $('#renter_id').val('');
            $('#renterInputOuter').show('slow');
            $('html,body').animate({scrollTop: $('#renterInputOuter').offset().top - 10}, 'slow');
            $('#save-close').show();

        })


        $('#herestore').on('change', function () {
            getSetDescription();
        })

        $('#continueForInvoice').on('click', function () {
            $('div.idealforms').idealforms('nextStep');
            getInvoice();
        })
    })

    var webootUrl = '<?php echo $this->webroot; ?>';

    function deleteRenter(id) {
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {id: id},
            url: ajaxUrl + '/rental/deleteRenterList', success: function (response) {
                if (response.status == 'oneLove') {
                    getRenterList();
                }
            }
        })

    }

    function editRenter(id) {
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {id: id},
            url: ajaxUrl + '/rental/editRenterList', success: function (response) {
                if (response.status == 1) {
                    console.log(response.renter);
                    $('#renter_id').val(response.renter.id);
                    $('#fname').val(response.renter.firstname);
                    $('#lname').val(response.renter.lastname);
                    $('#lname').val(response.renter.lastname);
                    $('#age').val(response.renter.age);
                    $('#sex').val(response.renter.gender);
                    $('#height').val(response.renter.height);
                    $('#weight').val(response.renter.weight);
                    $('#bootSize').val(response.renter.boot_size);
                    $("#ProductId").val(response.renter.product_id);
                    $("#userPackage").val(response.renter.package);
                    $("#ability").val(response.renter.ability);

                    // selected package
                    var selected_package = $(".package-common").find("[pid='" + response.renter.package + "'][productid='" + response.renter.product_id + "']");
                    console.log(selected_package);
                    $("#package_list").find('.package-common').removeClass('current-package');
                    $("#package_list").find('.pkgbxouter').removeClass('activebr');
                    selected_package.parent().addClass('current-package');
                    $("#package_list").find('.package-common:not(.current-package)').addClass('auto_hide');
                    selected_package.addClass('activebr');
                    selected_package.parent().removeClass('auto_hide');
                    // end

                    $('#renterInputOuter').show('slow');
                    $('html,body').animate({scrollTop: $('#renterInputOuter').offset().top - 10}, 'slow');
                    $('#save-close').show();
                } else {
                    alert('Something went wrong, please try again later.');
                }
            }
        })

    }


    function getStoreByCityState(vval) {
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {
                state: $('.UserState').val(),
                city: $('#hereCity').val(),
                pid: '<?php echo!empty($productId) ? $productId : ''; ?>'
            },
            url: ajaxUrl + '/rental/getStoreByCity', success: function (response) {
                $("#herestore").html(response.respectiveStores);
                $("#storeDesc").text(response.proDesc);
            }
        });
    }
    function getRenterList() {
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {productId: '<?php echo!empty($productId) ? $productId : ''; ?>'},
            url: ajaxUrl + '/rental/getRenterList', success: function (response) {
                $("#renterInfo").html(response.renterList);
                if (response.entry != '') {
                    $('#renterInputOuter').hide();
                    $('#continueProceedButton').show();
                    $('#save-close').hide();
                }

            }
        });
    }
    function getInvoice() {
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {productId: '<?php echo!empty($productId) ? $productId : ''; ?>'},
            url: ajaxUrl + '/rental/getInvoice', success: function (response) {
                //console.log(response.inVoiceList);
                if (response.inVoiceList == '') {
                    $("#invoicePart").html('<tr><td colspan="8">No invoice found...</td></tr>');
                    $(".form-for-paypal").hide();
                } else {
                    $("#invoicePart").html(response.inVoiceList);
                    $(".form-for-paypal").show();
//alert(response.gTotal);
//$("#getSetGTotalP").val(response.gTotal);
//$("#getSetTotalP").val(response.total);
//$("#getSetTaxP").val(response.tax);
                    $("#getSetGTotalP").val(response.payableAmt);
//console.log(response.payableAmt);


                }

            }
        });
    }

    function getSetCity(vval, whereIn) {
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {state: vval, city: '<?php echo!empty($city) ? $city : ''; ?>'},
            url: ajaxUrl + '/users/getCitiesByState', success: function (response) {
                $("#" + whereIn).html(response.respectiveCities);
                getStoreByCityState($("#" + whereIn).val());
            }
        });
    }

    function getSetDescription() {
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {pid: '<?php echo!empty($productId) ? $productId : ''; ?>'},
            url: ajaxUrl + '/rental/getProductDesc', success: function (response) {
                $("#storeDesc").text(response.proDesc);
            }
        });
    }
    function getSetGoPayment() {
        //alert('hi');
        window.location.href = ajaxUrl + "/rental/getSetPayment";
    }
    function closediv() {
        $('#renterInputOuter').hide();
        $('#save-close').hide();
        $("html, body").animate({scrollTop: 0}, "slow");
    }
    $('div.idealforms').idealforms({
        silentLoad: false,
        rules: {
            'username': 'required username ajax',
            'email': 'required email',
            'username': 'required',
            'password': 'required pass',
            'confirmpass': 'required equalto:password',
            'date': 'required date',
            'picture': 'required extension:jpg:png',
            'website': 'url',
            'hobbies[]': 'minoption:2 maxoption:3',
            'phone': 'required phone',
            'zip': 'required zip',
            'options': 'select:default',
        },
        errors: {
            'username': {
                ajaxError: 'Username not available'
            }
        },
        onSubmit: function (invalid, e) {
            e.preventDefault();
            $('#invalid')
                .show()
                .toggleClass('valid', !invalid)
                .text(invalid ? (invalid + ' invalid fields') : 'All good!');
        }
    });


    $('div.idealforms').find('input, select, textarea').on('change keyup', function () {
        $('#invalid').hide();
    });

    $('div.idealforms').idealforms('addRules', {
        'comments': 'required minmax:50:200'
    });

    $('.prev').click(function () {
        $('.prev').show();
        $('div.idealforms').idealforms('prevStep');
    });
    $('.next').click(function () {
        $('.next').show();
        $('div.idealforms').idealforms('nextStep');
    });

</script>


<?php
//echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js');
echo $this->Html->script('oauthpopup');
?>


<script type="text/javascript">
    $(document).ready(function () {
        $('#facebook').click(function (e) {
//var gotoVar = "<?php echo $_SERVER['QUERY_STRING']; ?>";
            $.oauthpopup({
                path: '<?php echo BASE_URL; ?>facebookCps/login',
                width: 600,
                height: 300,
                callback: function () {
                    //window.location.href = "<?php echo BASE_URL; ?>syst/dashboard";
                    location.reload();
                }
            });
            e.preventDefault();
        });

        $(".idealsteps-nav li:nth-child(4)").on('click', function () {
            $(".form-for-paypal").show();
        });
        $(".idealsteps-nav li:nth-child(1)").on('click', function () {
            $(".form-for-paypal").hide();
        });
        $(".idealsteps-nav li:nth-child(2)").on('click', function () {
            $(".form-for-paypal").hide();
        });
        $(".idealsteps-nav li:nth-child(3)").on('click', function () {
            $(".form-for-paypal").hide();
        });
        $(".idealsteps-nav li:nth-child(5)").on('click', function () {
            $(".form-for-paypal").hide();
        });
        $(".pkgbxouter").click(function () {
            //alert("dfd");
            var packid = $(this).attr("pid");
            $("#userPackage").val(packid);
            var prodid = $(this).attr("productId");
            $("#ProductId").val(prodid);
            $(".pkgbxouter").removeClass('activebr');
            $("#package_list").find('.package-common').removeClass('current-package');
            $(this).parent().addClass('current-package');
            $("#package_list").find('.package-common:not(.current-package)').addClass('auto_hide');
            $(this).addClass('activebr');
        });
    });
    function changePackage() {
        if ($("#package_list").find('.package-common').hasClass('auto_hide')) {
            $("#package_list").find('.package-common').removeClass('auto_hide');
        } else {
            $("#package_list").find('.package-common:not(.current-package)').addClass('auto_hide');
        }
    }
</script>


<style type="text/css">
    .paypal-button {
        width: 250px;
    }

    .form-for-paypal {
        display: none;
    }

    .step2 select {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 1px;
        text-overflow: '';
    }
    .auto_hide {
        display: none;
    }
</style>



