

<script type="text/javascript">
    $(document).ready(function () {
        $('#facebook').click(function (e) {
            $.oauthpopup({
                path: '<?php echo BASE_URL; ?>/facebookCps/login',
                width: 600,
                height: 300,
                callback: function () {
                    window.location.href = "<?php echo BASE_URL; ?>/syst/profile";
                }
            });
            e.preventDefault();
        });
    });
</script>
<div class="contentdiv" style="margin-top: 50px;">
    <div style="margin-top: 5px;">
        <?php echo $this->Session->flash(); ?>
    </div>
    <?php if ($this->session->read('fe.logedIn')) { ?>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            You are already logged in!
        </div>
        <?php } else {
        ?>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
            <div class="login formwrap">
                <h1 style="font-weight: bold;">Login</h1>
                <form method="post" action="<?php echo $this->base ?>/users/login">
                    <div class="rowgroup rowgroup1">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" id="" class="" name="data[User][email]" required="required">
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" id="" class=""  name="data[User][password]" required="required">
                        </div><!--form-group -->
                        <div class="form-group">
                            <a href="<?php echo $this->base ?>/users/forgotPassword">Forgot Password?</a>
                        </div><!--form-group -->
                        <div class="pull-left col-sm-4 " style="padding:0px">
                            <input name="" type="submit" value="Login" class="cstmbuttons skibtn">

                        </div><!--form-group -->
                       


                    </div><!--rowgroup1 -->
                </form>
            </div><!--formwrap -->

        </div><!--col-5 -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
            <div class="bluebx formwrap">
               <h3>Login instantly with your Facebook Account!</h3>
                <p>
You can login to your FREE skirentalDb.com account in seconds! It’s
AUTOMATIC, SAFE & SECURE! We NEVER post anything to your Facebook
account</p>


 <p  class="pull-left col-sm-12" style="padding:0px;text-align:center;width:100%;margin-top:20px;">
                            <?php              
                            echo $this->Html->image('f2.jpg', array('id' => 'facebook', 'style' => 'cursor:pointer'));
                                                   ?>
                        </p>

 <p style="margin-top:32px;width:100%;float:left">              
<div style="font-size:11px;">Don’t have a FREE skirentalDb.com account yet?</div>
<a href="<?php echo $this->base ?>/users/register/1">New Register as Customer</a><br>
                <a href="<?php echo $this->base ?>/users/register/2">New Register as Vendor</a></p>

            </div><!--bluebox -->
        </div><!--col-6 -->
    <?php } ?>
</div><!--contentdiv -->

<?php
echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js');
echo $this->Html->script('oauthpopup');
?>

