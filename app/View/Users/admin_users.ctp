
<?php echo $this->Session->flash(); ?>

<!-- Widget ID (each widget will need unique ID)-->
<div data-widget-sortable="false" data-widget-custombutton="false" data-widget-fullscreenbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" id="wid-id-x" class="jarviswidget jarviswidget-color-thistle" role="widget">

    <header>

        <h2>Users</h2>				
<h2 class="col col-2 pull-right" style="line-height: 30px;"><a style="padding: 5px;" href="<?php echo $this->base; ?>/users/adminAdd" class="btn btn-primary btn-sm">Create New User</a></h2>
    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->
            <input class="form-control" type="text">	
        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body  no-padding">

            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                <thead>			                
                    <tr>
                        <th>ID</th>
                        <th> First Name  Last Name</th>
                        <th> User Type</th>
                        <th>Email</th>
                        <th>City / State / Zip</th>
						<th>Business Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($result as $row): ?>
                    <tr>
                        <td><?php echo $row['User']['id'] ?></td>
                        <td><?php echo $row['User']['firstname'] ?>  <?php echo $row['User']['lastname'] ?></td>
                        <td><?php echo ($row['User']['type']==2)?"Vendor":"Customer"; ?></td>
                        <td><?php echo $row['User']['email'] ?></td>
                        <td><?php if($row['User']['type']==2) { echo $row['User']['b_city'].'/'.$row['User']['b_state'].'/'.$row['User']['b_postalcode'];} else { echo $row['User']['city'].'/'.$row['User']['state'].'/'.$row['User']['postalcode'];} ?></td>
						 <td><?php echo ($row['User']['type']==2)?$row['User']['b_name']:"N/A";?></td>
                        <td><a class="btn btn-xs bg-color-<?php echo ($row['User']['status'])?"green":"red" ?> txt-color-white" href="javascript:void(0);"><?php  echo ($row['User']['status'])?"Activated":"Deactivated" ?></a></td>
                        <td>
                        <a href="<?php echo $this->base; ?>/users/adminEdit/<?php echo $row['User']['id'] ?>" class="btn bg-color-magenta txt-color-white">Edit</a>
                        <a href="<?php echo $this->base; ?>/users/adminDelete/<?php echo $row['User']['id'] ?>" onclick="return confirm('You want to delete this row?')" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                   
                </tbody>
            </table>


        </div>
        <!-- end widget content -->

    </div>
    <!-- end widget div -->

</div>
