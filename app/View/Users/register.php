<script type="text/javascript">
    $(document).ready(function () {
        $('#facebook').click(function (e) {
            $.oauthpopup({
                path: '<?php echo BASE_URL; ?>/facebookCps/login',
                width: 600,
                height: 300,
                callback: function () {
                    window.location.href = "<?php echo BASE_URL; ?>/syst/profile";
                }
            });
            e.preventDefault();
        });
    });
</script>
<?php if ($this->session->read('fe.logedIn')) { ?>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        You are already logged in.
    </div>
    <?php
} else {

    if ($id == 2) {
        ?>
        <div class="contentdiv">
            <div style="margin-top: 5px;">
                <?php echo $this->Session->flash(); ?>
            </div>
            <h1 class="pagetitle">Vendor Registor</h1>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <div class="vendorrgistrationform formwrap">
<h4>Business / Store Information</h4>
                    <form method="post" action="<?php echo $this->base ?>/users/register/2">
<?php /* ?>
                        <div class="rowgroup rowgroup2"  style="display: none">
                            <h3 style="margin-bottom: 20px;">Business / Store Information</h3> 
                            <div class="form-group">
                                <label>Business Name</label>
                                <input type="text" id="" class=""  name="data[User][b_name]" required="required">
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Business Email</label>
                                <input type="email" id="" class="" required="required" name="data[User][b_email]">
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Business State</label>
                                <select  size="1" class="UserState" whereDiv="hereCity2" required="required" name="data[User][b_state]">
                                    <option selected value="">Select State</option>
                                    <?php
                                    $optns = array();
                                    foreach ($this->App->getStateList() as $st):
                                        ?>
                                        <option value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state']; ?></option>

                                    <?php endforeach; ?>
                                </select>
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Business City</label>
                                <select id="hereCity2"  name="data[User][b_city]" required="required">
                                    <option>Select City</option>
                                </select>
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Business Address</label>
                                <input type="text" id="" class="" required="required" name="data[User][b_address]">
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Business Postal Code</label>
                                <input type="text" id="" class="" required="required" name="data[User][b_postalcode]">
                            </div><!--form-group -->



                        </div><!--rowgroup2 -->

                        <div class="rowgroup rowgroup3"  style="display: none">
                            <div class="form-group">
                                <label>Rental Phone Line</label>
                                <input type="text" id="" class="" required="required" name="data[User][b_phone]" max="10"  title="Insert 10 digits number only">
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Business Fax</label>
                                <input type="text" id="" class="" placeholder="Optional" name="data[User][b_fax]">
                            </div><!--form-group -->

                        </div><!--rowgroup3 -->

                        <div class="rowgroup rowgroup4"  style="display: none">
                            <div class="form-group">
                                <label>Business Website</label>
                                <input type="text" id="" class="" placeholder="Optional" name="data[User][b_website]">
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Owner/Manager</label>
                                <input type="text" id="" class="" required="required" name="data[User][b_owner]">
                            </div><!--form-group -->

                            <div class="form-group" style="width: 26%;display: none">
                                <label>Store Open Time</label>
                                <?php
                                $oC = array('AM' => 'AM', 'PM' => 'PM');
                                //$rng = range(0,12);
                                $starttime = '00:00';
                                $time = new DateTime($starttime);
                                $interval = new DateInterval('PT30M');
                                $temptime = $time->format('H:i');
                                do {
                                    $rng[$temptime] = $temptime;
                                    $time->add($interval);
                                    $temptime = $time->format('H:i');
                                } while ($temptime !== $starttime);
                                ?>

                                <?php echo $this->Form->select('opentime', $rng, array('name' => 'data[User][opentime]', 'label' => false, 'div' => false, 'empty' => false, 'pattern' => '^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$')); ?>
                            </div><!--form-group -->
                            <div class="form-group" style="width: 14%">
                                <label>&nbsp;</label>

                                <?php echo $this->Form->select('opentime_val', $oC, array('name' => 'data[User][opentime_val]', 'label' => false, 'div' => false, 'empty' => false)); ?>
                            </div>
                            <div class="form-group" style="width: 26%">
                                <label>Store Close Time</label>
                                <?php echo $this->Form->select('closetime', $rng, array('name' => 'data[User][closetime]', 'label' => false, 'div' => false, 'empty' => false, 'pattern' => '^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$')); ?>
                            </div><!--form-group -->
                            <div class="form-group" style="width: 14%">
                                <label>&nbsp;</label>
                                <?php echo $this->Form->select('closetime_val', $oC, array('name' => 'data[User][closetime_val]', 'label' => false, 'div' => false, 'empty' => false)); ?>
                            </div>



                        </div><!--rowgroup4 -->
                        <div class="rowgroup rowgroup1" style="display: none">
                            <h3 style="margin-bottom: 20px">Basic Information</h3>    
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" id="" class="" required="required" name="data[User][firstname]">
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" id="" class="" required="required" name="data[User][lastname]">
                            </div><!--form-group -->
                        </div>
                        <div class="rowgroup rowgroup1" style="display: none">
                            <div class="form-group">
                                <label>State</label>
                                <select  size="1" class="UserState" whereDiv="hereCity1" name="data[User][state]">
                                    <option selected value="">Select State</option>
                                    <?php
                                    $optns = array();
                                    foreach ($this->App->getStateList() as $st):
                                        $optns[$st['states']['state_code']] = $st['states']['state'];
                                        ?>
                                        <option value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state']; ?></option>

                                    <?php endforeach; ?>
                                </select>
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>City</label>
                                <select id="hereCity1"  name="data[User][city]" >
                                    <option>Select City</option>
                                </select>
                            </div><!--form-group -->
                        </div> <?php */ ?>
                        <div class="rowgroup rowgroup1">

                            <div class="form-group">
                                <label>Business Name</label>
                                <input type="text" required="required" name="data[User][b_name]">

                            </div><!--form-group -->

                            <div class="form-group">
                                <label>Business Email</label>
                                <input type="email" whereIn="venEmail" id="uEmail" class="checkEmail" required="required" name="data[User][email]">

                            </div><!--form-group -->
                            
                            <div class="form-group">
                                <label>Business State</label>
                                <select  size="1" class="UserState" whereDiv="hereCity2" whereDiv1="hereCity3" required="required" name="data[User][b_state]">
                                    <option selected value="">Select State</option>
                                    <?php
                                    $optns = array();
                                    foreach ($this->App->getStateList() as $st):
                                        ?>
                                        <option value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state']; ?></option>

                                    <?php endforeach; ?>
                                </select>
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Business City</label>
                                <select id="hereCity2"  name="data[User][b_city]" required="required">
                                    <option>Select City</option>
                                </select>
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Ski Area You Service</label>
									<select id="hereCity3"  name="data[User][resort]" required="required">
										<option value="">Ski Area You Service</option>
									</select>
                            </div>
                        </div><!--rowgroup1 -->

                        <div class="rowgroup rowgroup1">

                            <div class="form-group">
                                <label>Password</label>
                                <input type="password"  id="norPass" class="checkEmail" required="required" name="data[User][password]">

                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" id="custPass" class=""  required="required" style="width: 80%;float: left;margin-right: 5px;">
                                <img id="conPT" src='<?php echo $this->webroot ?>img/test-pass-icon.png' style="display: none;float: left;margin-left: 15px; margin-top: 10px;">
                                <img id="conWT" width="16" src='<?php echo $this->webroot ?>img/remove.png' style="display: none;float: left;margin-left: 15px; margin-top: 10px;"></div><!--form-group -->

                            <div id="venEmail" style="margin-bottom: 13px;"></div>
                        </div><!--rowgroup1 -->
                        <div class="rowgroup rowgroup1">
                            <div class="form-group" style="float:left;width: 100%;">
                                <label>&nbsp;</label>
                                <label><input type="checkbox" <?php echo (!empty($_REQUEST['tc'])) ? 'checked' : '' ?>  name="data[User][tc]" required="required"> I have read and agree to the <a href="<?php echo $this->base; ?>/pages/terms-of-vendor">Terms & Conditions</a>.</label>
                            </div><!--form-group -->
                        </div><!--rowgroup5 -->
       <div class="rowgroup rowgroup1">
                            <div class="form-group" style="float:left;width: 100%;">
                                <label>&nbsp;</label>
                                <label><input type="checkbox"   name="data[User][delivery_service]" > Do you offer delivery services</label>
                            </div><!--form-group -->
                        </div><!--rowgroup5 -->

                        <div class="rowgroup rowgroup1" style="float: left">
                            <div class="form-group">
								<input type="hidden" id="fstate" class="fstate"  name="data[User][full_state]">
                                <input name="" type="submit" value="Submit" class="cstmbuttons">
                            </div><!--form-group -->

                        </div><!--rowgroup5 -->
                    </form>
                </div><!--vendorrgistrationform -->

            </div><!--col-6 -->

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="bluebx formwrap">
                    <h2>Why sign up as a SkirentalDb.com Vendor?</h2>
                    <p>                   
                        There's no cost to list your business on the skirentalDb.com site!
                        We know you're popular in your neighborhood, but… what if your
                        neighborhood got a little bigger? People are out there searching
                        for the best ski rental experience, and… we're going to help them
                        find you!</p><p>
                        With SkiretnalDb.com you'll enjoy unprecedented exposure, new
                        customers, more rentals… and that's just the tip of the iceberg!
                    <ul><li> Worldwide exposure for your rental business!</li>
                        <li> It's like having your own online reservation system!</li>
                        <li> No advertising fees or monthly costs!</li>
                        <li>You only pay a small fee when we perform (<a href="<?php echo $this->base; ?>/pages/vendor-reg-more" target="_blank">Find out more</a>)</li>
                        <li> Full control over your reservation flow!</li></ul></p>
                    <h3 ><a href="<?php echo $this->base ?>/users/register/1">Click Here if you need to register as a Customer</a></h3>
                    </p>
                </div><!--bluebox -->
            </div><!--col-6 -->
        </div><!--contentdiv -->
    <?php } else { ?>
        <div class="contentdiv">
            <div style="margin-top: 5px;">
                <?php echo $this->Session->flash(); ?>
            </div>
            <h1 class="pagetitle">Customer Registration Form</h1>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <div class="clientregistration formwrap">
                    <form method="post" action="<?php echo $this->base ?>/users/register/1">
                        <div class="rowgroup rowgroup1">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" id="" class=""  name="data[User][firstname]" required="required">
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" id="" class=""  name="data[User][lastname]" required="required">
                            </div><!--form-group -->
                        </div>
                        <div class="rowgroup rowgroup1">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" whereIn="custEmail" id="uEmail" class="checkEmail" name="data[User][email]" required="required">

                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" id="phone" class="" name="data[User][telephone]" required="required" title="Insert 10 digits number only">
                            </div><!--form-group -->
                            <div id="custEmail" style="margin-bottom: 13px;" ></div>
                        </div>

                        <div class="rowgroup rowgroup1">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" id="norPass" class="" name="data[User][password]" required="required">
                            </div><!--form-group -->



                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" id="custPass" class=""  required="required" style="width: 100%;float: left;margin-right: 5px;">
                                <img id="conPT" src='<?php echo $this->webroot ?>img/test-pass-icon.png' style="display: none;float: left;margin-left: 15px; margin-top: 10px;">
                                <img id="conWT" width="16" src='<?php echo $this->webroot ?>img/remove.png' style="display: none;float: left;margin-left: 15px; margin-top: 10px;">
                            </div><!--form-group -->
                        </div>
			<div class="rowgroup rowgroup1">
 				<div class="form-group full-field">
                                	<label>Address</label>
                                	<input type="text" id="" class=""  name="data[User][address]" placeholder="Optional">
                            	</div>
			</div>
                        <div class="rowgroup rowgroup1">
 			<div class="form-group">
                                <label>City</label>
				<input type="text" id="" class=""  name="data[User][city]" required="required">
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <select  size="1" class="UserState" whereDiv="hereCity" required="required" name="data[User][state]">
                                    <option selected value="">Select State</option>
                                    <?php
                                    $optns = array();
                                    foreach ($this->App->getStateList() as $st):
                                        $optns[$st['states']['state_code']] = $st['states']['state'];
                                        ?>
                                        <option value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state']; ?></option>

                                    <?php endforeach; ?>
                                </select>
                            </div><!--form-group -->
                           <!--form-group -->
                        </div>

                        <div class="rowgroup rowgroup1">
                            <div class="form-group">
                                <label>Postal Code</label>
                                <input type="text" id="" class="" name="data[User][postalcode]" required="required">
								<input type="hidden" id="fstate" class="fstate"  name="data[User][full_state]">
                            </div><!--form-group -->

                        </div><!--rowgroup2 -->
                       
                        <div class="rowgroup rowgroup1 clearfix">
                            <div class="form-group" >
                                <label>&nbsp;</label>
                                <label><a href="<?php echo $this->base ?>/pages/terms-of-use">Terms & Conditions</a><br/><input type="checkbox"  name="data[User][tc]" required="required" class="registerckbx"> I have read and agree.</label>
                            </div><!--form-group -->
                        </div><!--rowgroup5 -->
                        <div class="form-group subbtn">
                            <input name="" type="submit" value="Submit" class="cstmbuttons">
                           
                        </div><!--form-group -->
                    </form>
                </div><!--vendorrgistrationform -->

            </div><!--col-6 -->

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="bluebx formwrap">
                    <h2>Sign Up with your Facebook Account</h2>
                    <p>Register and set up your FREE skirentalDb.com account automatically! It’s SAFE & SECURE! We NEVER post anything to your Facebook account.</p>
                    <p  class="pull-left col-sm-12" style="padding:0px;text-align:center;width:100%;margin-top:20px;">
                            <?php              
                            echo $this->Html->image('f2.jpg', array('id' => 'facebook', 'style' => 'cursor:pointer'));
                                                   ?>
                        </p>
                    <!--<h3 style="float:left;margin-top:30px;"><a href="<?php echo $this->base ?>/pages/terms-of-vendor">Click Here For Register as Vendor</a></h3>-->
                    </p>
                </div><!--bluebox -->
            </div><!--col-6 -->
            <?php
        }
    }
    ?>

    <script type="text/javascript">
        $(document).ready(function () {
          $('.UserState').change(function () {
			  	var full_state=$(this).find(":selected").text();
			  $("#fstate").val(full_state);
                var whereIn = $(this).attr('whereDiv');
                $.ajax({
                    dataType: 'json',
                    type: "POST",
                    data: {state: $(this).val(), city: ''},
                    url: ajaxUrl + '/users/getCitiesByState', success: function (response) {
                        $("#" + whereIn).html(response.respectiveCities);
                    }});
            })
			
			<!------------------------------------------------>
				$('.UserState').change(function () {
			  	var full_state=$(this).find(":selected").text();
			 $("#fstate").val(full_state);
                var whereIn = $(this).attr('whereDiv1');
                $.ajax({
                    dataType: 'json',
                    type: "POST",
                    data: {state: $(this).val(), resort: ''},
                    url: ajaxUrl + '/users/getResortByState', success: function (response) {
                        $("#" + whereIn).html(response.respectiveCities1);
                    }});
            })
			<!---------------------------------------------->
			
            $('#uEmail').on('blur',function () {
                var whereIn = $(this).attr('whereIn');
                var thiVal = $(this).val();
				if(thiVal){
                $.ajax({
                    dataType: 'json',
                    type: "POST",
                    data: {email: $(this).val()},
                    url: ajaxUrl + '/users/isExistEmail', success: function (response) {

                        if (response.isEmail) {
                            //$("#" + whereIn).html("<span style='color:green'>You can use this <strong>" + thiVal + "</strong> email address.</span>");
							 $("#" + whereIn).html("<span style='color:red'>This email <del><strong>" + thiVal + "</strong></del> already exists.</span>");
                        } 
						else
						{
								 $("#" + whereIn).html("");
						}
						
						/*else {
                            $("#" + whereIn).html("<span style='color:red'>This email <del><strong>" + thiVal + "</strong></del> already exists.</span>");
                        }*/
						
						
                    }});
				}
            })

            $('#custPass').on('keyup', function () {

                if ($(this).val() == $("#norPass").val()) {
                    $("#conPT").show("slow");
                    $("#conWT").hide("fast");
                } else {
                    $("#conWT").show("slow");
                    $("#conPT").hide("fast");
                }
            })
   $("#phone").mask("(999) 999-9999");
   })
    </script>
<?php
echo $this->Html->script('oauthpopup');
?>