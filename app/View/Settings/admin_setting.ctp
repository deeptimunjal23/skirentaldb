   <?php echo $this->Session->flash(); ?>	     
<!-- Widget ID (each widget will need unique ID)-->
<div data-widget-sortable="false" data-widget-custombutton="false" data-widget-fullscreenbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" id="wid-id-x" class="jarviswidget jarviswidget-color-thistle" role="widget">

    <header>

        <h2>Settings</h2>				

    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->
            <input class="form-control" type="text">	
        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body  no-padding">

                     <?php echo $this->Form->create('Settings',array('class' => 'smart-form')); ?>

            <fieldset>
                <div class="row">
                    <section class="col col-6">
                        <label class="label">Today Payable Payment</label>
                        <label class="input"> <i class="icon-prepend fa fa-user"></i>
          <?php echo $this->Form->input('due_today',array('label'=>false,'div'=>false,'required'=>'required','placeholder' => 'e.g. 4.50')); ?>
                        </label>
                    </section>       
                </div>

               
  
            </fieldset>
            <footer>
                <button class="btn btn-primary" type="submit">
                    Save
                </button>
            </footer>
            </form>

        </div>
        <!-- end widget content -->

    </div>
    <!-- end widget div -->

</div>
<!-- end widget -->
