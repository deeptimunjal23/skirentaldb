
<div class="contentdiv">
    <div style="margin-top: 5px;">
<?php echo $this->Session->flash(); ?>
    </div>
    <?php echo $this->element('sidebar'); ?>
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="statusbar col-lg-12">
            <span class="pagetitle">Variations</span>
            <a href="<?php echo $this->base; ?>/variations/add"><button class="btn btn-success" type="button"><span class="glyphicon glyphicon-plus"></span>Add New Variation</button></a>
        </div>

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th width='50'>#</th>
                    <th>Name</th>
                    <th width='200'>Type</th>
                    <th width='200'>Price</th>
                    <th width='150'>Action</th>

                </tr>
            </thead>
            <tbody>
                <?php 
                $x = 1;
                foreach($result as $row): ?>
                <tr>
                    
                    <td><?php echo $x++; ?></td>
                    <td><?php echo $row['VendorVariation']['name'] ?></td>
                    <td><?php echo $row['VT']['name'] ?></td>
                    <td>$<?php echo number_format($row['VendorVariation']['price'],2) ?></td>
                               
                    <td><a class="btn btn-primary " href="<?php echo $this->base ?>/variations/edit/<?php echo $row['VendorVariation']['id']; ?> ">Edit</a> 
                        
                        <a class="btn btn-danger" onclick="return confirm('You want to delete this row?')" href="<?php echo $this->base ?>/variations/delete/<?php echo $row['VendorVariation']['id']; ?> ">Delete</a> </td>
    
                 
                            <?php endforeach; ?>
                </tr>

            </tbody>


        </table>
    </div> <!--col-lg-9 -->
</div>