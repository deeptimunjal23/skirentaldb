<div class="contentdiv">
    <?php echo $this->element('sidebar'); ?>
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="statusbar col-lg-12">
            <span class="pagetitle">Add New Variation</span>
        </div><!--status bar -->


        <div class="addproduct formwrap">
            <?php echo $this->Form->create('VendorVariation'); ?>
            <div class="rowgroup rowgroup1">
                <div class="row">
                    <div class="form-group">
                        <label>Variation Type</label>
                        <?php
                        $optns = array();
                        foreach ($this->App->getVariationType() as $st):
                            $optns[$st['variation_types']['id']] = $st['variation_types']['name'];
                        endforeach;
                        echo $this->Form->select('type', $optns, array('label' => false, 'div' => false, 'placeholder' => 'State', "empty" => "Select Type", 'required' => 'required'));
                        ?>
                    </div><!--form-group -->
                </div>
<div class="row">
                    <div class="form-group">
                        <label>Variation Name</label>
                        <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'placeholder' => '', 'required' => 'required')); ?>
                    </div><!--form-group -->
                </div>
                <div class="row">
                    <div class="form-group">
                        <label>Variation Description</label>
                        <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'placeholder' => '', 'required' => 'required')); ?>
                    </div><!--form-group -->
                </div>
                <div class="row">
                    <div class="form-group">
                        <label style="width:100%;float:left">Variation Price</label>
                        <?php echo $this->Form->input('price', array('label' => false, 'div' => false, 'required' => 'required', 'style' => 'width:100px;float:left', 'placeholder' => 'e.g. 250.00')); ?>
                    </div><!--form-group -->
                </div>

                <div class="form-group clearfix">
                    <input  type="submit" value="Submit" class="cstmbuttons">
                </div><!--form-group -->

            </div><!--rowgroup1 -->
            </form>
        </div><!--addproduct -->
    </div> <!--col-lg-9 -->
</div>