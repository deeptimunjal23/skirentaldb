   <?php echo $this->Session->flash(); ?>	     
<!-- Widget ID (each widget will need unique ID)-->
<div data-widget-sortable="false" data-widget-custombutton="false" data-widget-fullscreenbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" id="wid-id-x" class="jarviswidget jarviswidget-color-thistle" role="widget">

        <header>
                
                <h2>Edit Profile</h2>				

        </header>

        <!-- widget div-->
        <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">	
                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body  no-padding">

                     <?php echo $this->Form->create('User',array('class' => 'smart-form')); ?>

        <fieldset>
        <div class="row">
       <section class="col col-6">
           <label class="label">First Name</label>
    <label class="input"> <i class="icon-prepend fa fa-user"></i>
          <?php echo $this->Form->input('firstname',array('default'=>$row['User']['firstname'],'label'=>false,'div'=>false,'required'=>'required','placeholder' => 'First Name')); ?>
          </label>
         </section>       
                </div>

<div class="row">
       <section class="col col-6">
           <label class="label">Last Name</label>
    <label class="input"> <i class="icon-prepend fa fa-user"></i>
          <?php echo $this->Form->input('lastname',array('default'=>$row['User']['lastname'],'label'=>false,'div'=>false,'required'=>'required','placeholder' => 'Last Name')); ?>
          </label>
         </section>       
                </div>

<div class="row">
       <section class="col col-6">
           <label class="label">Email</label>
    <label class="input"> <i class="icon-prepend fa fa-envelope-o"></i>
          <?php echo $this->Form->input('email',array('default'=>$row['User']['email'],'label'=>false,'div'=>false,'disabled','placeholder' => 'Email')); ?>
          </label>
         </section>       
                </div>
 <div class="row">
                    <section class="col col-6">
                        <label class="label">State</label>
                        <label class="select">           
          <?php      
          $optns = array();
          foreach($this->App->getStateList() as $st):
              $optns[$st['states']['state_code']] = $st['states']['state'];
              endforeach;
         echo $this->Form->select('state',$optns,array('label'=>false,'div'=>false,'placeholder' => 'State',"empty"=>"Select State",'required' => 'required')); ?><i></i> 
                        </label>
                    </section>       
                </div>
                <div class="row">
                    <section class="col col-6">
                        <label class="label">City</label>
                        <label class="select"> 
                              <select id="myCities" name="data[User][city]" required="required">
                                <option>Select City</option>
                            </select><i></i>        
                        </label>
                    </section>       
                </div>
 <div class="row">
       <section class="col col-6">
           <label class="label">Postal Code</label>
    <label class="input"> <i class="icon-prepend fa fa-location-arrow"></i>
          <?php echo $this->Form->input('postalcode',array('default'=>$row['User']['postalcode'],'label'=>false,'div'=>false,'placeholder' => 'Postal Code')); ?>
    </label>
         </section>       
                </div>
        </fieldset>
<footer>
								<button class="btn btn-primary" type="submit">
									Save
								</button>
							</footer>
</form>

                </div>
                <!-- end widget content -->

        </div>
        <!-- end widget div -->

</div>
<!-- end widget -->

<script type="text/javascript">
    $(document).ready(function () {
        getSetCity($('#UserState').val(),'<?php echo $row['User']['city'] ?>');
         $('#UserState').change(function () {
          getSetCity($('#UserState').val(),"");   
         });
    })
    function getSetCity(state,city){
         $.ajax({
            dataType: 'json',
            type: "POST",
            data: {state: state,city:city},
            url: ajaxUrl + '/users/getCitiesByState', success: function (response) {
                $("#myCities").html(response.respectiveCities);
            }});
    }
</script>  