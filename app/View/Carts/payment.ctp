<div class="contentdiv">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="row">
    		<div class="col-md-12">
    			<h1 class="protitle">Choose Payment Option</h1>
    		</div>
    		<div class="col-md-6">
    			<a id="card_pay" href="javascript:void(0)"><img style="width:50%" src="<?php echo $this->webroot ?>img/visas.jpeg"></a>
    		</div>
    		<div class="col-md-6">
    			<form class="form step4" action="<?php echo $this->base . '/payments/paymentProcess' ?>" id="myContainer"
              method="POST">


		            <input type="hidden" name="PAYMENTREQUEST_0_NAME" value="biplob">
		            <input type="hidden" name="PAYMENTREQUEST_0_NUMBER" value="Heartbeat-174">
		            <input type="hidden" name="PAYMENTREQUEST_0_DESC" value="I Love You Rubin">
		            <input type="hidden" name="PAYMENTREQUEST_0_QTY" value="1" readonly>
		            <input type="hidden" name="PAYMENTREQUEST_0_AMT" value="<?php echo $total_amount; ?>" id="getSetGTotalP"></input>

		            <input type="hidden" name="currencyCodeType" value="USD"></input>
		            <input type="hidden" name="paymentType" value="Sale"></input>

		            <input type="hidden" name="PAYMENTREQUEST_0_CUSTOM"
		                   value="00222">


		            <!--Pass additional input parameters based on your shopping cart. For complete list of all the parameters click here -->
		        </form>
		        <script type="text/javascript">
				    window.paypalCheckoutReady = function () {
				        paypal.checkout.setup("<?php echo Configure::read('Paypal.username'); ?>", {
				            container: 'myContainer',
				            environment: "<?php echo Configure::read('Paypal.environment'); ?>"
				        });
				    };
				</script>
				<script src="//www.paypalobjects.com/api/checkout.js" async></script>
    		</div>
    	</div>
    	<div class="row" id="card_details" style="display: none;">
	        <h1 class="protitle">Give your card details</h1>
	        <div class="panel-body"> 
				<form class="form step4" action="<?php echo $this->base . '/carts/payment' ?>" id="myContainer" method="POST">
					<div class="field">
		                <div class="form-group fields2 col-md-6">
		                    <label>First Name</label>
		                    <input type="text" class="col-md-12 addFirstName" id="fname" name="data[Sale][first_name]" autocomplete="off">
		                </div>
		                <!--formgroup -->
		                <div class="form-group col-md-6">
		                    <label>Last Name</label>
		                    <input type="text" class="col-md-12 addFirstName" id="fname" name="data[Sale][last_name]" autocomplete="off">
		                </div>
		                <!--formgroup -->
		            </div>
		            <div class="field">
		            	<div class="form-group fields2 col-md-6">
		                    <label>Card Type</label>
			            	<select class="select-for-create-test-account form-control" id="card_type" name="data[Sale][card_type]">
			            		<option class="ccards" value="Visa">Visa</option>
			            		<option class="ccards" value="MasterCard">MasterCard</option>
			            		<option class="ccards" value="Discover">Discover</option>
			            		<option class="ccards" value="Amex">American Express</option>
			            	</select>
			            </div>
			            <div class="form-group col-md-6">
		                    <label>Card Number</label>
		                    <input type="text" class="col-md-12 addFirstName" id="card_number" name="data[Sale][card_number]" autocomplete="off">
		                </div>
		            </div>
		            <div class="field">
		            	<div class="form-group fields2 col-md-6">
		            		<label>Expiration Month</label>
				            <select id="expiration-month" name="data[Sale][exp][month]">
				            <option value="1">January</option>
				            <option value="2">February</option>
				            <option value="3">March</option>
				            <option value="4">April</option>
				            <option value="5">May</option>
				            <option value="6">June</option>
				            <option value="7">July</option>
				            <option value="8">August</option>
				            <option value="9">September</option>
				            <option value="10">October</option>
				            <option value="11">November</option>
				            <option value="12">December</option>
				            </select>
		            	</div>
			            <div class="form-group col-md-6"> 
			            	<label>Expiration Year</label>
				            <select id="expiration-year" name="data[Sale][exp][year]">
				                <?php 
				                    $yearRange = 20;
				                    $thisYear = date('Y');
				                    $startYear = ($thisYear + $yearRange);
				                 
				                    foreach (range($thisYear, $startYear) as $year) 
				                    {
				                        if ( $year == $thisYear) {
				                            print '<option value="'.$year.'" selected="selected">' . $year . '</option>';
				                        } else {
				                            print '<option value="'.$year.'">' . $year . '</option>';
				                        }
				                    }
				                ?>
				            </select>
				        </div>
				    </div>
		            <div class="field">
		            	<div class="form-group fields2 col-md-6"> 
				            <label>CVC</label>
				            <input type="text" class="col-md-12 addFirstName" id="card-security-code" name="data[Sale][cvv2]" autocomplete="off">
				        </div>
				        <!-- <div class="form-group col-md-6">
				        	<label>Amount</label>
				            <input type="text" class="col-md-12 addFirstName" id="amount" name="data[Sale][amount]" autocomplete="off">
				        </div> -->
				        <input type="hidden" class="col-md-12 addFirstName" id="amount" name="data[Sale][amount]" autocomplete="off" value="<?php echo $total_amount; ?>">
				    </div>
				    <div class="field">
		                <div class="form-group clearfix col-md-12 " style="width:90%;">
		                    <input type="submit" class="cstmbuttons" value="Pay Now">
		                </div><!--formgroup -->
		            </div>
		        </form>
			</div> 
		</div>
    </div>
</div>

<style type="text/css">
    .paypal-button {
        width: 388px !important;
        height: 100px !important;
    }
</style>

<script type="text/javascript">
	$("#card_pay").click(function(){
		$("#card_details").toggle();
	});
</script>