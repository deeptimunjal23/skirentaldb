<div class="contentdiv">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1 class="protitle">Products in Cart</h1> 
        <?php if (empty($carts)) : ?>
        	<p>Sorry, your cart is empty!</p>
    	<?php else: ?>
    		<div class="table-responsive" id="">
                <table class="table">
                	<thead>
                		<tr>
	                        <th>Order No</th>
	                        <th>Product Image</th>
	                        <th>Renter Name</th>
	                        <th>Age</th>
	                        <th>Days</th>
	                        <th>Package</th>
	                        <th>Gender</th>
	                        <th style="text-align:right;">Amount</th>
	                    </tr>
                	</thead>
                    <tbody id="invoicePart">
                    	<?php 
                    		$sl = 1; 
                    		$payment = array();
                    		$total_amount = 0;
                    	?>
			    		<?php foreach ($carts as $key => $cart) : ?> 
			    			<tr>
				    			<?php 
				    				if (!in_array($cart['Renter']['Payment']['id'], $payment)) {
				    					echo '<td>' . $sl . '</td>';
				    				} else {
				    					echo '<td></td>';
				    				} 
				    			?> 
			            		<td><img height="100px" width="100px" src="<?php echo $this->request->base . '/files/products/' . $this->App->productPicById($cart['Renter']['Product']['id']); ?>"</td>
                                <td><?php echo $cart['Renter']['firstname'] . ' ' . $cart['Renter']['lastname']; ?></td>
                                <td><?php echo $cart['Renter']['age']; ?></td>
                                <?php 

			                        $expF = explode(', ', $cart['Renter']['Payment']['picup_date']);
			                        $expF2 = explode('/', end($expF));
			                        $makeFromDate = $expF2[2] . '-' . $expF2[0] . '-' . $expF2[1];
			                   
			                        $expT = explode(', ', $cart['Renter']['Payment']['return_date']);
			                        $expT2 = explode('/', end($expT));
			                        $makeToDate = $expT2[2] . '-' . $expT2[0] . '-' . $expT2[1];
				                    
                                	$date1=date_create($makeToDate);
									$date2=date_create($makeFromDate);
									$diff=date_diff($date1,$date2);
                                ?>
                                <td><?php echo $diff->format("%a"); ?></td>
                                <td><?php echo $cart['Renter']['Package']['title']; ?></td>
                                <td><?php echo ucfirst($cart['Renter']['gender']); ?></td>
                                
                                <?php 
				    				if (!in_array($cart['Renter']['Payment']['id'], $payment)) {
				    					echo '<td style="text-align:right;">' . $cart['Cart']['amount'] . '</td>';
				    					$payment[] = $cart['Renter']['Payment']['id'];
				    					$sl++;
				    					$total_amount = $total_amount + $cart['Cart']['amount'];
				    				} else {
				    					echo '<td></td>'; 
				    				}
				    			?> 
                            </tr>
			    		<?php endforeach; ?>
			    		<tr>
			    			<td style="text-align:right;" colspan="7"><b>Grand Total</b></td>
			    			<td style="text-align:right;"><?php echo '$' . number_format($total_amount, 2); ?></td>
			    		</tr>
    				</tbody>
                </table>
                <div class="col-md-6"><?php echo $this->Html->link('Continue Shopping', '/', array('class' => 'cstmbuttons')); ?></div>
                <div class="col-md-6" style="text-align: right;"><a class="cstmbuttons" href="<?php echo $this->base . '/carts/payment' ?>">Checkout</a></div>
                
            </div>
		<?php endif; ?>
    </div>
</div>

