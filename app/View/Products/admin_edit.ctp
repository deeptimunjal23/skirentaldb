   <?php echo $this->Session->flash(); ?>	     
<!-- Widget ID (each widget will need unique ID)-->
<div data-widget-sortable="false" data-widget-custombutton="false" data-widget-fullscreenbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" id="wid-id-x" class="jarviswidget jarviswidget-color-thistle" role="widget">

    <header>

        <h2>Edit Package</h2>				

    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->
            <input class="form-control" type="text">	
        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body  no-padding">

                     <?php echo $this->Form->create('Packages',array('class' => 'smart-form')); ?>

            <fieldset>
                <div class="row">
                    <section class="col col-6">
                        <label class="input"> <i class="icon-prepend fa fa-text-width"></i>
          <?php echo $this->Form->input('title',array('label'=>false,'div'=>false,'required'=>'required','placeholder' => 'Title')); ?>
                        </label>
                    </section>       
                </div>

                <div class="row">
                    <section class="col col-6">
                        <label class="textarea">
          <?php echo $this->Form->textarea('description',array('label'=>false,'div'=>false,'required'=>'required','placeholder' => 'Description')); ?>
                        </label>
                    </section>       
                </div>
 <div class="row">
                    <section class="col col-6">
                        
                        <label class="select">
                            <select name="data[Packages][type]" required="">
                                <option value="">Select Packages Type</option>
                                
                                <?php 
                                
                                foreach($this->App->getPackagesTypes() as $row):
                                    if($row['PackagesTypes']['id'] === $row2['Packages']['type']){ ?>
                                <option value="<?php echo $row['PackagesTypes']['id'] ?>" selected="selected"><?php echo $row['PackagesTypes']['name'] ?></option>
                                <?php }else{ ?>
                                <option value="<?php echo $row['PackagesTypes']['id'] ?>"><?php echo $row['PackagesTypes']['name'] ?></option>
                                <?php }endforeach; ?>
                               
                              
                            </select> <i></i> </label>
                    </section> 
                </div>
                <div class="row">
                    <div class="col col-4">
                        <label class="checkbox">
                            <input type="checkbox" value="1" <?php echo ($row2['Packages']['status'])?"checked='checked'":""; ?> name="data[Packages][status]">
                            <i></i>Active</label>

                    </div>
                    
                </div>
               
                        </fieldset>
                        <footer>
                            <button class="btn btn-primary" type="submit">
                                Update
                            </button>
                        </footer>
                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

        </div>
        <!-- end widget -->
       