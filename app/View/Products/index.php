<div class="contentdiv">
    <div style="margin-top: 5px;">
        <?php echo $this->Session->flash(); ?>
    </div>
    <?php echo $this->element('sidebar'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="statusbar col-lg-12">
            <span class="pagetitle">Products</span>
            <a href="<?php echo $this->base; ?>/products/add"><button class="btn btn-success" type="button"><span class="glyphicon glyphicon-plus"></span>Add New Product</button></a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 proouter">
       <?php if(empty($result)){ echo "NO products added yet!"; } ?>
            <?php foreach ($result as $row): ?>
                <div class="row">
                        <div class="proinner" style="<?php echo ($row['Products']['status'] == 0)?"border: 2px red solid;":""; ?>" >
                            <div class="col-xs-2 <?php if($row['Products']['status'] == 0){ echo ' opac';} ?>">
                            <div class="pull-left proimage">
                                <div style="margin-bottom: 5px;"><span><img src="<?php echo $this->webroot; ?>images/<?php
                                        switch ($row['SF']['name']) {
                                            case 'Beginner': echo 'be.gif';
                                                break;
                                            case 'Intermediate':echo 'in.gif';
                                                break;
                                            default:echo 'ex.gif';
                                        }
                                        ?>" height="18"></span></div>
                                <img style="border:5px solid #fff;width:200px" src="<?php echo $this->webroot; ?><?php echo empty($row['PPIC']['pic']) ? 'images/nopic.png' : 'files/products/' . $row['PPIC']['pic']; ?>"> 
                            </div><!--proimg -->

                        </div><!--col-lg-4 -->
                        <div class="col-xs-7 proinfo <?php if($row['Products']['status'] == 0){ echo ' opac';} ?>">
                            <h1 class="protitle"><a href="#"><?php echo $row['Products']['title'] ?></a></h1>
                            <div class="prodescription">
                                <p style="font-size: 15px;"><?php echo substr($row['Products']['description'], 0, 150) . '...' ?></p>
                            </div><!--prodescrption -->
                            <div class=" dsplnon skilevl">
                                <div>Ski Brand: <span><?php echo!empty($row['Products']['ski_brand']) ? $row['Products']['ski_brand'] : "-----"; ?></span></div>
                                <div>Boot Brand: <span><?php echo!empty($row['Products']['boot_brand']) ? $row['Products']['boot_brand'] : "-----"; ?></span></div>
                                    <?php if(!empty($row['Products']['other_brand']) AND !empty($row['Products']['other_brand_avi'])){
                                    $oResult = unserialize($row['Products']['other_brand']);
                                    $brndAvi = unserialize($row['Products']['other_brand_avi']);
                                    foreach($oResult as $kk => $ob):
                                    if(!empty($brndAvi[$kk]) AND !empty($ob)){ ?>
                                <div><?php echo $ob; ?>: <span><?php echo $brndAvi[$kk]; ?></span></div>
                                    <?php };
                                    endforeach;} ?>
                            </div>
                        </div><!--profileinfo -->
                        <div class="col-xs-3 proinfo">
                            <div class="col-xs-12 <?php if($row['Products']['status'] == 0){ echo ' opac';} ?>">
                                <div class="pull-right"><strong>Walk In Rate:</strong> <span style="font-size: 18px;color: #2167B8">$<?php echo ($row['Products']['walk_price']) ? $row['Products']['walk_price'] : '0'; ?></span></div>
                                <div class="pull-right"><strong>Discounted Percentage:</strong> <span style="font-size: 18px;color: #00a300"><?php echo!empty($row['Products']['discount']) ? $row['Products']['discount'] : '0%'; ?>%</span></div>
                                <div class="pull-right"><strong>SRDB Rate:</strong> <span style="font-size: 18px;color: #2167B8">$<?php echo!empty($row['Products']['srdb_price']) ? $row['Products']['srdb_price'] : '0'; ?></span></div><div>&nbsp;</div>
                            </div> 
                            <div class="<?php if($row['Products']['status'] == 0){ echo 'opac';} ?>">
                                <a class="btn btn-danger pull-right col-xs-5" onclick="return confirm('You want to delete this product?')" href="<?php echo $this->base ?>/products/delete/<?php echo $row['Products']['id']; ?> " style="margin-left: 3px;margin-top: 10px">Delete</a> 
                                <a class="btn btn-primary pull-right col-xs-5"  style="margin-top: 10px" href="<?php echo $this->base ?>/products/edit/<?php echo $row['Products']['id']; ?> ">Edit</a> 
                            </div><!--btngroup -->
                            <div class="clearfix actbtn">
                             <?php if($row['Products']['status'] == 0) { ?><a class="btn btn-primary pull-right col-xs-5"  style="margin-top: 10px" href="<?php echo $this->base ?>/products/activate/<?php echo $row['Products']['id']; ?> ">Activate</a> <?php } ?>
                            </div>
                        </div>
                    </div><!--proinner -->
                </div><!--row -->
            <?php endforeach; ?>
        </div>
    </div> <!--col-lg-9 -->
</div>
<style type="text/css">
    .proinner{background: none repeat scroll 0 0 #f1f5f8;
              border: 1px solid #e0ecf5;
              clear: both;
              content: "";
              display: table;
              margin-bottom: 20px;
              padding-top: 15px;
              padding-bottom: 15px;
              width: 100%;
    }
    .proimage img{max-width:120px; width:100%;}
    .skilevl h2{font-size:12px;color:#004d60}
    .skilevl div span{color:#5fcb08;font-size: 14px;}
    .proinfo .protitle{font-size:16px; margin-top:6px;}
    .proinfo .prodescription{margin-top:10px;}
    @media screen and (max-width: 999px){
        .dsplnon{margin:0px!important;}
    }
</style>
