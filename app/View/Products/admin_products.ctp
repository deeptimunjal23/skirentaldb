<?php

echo $this->Session->flash(); ?>

<!-- Widget ID (each widget will need unique ID)-->
<div data-widget-sortable="false" data-widget-custombutton="false" data-widget-fullscreenbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" id="wid-id-x" class="jarviswidget jarviswidget-color-thistle" role="widget">

    <header>

        <h2>Products</h2>				

    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->
            <input class="form-control" type="text">	
        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body  no-padding">

           <?php echo $this->Form->create('Products',array('class' => 'smart-form','type'=>'GET','action'=>'adminProducts')); ?>
            <fieldset>
                <div class="row">
                    <section class="col col-2">
                        <label class="input"> <i class="icon-prepend fa fa-text-width"></i>
          <?php echo $this->Form->input('title',array('value'=> isset($_GET['title'])?$_GET['title']:'','label'=>false,'div'=>false,'placeholder' => 'Title')); ?>
                        </label>
                    </section>
                    <section class="col col-2">
                        <label class="select">
                                <?php 
                                $pkge = array();
                                foreach($this->App->getSkiTypes() as $row): 
                                   $pkge[$row['skis_types']['id']] =  $row['skis_types']['name'];
                                endforeach; ?>
                                    <?php echo $this->Form->select('skitype',$pkge,array('value'=> isset($_GET['skitype'])?$_GET['skitype']:'','label'=>false,'div'=>false,'placeholder' => 'State',"empty"=>"Select Ski Type")); ?><i></i> 
                            <i></i> 
                        </label>
                    </section>
                    <section class="col col-2">
                        <label class="select">
                                <?php 
                                $pkge = array();
                                foreach($this->App->getSkiFor() as $row): 
                                   $pkge[$row['skis_for']['id']] =  $row['skis_for']['name'];
                                endforeach; ?>
                                    <?php echo $this->Form->select('skifor',$pkge,array('value'=> isset($_GET['skifor'])?$_GET['skifor']:'','label'=>false,'div'=>false,'placeholder' => 'State',"empty"=>"Select Ski For")); ?><i></i> 
                            <i></i> 
                        </label>
                    </section>
                    <section class="col col-1">
                        <label class="select">           
          <?php      
          $optns = array();
          foreach($this->App->getStateList() as $st):
              $optns[$st['states']['state_code']] = $st['states']['state'];
              endforeach;
         echo $this->Form->select('state',$optns,array('value'=> isset($_GET['state'])?$_GET['state']:'','label'=>false,'div'=>false,'placeholder' => 'State',"empty"=>"Select State")); ?><i></i> 
                        </label>
                    </section> 
                    <section class="col col-1">
                        <label class="select"> 
                            <select id="myCities" name="city">
                                <option>Select City</option>
                            </select><i></i>        
                        </label>
                    </section> 
                    <section class="col col-2">
                        <label class="select"> 
                            <select id="myStore" name="store" >
                                <option>Select Store</option>
                            </select><i></i>        
                        </label>
                    </section> 
                    <section class="col col-2">

                        
                       <button class="btn btn-primary padding-5" type="submit">
                            Search
                        </button>
                        <a class="btn btn-primary padding-5 " href="<?php echo $this->base; ?>/products/adminProducts">Reset</a>

                    </section>
                   
                </div>

            </fieldset>


            </form>
        </div>
        <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
            <thead>			                
                <tr>
                    <th>ID</th>
                    <th> Title</th>
                    <th> Ski Type </th>
                    <th> Ski For </th>
                    <th> User</th>
                    <th>Status</th>

                </tr>
            </thead>
            <tbody>
                    <?php foreach($result as $row): 
                        $confirmMsg = ($row['Products']['status']==true)?"You want to deactivate this product?":"You want to activate this product?";
                        ?>
                <tr>
                    <td><?php echo $row['Products']['id'] ?></td>
                    <td><?php echo $row['Products']['title'] ?> </td>
                    <td><?php echo $row['ST']['name']?></td>
                    <td><?php echo $row['SF']['name']?></td>
                    <td><?php echo $row['U']['firstname'].' '.$row['U']['lastname']?></td>
                    <td><a pid="<?php echo $row['Products']['id'] ?>"  status ="<?php echo $row['Products']['status'] ?>"  class="myProductStatus btn btn-xs bg-color-<?php echo ($row['Products']['status'])?"green":"red" ?> txt-color-white" ><?php  echo ($row['Products']['status'])?"Activated":"Deactivated" ?></a></td>

                </tr>
                    <?php endforeach; ?>

            </tbody>
        </table>



        <div class="col-xs-12 col-sm-6 pull-right"><div id="dt_basic_paginate" class="dataTables_paginate paging_simple_numbers">
                <ul class="pagination pagination-sm">


                    <li><?php  echo $this->Paginator->prev( __('Previous'), array('currentTag' => 'a','tag' => 'li'), null, array('aria-controls'=>'dt_basic','tag' => 'a')); ?></li>


    <?php echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a','tag' => 'li','aria-controls'=>'dt_basic','class'=>'paginate_button','currentClass' => 'paginate_button active')); ?>


                    <li><?php echo $this->Paginator->next(__('Next') , array('currentTag' => 'a','tag' => 'li'), null, array('aria-controls'=>'dt_basic','class' => '','id'=>'dt_basic_next','tag' => 'a'));
  ?></li>
                </ul></div></div>      

    </div>
    <!-- end widget content -->

</div>
<!-- end widget div -->

</div>
<script type="text/javascript">
    $(document).ready(function () {
    getSetCity($("#ProductsState").val(), "<?php echo isset($city)?$city:""; ?>");
            getSetStore("<?php echo isset($city)?$city:""; ?>", "<?php echo isset($store)?$store:""; ?>");
            $('#ProductsState').change(function () {
    getSetCity($(this).val());
    });
            $("#myCities").on('change', function () {
    getSetStore($(this).val());
    });
       $(".myProductStatus").on('click', function () {
          var status = $(this).attr('status');
          var conMsg = (status==true)?"You want to deactivate this product?":"You want to activate this product?";
                if(confirm(conMsg)){
    $.ajax({
    dataType: 'json',
            type: "POST",
            url: ajaxUrl + '/products/changeStatus?status='+$(this).attr('status')+'&pid='+$(this).attr('pid'), success: function (response) {
            if (response.changeStatus == true) {
            location.reload();
            } else {
                alert('Status has not been change, pls try again');
            }
            }});
    }
    });
    })
            function getSetCity(state, city = '') {
            $.ajax({
            dataType: 'json',
                    type: "POST",
                    data: {state: state, city: city},
                    url: ajaxUrl + '/users/getCitiesByState', success: function (response) {
                    if (response.respectiveCities == "") {
                    $("#myCities").html("<option>Select City</option>");
                    } else {
                    $("#myCities").html(response.respectiveCities);
                    }
                    }});
            }
    function getSetStore(city, store = '') {
    $.ajax({
    dataType: 'json',
            type: "POST",
            data: {city:city, store: store},
            url: ajaxUrl + '/users/getStoresByCity', success: function (response) {
            if (response.respectiveStore == "") {
            $("#myStore").html("<option>No store found</option>");
            } else {
            $("#myStore").html(response.respectiveStore);
            }
            }});
    }
</script>