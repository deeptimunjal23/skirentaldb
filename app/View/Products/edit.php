<style type="text/css">
    #file_browse_wrapper 
    {
        width: 100%;
        margin-top:20px;
        background: url('<?php echo $this->webroot; ?>img/file_browse_normal.png') 0 0 no-repeat;
        border:none;
        overflow:hidden;
        float: left;
    }
    #file_browse_wrapper:hover 
    {
        background: url('<?php echo $this->webroot; ?>img/file_browse_hover.png') 0 0 no-repeat;
    }
    #file_browse_wrapper:active
    {
        background: url('<?php echo $this->webroot; ?>img/file_browse_pressed.png') 0 0 no-repeat;
    }

    #photoimg
    {
        margin-left:-145px;
        opacity:0.0;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
        filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
    }
</style>
<div class="contentdiv">
    <?php
    echo $this->element('sidebar');
    $skiForRow = $this->App->getSkiForRow($row['Products']['skifor']);
    ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="statusbar col-lg-12">
            <span class="pagetitle ">Edit Product</span>

        </div><!--status bar -->


        <div class="addproduct formwrap">
            <form enctype="multipart/form-data" id="imageform" method="post" action="<?php echo $this->base ?>/products/imageUpload">
                <div class="form-group fulbx" style="float: left;width:100%">
                    <input type="hidden" id="random1" class="myrandomnumberclass" name="randNo" value="<?php echo $row['Products']['rand_no']; ?>">
                    <input type="hidden" id="" class="" name="productId" value="<?php echo $row['Products']['id']; ?>">


                    <div id="preview" style="width:100%;float:left;display:block;">
                        <?php
                        foreach ($this->App->getPicturesByRandNo($row['Products']['id'], $row['Products']['rand_no']) as $row2):
                            $picId = $row2['Pictures']['id'];
                            echo "<span id='mypic" . $row2['Pictures']['id'] . "' style='width: 110px; float: left; position: relative;cursor:pointer'>"
                            . "<img onclick='javascript: getDelImg($picId)' src ='" . $this->webroot . "img/remove.png' style='z-index: 222222; position: absolute; top: -8px; right: 4px;' width='16'>"
                            . "<img src='" . $this->webroot . 'files/products/' . $row2['Pictures']['pic'] . "' width='100' style='padding: 2px; margin-right: 2px; border: 1px solid;' class='hereCount'>"
                            . "</span>";
                        endforeach;
                        ?>
                    </div>

                    <div style="display:none" id="imageloadstatus"><img alt="Uploading...." src="<?php echo $this->base ?>/img/loading.gif"></div>
                    <div id="imageloadbutton">
                        <div id='file_browse_wrapper'>
                            <input type="file" id="photoimg" name="photoimg">
                        </div>
                    </div>

                    <span id="maxImgUpld" style="font-size:11px;color:#000000;padding:5px;">You can upload max 1 images one by one ( Format: jpg, jpeg, png, Images Size 400 by 400 or larger.)</span>
                </div>

            </form> </div>

        <div class="addproduct formwrap">

            <?php echo $this->Form->create('Products', array('class' => 'smart-form', 'enctype' => 'multipart/form-data')); ?>

            <div class="rowgroup rowgroup1 col-md-12">
                <div style="width: 100%;float: left">
                <div class="form-group">
                    <label>Equipment Type</label>
                    <?php
                    $optns = array();
                    foreach ($this->App->getSkiTypes() as $st):
                        $optns[$st['skis_types']['id']] = $st['skis_types']['name'];
                    endforeach;
                    echo $this->Form->select('skitype', $optns, array('label' => false, 'div' => false, 'placeholder' => 'State', "empty" => "Select Equipment Type", 'required' => 'required'));
                    ?><i></i> 

                </div><!--form-group -->
                <div class="form-group" style="width: 30%;">
                    <label>Skill Level</label>
                    <?php
                    $optns2 = array();
                    foreach ($this->App->getSkiFor() as $st):
                        $optns2[$st['skis_for']['id']] = $st['skis_for']['name'];
                    endforeach;
                    echo $this->Form->select('skifor', $optns2, array('label' => false, 'div' => false, 'placeholder' => 'State', "empty" => "Select Skill Level", 'required' => 'required'));
                    ?><i></i> 

                </div><!--form-group -->

                <div class="form-group" style="width: 15%;padding-top:33px; ">
                 <img src="<?php echo $this->webroot; ?>images/<?php
                    switch ($skiForRow['skis_for']['name']) {
                        case 'Beginner': echo 'be.gif';
                            break;
                        case 'Intermediate':echo 'in.gif';
                            break;
                        case 'Expert':echo 'ex.gif';
                    }
                    ?>"></div>
                
                </div>
               

                <div class="form-group">
                    <label>Product Type</label>
                    <select id="ProductsPackage" name="data[Products][package]" required="required">

                    </select>
                </div><!--form-group -->

                <div class="form-group" style="width:150px;float: left;padding-right: 10px;">
                    <label>Walk In Rate</label>
                    <?php echo $this->Form->input('walk_price', array('label' => false, 'div' => false,'class' => 'ProductsDiscount', 'type' => 'text', 'required' => 'required', 'style' => 'width:100px', 'placeholder' => 'e.g. 280.00')); ?>
                </div><!--form-group -->
                <div class="form-group" style="width:150px;float: left;padding-right: 10px;">
                    <label>Discount Percentage</label>
                    <?php echo $this->Form->input('discount', array('label' => false, 'div' => false, 'class' => 'ProductsDiscount', 'type' => 'text', 'required' => 'required', 'style' => 'width:100px', 'placeholder' => 'e.g. 10')); ?>%
                </div><!--form-group -->
                <div class="form-group" style="width:150px;float: left;">
                    <label>SRDB Rate</label>
                    <?php echo $this->Form->input('srdb_price', array('label' => false, 'type' => 'text', 'div' => false, 'required' => 'required', 'style' => 'width:100px', 'placeholder' => 'e.g. 280.00')); ?>
                </div><!--form-group -->

                <div class="form-group fulbx">
                    <label>Title</label>
                    <?php echo $this->Form->input('title', array('label' => false, 'type' => 'text', 'div' => false, 'required' => 'required', 'readonly' => 'readonly')); ?>
                </div><!--form-group -->
                <div class="form-group fulbx">
                    <label>Description</label>
<?php echo $this->Form->input('description', array('label' => false, 'type' => 'textarea', 'div' => false, 'required' => 'required','style' => 'width: 100%;height: 100px;')); ?>
                   
                </div>
                <h4 class="fulbx">Brand Availability In Your Store</h4>
                <br>
                <div class="form-group fulbx">
                    <label>Ski Brand</label>
                    <?php echo $this->Form->input('ski_brand', array('label' => false, 'type' => 'text', 'div' => false, 'placeholder' => 'e.g. K2, Salomon, Fischer, Atomic')); ?>
                </div><!--form-group -->
                <div class="form-group fulbx">
                    <label>Boot Brand</label>
                    <?php echo $this->Form->input('boot_brand', array('label' => false, 'type' => 'text', 'div' => false, 'placeholder' => 'e.g. K2, Nike, Puma, Reeboke')); ?>
                </div><!--form-group -->
                 <div class="form-group">
                    <label>Status</label>
                    <?php
                    $sts[0] = 'Deactivate';
                    $sts[1] = 'Activate';
                    echo $this->Form->select('status', $sts, array('label' => false, 'div' => false, "empty" => false, 'required' => 'required'));
                    ?>

                </div>
                <div class="form-group clearfix" >
                    <input type="button" class="button-add btn-primary" style="width:100%;line-height: 25px;font-size: 16px;font-weight: bold" value="Add Additional Product">
                </div><!--form-group -->
                <div class="form-group">
                    <input type="button" class="button-remove btn-danger" style="width:100%;line-height: 25px;font-size: 16px;font-weight: bold" value="Delete Additional Product">
                </div><!--form-group -->
                <?php
                if (!empty($row['Products']['other_brand'])) {
                    //print_r(unserialize($row['Products']['other_brand']));
                    foreach (unserialize($row['Products']['other_brand']) as $kk => $ob):
                        $aviB = unserialize($row['Products']['other_brand_avi']);
                        ?>
                        <div class="box">
                            <div class="form-group">
                                <label>Product Type</label>

                                <select name="data[Products][other_brand][]">
                                    <option value="">Select Brand Type</option>
                                    <?php
                                    foreach ($this->App->getGoods() as $gds):
                                        if ($ob == $gds['ski_goods']['name']) {
                                            ?>
                                            <option value="<?php echo $gds['ski_goods']['name'] ?>" selected="selected"><?php echo $gds['ski_goods']['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $gds['ski_goods']['name'] ?>"><?php echo $gds['ski_goods']['name'] ?></option>  
                                        <?php };
                                    endforeach;
                                    ?>
                                </select>

                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Available Brand</label>
                                <input type="text" name="data[Products][other_brand_avi][]" value="<?php echo $aviB[$kk]; ?>" placeholder="e.g. Nike, Puma, Reeboke">
                            </div><!--form-group -->
                        </div>

                        <?php
                    endforeach;
                } else {
                    ?>
                    <div class="box">
                        <div class="form-group">
                            <label>Product Type</label>

                            <select name="data[Products][other_brand][]">
                                <option value="">Select Brand Type</option>
                                <?php foreach ($this->App->getGoods() as $gds): ?>
                                    <option value="<?php echo $gds['ski_goods']['name'] ?>"><?php echo $gds['ski_goods']['name'] ?></option>
    <?php endforeach; ?>
                            </select>

                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Available Brand</label>
                            <input type="text" name="data[Products][other_brand_avi][]"  placeholder="e.g. Nike, Puma, Reeboke">
                        </div><!--form-group -->
                    </div>
<?php } ?>
                <div class="form-group fulbx" style="display: none;">
                    <h4>Variations</h4>
                    <?php
                    $hasVari = $this->App->countUsersVariation($this->Session->read('fe.userId'));
                    if (!empty($hasVari)) {
                        foreach ($this->App->getVariationsTypes() as $var):
                            if ($this->App->countVariationChild($var['variation_type']['id'], $this->Session->read('fe.userId')) > 0) {
                                ?>
                                <label style="float:left;width: 100%;"><?php echo $var['variation_type']['name'] ?></label>
                                <?php echo $this->App->getVariationsByUserId($this->Session->read('fe.userId'), $var['variation_type']['id'], $row['Products']['id']) ?>
                                <?php
                            } endforeach;
                    } else {

                        echo "No variations found";
                    }
                    ?>
                </div><!--form-group -->
               <!--form-group -->

                <div class="form-group clearfix">
<?php echo $this->Form->hidden('rand_no', array('label' => false, 'div' => false, 'required' => 'required')); ?>

                    <input name="" type="submit" value="Submit" class="cstmbuttons">
                </div><!--form-group -->

            </div><!--rowgroup1 -->
            </form>

        </div><!--addproduct -->

    </div> <!--col-lg-9 -->
</div>

<script type="text/javascript">


    $(document).on('click', '.button-add', function () {
        $('.box').last().clone().insertAfter('.box:last');
        $('.box:last input').val('');

    })

    $(document).on('click', '.button-remove', function () {
        if ($('.box').length > 1) {
            $('.box').last().closest('.box').remove();
        }
    });

</script>



<script src="<?php echo $this->webroot; ?>js/jquery.form.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // oneTime();

        $(".ProductsDiscount").blur(function () {
            var wP = $("#ProductsWalkPrice").val();
            var dP = $("#ProductsDiscount").val();
            if (wP != "") {
              //  var minusP = Number(wP) * Number(dP) / 100;
               var minusP= parseFloat(wP.replace(/[^\d.]/g, ''))*parseFloat(dP.replace(/[^\d.]/g, ''))/100;
                $("#ProductsSrdbPrice").val('$'+(parseFloat(wP.replace(/[^\d.]/g, '')) - minusP).toFixed(2));
                $("#ProductsWalkPrice").val('$'+(parseFloat(wP.replace(/[^\d.]/g, ''))).toFixed(2));
            }
        });

        $('#photoimg').on('change', function () {
            var ext = $(this).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                alert('Only jpg, jpeg and png image formats are acceptable');
            } else {
                oneTime();
            }
        });
        var imgCount = $('.hereCount').length;
        if (imgCount >= 1) {
            $("#maxImgUpld").hide();
            $("#imageloadbutton").text("Your pictures limit for this product is over.")
        }

        packages($("#ProductsSkifor").val());
        $('#ProductsSkifor').change(function () {
            val = $(this).val();
            packages(val);
        })

        $('#ProductsSkitype').change(function () {
            $("#ProductsSkifor").val('');
            $("#ProductsPackage").val('');
        });


        $('#ProductsPackage').on('change', function () {
            getPackageDesc($(this).val());
        });


    });

    function getPackageDesc(pval) {
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {pid: pval},
            url: ajaxUrl + '/products/getPackagesDesc', success: function (response) {
                $("#productDesc").val(response.desc);
                $("#ProductsTitle").val(response.title);
            }});
    }
    function packages(val) {
        var sf = val;
        var st = $("#ProductsSkitype").val();
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {sf: sf, st: st, pckg:<?php echo $row['Products']['package'] ?>},
            url: ajaxUrl + '/products/getPackages', success: function (response) {

                $("#ProductsPackage").html(response.html);
                getPackageDesc($("#ProductsPackage").val());
            }});
    }

    function getDelImg(id) {
        var url = $("#urlforcat").val();
        $.post(ajaxUrl + "/products/productImageDelete",
                {
                    id: id,
                },
                function (data, status) {
                    $("#mypic" + id).remove();
                    var imgCount = $('.hereCount').length;
                    if (imgCount != 1) {
                        $("#maxImgUpld").show();
                        $("#imageloadbutton").html('<div id="file_browse_wrapper"><input type="file" id="photoimg" name="photoimg"></div>');
                        reCallUpload();
                    } else {
                        $("#maxImgUpld").hide();
                        $("#imageloadbutton").text("Your pictures limit for this product is over.");
                    }

                });
    }


    function reCallUpload() {
        $('#photoimg').on('change', function () {
            var ext = $(this).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                alert('Only jpg, jpeg and png image formats are acceptable');
            } else {
                oneTime();
            }
        });
    }


    function oneTime() {
        $("#imageform").ajaxForm({target: '#preview',
            beforeSubmit: function () {

                console.log('v');
                $("#imageloadstatus").show();
                $("#imageloadbutton").hide();
            },
            success: function () {
                console.log('z');
                $("#imageloadstatus").hide();
                $("#imageloadbutton").show();
            },
            error: function () {
                console.log('d');
                $("#imageloadstatus").hide();
                $("#imageloadbutton").show();
            }}).submit();

        var imgCount = $('.hereCount').length;
        if (imgCount >= 0) {
            $("#maxImgUpld").hide();
            $("#imageloadbutton").text("Your pictures limit for this product is over.")
        }
    }


</script>
