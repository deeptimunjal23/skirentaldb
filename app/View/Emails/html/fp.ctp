  <!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
 <title>Ski Rental-Listing</title>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
 <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
</head>
<body>
	<table style="max-width:700px; width:90%; margin:0px auto;  font-family:Arial, Helvetica, sans-serif; font-size:13px;">
    	
        <tr>
        	<td>
            	<table style="max-width:650px; width:100%; margin:20px auto; padding:0px 25px; box-sizing:border-box;"> 
                	<tr><td><h1 style="font-size:24px; margin:10px 0px"><?php echo !empty($header)?$header:""; ?></h1></td></tr>
                    <tr>
                    	<td>
                    		<p style="font-size:13px; text-align:justify; line-height:18px; color:#515151; margin-top:0px; margin-bottom:15px;"><?php echo !empty($body)?$body:""; ?></p>
                    	</td>
                    </tr>
                </table>
            </td>
        </tr>
       
        
       
    </table>
    
</body>
</html>

