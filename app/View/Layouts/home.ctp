 <?php 
 /**/
 //$this->App->getForSearch(); ?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <title>Ski Rental</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link href="<?php echo $this->webroot; ?>css/custome.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/bootstrap.min.css"> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	 <script src="<?php echo $this->webroot; ?>/js/jquery.maskedinput.min.js"></script>	
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" /> 
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55362fa53c7aa5fe" async="async"></script>
        <script>

            function log(message) {
                $("<div>").text(message).prependTo("#log");
                $("#log").scrollTop(0);
            }
            $(function () {
                $("#birds").autocomplete({
                    minLength: 1,
                    source: "<?php echo $this->base; ?>/home/search",
                    focus: function (event, ui) {
                        $("#birds").val(ui.item.label);
                        return false;
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li class='myLies'>").append("<a><strong  style='font-size:14px;'>" + item.label + "</strong><br></a>").appendTo(ul);
                };
               // $(".myLies li:last").append("<li>Cross</li>");

            });
        </script>
        <style type="text/css">
            #at-cv-lightbox{display: none !important;}

        </style>
    </head>
    <body class="home">
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-66408390-1', 'auto');
ga('send', 'pageview');

</script>

        <div class="newtoprow">
            <div class="container">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 social">
                    <div class="row">
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_sharing_toolbox"></div>    
                    </div><!--row -->
                </div><!--social -->
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 signin">
                    <div class="row">
                        <ul>
                            <?php if($this->session->read('fe.logedIn')){  ?>
                            <li><a href="<?php echo $this->base; ?>/syst/dashboard">My Account</a></li>
                            <li><a href="<?php echo $this->base; ?>/users/logout">Logout</a></li>
                            <?php }else{ ?>
                            <li id="registenMenu"><a href="javascript:void(0);">Register</a></li> 
                            <li id="loginWord"><a href="<?php echo $this->base; ?>/users/login">Login</a></li>
                            <li role="presentation"><a href="#">Group</a></li>
                            <?php } ?>
                        </ul>
                        <ul id="registenAs" style="display: none;">
                            <li>
                                <a href="<?php echo $this->base; ?>/pages/terms-of-vendor"><strong>Vendor Register</strong></a>
                            </li>
                            <li>
                                <a href="<?php echo $this->base; ?>/users/register/1"><strong>Customer Register</strong></a>
                            </li>

                        </ul>
                    </div><!--row -->
                </div><!--signin -->

            </div><!--container -->

        </div><!--top row -->


	<?php echo $this->fetch('content'); ?>

        
        <?php echo $this->element('footer'); ?>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#registenMenu").click(function () {
                    $("#registenAs").toggle('slow');

                })
            })
        </script>
    </body>
</html>
