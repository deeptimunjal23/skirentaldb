<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
 <title>Ski Rental-Vendor Registration</title>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
 <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
 <link href="<?php echo $this->webroot; ?>css/custome.css" rel="stylesheet" type="text/css">
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

 <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/bootstrap.min.css"> 
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">

 <script type="text/javascript">
     $(document).on('ready',function(){
    $('.close').click(function(){
       $('.alert').slideUp('slow');     
       });  
      $("#at-cv-lightbox").hide(); 
})

 var ajaxUrl = "<?php echo $this->base; ?>";
 </script>
 <style type="text/css">
     #at-cv-lightbox{display: none !important;}
 </style>
 <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo $this->webroot; ?>js/plugin/pace/pace.min.js"></script>

 <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55362fa53c7aa5fe" async="async"></script>

</head>

<body>
    
 
<?php echo $this->element('header'); ?>

<div class="contentrwrap">
	<div class="container">
    	<?php echo $this->fetch('content'); ?>	
    </div><!--contaner -->
</div><!--contentwrap -->

<?php echo $this->element('footer'); ?>
<script src="<?php echo $this->webroot; ?>js/jquery.maskedinput.min.js"></script>	
<?php //echo $this->element('sql_dump'); ?>

</body>
</html>
