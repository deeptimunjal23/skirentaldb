<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <title>Ski Rental-Vendor Registration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/rating/rating.css" />
        <link href="<?php echo $this->webroot; ?>css/custome.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->webroot; ?>css/custome2.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->webroot; ?>css/jquery.idealforms.css" rel="stylesheet" type="text/css">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/bootstrap.min.css"> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
		<style>
			.blue-box > a{background: #2782d1 !important; color:#fff !important;}
      .green-box > a{background: #5fcb08 !important; color:#fff !important;}
		</style>
        <script type="text/javascript">
            $(document).on('ready', function () {
                $('.close').click(function () {
                    $('.alert').slideUp('slow');
                });
                $("#at-cv-lightbox").hide();

            })

            $(function () {
                $("#datepicker").autocomplete({
                    minLength: 1,
                    source: "<?php echo $this->base; ?>/home/search",                   
                    focus: function (event, ui) {
                        $("#datepicker").val(ui.item.label);
                        return false;
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>").append("<a><strong  style='font-size:14px;'>" + item.label + "</strong><br></a>").appendTo(ul);
                };
            });
            function log(message) {
                $("<div>").text(message).prependTo("#log");
                $("#log").scrollTop(0);
            }
            var ajaxUrl = "<?php echo $this->base; ?>";
        </script>
        <style type="text/css">
            #at-cv-lightbox{display: none !important;}
            .backgroundColors{background-color: red !important ;}
        </style>
        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo $this->webroot; ?>js/plugin/pace/pace.min.js"></script>

        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55362fa53c7aa5fe" async="async"></script>



    </head>

    <body>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-66408390-1', 'auto');
ga('send', 'pageview');

</script>

        <?php echo $this->element('headerSearch'); ?>

        <div class="contentrwrap">
            <div class="container">

 



                <?php echo $this->fetch('content'); ?>	
            </div><!--contaner -->
        </div><!--contentwrap -->

        <?php echo $this->element('footer'); ?>

    </body>
</html>
