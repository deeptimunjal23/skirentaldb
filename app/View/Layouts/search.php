<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <title>Ski Rental-Vendor Registration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/rating/rating.css" />
        <link href="<?php echo $this->webroot; ?>css/custome.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->webroot; ?>css/custome2.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->webroot; ?>css/jquery.idealforms.css" rel="stylesheet" type="text/css">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/bootstrap.min.css"> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
		<style>
			.blue-box > a{background: #2782d1 !important; color:#fff !important;}
      .green-box > a{background: #5fcb08 !important; color:#fff !important;}
		</style>
        <script type="text/javascript">
            $(document).on('ready', function () {
                $('.close').click(function () {
                    $('.alert').slideUp('slow');
                });
                $("#at-cv-lightbox").hide();

            })

            $(function () {
                $("#birds").autocomplete({
                    minLength: 1,
                    source: "<?php echo $this->base; ?>/home/search",                   
                    focus: function (event, ui) {
                        $("#birds").val(ui.item.label);
                        return false;
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>").append("<a><strong  style='font-size:14px;'>" + item.label + "</strong><br></a>").appendTo(ul);
                };
            });
            function log(message) {
                $("<div>").text(message).prependTo("#log");
                $("#log").scrollTop(0);
            }
            var ajaxUrl = "<?php echo $this->base; ?>";
        </script>
        <style type="text/css">
            #at-cv-lightbox{display: none !important;}
            .backgroundColors{background-color: red !important ;}
        </style>
        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo $this->webroot; ?>js/plugin/pace/pace.min.js"></script>

        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55362fa53c7aa5fe" async="async"></script>



    </head>

    <body>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-66408390-1', 'auto');
ga('send', 'pageview');

</script>

        <?php echo $this->element('headerSearch'); ?>

        <div class="contentrwrap">
            <div class="container">

 



                <?php echo $this->fetch('content'); ?>	
            </div><!--contaner -->
        </div><!--contentwrap -->

        <?php echo $this->element('footer'); ?>

        <?php //echo $this->element('sql_dump'); ?>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <?php //if ($this->params['action'] == 'selectDate') { ?>
            <script>
                $(document).ready(function () {
                    /*
                     * From Date Settings
                     */
					$("#datepicker").attr("autocomplete", "off");
					$("#datepicker2").attr("autocomplete", "off");
                    $("#datepicker").datepicker({
                        defaultDate: "+1w",
                        dateFormat: 'DD, mm/dd/yy',
                        firstDay: 0,
						minDate: 0,
                        numberOfMonths: 1,
						
                        beforeShowDay: function (date) {
							$('#ui-datepicker-div').removeClass('datepicker2');
							$('#ui-datepicker-div').addClass('datepicker');
                            var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            var date2 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker2").val());
                           var cls = '';
           				 if (date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
              				cls += " dp-highlight ";
            				}

           					 var from_to_dates = {};
            				from_to_dates[$("#datepicker").val()] = $("#datepicker").val();
           				    from_to_dates[$("#datepicker2").val()] = $("#datepicker2").val();
           					 var highlight = from_to_dates[$.datepicker.formatDate('DD, mm/dd/yy', date)];
            				if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker").val())) {
             					 cls += " blue-box ";
           					 }
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker2").val())) {
              cls += " green-box ";
            }
            return [true, cls];
							
                        },
                        onSelect: function (selectedDate, instance) {
                             var fromDate = $('#datepicker').datepicker('getDate', '+1d');
                              fromDate.setDate(fromDate.getDate() + 1);
                            <?php if ($this->params['action'] == 'selectDate') { ?> $('#datepicker2').datepicker('setDate', fromDate); <?php } ?>
                        },
                        onClose: function (selectedDate) {
							var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            $("#datepicker2").datepicker("option", "minDate", selectedDate);
                            $("#datepicker2").datepicker("show");
							
                        },
                    });
					<?php if ($this->params['action'] == 'selectDate') { ?>
                    $('#datepicker').datepicker('setDate', new Date());
					$("#datepicker").datepicker("show");
					<?php }	?>
                    //$('#datepicker2').datepicker('getDate', '+1d');


                    /*
                     * From Date Settings
                     */
                    $("#datepicker2").datepicker({
                        defaultDate: "+1w",
                        dateFormat: 'DD, mm/dd/yy',
                        firstDay: 0,
                        numberOfMonths: 1,
                        beforeShowDay: function (date) {
							$('#ui-datepicker-div').removeClass('datepicker');
							$('#ui-datepicker-div').addClass('datepicker2');
                            var date1 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker").val());
                            var date2 = $.datepicker.parseDate('DD, mm/dd/yy', $("#datepicker2").val());
							/*if(date2 && date >= date1 && date <= date2)
							{
								return [true,"dp-highlight",""];
							}*/
							//return [true, date1 && ((date.getTime() === date1.getTime())) ? "dp-highlightOne" : ""];
                            //return [true,  (date2 && date >= date1 && date <= date2) ? "dp-highlight" : ""];
							 var cls = '';
            if (date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))) {
              cls += " dp-highlight ";
            }

            var from_to_dates = {};
            from_to_dates[$("#datepicker").val()] = $("#datepicker").val();
            from_to_dates[$("#datepicker2").val()] = $("#datepicker2").val();
            var highlight = from_to_dates[$.datepicker.formatDate('DD, mm/dd/yy', date)];
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker").val())) {
              cls += " blue-box ";
            }
            if (highlight && ($.datepicker.formatDate('DD, mm/dd/yy', date) === $("#datepicker2").val())) {
              cls += " green-box ";
            }
            return [true, cls];
							 
                        },
					<?php 	if ($this->params['action'] == 'selectDate') { ?>
						onSelect: function (selected) {
               $("#datepicker").datepicker("option", "maxDate", selected);
                var cs = $("#birds").val().split(',');
                var fD = $("#datepicker").val();
                var tD = $("#datepicker2").val();
                if ($("#birds").val() == '') {
                    alert('Please select resort/destination');
                    $("#datepicker2").val('');
                } else if (fD == '') {
                    alert('Please select picup date');
                    $("#datepicker2").val('');
                } else if (tD == '') {
                    alert('Please select return date');
                    $("#datepicker2").val('');
                } else {
                    $("#datePicForm").submit();
                }
            },
				<?php } ?>
			<?php 	if ($this->params['action'] == 'stores') { ?>
						onSelect: function () {

                var cs = $("#birds").val().split(',');
                var fD = $("#datepicker").val();
                var tD = $("#datepicker2").val();
                var qs = "resort=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=<?php echo $_REQUEST['skiType']; ?>" + "&skiFor=<?php echo $_REQUEST['skiFor']; ?>" + "&boot=<?php echo $_REQUEST['boot']; ?>" + "&pid=<?php echo isset($_REQUEST['pid']) ? $_REQUEST['pid'] : 0; ?>";
                window.location.href = ajaxUrl + "/search/stores/<?php echo $id; ?>?" + qs;

            },
			<?php } if ($this->params['action'] == 'getByPackages') {?>
						onSelect: function () {
                var cs = $("#birds").val().split(',');
                var fD = $("#datepicker").val();
                var tD = $("#datepicker2").val();
                var qs = "resort=" + cs[0] + "&state=" + cs[1] + "&fromDate=" + fD + "&toDate=" + tD + "&skiType=<?php echo isset($_REQUEST['skiType']) ? $_REQUEST['skiType'] : ''; ?>" + "&skiFor=<?php echo isset($_REQUEST['skiFor']) ? $_REQUEST['skiFor'] : ''; ?>" + "&boot=<?php echo isset($_REQUEST['boot']) ? $_REQUEST['boot'] : ''; ?>" + "&pid=<?php echo isset($_REQUEST['pid']) ? $_REQUEST['pid'] : 0; ?>";
                window.location.href = ajaxUrl + "/search/getByPackages?" + qs + "&pid=0";

            },
						<?php } ?>
            onClose: function (selectedDate) {
               $("#ui-datepicker-div").removeClass('hasDatepicker');
				 //$("#datepicker").datepicker("option", "maxDate", selectedDate);
            },
                       
                    });
			
                    
                });

            </script>

    </body>
</html>
