<div class="contentdiv">
    <div style="margin-top: 5px;">
        <?php echo $this->Session->flash(); ?>
    </div>
    <?php echo $this->element('sidebar'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="statusbar col-lg-12">
            <span class="pagetitle">Billing Information</span>
        </div><!--status bar -->


        <div class="addproduct formwrap">

            <?php echo $this->Form->create('User', array('class' => 'smart-form', 'enctype' => 'multipart/form-data')); ?>
 
            <div class="rowgroup rowgroup1">
             
                <div class="row">
   <h4 style="padding-bottom: 25px;">Paypal Information</h4>
                    <div class="form-group">
                        <label>Paypal Account Name </label>

                        <?php echo $this->Form->input('paypal_name', array('label' => false, 'div' => false)); ?>
                    </div><!--form-group -->
                    <div class="form-group">
                        <label>Paypal Account Email </label>
                        <?php echo $this->Form->input('paypal_email', array('label' => false, 'div' => false)); ?>
                    </div><!--form-group -->
                </div>
             
                <div class="row">
 <h4 style="padding-bottom: 15px;padding-top: 15px;">Credit Card Information</h4>
                    <div class="form-group">
                        <label>Credit Card Name </label>
                        <?php echo $this->Form->input('creditcard_name', array('label' => false, 'div' => false)); ?>
                    </div><!--form-group --> 
                    <div class="form-group">
                        <label>Credit Card Number</label>
                        <?php echo $this->Form->input('creditcard_no', array('label' => false, 'div' => false,'pattern'=>"[0-9]*", 'title'=>"Please insert numaric value")); ?>
                    </div><!--form-group -->
                </div>
                <div class="row">

                    <div class="form-group">
                        <label>Credit Card CSV Code </label>
                        <?php echo $this->Form->input('creditcard_csvno', array('label' => false, 'div' => false,'pattern'=>"[0-9]*", 'title'=>"Please insert numaric value")); ?>
                    </div><!--form-group --> 
                    <div class="form-group">
                        <label>Credit Card Expire Date </label>
                        <?php echo $this->Form->input('creditcard_expdate', array( 'label' => false, 'div' => false)); ?>
                    </div><!--form-group -->
                </div>



                <div class="form-group clearfix">
                    <input  type="submit" value="Save" class="cstmbuttons">
                </div><!--form-group -->

            </div><!--rowgroup1 -->
            </form>
        </div><!--addproduct -->
    </div> <!--col-lg-9 -->
</div>
