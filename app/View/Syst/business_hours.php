<div class="contentdiv">
    <div style="margin-top: 5px;">
        <?php echo $this->Session->flash(); ?>
    </div>
    <?php echo $this->element('sidebar'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="statusbar col-lg-12">
            <span class="pagetitle">Business Hours (Store Open & Close Time)</span>
        </div><!--status bar -->

        <?php
        if(!empty($row)){
            $getOpenTime = unserialize($row['BusinessHours']['open_time']);
            $getOpenTimeAmPm = unserialize($row['BusinessHours']['open_ampm']);
            $getCloseTime = unserialize($row['BusinessHours']['close_time']);
            $getCloseTimeAmPm = unserialize($row['BusinessHours']['close_ampm']);
            
        }
        $oC = array('AM' => 'AM', 'PM' => 'PM');
        //$rng = range(0,12);
        $starttime = '00:00';
        $time = new DateTime($starttime);
        $interval = new DateInterval('PT30M');
        $temptime = $time->format('H:i');
        $html = '';
        do {
            if($temptime < "12:00"){
         
              $html[$temptime] = $temptime;
         
            }
            $time->add($interval);
            $temptime = $time->format('H:i');
        } while ($temptime !== $starttime);
        
        $weeks = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
       
        
        
        ?>
        
        
        <div class="addproduct formwrap">
            <?php echo $this->Form->create('Syst', array('class'=>'smart-form')); ?>
            <div class="rowgroup rowgroup1">
<?php for($x=0;$x <= 6;$x++){ ?>
                
                    
                <div class="col-lg-12 " >
                      <div class="form-group" style="width: 15%;padding-top: 10px;">
                         <label><?php echo $weeks[$x]; ?></label>
                    </div><!--form-group -->
                     <div class="form-group" style="width: 25%;">
                         <select name="data[BusinessHours][open_time][]" required="required" >
                            <option value="">Select Open Time</option>
                            <?php foreach($html as $ot): ?>
                            <option value="<?php echo $ot; ?>" <?php echo (!empty($getOpenTime[$x]) AND $ot==$getOpenTime[$x])?'selected="selected"':'' ?>><?php echo $ot; ?></option>
                            <?php endforeach; ?>
                            <option value="closed" <?php echo (!empty($getCloseTime[$x]) AND $getOpenTime[$x] == 'closed')?'selected="selected"':'' ?>>Closed</option>
                        </select>
                    </div><!--form-group -->
                    <div class="form-group" style="width: 7%;">
                        <select name="data[BusinessHours][open_ampm][]" >
                            <option value="AM" <?php echo (!empty($getOpenTimeAmPm[$x]) AND $getOpenTimeAmPm[$x] == "AM")?'selected="selected"':'' ?>>AM</option>
                            <option value="PM" <?php echo (!empty($getOpenTimeAmPm[$x]) AND $getOpenTimeAmPm[$x] == "PM")?'selected="selected"':'' ?>>PM</option>
                        </select>
                    </div><!--form-group -->
                    <div class="form-group" style="width: 25%;margin-left: 30px;">
                        <select name="data[BusinessHours][close_time][]" required="required"  >
                             <option value="">Select Close Time</option>
                            <?php foreach($html as $ct): ?>
                            <option value="<?php echo $ct; ?>" <?php echo (!empty($getCloseTime[$x]) AND $ct==$getCloseTime[$x])?'selected="selected"':'' ?>><?php echo $ct; ?></option>
                            <?php endforeach; ?>
                            <option value="closed" <?php echo (!empty($getCloseTime[$x]) AND $getCloseTime[$x] == 'closed')?'selected="selected"':'' ?>>Closed</option>
                        </select>
                    </div><!--form-group -->
                    <div class="form-group" style="width: 7%;">
                        <select name="data[BusinessHours][close_ampm][]" >
                            <option value="PM" <?php echo (!empty($getCloseTimeAmPm[$x]) AND $getCloseTimeAmPm[$x] == "PM")?'selected="selected"':'' ?>>PM</option>
                            <option value="AM" <?php echo (!empty($getCloseTimeAmPm[$x]) AND $getCloseTimeAmPm[$x] == "AM")?'selected="selected"':'' ?>>AM</option>
                        </select>
                    </div><!--form-group -->
                </div>
                
<?php } ?>


                <div class="form-group clearfix">
                    <input  type="submit" value="Submit" class="cstmbuttons">
                </div><!--form-group -->

            </div><!--rowgroup1 -->
            </form>
        </div><!--addproduct -->
    </div> <!--col-lg-9 -->
</div>


