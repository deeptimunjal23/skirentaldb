<script>
$(document).on('ready',function(){
$("#UserBPhone").mask("(999) 999-9999");
$("#UserBFax").mask("(999) 999-9999");
$("#UserTelephone").mask("(999) 999-9999");

});
</script>
<div class="contentdiv">
    <div style="margin-top: 5px;">
        <?php echo $this->Session->flash(); ?>
    </div>
    <?php echo $this->element('sidebar'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="statusbar col-lg-12">
            <span class="pagetitle">Edit Profile</span>
        </div><!--status bar -->


        <div class="addproduct formwrap">

            <?php echo $this->Form->create('User', array('class' => 'smart-form','name'=>'the_form', 'enctype' => 'multipart/form-data')); ?>


            <div class="rowgroup rowgroup1">


                <?php if ($this->Session->read('fe.type') == 2) { ?>
                    <div class="row">
                        <h3 style="margin-top: -4px;float: left;">Business Profile Information </h3>

                    </div><!--status bar -->
                    <div class="row">
                        <div class="form-group">
                            <label>Business Name</label>
                            <?php echo $this->Form->input('b_name', array('label' => false, 'div' => false)); ?>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business Email</label>
                            <?php echo $this->Form->input('b_email', array('label' => false, 'div' => false)); ?>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business State</label>
                            <select  size="1" id="UserStateb" whereDiv="hereCity2" required="required" name="data[User][b_state]">
                                <option selected value="">Select State</option>
                                <?php
                                $optns = array();
                                foreach ($this->App->getStateList() as $st):

                                    if ($st['states']['state_code'] == $row['User']['b_state']) {
                                        ?>
                                        <option selected="selected" value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state']; ?></option>
                                    <?php } else { ?>
                                        <option  value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state']; ?></option> 
                                    <?php } ?>
                                <?php endforeach; ?>
                            </select>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business City</label>
                            <select id="myCities2"  name="data[User][b_city]" required="required">
                                <option>Select City</option>
                            </select>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business Street Address</label>
                            <?php echo $this->Form->input('b_address', array('label' => false, 'div' => false)); ?>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business Zip / Postal Code</label>
                            <?php echo $this->Form->input('b_postalcode', array('label' => false, 'div' => false)); ?>
                        </div><!--form-group -->





                        <div class="rowgroup rowgroup3">
                            <div class="form-group">
                                <label>Rental Phone Line</label>
                                <?php echo $this->Form->input('b_phone', array('label' => false, 'div' => false, 'titl