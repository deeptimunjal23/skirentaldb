<script>
$(document).on('ready',function(){
$("#UserBPhone").mask("(999) 999-9999");
$("#UserBFax").mask("(999) 999-9999");
$("#UserTelephone").mask("(999) 999-9999");

});
</script>
<div class="contentdiv">
    <div style="margin-top: 5px;">
        <?php echo $this->Session->flash(); ?>
    </div>
    <?php echo $this->element('sidebar'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="statusbar col-lg-12">
            <span class="pagetitle">Edit Profile</span>
        </div><!--status bar -->


        <div class="addproduct formwrap">

            <?php echo $this->Form->create('User', array('class' => 'smart-form','name'=>'the_form', 'enctype' => 'multipart/form-data')); ?>


            <div class="rowgroup rowgroup1">


                <?php if ($this->Session->read('fe.type') == 2) { ?>
                    <div class="row">
                        <h3 style="margin-top: -4px;float: left;">Business Profile Information </h3>

                    </div><!--status bar -->
                    <div class="row">
                        <div class="form-group">
                            <label>Business Name</label>
                            <?php echo $this->Form->input('b_name', array('label' => false, 'div' => false)); ?>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business Email</label>
                            <?php echo $this->Form->input('b_email', array('label' => false, 'div' => false)); ?>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business State</label>
                            <select  size="1" id="UserStateb" whereDiv="hereCity3" required="required" name="data[User][b_state]">
                                <option selected value="">Select State</option>
                                <?php
                                $optns = array();
                                foreach ($this->App->getStateList() as $st):

                                    if ($st['states']['state_code'] == $row['User']['b_state']) {
                                        ?>
                                        <option selected="selected" value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state']; ?></option>
                                    <?php } else { ?>
                                        <option  value="<?php echo $st['states']['state_code']; ?>"><?php echo $st['states']['state']; ?></option> 
                                    <?php } ?>
                                <?php endforeach; ?>
                            </select>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business City</label>
                            <select id="myCities2"  name="data[User][b_city]" required="required">
                                <option>Select City</option>
                            </select>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business Street Address</label>
                            <?php echo $this->Form->input('b_address', array('label' => false, 'div' => false)); ?>
                        </div><!--form-group -->
                        <div class="form-group">
                            <label>Business Zip / Postal Code</label>
                            <?php echo $this->Form->input('b_postalcode', array('label' => false, 'div' => false)); ?>
                        </div><!--form-group -->





                        <div class="rowgroup rowgroup3">
                            <div class="form-group">
                                <label>Rental Phone Line</label>
                                <?php echo $this->Form->input('b_phone', array('label' => false, 'div' => false, 'title' => 'Insert number value', 'placeholder' => 'e.g. 1234567890')); ?>
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Business Fax</label>
                                <?php echo $this->Form->input('b_fax', array('label' => false, 'div' => false)); ?>
                            </div><!--form-group -->

                        </div><!--rowgroup3 -->
                        
                        
                      <!--  <div class="rowgroup rowgroup3">
                            <div class="form-group">
                                <label>Latitude</label>
                                <?php echo $this->Form->input('latitude', array('label' => false, 'div' => false, 'pattern' => '[0-9]+([\.|,][0-9]+)?','title' => 'This should be a number with up to 2 decimal places.')); ?>
                            </div><!--form-group 
                            <div class="form-group">
                                <label>Longitude</label>
                                <?php echo $this->Form->input('longitude', array('label' => false, 'div' => false,'pattern' => '[0-9]+([\.|,][0-9]+)?','title' => 'This should be a number with up to 2 decimal places.')); ?>
                            </div>

                        </div>-->

                        <div class="rowgroup rowgroup4">
                            <div class="form-group">
                                <label>Business Website</label>
                                <?php echo $this->Form->input('b_website', array('label' => false, 'div' => false)); ?>
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>Owner/Manager</label>
                                <?php echo $this->Form->input('b_owner', array('label' => false, 'div' => false)); ?>
                            </div><!--form-group -->
                            <div class="form-group">
                                <label>No Of Employees</label>
                                <?php echo $this->Form->input('employees', array('label' => false, 'div' => false, 'type' => 'text')); ?>
                            </div><!--form-group -->



                            <div class="form-group">
                                <label>Shop Identifier Number</label>
                                <?php echo $this->Form->input('ssn', array('label' => false, 'div' => false, 'type' => 'text', 'readonly'=>'readonly', 'value' => substr(md5($this->Session->read('fe.userId')), 0, 15))); ?>
                            </div><!--form-group -->
                            
                        </div><!--rowgroup4 -->

  <div class="rowgroup rowgroup3">
                            <div class="form-group" style="width:44%;">
                                <label>Tax Percentage</label>
                              <?php echo $this->Form->input('tax', array('label' => false, 'div' => false, 'placeholder' => 'e.g. 4.50','pattern' => '[0-9]+([\.|,][0-9]+)?','title' => 'This should be a number with up to 2 decimal places.')); ?>
                            </div><!--form-group -->
                            <div class="form-group" style="width:45%;">
                                <label>Damage Waiver For a Day</label>
                               <div>$ <?php echo $this->Form->input('damage', array('label' => false, 'div' => false, 'placeholder' => 'e.g. 2.10','pattern' => '[0-9]+([\.|,][0-9]+)?','title' => 'This should be a number with up to 2 decimal places.','style'=>'width:96%')); ?></div>
                            </div><!--form-group -->

                        </div><!--rowgroup3 -->
                        		<div class="form-group" style="width:88%">
                                <label>About Us</label>
                                <?php echo $this->Form->input('about_store', array('label' => false, 'div' => false, 'type' => 'textarea','placeholder' => '')); ?>
                            </div>
<!--added on 9/29/2015-->
<div class="form-group" style="width:88%">
                                <label>Why You Should Choose Us</label>
                                <?php echo $this->Form->input('choose', array('label' => false, 'div' => false, 'type' => 'textarea','placeholder' => '')); ?>
                            </div>

<div class="form-group" style="width:88%">
                                <label>Meet the Team</label>
                                <?php echo $this->Form->input('meetteam', array('label' => false, 'div' => false, 'type' => 'textarea','placeholder' => '')); ?>
                            </div>
<!------------------------->
                         
                         <div class="form-group">
                                <label>Rental Reservation Email</label>
                                <?php echo $this->Form->input('rr_email', array('label' => false, 'div' => false, 'type' => 'text','placeholder' => 'Rental Reservation Email Id')); ?>
							 <?php echo $this->Form->input('full_state', array('label' => false, 'div' => false, 'type' => 'hidden','placeholder' => '')); ?>
							
                            </div><!--form-group -->
                            
                        <div class="form-group">
                              <label>&nbsp;</label>
                            <div style="font-size: 14px;"><a href="<?php echo $this->base; ?>/syst/businessHours">Set Business Hours </a></div>
							
                     
						</div>
 <div class="rowgroup rowgroup3"style="width:100%;float:left">
  <div class="form-group" style="width:44%;float:left">
                                <label>Do you offer delivery services </label>
                                <?php echo  $this->Form->input( 'delivery_service', array(
        'type' => 'select',
        'options' => array('No','Yes'),
       // 'empty' => 'choose a state',
	'label' => false,
    ));
?> 
                            </div><!--form-group -->
                            
                            <div class="form-group" style="width:43%;float:left">
                                <label>Ski Area You Service</label>
									<select id="hereCity3"  name="data[User][resort]" required="required">
										<option value="">Select Resort</option>
									</select>
                            </div><!--form-group -->
                      
                       </div>

                    </div>

<!-- 07/10/2015 -->
		
                    
				<div class="form-group clearfix" >
                    <input type="button" class="button-add btn-primary" style="width:100%;line-height: 25px;font-size: 16px;font-weight: bold" value="Add Store Not Available">
                </div><!--form-group -->
                <div class="form-group">
                    <input type="button" class="button-remove btn-danger" style="width:100%;line-height: 25px;font-size: 16px;font-weight: bold" value="Delete Store Not Available">
                </div><!--form-group -->
                <?php
                if (!empty($row['User']['store_avail_from'])) {
					 foreach (unserialize($row['User']['store_avail_from']) as $kk => $ob):
                        $aviB = unserialize($row['User']['store_avail_to']);
						$aviA = unserialize($row['User']['store_avail_from']);
                        ?>
                        <div class="box">
                            <div class="form-group">
                                <label>From</label>
								<input type="text" name="data[User][store_avail_from][]" class="datepicker1" value="<?php echo $aviA[$kk];?>" placeholder="Enter Store Not Avl From Date">


                            </div><!--form-group -->
                            <div class="form-group">
                                <label>To</label>
								<input type="text" name="data[User][store_avail_to][]" class="datepicker1" value="<?php echo $aviB[$kk];?>" placeholder="Enter Store Not Avl From Date">

							</div><!--form-group -->
                        </div>

                        <?php
						endforeach;
                } else {
                    ?>
                    <div class="box">
                        <div class="form-group">
                            <label>From</label>
							<input type="text" name="data[User][store_avail_from][]" id="datepicker1" class="datepicker1" value="" placeholder="Enter Store Not Avl From Date">
   </div><!--form-group -->
                        <div class="form-group">
                            <label>To</label>
                             <input type="text" name="data[User][store_avail_to][]" id="datepicker" class="datepicker1" value="" placeholder="Enter Store Not Avl To Date">
                            </div><!--form-group -->
                    </div>
<?php } ?>
<!--------------------------->
                    <div class="row">
                        <h3 style="margin-top: -4px;float: left;">Vendor Administrator Information  </h3>
                    </div><!--status bar -->
                <?php } ?>

                <div class="row">
                    <div class="form-group">
                        <label>First name </label>

                        <?php echo $this->Form->input('firstname', array('label' => false, 'div' => false, 'required' => 'required', 'placeholder' => 'First Name')); ?>
                    </div><!--form-group -->
                    <div class="form-group">
                        <label>Last name </label>
                        <?php echo $this->Form->input('lastname', array('label' => false, 'div' => false, 'required' => 'required', 'placeholder' => 'Last Name')); ?>
                    </div><!--form-group -->
                </div>

                <div class="row">
                    <div class="form-group">
                        <label>Email </label>
                        <?php echo $this->Form->input('email', array('label' => false, 'div' => false, 'disabled', 'placeholder' => 'Email')); ?>
                    </div><!--form-group --> 
                    <div class="form-group">
                        <label>Phone No </label>
                        <?php echo $this->Form->input('telephone', array('type' => 'text', 'label' => false, 'div' => false, 'placeholder' => 'Phone No','onblur' => 'addHyphen(this);', 'title' => 'Insert number value', 'placeholder' => 'e.g. 1234567890')); ?>
                    </div><!--form-group --> 

                </div>
                <?php if ($this->Session->read('fe.type') != 2) { ?>
                    <div class="row">
                        <div class="form-group">
                            <label>Address </label>
                            <?php echo $this->Form->input('address', array('label' => false, 'div' => false, 'placeholder' => 'Address')); ?>
                        </div><!--form-group --> 
                        <div class="form-group">
                            <label>Postal Code </label>
                            <?php echo $this->Form->input('postalcode', array('label' => false, 'div' => false, 'placeholder' => 'Postal Code')); ?>
							<?php echo $this->Form->input('full_state', array('label' => false, 'div' => false, 'type' => 'hidden','placeholder' => '')); ?>
                        </div><!--form-group -->
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label>State </label>
                            <?php
                            $optns = array();
                            foreach ($this->App->getStateList() as $st):
                                $optns[$st['states']['state_code']] = $st['states']['state'];
                            endforeach;
                            echo $this->Form->select('state', $optns, array('label' => false, 'div' => false, 'placeholder' => 'State', "empty" => "Select State", 'required' => 'required'));
                            ?><i></i> 
                        </div><!--form-group -->       
                        <div class="form-group">
                            <label>City </label>
                            <select id="myCities" name="data[User][city]" required="required">
                                <option>Select City</option>
                            </select> </div><!--form-group -->
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="form-group">
                        <label><a name ="editPicture">Edit Uploaded Profile Picture </a></label>
                        <input type="file" name="pic" id="myAvatar"> 
                        <br>Click to upload Store front profile photo <br>( Format: jpg, jpeg, png, Images Size 400 by 400 or larger.)
                    </div>
					<!-- profile image 2 -->
					<div class="form-group">
                        <label><a name ="editPicture">Edit Uploaded Profile Picture </a></label>
                        <input type="file" name="pic1" id="myAvatar1"> 
                        <br>Click to upload Store front profile photo <br>( Format: jpg, jpeg, png, Images Size 400 by 400 or larger.)
                    </div>
					<!-- profile image 3-->
					<div class="form-group">
                        <label><a name ="editPicture">Edit Uploaded Profile Picture </a></label>
                        <input type="file" name="pic2" id="myAvatar2"> 
                        <br>Click to upload Store front profile photo <br>( Format: jpg, jpeg, png, Images Size 400 by 400 or larger.)
                    </div>
                    <!--<div class="form-group" id="afterUpload">
                        <img src="<?php echo $this->webroot; ?><?php echo empty($row['User']['pic']) ? 'images/default.png' : 'img/avatars/' . $row['User']['pic']; ?>" width="100"> 
                    </div>-->
                </div>
                <div class="row">
                    <div class="form-group">
                        <label>Facebook Address </label>
                        <br>https://facebook.com/<?php 
 $getFbAdd = ! empty($row['User']['fb_address']) ? $row['User']['fb_address'] : $row['User']['fb_id'];
echo $this->Form->input('fb_address', 
array('label' => false, 'style' => 'width:200px;', 'div' => false, 'placeholder' => 'e.g. yourskishop', 'value' => $getFbAdd)); ?>
                    </div><!--form-group --> 
                    <div class="form-group">
                        <label> </label>
                        <?php //echo $this->Form->input('telephone', array('default' => $row['User']['telephone'],'type'=>'text', 'label' => false, 'div' => false, 'placeholder' => 'Phone No'));  ?>
                    </div><!--form-group --> 
                </div>



                <div class="form-group clearfix">
                    <input  type="submit" value="Save" class="cstmbuttons">
                </div><!--form-group -->

            </div><!--rowgroup1 -->
            </form>
        </div><!--addproduct -->
    </div> <!--col-lg-9 -->
</div>


<script type="text/javascript">

    $(document).ready(function () {
	/*	
		$('#UserStateb').change(function () {
			  	var full_state=$(this).find(":selected").text();
			 $("#fstate").val(full_state);
                var whereIn = $(this).attr('whereDiv');
                $.ajax({
                    dataType: 'json',
                    type: "POST",
                    data: {state: $(this).val(), resort: ''},
                    url: ajaxUrl + '/users/getResortByState', success: function (response) {
                        $("#" + whereIn).html(response.respectiveCities1);
                    }});
            })
			*/
<!----------Code for resort---->
	
	
	 getResortByState($('#UserStateb').val(), '<?php echo $row['User']['resort']; ?>');
        $('#UserStateb').change(function () {
			var full_state=$('#UserStateb').find(":selected").text();
			  $("#UserFullState").val(full_state);
            getResortByState($(this).val(), "");
        });
<!-----------end code ----->
			
			
        getSetCity($('#UserState').val(), '<?php echo $row['User']['city'] ?>');
        $('#UserState').change(function () {
			var full_state=$('#UserState').find(":selected").text();
			  $("#UserFullState").val(full_state);
            getSetCity($('#UserState').val(), "");
			
        });
        getCityByState($('#UserStateb').val(), '<?php echo $row['User']['b_city'] ?>');
        $('#UserStateb').change(function () {
			var full_state=$('#UserStateb').find(":selected").text();
			  $("#UserFullState").val(full_state);
            getCityByState($(this).val(), "");
        });
	})
    function getCityByState(val, city) {

        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {state: val, city: city},
            url: ajaxUrl + '/users/getCitiesByState', success: function (response) {
                $("#myCities2").html(response.respectiveCities);
				
            }});
    }
	<!--------code for resort------>
		 function getResortByState(val, resort) {

        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {state: val, resort: resort},
            url: ajaxUrl + '/users/getResortByState', success: function (response) {
                $("#hereCity3").html(response.respectiveCities1);
				
            }});
    }
	<!--------end code-------->

    function getSetCity(state, city) {
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: {state: state, city: city},
            url: ajaxUrl + '/users/getCitiesByState', success: function (response) {
                $("#myCities").html(response.respectiveCities);
            }});

    }
	<!------------>
	
</script>
<script src="//code.jquery.com/jquery-latest.min.js"></script>
<script src="http://malsup.github.io/jquery.form.js"></script>
<script>
    $(document).ready(function () {
    	
        function onsuccess(response, status) {
            $("#myAvatar").val('');
            $("#afterUpload").html(response);
        }

        $("#myAvatar").on('change', function () {
            var ext = $(this).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                alert('Only jpg, jpeg and png image formats are acceptable');
                $(this).val('');
            } else {
                var options = {
                    url: ajaxUrl + '/syst/uploadProfilePic',
                    success: onsuccess
                };
                $("#UserProfileForm").ajaxSubmit(options);
                return false;
            }
        });
    });
    function addHyphen(text)
    {
    		//console.log(text.name);
    		/*var t = document.forms['the_form'].elements[text.name];
     		 if (t.value.length > 0) 
     		 {
        		t.value = '1+ ('+t.value.substring(0,3)+') ' + t.value.substring(3,6) +' - '+ t.value.substring(6, t.value.length);
     		 }*/
}

</script>
<script>
$('#UserRrEmail').on('blur',function () {
                var whereIn = $(this).attr('whereIn');
                var thiVal = $(this).val();
    if(thiVal){
                $.ajax({
                    dataType: 'json',
                    type: "POST",
                    data: {email: $(this).val()},
                    url: ajaxUrl + '/users/isExistEmail', success: function (response) {

                        if (response.isEmail) {
                            //$("#" + whereIn).html("<span style='color:green'>You can use this <strong>" + thiVal + "</strong> email address.</span>");
       // $("#" + whereIn).html("<span style='color:red'>This email <del><strong>" + thiVal + "</strong></del> already exists.</span>");
                        } 
      else
      {
         $("#" + whereIn).html("");
      }
      
      /*else {
                            $("#" + whereIn).html("<span style='color:red'>This email <del><strong>" + thiVal + "</strong></del> already exists.</span>");
                        }*/
      
      
                    }});
    }
            })
</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
   <script src="//code.jquery.com/jquery-1.10.2.js"></script>
   <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script>
   // $(function() {$( "#datepicker" ).datepicker();});
   // $(function() {$( "#datepicker1" ).datepicker();});
	$(function() {$( ".datepicker1" ).datepicker();});
	$(function() {$( ".hasDatepicker" ).datepicker();});
   </script>
      <link rel="stylesheet" href="/resources/demos/style.css">
	<script src="<?php echo $this->webroot; ?>js/jquery.form.js"></script>
	<script type="text/javascript">


    $(document).on('click', '.button-add', function () {
        $('.box').last().clone().insertAfter('.box:last');
        $('.box:last input').val('');

    })

    $(document).on('click', '.button-remove', function () {
        if ($('.box').length > 1) {
            $('.box').last().closest('.box').remove();
        }
    });

</script>
