<div class="contentdiv">
    <div style="margin-top: 5px;">
<?php echo $this->Session->flash(); ?>
    </div>
    <?php echo $this->element('sidebar'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="statusbar col-lg-12">
            <span class="pagetitle">Change Password</span>
        </div><!--status bar -->


        <div class="addproduct formwrap">
            <?php echo $this->Form->create('Syst'); ?>
            <div class="rowgroup rowgroup1">
       
<div class="row">
                    <div class="form-group">
                       
                      <input type="password" name="oldPass" required="required" placeholder="Old Password">
                    </div><!--form-group -->
                </div>
                <div class="row">
                    <div class="form-group">
                        
                        <input type="password"  pattern=".{5,10}" id="norPass" required title="5 to 10 characters" name="newPass" required="required" placeholder="New Password">
                    </div><!--form-group -->
                </div>
                <div class="row">
                    <div class="form-group">
                      
                        <input type="password" id="custPass" pattern=".{5,10}" required title="5 to 10 characters" name="conPass" required="required" placeholder="Confirm Password">
                    </div><!--form-group -->
                                       <img id="conPT" src='<?php echo $this->webroot ?>img/test-pass-icon.png' style="display: none;float: left;margin-left: 15px; margin-top: 10px;">
                                       <img id="conWT" width="16" src='<?php echo $this->webroot ?>img/remove.png' style="display: none;float: left;margin-left: 15px; margin-top: 10px;">

                </div>

                <div class="form-group clearfix">
                    <input  type="submit" value="Submit" class="cstmbuttons">
                </div><!--form-group -->

            </div><!--rowgroup1 -->
            </form>
        </div><!--addproduct -->
    </div> <!--col-lg-9 -->
</div>


<script type="text/javascript">
        $('#custPass').on('keyup', function () {
                if ($(this).val() == $("#norPass").val()) {
                    $("#conPT").show("slow");
                    $("#conWT").hide("fast");
                } else {
                    $("#conWT").show("slow");
                    $("#conPT").hide("fast");
                }
            })

</script>