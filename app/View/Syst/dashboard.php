<div class="contentdiv">
    <?php echo $this->Session->flash(); ?>
    <?php 
        if(!isset($isExistBusinessHours['BusinessHours']['status']) || $isExistBusinessHours['BusinessHours']['status'] == 0 AND $this->Session->read('fe.type') == 2){
        ?> 
    <div class="alert fade in" style="background-color: #FF6347">
<button style="cursor: pointer;" class="close" data-dismiss="alert">
        �
</button>
<i class="fa-fw fa fa-check"></i>
<strong>Warning, </strong> <a href="<?php echo $this->base; ?>/syst/businessHours">Click here</a> to set your store open and close timing.</div>
   <?php    
     
    }
?>
   
    
    <?php echo $this->element('sidebar'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php if ($this->Session->read('fe.type') == 2) { ?>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">         
                <div class="row">
                    <h1 style="padding-left: 15px;padding-bottom: 15px;">Business Profile Information</h1>
                    <div class="profilinner">
                        <div class="col-xs-2 profileimage">


                            <img src="<?php echo $this->webroot; ?><?php echo empty($row['User']['pic']) ? 'images/default.png' : 'img/avatars/' . $row['User']['pic']; ?>" class="prflpic">
                      
                            <div class="btn-group editpic">
                                <a href="<?php echo $this->base; ?>/syst/profile#editPicture" ><button class="btn btn-success dropdown-toggle" type="button">Edit Picture </button></a>
                            </div><!--btngroup -->
                        </div><!--profileimg -->

                        <div class="col-xs-10 profileinfo">
                            <div>
                                <div style="font-size: 16px;font-weight: bold;width: 75%;float:left;"><?php echo!empty($row['User']['b_name']) ? $row['User']['b_name'] : 'Anonymous' ?></div>
                                <div style="width: 25%;float:right;text-align: right">
                                    <div  style="font-size: 18px;font-weight: bold;">SIC: <?php echo!empty($row['User']['ssn']) ? $row['User']['ssn'] : substr(md5($this->Session->read('fe.userId')), 0, 15); ?>
                                    </div><div class="note" style="">(Shop Identifier Code)</div></div>
                                <div style="font-size: 16px;"><?php echo!empty($row['User']['b_address']) ? $row['User']['b_address'].' , '.$row['User']['b_city'].' , '.$row['User']['b_state'].' '.$row['User']['b_postalcode']: 'Anonymous' ?></div>
                                <div style="font-size: 14px;"><strong>Business Phone:</strong> <?php echo!empty($row['User']['b_phone']) ? "+ 1 " .$row['User']['b_phone'] : 'Anonymous' ?></div>
                                <div style="font-size: 14px;"><strong>Business FAX:</strong> <?php echo!empty($row['User']['b_fax']) ? $row['User']['b_fax'] : 'Anonymous' ?></div>
                                <div style="font-size: 14px;"><strong>Contact Email:</strong> <?php echo!empty($row['User']['rr_email']) ? $row['User']['rr_email'] : 'Anonymous' ?></div>
                                <div style="font-size: 14px;"><strong>Ski Shop Owner/Manager Name:</strong> <?php echo!empty($row['User']['firstname']) ? $row['User']['firstname'] : '' ?> <?php echo!empty($row['User']['lastname']) ? $row['User']['lastname'] : '' ?></div>
                                <div style="font-size: 14px;"><strong>Business Website:</strong> <?php echo!empty($row['User']['b_website']) ? $row['User']['b_website'] : 'Anonymous' ?> - <strong>Employee Size:</strong> <?php echo!empty($row['User']['employees']) ? $row['User']['employees'] : '0' ?>
</div>
<div style="font-size: 14px;"><strong>Does Your Shop Offer Delivery Services:</strong> <?php echo ($row['User']['delivery_service']) ? 'Yes' : 'No' ?> 
                            </div>

                        </div><!--profileinfo -->

                    </div><!--profilinner -->
                    <a href="<?php echo $this->base; ?>/syst/profile" class="btn btn-primary pull-right col-xs-1" style="margin-bottom: 10px;margin-right: 15px">Edit</a>

                </div><!--row -->
            </div> <!--col-lg-12 -->
		<!-- code of store not available -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">
			<div style="font-size: 14px;"><strong>Store Not Available</strong></div>
			<!-- code for closing date of shop 06-oct-2015 -->
                <?php
                if (!empty($row['User']['store_avail_from'])) {
					 foreach (unserialize($row['User']['store_avail_from']) as $kk => $ob):
                        $aviB = unserialize($row['User']['store_avail_to']);
						$aviA = unserialize($row['User']['store_avail_from']);
                        ?>
                        <div class="box col-xs-12">
                            <div class="form-group col-xs-3">
                                <label>From:  <?php echo $aviA[$kk];?></label>
                            </div><!--form-group -->
                            <div class="form-group col-xs-3">
                                <label>To:    <?php echo $aviB[$kk];?></label>
							</div><!--form-group -->
                        </div>

                        <?php
						endforeach;
                }else{?>
				Everday Available Store.
				<?php }?>
				
			</div>
	<!-- end of the code -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 proouter">
                <div class="row">
                    <div class="proinner">
                        <div class="col-xs-12 ">
                            <div class="col-xs-3 " style="font-size: 16px">Performance Metrics / Reports: </div>
                            <div class="col-xs-3 "><strong>Total Revenue:</strong> <span style='font-size:18px;color: #ed3333'>$0</span> </div>
                            <div class="col-xs-3 "><strong>Total Ski Rental Days:</strong> <span style='font-size:18px;color: #ed3333'>0</span> </div>
                            <div class="col-xs-3 "><strong>Total SRD Clients:</strong> <span style='font-size:18px;color: #ed3333'>0</span></div> 

                        </div><!--col-lg10 -->


                    </div><!--proinner -->
                </div><!--row -->
            </div> <!--proouter -->   
            <div class="statusbar col-lg-12" style="margin-top: 0px;">
                <span class="pagetitle">Products</span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 proouter">

                <?php foreach ($result as $row): ?>
                    <div class="row">
                        <div class="proinner">
                            <div class="col-xs-2">

                                <div class="pull-left proimage">
                                    <div style="margin-bottom: 5px;"><span><img src="<?php echo $this->webroot; ?>images/<?php
                                            switch ($row['SF']['name']) {
                                                case 'Beginner': echo 'be.gif';
                                                    break;
                                                case 'Intermediate':echo 'in.gif';
                                                    break;
                                                default:echo 'ex.gif';
                                            }
                                            ?>" height="18"></span></div>
                                    <img style="border:5px solid #fff" src="<?php echo $this->webroot; ?><?php echo empty($row['PPIC']['pic']) ? 'images/nopic.png' : 'files/products/' . $row['PPIC']['pic']; ?>"> 
                                </div><!--proimg -->

                            </div><!--col-lg-4 -->

                            <div class="col-xs-7 proinfo">
                                <h1 class="protitle"><a href="#"><?php echo $row['Products']['title'] ?></a></h1>
                                <div class="prodescription">
                                    <p style="font-size: 15px;"><?php echo substr($row['Products']['description'], 0, 150) . '...' ?></p>

                                </div><!--prodescrption -->
                                <div class=" dsplnon skilevl">
                                    <div>Ski Brand: <span><?php echo!empty($row['Products']['ski_brand']) ? $row['Products']['ski_brand'] : "-----"; ?></span></div>
                                     <div>Boot Brand: <span><?php echo!empty($row['Products']['boot_brand']) ? $row['Products']['boot_brand'] : "-----"; ?></span></div>
                                        <?php
                                    if (!empty($row['Products']['other_brand']) AND ! empty($row['Products']['other_brand_avi'])) {
                                        $oResult = unserialize($row['Products']['other_brand']);
                                        $brndAvi = unserialize($row['Products']['other_brand_avi']);
                                        foreach ($oResult as $kk => $ob):
                                            if (!empty($brndAvi[$kk]) AND ! empty($ob)) {
                                                ?>
                                                <div><?php echo $ob; ?>: <span><?php echo $brndAvi[$kk]; ?></span></div>
                                            <?php
                                            };
                                        endforeach;
                                    }
                                    ?>
                                </div>

                            </div><!--profileinfo -->
                            <div class="col-xs-3 proinfo">
                                <div class="col-xs-12">
                                    <div class="pull-right"><strong>Walk In Rate:</strong> <span style="font-size: 18px;color: #2167B8">$<?php echo ($row['Products']['walk_price']) ? $row['Products']['walk_price'] : '0'; ?></span></div>
                                    <div class="pull-right"><strong>Discounted Percentage:</strong> <span style="font-size: 18px;color: #00a300"><?php echo!empty($row['Products']['discount']) ? $row['Products']['discount'] : '0%'; ?>%</span></div>
                                    <div class="pull-right"><strong>SRDB Rate:</strong> <span style="font-size: 18px;color: #2167B8">$<?php echo!empty($row['Products']['srdb_price']) ? $row['Products']['srdb_price'] : '0'; ?></span></div><div>&nbsp;</div>
                                </div> 
                                <div>
                                    <a class="btn btn-danger pull-right col-xs-5" onclick="return confirm('You want to delete this product?')" href="<?php echo $this->base ?>/products/delete/<?php echo $row['Products']['id']; ?> " style="margin-left: 3px;margin-top: 10px">Delete</a> 
                                    <a class="btn btn-primary pull-right col-xs-5"  style="margin-top: 10px" href="<?php echo $this->base ?>/products/edit/<?php echo $row['Products']['id']; ?> ">Edit</a> 

                                </div><!--btngroup -->
                            </div>

                        </div><!--proinner -->
                    </div><!--row -->
    <?php endforeach; ?>

            </div>



<?php } else { ?>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">         
                <div class="row">
                    <h1 style=" padding-left: 15px;padding-bottom: 15px;">Basic Profile Information</h1>
                    <div class="profilinner">
                        <div class="col-xs-2 profileimage">


                            <img src="<?php echo $this->webroot; ?><?php echo empty($row['User']['pic']) ? 'images/default.png' : 'img/avatars/' . $row['User']['pic']; ?>" class="prflpic">
                            <div class="btn-group editpic">
                                <a href="<?php echo $this->base; ?>/syst/profile#editPicture" ><button class="btn btn-success dropdown-toggle" type="button">Edit Picture </button></a>
                            </div><!--btngroup -->
                        </div><!--profileimg -->

                        <div class="col-xs-10 profileinfo">

                            <table class="table table1" style="border-collapse: separate; ">

                                <tbody> 
                                    <tr>
                                        <td style="border:none;">
                                            <div><strong>First Name: </strong><?php echo!empty($row['User']['firstname']) ? $row['User']['firstname'] : 'Anonymous' ?></div>
                                            <div><strong>Last Name: </strong><?php echo!empty($row['User']['lastname']) ? $row['User']['lastname'] : 'Anonymous' ?></div>
                                            <div><strong>Email: </strong><?php echo!empty($row['User']['email']) ? $row['User']['email'] : 'Anonymous' ?></div>
                                            <div><strong>Address: </strong><?php echo!empty($row['User']['address']) ? $row['User']['address'] : 'Anonymous' ?></div>
                                            <div><strong>Business Phone:</strong> <?php echo!empty($row['User']['telephone']) ? $row['User']['telephone']: 'Anonymous'; ?></div>
                                            <div><strong>City / State / Postal Code: </strong><?php echo!empty($row['User']['city']) ? $row['User']['city'] : 'Anonymous' ?> / <?php echo!empty($row['User']['state']) ? $this->App->getStateName($row['User']['state']) : 'Anonymous' ?> / <?php echo!empty($row['User']['postalcode']) ? $row['User']['postalcode'] : '-----' ?></div></td>
                                        <td style="border:none;"><a class="btn btn-primary pull-right" style="margin-bottom: 10px;margin-right: 15px;padding:12px 20px; font-size: 15px;" href="<?php echo $this->webroot;?>">RENT NOW</a></td>
                                   </tr>
                                </tbody>


                                <tbody>


                                    <tr>
                                        <th style="border-top:none">Facebook Address</th>
                                       <!-- <th style="border-top:none">Renter Info</th>-->
                                        <th style="border-top:none">Previous Package</th>
                                    </tr>

                                    <tr>
                                        <td style="padding-top:15px;">
                                            <?php 
                                            if (!empty($row['User']['fb_address']) || !empty($row['User']['fb_id'])) { ?>
                                                <a href="https://facebook.com/<?php echo!empty($row['User']['fb_address']) ? $row['User']['fb_address'] : $row['User']['fb_id']; ?>" target="_blank">https://facebook.com/<?php echo!empty($row['User']['fb_address']) ? $row['User']['fb_address'] : $row['User']['fb_id']; ?></a>
                                                <?php
                                            } else {
                                                echo '---------------';
                                            }
                                            ?>
                                        </td>
                                       <?php  if(isset($package[0]['Renters']['ability'])){ ?>
                                        <!--<td style="padding-top:15px"><a href="#" style="padding: 3px;background-color: #FF6347;color: #fff;font-weight: bold;font-size: 11px;">Other Family Renters</a></td>-->
                                       <td style="padding-top:15px"><?php if($package[0]['Renters']['ability']=='Beginner'){ ?> <img src="<?php echo $this->webroot; ?>images/be.gif"> <?php } if($package[0]['Renters']['ability']=='Intermediate'){ ?><img src="<?php echo $this->webroot; ?>images/in.gif"> <?php } if($package[0]['Renters']['ability']=='Expert'){ ?><img src="<?php echo $this->webroot; ?>images/ex.gif"><?php } ?></td>
                                       <?php } 
                                       else
                                       {?>
                                       <td style="padding-top:15px"></td>
                                       <?php }
                                       ?><!--<td style="padding-top:15px"><?php if(isset($package[0]['PCK'])&& !empty($package[0]['Renters'])){ echo $package[0]['PCK']['title']; } ?></td>-->
                                    </tr>
                                </tbody>
                            </table>
                            <div class="">

                            </div><!--btngroup -->
                        </div><!--profileinfo -->


                    </div><!--profilinner -->
                  
<a href="<?php echo $this->base; ?>/syst/profile" class="btn btn-primary pull-right" style="margin-bottom: 10px;margin-right: 15px;">Edit</a>
<!--<a href="<?php echo $this->base; ?>" class="btn btn-primary pull-right" style="margin-bottom: 10px;margin-right: 15px;">RENT NOW</a> -->
                </div><!--row -->
            </div> <!--col-lg-12 -->

           

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">
                <div class="proinner">
                  <h3 style=" padding-left: 15px;padding-bottom: 15px;">Renter Reservation</h3>
                  <table class="table table1">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Renter Name</th>
                        <th>Age</th>
                        <th>Height</th>
                        <th>Weight</th>
                        <th>Package</th>
                        <th>Gender</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if(!empty($cart_renters)){
                      foreach($cart_renters as $row): ?>
                        <tr>
                          <td><?php echo $row['Renter']['id']; ?></td>
                          <td><?php echo $row['Renter']['firstname'] . " " . $row['Renter']['lastname']; ?></td>
                          <td><?php echo $row['Renter']['age']; ?></td>
                          <td><?php echo $row['Renter']['height']; ?></td>
                          <td>$<?php echo $row['Renter']['weight']; ?></td>
                          <td><?php echo $row['PCK']['title']; ?></td>
                          <td><?php echo $row['Renter']['gender']; ?></td>
                        </tr>

                      <?php endforeach; }else{ ?>
                      <tr>
                      <td colspan='7'>No result found..</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <?php if (!empty($cart_info)) : ?>
                    <div class="row">
                      <div class="col-md-4">
                        <a class="cstmbuttons" href="<?php echo $cart_info['Cart']['url']; ?>">Edit Renter</a>
                      </div>
                      <div class="col-md-4">
                        <a class="cstmbuttons" href="<?php echo $this->base . '/carts/remove_renters'; ?>"  onclick="return confirm('Are you sure that you want to delete?')">Delete All</a>
                      </div>
                      <div class="col-md-4">
                        <a class="cstmbuttons" href="<?php echo $cart_info['Cart']['url'] . '&dash=true'; ?>">Checkout</a>
                      </div>
                    </div>
                  <?php endif; ?>
                </div>
              </div> <!--col-lg-9 -->

 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">

                <div class="row">
                    <h3 style=" padding-left: 15px;padding-bottom: 15px;">Billing Information</h3>
                    <div class="proouter">

                        <div class="col-xs-4" style="padding-bottom: 20px;">
                            <img src="<?php echo $this->webroot; ?>images/creditcard.png" title="Credit Card" alt="Credit Card">

                        </div><!--col-lg-4 -->
                        <div class="col-xs-2" style="font-size: 18px;font-weight: bold;">OR</div>
                        <div class="col-xs-4">
                            <img src="<?php echo $this->webroot; ?>images/paypal.png" title="Paypal" alt="Paypal">


                        </div><!--col-lg-4 -->

                        <div class="col-xs-2">
                            <a class="btn btn-primary pull-right" href="<?php echo $this->base ?>/syst/billingInfo" style="margin-bottom: 10px;">Edit</a> 
                        </div><!--skilevl -->


                    </div><!--proinner -->
                </div><!--row -->
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profilouter">

                <div class="row">

                    <div class="proinner">

                        <h3 style=" padding-left: 15px;padding-bottom: 15px;">Rental History</h3>
                        <table class="table table1">
                             <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Store</th>
                                    <th>Pickup Date</th>
                                    <th>Return Date</th>
                                    <th style="text-align:right">Rental Price</th>
                                    <th>Date Time</th>
                                    <th>Payment Status</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
if(!empty($orderHistory)){
 foreach($orderHistory as $row): 
 ?>
                                <tr>
                                    <td><a href="<?php echo $this->base; ?>/orders/invoice/<?php echo $row['Orders']['id']; ?>"><?php echo $row['Orders']['token'] ?></a></td>
                                    <td><?php if($row['U']['b_name']!='') { echo $row['U']['b_name'].','.$row['U']['b_postalcode'].','.$row['U']['b_phone']; } ?></td>
                                    <td><?php echo $row['OrderHistory']['picup_date'] ?></td>
                                    <td><?php echo $row['OrderHistory']['return_date'] ?></td>
                                    <td style="text-align:right">$<?php echo $row['Orders']['amount'] ?></td>
                                    <td><?php echo $row['Orders']['created'] ?></td>
                                    <td><a style="color:white;background-color:green;padding:5px;">Success</a></td>
                                </tr>

<?php endforeach; }else{ ?>
 <tr>
                                    <td colspan='7'>No result found..</td>
                                </tr>
<?php } ?>
                            </tbody>


                        </table>
                    </div>
                </div><!--row -->
            </div>

<?php } ?>


    </div> <!--col-lg-9 -->

</div><!--contentdiv -->

<style type="text/css">
    .bldtxt1{font-size:18px;}
    .prflpic{clear:both; margin-bottom:15px; display:block; max-width:156px;}
    .profilouter{background: none repeat scroll 0 0 #f1f5f8;border: 1px solid #e0ecf5;
                 clear: both;
                 content: "";
                 display: table;
                 margin-bottom: 10px;

                 width: 100%;
    }
    .profilinner{padding:0px 20px;}
    .table1 tr td{margin-bottom:10px;}
    .editpic button{width:152px;}
    hdetails5 span{color:#ed3333;}

    .proinner{background: none repeat scroll 0 0 #f1f5f8;
              border: 1px solid #e0ecf5;
              clear: both;
              content: "";
              display: table;
              margin-bottom: 10px;
              padding-top: 15px;
              padding-bottom: 10px;
              width: 100%;
    }
    .proimage img{max-width:120px; width:100%;}
    .skilevl div{font-size:12px;color:#004d60}
    .skilevl div span{color:#5fcb08;font-size: 14px;}
    .proinfo .protitle{font-size:16px; margin-top:6px;}
    .proinfo .prodescription{margin-top:10px;}
    @media screen and (max-width: 999px){
        .dsplnon{margin:0px!important;}
    }
</style>
