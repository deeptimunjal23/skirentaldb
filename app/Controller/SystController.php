<?php

App::uses('AppController', 'Controller');

class SystController extends AppController {

    public $layout = 'inner';
    public $uses = array('Products', 'Picture', 'Variation', 'User', 'Packages', 'BusinessHours','Renters');
    public $components = array('Heartbeat');


    public function index() {

        $this->set(compact('index'));
    }

    function dashboard() {
        $sess = $this->Session->read('fe');

        if (!empty($sess['logedIn']) AND $sess['type'] == 2) {
            $this->layout = 'inner';
            $this->Products->recursive = 0;
            $this->paginate = array(
                'limit' => 60,
                'conditions' => array('Products.user_id' => $this->Session->read('fe.userId'), 'Products.status' => true),
                'joins' => array(
                    array(
                        'alias' => 'S',
                        'table' => 'stores',
                        'type' => 'LEFT',
                        'conditions' => 'S.id = Products.store_id'
                    ),
                    array(
                        'alias' => 'U',
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' => 'U.id = Products.user_id'
                    ), array(
                        'alias' => 'ST',
                        'table' => 'skis_types',
                        'type' => 'LEFT',
                        'conditions' => 'ST.id = Products.skitype'
                    ), array(
                        'alias' => 'SF',
                        'table' => 'skis_fors',
                        'type' => 'LEFT',
                        'conditions' => 'SF.id = Products.skifor'
                    ),
                    array(
                        'alias' => 'PCK',
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'conditions' => 'PCK.id = Products.package'
                    ),
                    array(
                        'alias' => 'PPIC',
                        'table' => 'pictures',
                        'type' => 'LEFT',
                        'conditions' => 'PPIC.product_id = Products.id',
                    ),
                ),
                'fields' => array(
                    'ST.name',
                    'SF.name',
                    'Products.id',
                    'Products.title',
                    'Products.pole',
                    'Products.boot_brand',
                    'Products.ski_brand',
                    'Products.discount',
                    'Products.walk_price',
                    'Products.srdb_price',
                    'Products.status',
                    'Products.description',
                    'Products.other_brand',
                    'Products.other_brand_avi',
                    'U.firstname',
                    'U.lastname',
                    'U.b_name',
                    'U.b_email',
                    'U.b_fax',
                    'U.b_state',
                    'U.b_city',
                    'U.b_postalcode',
                    'U.b_address',
                    'PPIC.pic',
					'PPIC.pic1', // 03/11/2015
					'PPIC.pic2', // 03/11/2015
                    'PCK.description',
		'U.store_avail_from',
	
                // 07/10/2015
     'U.store_avail_to', // 07/10/2015	
                ),
                'order' => array('Products.id' => 'desc'),
               // 'group' => array('PPIC.product_id')
            );
            $result = $this->paginate();
        }
        $userId = $this->Session->read('fe.userId');
$orderHistory = $this->Heartbeat->historyByUserId($userId);
        /* Check is Exist Row */
        $isExistBusinessHours = $this->Heartbeat->getBusinessHoursRow($userId,'buyer');
        $row = $this->User->find('first', array(
            'conditions' => array('type !=' => '3', 'status' => true, 'id' => $userId)));
            $package = $this->Renters->find('all', array(
            'conditions' => array('Renters.user_id' => $userId),'order'=>array('Renters.id Desc'),'joins' => array(
                    array(
                        'alias' => 'PCK',
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'conditions' => 'PCK.id = Renters.package'
                    )),'limit'=>1,'fields' => array('Renters.*','PCK.*')));
		
                
			//print_r($package);    
			//$log = $this->Renters->getDataSource()->getLog(false, false);
//debug($log);   
        $this->set(compact('row', 'result','isExistBusinessHours','orderHistory','package'));

        // cart data load
        $this->loadModel('Cart');
        $cart_info = $this->Cart->findByUserId($userId);
        $cart_renters = array();
        if (!empty($cart_info)) {
            $orderId = $cart_info['Cart']['payment_id'];
            $this->Session->write('newId', $cart_info['Cart']['payment_id']);
            $this->loadModel('Renter');
            $cart_renters = $this->Renter->find('all', array(
                'conditions' => array('order_id' => $orderId, 'user_id' => $userId),
                'joins' => array(
                    array(
                        'alias' => 'PCK',
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'conditions' => 'Renter.package = PCK.id'
                    ),
                ),
                'fields' => array(
                    'PCK.title', 'Renter.*'

                ),
            ));
        }
        $this->set(compact('cart_renters', 'cart_info'));
    }

    function billingInfo() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->User->id = $this->Session->read('fe.userId');

            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Billing information has been saved', 'success');
                $this->redirect("dashboard");
            } else {
                $this->Session->setFlash('Billing information could not be saved. Please, try again.', 'error');
            }
        } else {
            $this->request->data = $this->User->read(null, $this->Session->read('fe.userId'));
        }
    }

    function businessHours() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $id = $this->Session->read('fe.userId');
            /* Check is Exist Row */
            $isExist = $this->Heartbeat->getBusinessHoursRow($id);
            if (count($isExist) > 0) {
                $this->BusinessHours->id = $isExist['BusinessHours']['id'];
            }
            if (!empty($this->request->data['BusinessHours']['open_time'])) {

                $this->request->data['BusinessHours']['vid'] = $id;
                $this->request->data['BusinessHours']['status'] = true;
                $this->request->data['BusinessHours']['open_time'] = serialize($this->request->data['BusinessHours']['open_time']);
                $this->request->data['BusinessHours']['open_ampm'] = serialize($this->request->data['BusinessHours']['open_ampm']);
                $this->request->data['BusinessHours']['close_time'] = serialize($this->request->data['BusinessHours']['close_time']);
                $this->request->data['BusinessHours']['close_ampm'] = serialize($this->request->data['BusinessHours']['close_ampm']);
            }

            if ($this->BusinessHours->save($this->request->data)) {
                $this->Session->setFlash('Business Hours has been saved', 'success');
                $this->redirect("dashboard");
            } else {
                $this->Session->setFlash('The Business Hours could not be saved. Please, try again.', 'error');
            }
        }
        $row = $this->BusinessHours->find('first', array(
            'conditions' => array('vid' => $this->Session->read('fe.userId'))
        ));
        $this->set(compact('row'));
    }

    function profile($id = NULL) {
        if ($this->request->is('post') || $this->request->is('put')) {
            $id = $this->Session->read('fe.userId');
            $this->User->id = $id;
            if (!empty($_FILES['pic']['name'])) {
                $pic = $_FILES['pic'];
                $random = substr(md5(rand()), 0, 7);
                $picName = $random . '-' . $pic['name'];
                move_uploaded_file($pic['tmp_name'], "img/avatars/$picName");
                $this->request->data['User']['pic'] = isset($picName) ? $picName : '';
            }
			if (!empty($_FILES['pic1']['name'])) {
                $pic1 = $_FILES['pic1'];
                $random = substr(md5(rand()), 0, 7);
                $picName1 = $random . '-' . $pic1['name'];
                move_uploaded_file($pic1['tmp_name'], "img/avatars/$picName1");  
                $this->request->data['User']['pic1'] = isset($picName1) ? $picName1 : '';
            }
			if (!empty($_FILES['pic2']['name'])) {
                $pic2 = $_FILES['pic2'];
                $random = substr(md5(rand()), 0, 7);
                $picName2 = $random . '-' . $pic2['name'];
                move_uploaded_file($pic2['tmp_name'], "img/avatars/$picName2");
                $this->request->data['User']['pic2'] = isset($picName2) ? $picName2 : '';
            }
			if (!empty($this->request->data['User']['store_avail_from']) AND ! empty($this->request->data['User']['store_avail_to'])) {
                    $this->request->data['User']['store_avail_from'] = !empty($this->request->data['User']['store_avail_from'])?serialize($this->request->data['User']['store_avail_from']):"";
                    $this->request->data['User']['store_avail_to'] = !empty($this->request->data['User']['store_avail_to'])?serialize($this->request->data['User']['store_avail_to']):"";
                }
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Profile has been saved', 'success');
                $this->redirect("dashboard");
            } else {
                $this->Session->setFlash('The profile could not be saved. Please, try again.', 'error');
            }
        } else {
            $this->request->data = $this->User->read(null, $this->Session->read('fe.userId'));
        }
        $row = $this->User->find('first', array(
            'conditions' => array('type !=' => '3', 'status' => true, 'id' => $this->Session->read('fe.userId'))));
        $this->set(compact('row'));
    }

    function changePassword() {
        if ($this->request->is('post')) {
            extract($_POST);
            $userId = $this->Session->read('fe.userId');
            $row = $this->User->find('first', array(
                'conditions' => array('type !=' => 3, 'status' => true, 'id' => $userId)));

            if ($row['User']['password'] == md5($oldPass)) {
                if ($newPass == $conPass) {
                    $this->User->id = $userId;
                    $this->User->saveField('password', md5($newPass));
                    $this->Session->setFlash('Your password has been changed successfuly.', 'success');
                } else {
                    $this->Session->setFlash('New password and Confirm password did not matched with each other.', 'error');
                }
            } else {
                $this->Session->setFlash('Please insert correct old password.', 'error');
            }
        }
    }

    function uploadProfilePic() {
        extract($_POST);
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            if (!empty($_FILES['pic']['name'])) {
                $pic = $_FILES['pic'];
                $random = substr(md5(rand()), 0, 7);
                $picName = $random . '-' . $pic['name'];
                move_uploaded_file($pic['tmp_name'], "img/avatars/$picName");
                $this->User->id = $this->Session->read('fe.userId');
                $this->User->saveField('pic', $picName);
                $uPicName = $this->webroot . "img/avatars/$picName";
                echo "<img src='$uPicName' width='100'>";
            }
			if (!empty($_FILES['pic1']['name'])) {
                $pic1 = $_FILES['pic1'];
                $random = substr(md5(rand()), 0, 7);
                $picName1 = $random . '-' . $pic1['name'];
                move_uploaded_file($pic1['tmp_name'], "img/avatars/$picName1");
                $this->User->id = $this->Session->read('fe.userId');
                $this->User->saveField('pic1', $picName1);
                $uPicName1 = $this->webroot . "img/avatars/$picName1";
                echo "<img src='$uPicName1' width='50'>";
            }
			if (!empty($_FILES['pic2']['name'])) {
                $pic1 = $_FILES['pic2'];
                $random = substr(md5(rand()), 0, 7);
                $picName2 = $random . '-' . $pic2['name'];
                move_uploaded_file($pic1['tmp_name'], "img/avatars/$picName2");
                $this->User->id = $this->Session->read('fe.userId');
                $this->User->saveField('pic2', $picName2);
                $uPicName2 = $this->webroot . "img/avatars/$picName2";
                echo "<img src='$uPicName2' width='50'>";
            }
        }
    }

    function beforeFilter() {
        parent::beforeFilter();
        $sess = $this->Session->read('fe');
        if (!$sess['logedIn']) {
            $this->redirect("/");
        }
    }

}
