<?php

App::uses('AppController', 'Controller');

class OrdersController extends AppController {

    public $uses = array('Products','Orders','Invoice');
   public $components = array('Heartbeat'); 
   public function beforeFilter(){
        parent::beforeFilter();
    }
    public function index() {
        $this->layout = 'admin';
        $this->set(compact('index', 'subpage', 'title_for_layout'));
    }

function history(){
$sess = $this->Session->read('fe');
        if (!$sess['logedIn']) {
            $this->redirect("/");
        }
$this->layout = 'inner';
$cond = array();
$userId = $this->Session->read('fe.userId');
$result = $this->Heartbeat->historyByUserId($userId,'buyer');
 $this->set(compact('result'));
}


function invoice($id=""){
$sess = $this->Session->read('fe');
        if (!$sess['logedIn']) {
            $this->redirect("/");
        }
if($this->Session->read('fe.type') == "1"){
$typeUser = "buyer";
}else{
$typeUser = "seller";
}
$this->layout = 'inner';
$row = $this->Invoice->find('first', array(
                'conditions' => array('order_no' => $id, $typeUser => $this->Session->read('fe.userId'))
            ));
$this->set(compact('row'));
}


function vendorHistory(){
$sess = $this->Session->read('fe');
        if (!$sess['logedIn']) {
            $this->redirect("/");
        }
$this->layout = 'inner';
$cond = array();
$userId = $this->Session->read('fe.userId');
$result = $this->Heartbeat->historyByUserId($userId,'seller');
 $this->set(compact('result'));
}

    function adminOrders() {
        
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->layout = "admin";
            $result = array();//$this->Products->find('all');
            $breadcrumb = "Orders";
            $this->set(compact('result','breadcrumb'));
        } else {
            $this->redirect('/');
        }
    }

    

    function adminDelete($id = NULL) {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->Packages->id = $id;
            if ($this->Packages->delete()) {
                $this->Session->setFlash('Package has been deleted successfuly', 'success');
                $this->redirect(array('action' => 'adminPackages'));
            }
        } else {
            $this->redirect('/');
        }
    }

    function logout() {
        $this->Session->destroy();
        $this->Session->setFlash('You have successfuly loged out', 'success');
        $this->redirect(array(
            "controller" => "admin",
            "action" => "index"));
    }

}
