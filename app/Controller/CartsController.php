<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class CartsController extends AppController {

    public $components = array(
        'PaypalWPP',
    );
    public $layout = 'search';

    public function beforeFilter(){
        parent::beforeFilter();
    }

    // payment add
    public function payment() {
        $user_id = $this->Session->read('fe.userId');

        if (empty($user_id)) {
            $this->Session->setFlash('Please login to continue shopping.');
            return $this->redirect('/');   
        }
        
        $carts = $this->Cart->find('all', array(
            'conditions' => array('Cart.user_id' => $user_id),
            'fields' => array('Cart.amount'),
            'group' => array('Cart.created')
        ));
        
        if (empty($carts)) {
            return $this->redirect(array('controller' => 'carts', 'action' => 'view'));
        }
        $total_amount = 0;
        foreach ($carts as $key => $cart) {
            $total_amount = $total_amount + $cart['Cart']['amount'];
        }

        // payment form submit
        if ($this->request->is('post') || $this->request->is('put')) {
            // if payment value change by user from browser inspect
            if ($total_amount != $this->request->data['Sale']['amount']) {
                return $this->redirect($this->referer());
            }
            $firstName = urlencode($this->request->data['Sale']['first_name']);
            $lastName = urlencode($this->request->data['Sale']['last_name']);
            $creditCardType = urlencode($this->request->data['Sale']['card_type']);
            $creditCardNumber = urlencode($this->request->data['Sale']['card_number']);
            $expDateMonth = $this->request->data['Sale']['exp']['month'];
            $padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
            $expDateYear = urlencode($this->request->data['Sale']['exp']['year']);
            $cvv2Number = urlencode($this->request->data['Sale']['cvv2']);
            $amount = urlencode($this->request->data['Sale']['amount']);
            $nvp = '&PAYMENTACTION=Sale';
            $nvp .= '&AMT='.$amount;
            $nvp .= '&CREDITCARDTYPE='.$creditCardType;
            $nvp .= '&ACCT='.$creditCardNumber;
            $nvp .= '&CVV2='.$cvv2Number;
            $nvp .= '&EXPDATE='.$padDateMonth.$expDateYear;
            $nvp .= '&FIRSTNAME='.$firstName;
            $nvp .= '&LASTNAME='.$lastName;
            $nvp .= '&COUNTRYCODE=US&CURRENCYCODE=USD';

            // pr($nvp);
            // exit;

            $response = $this->PaypalWPP->wpp_hash('DoDirectPayment', $nvp);
            // pr($response);
            // exit;
            //SuccessWithWarning
            if (($response['ACK'] == 'Success') || ($response['ACK'] == 'SuccessWithWarning')) {
                $this->Cart->deleteAll(array('Cart.user_id' => $user_id));
                return $this->redirect(array('controller' => 'payments', 'action' => 'paymentSuccess'));

            } else {
                $this->Session->setFlash('Payment Failed');
            }
            //pr($response);
        } 


        $this->set('total_amount', $total_amount); 
    }
    
    // Method for product adding on cart
    public function ajax_add() {
        $this->autoRender = false;
        $response = array();
        if (!$this->Session->check('fe.userId')) {
            $response['status'] = 0;
            $response['message'] = __('Please login to continue');
        } else {
            $this->request->data['rentar_ids'] = explode(',', $this->request->data['rentar_ids']);
            // pr($this->request->data['rentar_ids']);
            // exit;
            $user_id = $this->Session->read('fe.userId');
            if (!empty($this->request->data['rentar_ids'])) {
                foreach ($this->request->data['rentar_ids'] as $key => $renter_id) {
                    if (!$this->Cart->hasAny(array('Cart.user_id' => $user_id, 'Cart.renter_id' => $renter_id))) {
                        $this->Cart->saveAll(array('user_id' => $user_id, 'renter_id' => $renter_id, 'amount' => $this->request->data['amount']));
                    }
                }
                $response['status'] = 1;
                $response['message'] = __('Successfully added on your cart!');
            } else {
                $response['status'] = 0;
                $response['message'] = __('Renter not found to add on cart!');
            }
        }
        echo json_encode($response);
    }

    // method for cart add in new way
    public function ajax_cart_save() {
        $this->autoRender = false;
        $response = array();
        if (!$this->Session->check('fe.userId')) {
            $response['status'] = 0;
            $response['message'] = __('Please login to continue');
        } else {
            $payment_id = $this->Session->read('newId');
            $user_id = $this->Session->read('fe.userId');
            if (!empty($this->request->data['url']) && !empty($payment_id)) {
                $cart_info = $this->Cart->findByUserId($user_id);
                if (empty($cart_info)) {
                    $this->Cart->saveAll(array('user_id' => $user_id, 'payment_id' => $payment_id, 'url' => $this->request->data['url']));
                    $response['status'] = 1;
                    $response['message'] = __('Successfully added on your cart!');
                } else {
                    if ($cart_info['Cart']['payment_id'] == $payment_id) {
                        $response['status'] = 1;
                        $response['message'] = __('Successfully added on your cart!');
                    } else {
                        $response['status'] = 2;
                        $response['message'] = __('Product exist on cart!');
                    }
                }
                
            } else {
                $response['status'] = 0;
                $response['message'] = __('Renter not found to add on cart!');
            }
        }
        echo json_encode($response);
    }

    // method for view cart 
    public function view() {
        if (!$this->Session->check('fe.userId')) {
            $this->Session->setFlash('Please login to view your cart', 'error');
            $this->redirect('/');
        }
        $user_id = $this->Session->read('fe.userId');
        $this->Cart->Renter->bindModel(
            array(
                'belongsTo' => array(
                    'Product' => array(
                        'className' => 'Product',
                    ),
                    'Payment' => array(
                        'className' => 'Payment',
                        'foreignKey'   => 'order_id',
                        'associatedKey'   => 'id'
                    ),
                    'Package' => array(
                        'className' => 'Package',
                        'foreignKey'   => 'package',
                        'associatedKey'   => 'id'
                    )
                )
            )
        );
        $carts = $this->Cart->find('all', array(
            'conditions' => array('Cart.user_id' => $user_id),
            'contain' => array('Renter' => array('Payment', 'Package', 'Product')),
        ));
        // pr($carts);
        // exit;
        $this->set(compact('carts'));
    }

    // payment add
    public function card_payment() {
        $user_id = $this->Session->read('fe.userId');

        if (empty($user_id)) {
            $this->Session->setFlash('Please login to continue shopping.');
            return $this->redirect('/');   
        }

        // payment form submit
        if ($this->request->is('post') || $this->request->is('put')) {
            $firstName = urlencode($this->request->data['Sale']['first_name']);
            $lastName = urlencode($this->request->data['Sale']['last_name']);
            $creditCardType = urlencode($this->request->data['Sale']['card_type']);
            $creditCardNumber = urlencode($this->request->data['Sale']['card_number']);
            $expDateMonth = $this->request->data['Sale']['exp']['month'];
            $padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
            $expDateYear = urlencode($this->request->data['Sale']['exp']['year']);
            $cvv2Number = urlencode($this->request->data['Sale']['cvv2']);
            $amount = urlencode($this->request->data['Sale']['amount']);
            $nvp = '&PAYMENTACTION=Sale';
            $nvp .= '&AMT='.$amount;
            $nvp .= '&CREDITCARDTYPE='.$creditCardType;
            $nvp .= '&ACCT='.$creditCardNumber;
            $nvp .= '&CVV2='.$cvv2Number;
            $nvp .= '&EXPDATE='.$padDateMonth.$expDateYear;
            $nvp .= '&FIRSTNAME='.$firstName;
            $nvp .= '&LASTNAME='.$lastName;
            $nvp .= '&COUNTRYCODE=US&CURRENCYCODE=USD';

            // pr($nvp);
            // exit;

            $response = $this->PaypalWPP->wpp_hash('DoDirectPayment', $nvp);
            // pr($response);
            // exit;
            //SuccessWithWarning
            if (($response['ACK'] == 'Success') || ($response['ACK'] == 'SuccessWithWarning')) {
                return $this->redirect(array('controller' => 'payments', 'action' => 'paymentSuccess', 'card_payment' => 1));

            } else {
                $this->Session->setFlash('Payment Failed. Please fill up card details.');
                return $this->redirect($this->referer());
            }
            //pr($response);
        } 
    }

    // cart view from dashboard
    public function check() {
        $sess = $this->Session->read('fe');
        if (!$sess['logedIn']) {
            $this->redirect("/");
        }
        $this->layout = 'inner';
        $user_id = $this->Session->read('fe.userId');
        if (empty($user_id)) {
            $this->Session->setFlash('Please login to view your cart.');
            return $this->redirect($this->referer());   
        }
        $cart_info = $this->Cart->findByUserId($user_id);
        $results = array();
        if (!empty($cart_info)) {
            $orderId = $cart_info['Cart']['payment_id'];
            $this->Session->write('newId', $cart_info['Cart']['payment_id']);
            $this->loadModel('Renter');
            $results = $this->Renter->find('all', array(
                'conditions' => array('order_id' => $orderId, 'user_id' => $user_id),
                'joins' => array(
                    array(
                        'alias' => 'PCK',
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'conditions' => 'Renter.package = PCK.id'
                    ),
                ),
                'fields' => array(
                    'PCK.title', 'Renter.*'

                ),
            ));
        }
        $this->set(compact('results', 'cart_info')); 
    }

    public function remove_renters() {
        $this->autoRender = false;
        $user_id = $this->Session->read('fe.userId');
        if (empty($user_id)) {
            $this->redirect('/');
        }
        $this->Cart->deleteAll(array('Cart.user_id' => $user_id));
        $this->Session->setFlash('You have successfully deleted.', 'success');
        $this->redirect($this->referer());    
    }

}
