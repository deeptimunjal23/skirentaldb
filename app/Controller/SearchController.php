<?php

App::uses('AppController', 'Controller');

class SearchController extends AppController {

    public $layout = 'search';
    public $uses = array('Products', 'Picture', 'Variation', 'User', 'Packages', 'BusinessHours', 'Rating');
    public $components = array('Heartbeat');

    public function index() {
        $this->redirect('/');
    }

    function selectDate() {
        if ($this->request->is('Post') || $this->request->is('Put')) {
            extract($_REQUEST);
            $exp = explode(',', $setMyLoc);
			if(current($exp) == ''){
				$qs = 'state=' . trim(end($exp)) . '&fromDate=' . $fromDate . '&toDate=' . $toDate;
			}
			else{
					$qs = 'resort=' . current($exp) . '&state=' . trim(end($exp)) . '&fromDate=' . $fromDate . '&toDate=' . $toDate;
				}
				$this->redirect(array(
					'controller' => 'search',
					'action' => "selectType?$qs"
				));
        }
    }

    function selectType() {
        //using dnt delete
    }

    function selectSkifor() {
        //using dnt delete   
    }

    function selectBoot() {
        //using dnt delete
    }

    function selectPackage() {
        if ($this->request->is('Post') || $this->request->is('Put')) {
            extract($_REQUEST);
            $exp = explode(',', $setMyLoc);
			if(current($exp) == ''){
				$qs = '&state=' . trim(end($exp)) . '&fromDate=' . $fromDate . '&toDate=' . $toDate;
			}
			else{
				$qs = 'resort=' . current($exp) . '&state=' . trim(end($exp)) . '&fromDate=' . $fromDate . '&toDate=' . $toDate;
			}
			$this->redirect(array(
                'controller' => 'search',
                'action' => "selectType?$qs"
            ));
        }
		
    }

    function getProductsById($packageId = array(), $location, $seg = 'bySearch', $selectedBoots = '', $selectedSkiType = '', $selectedSkiFor = '', $selectsortby = '') {
		
        if ($seg == 'bySearch') {
			if(empty($location)){
			// remove location
				$arr = array('Products.package' => $packageId, 'sortby' => $sortby, 'Products.status' => true);
			}
			else{
				$arr = array('Products.package' => $packageId, 'U.resort' => $location, 'sortby' => $sortby, 'Products.status' => true);
			}
          //  $arr = array('Products.package' => $packageId, 'U.resort' => $location, 'sortby' => $sortby, 'Products.status' => true);
        } else {


            if (!empty($packageId)) {
                $cond = array();
                $expPack = explode(',', $packageId);
                $cond['Products.package'] = $expPack;
            }if (!empty($selectedBoots)) {
                $expBoot = explode(',', $selectedBoots);
                $cond['PCK.boot'] = $expBoot;
            } else {
                $cond['PCK.boot'] = 0;
            }if (!empty($selectedSkiType)) {
                $expSkiType = explode(',', $selectedSkiType);
                $cond['Products.skitype'] = $expSkiType;
            }if (!empty($selectedSkiFor)) {
                $expSkiFor = explode(',', $selectedSkiFor);
                $cond['Products.skifor'] = $expSkiFor;
            }if (empty($packageId) AND empty($selectedBoots) AND empty($selectedSkiType) AND empty($selectedSkiFor)) {
                if(empty($location)){
					$arr = array('Products.status' => true);
				}else{
					$arr = array('U.resort' => $location, 'Products.status' => true);
				}	
            } else {
				if(empty($location)){
					$arr = array($cond, 'Products.status' => true);
				}
				else{
					$arr = array($cond, 'U.resort' => $location, 'Products.status' => true);
				}
            }
        }


        $this->Products->recursive = 0;
        $this->paginate = array(
            'limit' => 20,
            'conditions' => $arr,
            'joins' => array(
                array(
                    'alias' => 'S',
                    'table' => 'stores',
                    'type' => 'LEFT',
                    'conditions' => 'S.id = Products.store_id'
					
                ),
                array(
                    'alias' => 'U',
                    'table' => 'users',
                    'type' => 'LEFT',
                    'conditions' => 'U.id = Products.user_id'
					
                ), array(
                    'alias' => 'ST',
                    'table' => 'skis_types',
                    'type' => 'LEFT',
                    'conditions' => 'ST.id = Products.skitype'
					
                ), array(
                    'alias' => 'SF',
                    'table' => 'skis_fors',
                    'type' => 'LEFT',
                    'conditions' => 'SF.id = Products.skifor'
					
                ),
                array(
                    'alias' => 'PCK',
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'conditions' => 'PCK.id = Products.package',
                ),
                array(
                    'alias' => 'PPIC',
                    'table' => 'pictures',
                    'type' => 'LEFT',
                    'conditions' => 'PPIC.product_id = Products.id',
					'order' => array('$sort_by' => 'asc'),
                ),
            ),
            'fields' => array(
                'ST.name',
                'SF.name',
                'Products.id',
                'U.b_name',
                'Products.pole',
                'Products.boot_brand',
                'Products.ski_brand',
                'Products.discount',
                'Products.walk_price',
                'Products.srdb_price',
                'Products.status',
                'Products.description',
                'Products.other_brand',
                'Products.other_brand_avi',
                'Products.user_id',
                'U.firstname',
                'U.lastname',
		'U.b_address',
		'U.b_postalcode',
		'U.b_city',
        'U.b_state',
		'U.pic',
                'PPIC.pic',
                'PCK.description',
				'U.store_avail_from',
				'U.store_avail_to',
				'U.resort',
            ),
            'order' => array('Products.title' => 'asc'),
			'order' => array('Products.srdb_price' => 'asc'),
            'group' => array('Products.user_id'),
			
        );
        return $this->paginate();
    }

    function stores($id = NULL) {
        extract($_REQUEST);
        $skiType = ($skiType == 1) ? 1 : 2;
        switch (strtolower($skiFor)) {
            case 'intermediate': $skiFor = 2;
                break;
            case 'beginners': $skiFor = 1;
                break;
            default: $skiFor = 3;
        }
        if (!empty($filter)) {
			if(empty($resort)){
				$results = $this->getProductsByIdWithFilter($id, "byPackage", $boot, $skiType, $skiFor, $filter);
			}
			else{
				$results = $this->getProductsByIdWithFilter($id, $resort, "byPackage", $boot, $skiType, $skiFor, $filter);            
            }
			$this->set(compact('filter'));
        } else {
			if(empty($resort)){
				$results = $this->getProductsById($id, "byPackage", $boot, $skiType, $skiFor);
			}
			else{
				$results = $this->getProductsById($id, $resort, "byPackage", $boot, $skiType, $skiFor);
			}
		}
        $this->set(compact('results', 'id'));
    }

    function getProductsByIdWithFilter($packageId = array(), $location, $seg = 'bySearch', $selectedBoots = '', $selectedSkiType = '', $selectedSkiFor = '', $filter = null) {

        if ($seg == 'bySearch') {
			if(empty($location)){
			// remove location
				$arr = array('Products.package' => $packageId, 'Products.status' => true);
			}
			else{
				$arr = array('Products.package' => $packageId, 'U.resort' => $location, 'Products.status' => true);
			}
		} else {


            if (!empty($packageId)) {
                $cond = array();
                $expPack = explode(',', $packageId);
                $cond['Products.package'] = $expPack;
            }if (!empty($selectedBoots)) {
                $expBoot = explode(',', $selectedBoots);
                $cond['PCK.boot'] = $expBoot;
            } else {
                $cond['PCK.boot'] = 0;
            }if (!empty($selectedSkiType)) {
                $expSkiType = explode(',', $selectedSkiType);
                $cond['Products.skitype'] = $expSkiType;
            }if (!empty($selectedSkiFor)) {
                $expSkiFor = explode(',', $selectedSkiFor);
                $cond['Products.skifor'] = $expSkiFor;
            }if (empty($packageId) AND empty($selectedBoots) AND empty($selectedSkiType) AND empty($selectedSkiFor)) {
				if(empty($location)){
					$arr = array('Products.status' => true);
				}
				else{
					$arr = array('U.resort' => $location, 'Products.status' => true);
				}
			} else {
				if(empty($location)){
					$arr = array($cond, 'Products.status' => true);
				}
				else{
					$arr = array($cond, 'U.resort' => $location, 'Products.status' => true);
				}
			}
        }


        $ratings_table = $this->Products->tablePrefix . 'ratings';
        $users_table = $this->Products->tablePrefix . 'users';
        $this->Products->virtualFields = array(
            'rating' => 'SELECT rate FROM ' . $ratings_table .
            ' WHERE pid = Products.id',
            'b_address' => 'SELECT b_address FROM ' . $users_table .
            ' WHERE id = Products.user_id'
        );

	 if ($filter == 1) {
            $order_array = array('Products.srdb_price desc');
        } elseif ($filter == 2) {
            $order_array = array('Products.b_address asc');
        } else {
            $order_array = array('Products.rating desc');
        }

        //pr($order_array);
        //exit;

        $this->Products->recursive = 0;
        $this->paginate = array(
            'limit' => 20,
            'conditions' => $arr,
            'joins' => array(
                array(
                    'alias' => 'S',
                    'table' => 'stores',
                    'type' => 'LEFT',
                    'conditions' => 'S.id = Products.store_id'
                ),
                array(
                    'alias' => 'U',
                    'table' => 'users',
                    'type' => 'LEFT',
                    'conditions' => 'U.id = Products.user_id'
                ), array(
                    'alias' => 'ST',
                    'table' => 'skis_types',
                    'type' => 'LEFT',
                    'conditions' => 'ST.id = Products.skitype'
                ), array(
                    'alias' => 'SF',
                    'table' => 'skis_fors',
                    'type' => 'LEFT',
                    'conditions' => 'SF.id = Products.skifor'
                ),
                array(
                    'alias' => 'PCK',
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'conditions' => 'PCK.id = Products.package'
                ),
                array(
                    'alias' => 'PPIC',
                    'table' => 'pictures',
                    'type' => 'LEFT',
                    'conditions' => 'PPIC.product_id = Products.id',
                ),
            ),
            'fields' => array(
                'ST.name',
                'SF.name',
                'Products.id',
                'U.b_name',
                'Products.pole',
                'Products.boot_brand',
                'Products.ski_brand',
                'Products.discount',
                'Products.walk_price',
                'Products.srdb_price',
                'Products.status',
                'Products.description',
                'Products.other_brand',
                'Products.other_brand_avi',
                'Products.user_id',
                'Products.rating',
                'Products.b_address',
                'U.firstname',
                'U.lastname',
        'U.b_address',
        'U.b_postalcode',
        'U.b_city',
        'U.b_state',
        'U.pic',
        'U.store_avail_from',
        'U.store_avail_to',
                'PPIC.pic',
                'PCK.description',
				'U.resort',
            ),
            'order' => $order_array,
            'group' => array('Products.user_id')
        );
 //pr($this->paginate());
 //die();
        return $this->paginate();
    }


    function getByPackages() {
        extract($_REQUEST);
        $pIds = explode(',', $skiFor);
        $selectedBoots = explode(',', $boot);
        $selectedSkiType = explode(',', $skiType);
// biplob added condition
        if (!empty($filter)) {
           // $results = $this->getProductsByIdWithFilter($id, $city, "byPackage", $boot, $skiType, $skiFor, $filter);            
            if(empty($resort)){
                $results = $this->getProductsByIdWithFilter('', "byPackage", $boot, $skiType, $skiFor, $filter);
            }
            else{
                $results = $this->getProductsByIdWithFilter('', $resort, "byPackage", $boot, $skiType, $skiFor, $filter);            
            }
            
            $this->set(compact('filter'));
            // biplob end
        } else {
          //  $results = $this->getProductsById($id, $city, "byPackage", $boot, $skiType, $skiFor);
            if(empty($resort)){
                $results = $this->getProductsById('', "byPackage", $boot, $skiType, $skiFor);
            }
            else{
                $results = $this->getProductsById('', $resort, "byPackage", $boot, $skiType, $skiFor);
            }
        }
        //$results = $this->getProductsById('', $city, "byPackage", $boot, $skiType, $skiFor);
		//$selectsortby = explode(',', $sortby);
        //$results = $this->getProductsById('', $resort, "byPackage", $boot, $skiType, $skiFor);
        $this->set(compact('results', 'pIds', 'selectedBoots', 'selectedSkiType'));
    }

    function storeDetail($id = NULL) {
        extract($_REQUEST);
        $row = $this->Heartbeat->getForStore($id);
        $caluRate = $this->countRatingById($id);
        $this->set(compact('row', 'caluRate'));
    }
function storeFeedback($id = NULL,$uid=NULL) {
        extract($_REQUEST);
        $row = $this->Heartbeat->getFeedStore($id);
		$bh = $this->Heartbeat->gethours($id);
        $caluRate = $this->countRatingById($id);
        $this->set(compact('row', 'caluRate','uid','bh'));
    }
    function getSetRating() {
        extract($_REQUEST);
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $count = $this->Rating->find('count', array(
                'conditions' => array('pid' => $id, 'user_id' => $this->Session->read('fe.userId'))
            ));
            if ($count <= 0) {
                $this->Rating->saveField('rate', $stars);
                $this->Rating->saveField('pid', $id);
                $this->Rating->saveField('user_id', $this->Session->read('fe.userId'));
                echo $this->countRatingById($id);
                echo 'Thanks!!';
            } else {
                echo $this->countRatingById($id);
                echo 'Already done.';
            }
        }
    }
function setFeedbackRating() {
        extract($_REQUEST);
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $count = $this->Rating->find('count', array(
                'conditions' => array('pid' => $id, 'user_id' => $uid)
            ));
            if ($count <= 0) {
                $this->Rating->saveField('rate', $stars);
                $this->Rating->saveField('pid', $id);
                $this->Rating->saveField('user_id', $uid);
				$this->Rating->saveField('review', $review);
               // echo $this->countRatingById($id);
                echo '   <b>Thanks for submitting your feedback.</b> ';
            } else {
               // echo $this->countRatingById($id);
                echo 'Already done.';
            }
        }
    }
    function countRatingById($id) {
        $countRate = $this->Rating->query("SELECT SUM(rate) AS totalRate FROM ratings where pid = '$id'");
        $countRow = $this->Rating->query("SELECT COUNT(*) AS totalRow FROM ratings where pid = '$id'");
        if (!empty($countRow[0][0]['totalRow'])) {
            $caluRate = ceil($countRate[0][0]['totalRate'] / $countRow[0][0]['totalRow']);
            $pic = $this->webroot . "css/rating/rate" . $caluRate . ".png";
            return "<img src='$pic'>";
        } else {
            $pic = $this->webroot . "css/rating/rate0.png";
            return "<img src='$pic'>";
        }
    }

}
