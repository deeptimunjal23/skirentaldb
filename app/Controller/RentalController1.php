<?php

App::uses('AppController', 'Controller');

class RentalController extends AppController {

    public $layout = 'search';
    public $uses = array('Products', 'Picture', 'Variation', 'User', 'Packages', 'BusinessHours', 'Rating', 'Payment', 'Renter','Setting');
    public $components = array('Heartbeat', 'Paypal');

    public function index($id = NULL) {
        $qs = $_SERVER['QUERY_STRING'];
        $sess = $this->Session->read('Ad');
        //if (!$sess['logedIn']) {
        $this->redirect("processRent/$id" . "?" . $qs);
        //}
    }

    function processRent($id = NULL) {

        if (SANDBOX_FLAG) {
            $merchantID = PP_USER_SANDBOX;  /* Use Sandbox merchant id when testing in Sandbox */
            $env = 'sandbox';
        } else {
            $merchantID = PP_USER;  /* Use Live merchant ID for production environment */
            $env = 'production';
        }
//Based on the USERACTION_FLAG assign the page
        if (USERACTION_FLAG) {
            $page = 'return.php';
        } else {
            $page = 'review.php';
        }
        $row = $this->Heartbeat->getForStore($id);
        $this->set(compact('row','merchantID','env'));
    }

    function login() {
        extract($_POST);
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $count = $this->User->find('count', array(
                'conditions' => array('type != ' => '3', 'status' => true, 'email' => $uemail, 'password' => md5($upass))
            ));
            if ($count === 1) {
                $row = $this->User->find('first', array(
                    'conditions' => array('type !=' => '3', 'status' => true, 'email' => $uemail, 'password' => md5($upass))
                ));

                $sess = array(
                    'fe.userId' => $row['User']['id'],
                    'fe.logedIn' => true,
                    'fe.email' => $row['User']['email'],
                    'fe.firstname' => $row['User']['firstname'],
                    'fe.lastname' => $row['User']['lastname'],
                    'fe.type' => $row['User']['type']
                );
                $this->Session->write($sess);

                echo json_encode(array('status' => 1));
            } else {
                echo json_encode(array('status' => 0));
            }
        }
    }





    function getStoreByCity() {
        extract($_POST);
        $this->layout = "Ajax";
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $productRow = $this->Products->read(null, $pid);
            $packageRow = $this->Packages->read(null, $productRow['Products']['package']);
            $html = "<option value=''>Select Store</option>";
            foreach ($this->Heartbeat->getStoreByCityState($state, $city) as $st):
                if ($st['Users']['id'] == $productRow['Products']['user_id']) {
                    $html .= "<option value='" . $st['Users']['id'] . "' selected>" . $st['Users']['b_name'] . "</option>";
                } else {
                    $html .= "<option value='" . $st['Users']['id'] . "'>" . $st['Users']['b_name'] . "</option>";
                }
            endforeach;

            echo json_encode(array('respectiveStores' => $html, 'proDesc' => $packageRow['Packages']['description']));
        }
    }

    function saveStore() {
        extract($_POST);
        $this->layout = "Ajax";
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $isExist = $this->Payment->find('first', array(
                'conditions' => array('product_id' => $productId, 'user_id' => $this->Session->read('fe.userId'), 'store_id' => $store)
            ));
            $productRow = $this->Products->read(null, $productId);
		$userRow = $this->User->read(null, $this->Session->read('fe.userId'));
		$fname = $userRow['User']['firstname'];
		$lname = $userRow['User']['lastname'];
                $this->Session->write('fe.makeFirstName',$fname);
                $this->Session->write('fe.makeLastName',$lname);
            /* Save info */

            $arr = array('Payment' => array(
                    'state' => $state,
                    'city' => $city,
                    'store_id' => $store,
                    'product_id' => $productId,
                    'user_id' => $this->Session->read('fe.userId'),
                    'picup_date' => $picDate,
                    'return_date' => $returnDate,
                    'q_string' => $qs,
                    'package' => $productRow['Products']['package']
            ));
            if (count($isExist) > 0) {
                $this->Payment->id = $isExist['Payment']['id'];
            }
            if ($this->Payment->save($arr)) {
                if (count($isExist) > 0) {
			$newId = $isExist['Payment']['id'];
                    $this->Session->write('newId',$newId);
                } else {
                    $newId = $this->Payment->getLastInsertId();
                    $this->Session->write('newId', $newId);
                }
                echo json_encode(array('maashaaAllah' => 'accept','newOrderId' => $newId,'fname' => $fname,'lname' => $lname));
            }
        } else {
            echo json_encode(array('maashaaAllah' => 'reject'));
        }
    }

    function saveRental() {
        extract($_POST);
        $this->layout = "Ajax";
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {

            $orderId = $this->Session->read('newId');
           $results = $this->Renter->find('all', array(
                'conditions' => array('order_id' => $orderId, 'user_id' => $this->Session->read('fe.userId')),
				'joins' => array(
                        array(
                            'alias' => 'PCK',
                            'table' => 'packages',
                            'type' => 'LEFT',
                            'conditions' => 'Renter.package = PCK.id'
                        ),                      
                    ),
				 'fields' => array(
                        'PCK.title','Renter.*'

                    ),
            ));
            $html = "<table><tr><th>#</th>
                                        <th>Renter Name</th>
                                        <th>Age</th>
                                        <th>Height</th>
                                        <th>Weight</th>
                                        <th>Package</th>
                                        <th>Gender</th>
                                        <th></th>
                                    </tr>";
            /* Save info */

            $arr = array('Renter' => array(
                    'firstname' => $fname,
                    'lastname' => $lname,
                    'gender' => $sex,
                    'height' => $height,
                    'age' => $age,
                    'weight' => $weight,
                    'ability' => $ability,
                    'boot_size' => $bootSize,
                    'product_id' => $pro_id,
                    'order_id' => $orderId,
                    'package'=>$package,
                    'user_id' => $this->Session->read('fe.userId'),
            ));
            foreach ($results as $row):
                $html .= "<tr><td>" . $row['Renter']['id'] . "</td>
                                        <td>" . $row['Renter']['firstname'] . " " . $row['Renter']['lastname'] . "</td>
                                        <td>" . $row['Renter']['age'] . "</td>
                                        <td>" . $row['Renter']['height'] . "</td>
                                        <td>" . $row['Renter']['weight'] . "</td>
                                       <td>" . $row['PCK']['title'] . "</td>
                                        <td>" . $row['Renter']['gender'] . "</td>
                                        <td><img class='renterDel' delete=" . $row['Renter']['id'] . " src='" . $this->webroot . "img/remove.png' style='cursor:pointer'></td>
                                    </tr>";

            endforeach;
            $html .= "</table>";
            if ($this->Renter->save($arr)) {

                    
                echo json_encode(array('maashaaAllah' => 'accept', 'renterList' => $html));
            } else {
                echo json_encode(array('maashaaAllah' => 'reject'));
            }
        }
    }

    function getRenterList() {
        extract($_POST);
        $this->layout = "Ajax";
        $entries="";
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $orderId = $this->Session->read('newId');
            $results = $this->Renter->find('all', array(
                'conditions' => array('order_id' => $orderId, 'user_id' => $this->Session->read('fe.userId')),
				'joins' => array(
                        array(
                            'alias' => 'PCK',
                            'table' => 'packages',
                            'type' => 'LEFT',
                            'conditions' => 'Renter.package = PCK.id'
                        ),                      
                    ),
				 'fields' => array(
                        'PCK.title','Renter.*'

                    ),
            ));
            if(!empty($results))
            {
            		$entries="done";
            }
            $html = "<table><tr><th>#</th>
                                        <th>Renter Name</th>
                                        <th>Age</th>
                                        <th>Height</th>
                                        <th>Weight</th>
                                        <th>Package</th>
                                        <th>Gender</th>
                                        <th></th>
                                    </tr>";


            foreach ($results as $row):
                $html .= "<tr><td>" . $row['Renter']['id'] . "</td>
                                        <td>" . $row['Renter']['firstname'] . " " . $row['Renter']['lastname'] . "</td>
                                        <td>" . $row['Renter']['age'] . "</td>
                                        <td>" . $row['Renter']['height'] . "</td>
                                        <td>" . $row['Renter']['weight'] . "</td>
                                        <td>" . $row['PCK']['title'] . "</td>
                                        <td>" . $row['Renter']['gender'] . "</td>
                                        <td><img onClick='deleteRenter(" . $row['Renter']['id'] . ")' class='renterDel' delete=" . $row['Renter']['id'] . " src='" . $this->webroot . "img/remove.png' style='cursor:pointer'></td>
                                    </tr>";

            endforeach;
            $html .= "</table>";
            echo json_encode(array('renterList' => $html,'entry'=>$entries));
        }
    }

    function getInvoice() {
        extract($_POST);
        $this->layout = "Ajax";
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $orderId = $this->Session->read('newId');
            $paymentRow = $this->Payment->read(null, $orderId);
            $countRow = $this->Renter->find('count', array(
                'conditions' => array('order_id' => $orderId, 'user_id' => $this->Session->read('fe.userId'))
            ));
		$settingRow = 	$this->Setting->read(null, 1);	
			
            $results = $this->Renter->find('all', array(
                'conditions' => array('Renter.order_id' => $orderId, 'Renter.user_id' => $this->Session->read('fe.userId')),
                'joins' => array(
                    array(
                        'alias' => 'Pay',
                        'table' => 'payments',
                        'type' => 'LEFT',
                        'conditions' => 'Pay.id = Renter.order_id'
                    ),
                    array(
                        'alias' => 'PCK',
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'conditions' => 'PCK.id = Renter.package'
                    ),
			
                    array(
                        'alias' => 'Products',
                        'table' => 'products',
                        'type' => 'LEFT',
                        'conditions' => 'Products.id = Renter.product_id'
                    )
                ),

                'fields' => array(
                    'Pay.picup_date',
                    'Pay.return_date',
                    'Pay.package',
                    'Renter.firstname',
                    'Renter.lastname',
                    'Renter.age',
                    'Renter.gender',
                    'PCK.title',
                    'Products.discount',
                    'Products.walk_price',
                    'Products.srdb_price',
		'Products.user_id',
			
                )
            ));


            $x = 1;
            $inTotal=0;$disc=0;$walkPrice=0;
            $bahrWalaArray = array();
            $totalDays = 0;
            if (!empty($results)) {
                $html = "";
	

                foreach ($results as $row):
if (isset($row['Pay']['picup_date']) AND ! empty($row['Pay']['picup_date'])) {
    $expF = explode(', ', $row['Pay']['picup_date']);
    $expF2 = explode('/', end($expF));
    $makeFromDate = $expF2[2] . '-' . $expF2[0] . '-' . $expF2[1];
}
if (isset($row['Pay']['return_date']) AND ! empty($row['Pay']['return_date'])) {
    $expT = explode(', ', $row['Pay']['return_date']);
    $expT2 = explode('/', end($expT));
    $makeToDate = $expT2[2] . '-' . $expT2[0] . '-' . $expT2[1];
}
                    $daysDiff = $this->howDays($makeFromDate, $makeToDate) + 1;
                    $html .= "<tr><td>" . $x . "</td>
                                        <td>" . $row['Renter']['firstname'] . " " . $row['Renter']['lastname'] . "</td>
                                        <td>" . $row['Renter']['age'] . "</td>
                                        <td>" . $daysDiff . "</td>
                                        <td>" . $row['PCK']['title'] . "</td>
                                        <td>" . ucfirst($row['Renter']['gender']) . "</td>
                                            <td style='text-align:right;'>$" . number_format($row['Products']['walk_price'],2) . "</td>
                                        <td style='text-align:right;'>$" . number_format($row['Products']['srdb_price'],2) . "</td>
                                    </tr>";
                    $bahrWalaArray['discount'] = $row['Products']['discount'];
                    $bahrWalaArray['WP'] = $row['Products']['walk_price'];
                    $bahrWalaArray['SP'] = $row['Products']['srdb_price'];
		    $bahrWalaArray['vendorId'] = $row['Products']['user_id'];
                    $walkPrice=$walkPrice+ ($row['Products']['walk_price'] * $daysDiff);
                    $inTotal =$inTotal+ ($row['Products']['srdb_price'] * $daysDiff);
                    $disc=$disc+(($row['Products']['walk_price']-$row['Products']['srdb_price'])*$daysDiff);
                    $totalDays += $daysDiff;
                    $x++;
                endforeach;
               // print_r($bahrWalaArray['SP']);
                $disperc=$disc/$walkPrice*100;
                
		$vendorRow = $this->User->read(null, $bahrWalaArray['vendorId']);
		$damageWaiverForDay = !empty($vendorRow['User']['damage'])?$vendorRow['User']['damage']:'0';
		$damageWaiver = $damageWaiverForDay * $totalDays;
                
                $aamAadmiTax = !empty($vendorRow['User']['tax'])?$vendorRow['User']['tax']:'0.00';
                $taxDiKullRakam = ($inTotal * $aamAadmiTax) / 100; //don't smile focus on your work :)
                $grandTotalWithTax = $inTotal + $taxDiKullRakam;
		$grandTotal = $grandTotalWithTax + $damageWaiver;
		
		/*Payable Amount percentage*/
	$payDoler = !empty($settingRow['Setting']['due_today'])?$settingRow['Setting']['due_today']:"0.00";
	$payableTotalAmt = $payDoler * $totalDays;
	$payableBalance = $grandTotal - $payableTotalAmt;
	

                $html .= " <tr class='subtltrow'>
                                        <td colspan='4'>&nbsp;</td>
                                        <td colspan='2' class='savedtd'>YOU SAVED " . number_format($disperc,2). "% ($" . number_format($disc, 2) . ")</td>
                                        <td colspan='1'><strong>Subtotal:</strong></td>
                                              <td colspan='1'><strong> $" . number_format($inTotal, 2) . "</strong></td>
                                    </tr>

                                    <tr class='subtltrow'>
                                        <td colspan='5'>&nbsp;</td>
                                        <td colspan='2' class='txdetail'><strong>Tax  <i>" . number_format($aamAadmiTax, 2) . "%</i></strong><br/>
<strong>Damage Waiver:</strong><br>
<strong>Grand Total:</strong> </td>
 <td style='text-align:right'>$".number_format($taxDiKullRakam, 2)."<br/>$".number_format($damageWaiver,2)."<br><u>$".number_format($grandTotal, 2)."</u></td>
                                    </tr>

<tr class='' style='border:none'>
                                        <td  style='border:none;text-align:left;' colspan='4'>**Only the amount shown in Deposit Due Today  will be charged to your card or PayPal account today!
The balance due will be collected by your chosen Ski Shop when you pickup you requipment.</td>
                                        <td style='border:none' colspan='3' class='txdetail'>
<strong style='background-color:#00FF01;padding:2px;'>Deposit Due Today** </strong><br><br/> <strong>Balance Due at Pickup: </strong></td>
                                        <td style='border:none;text-align:right'> $" .number_format($payableTotalAmt, 2) . "<br/><br>  <u>$" . number_format($payableBalance,2) . "</u> </td>
                                    </tr>";


         echo json_encode(array('inVoiceList' => $html, 'payableAmt' => number_format($payableTotalAmt, 2),'gTotal' => number_format($grandTotal, 2),'total' => $inTotal,'tax' => number_format($taxDiKullRakam, 2)));
            }else {
                echo json_encode(array('inVoiceList' => false));
            }
        }
    }

    function deleteRenterList() {
        extract($_POST);
        $this->layout = "Ajax";
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $this->Renter->id = $id;
            $this->Renter->delete();
            echo json_encode(array('status' => "oneLove"));
        }
    }

    function getProductDesc() {
        extract($_POST);
        $this->layout = "Ajax";
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $productRow = $this->Products->read(null, $pid);
            $packageRow = $this->Packages->read(null, $productRow['Products']['package']);
            echo json_encode(array('proDesc' => $packageRow['Packages']['description']));
        }
    }

    function howDays($from, $to) {
        $first_date = strtotime($from);
        $second_date = strtotime($to);
        $offset = $second_date - $first_date;
        return floor($offset / 60 / 60 / 24);
    }

    function test() {
$this->layout = "Ajax";
        $this->autoRender = false;

    $expF = explode(', ', 'Wednesday, 22/07/2015');
    $expF2 = explode('/', end($expF));
    echo $makeFromDate = $expF2[2] . '-' . $expF2[1] . '-' . $expF2[0];


    $expT = explode(', ', 'Friday, 24/07/2015');
    $expT2 = explode('/', end($expT));
   echo $makeToDate = $expT2[2] . '-' . $expT2[1] . '-' . $expT2[0];

                  echo  $daysDiff = $this->howDays($makeFromDate, $makeToDate) + 1;
        //die; // output result for all the kings
    }

    function getSetPayment() {
        //if (!empty($this->data)) {
        //build nvp string
        //use your own logic to get and set each variable
        $returnURL = "http://localhost/skirental/ok"; //Router::url(array('controller' => 'purchases', 'action' => 'paypal_return'), true);
        $cancelURL = "http://localhost/skirental/no"; //Router::url(array('controller' => 'purchases', 'action' => 'paypal_cancel'), true);
        $nvpStr = "RETURNURL=$returnURL&CANCELURL=$cancelURL"
                . "&PAYMENTREQUEST_0_CURRENCYCODE=USD"
                . "&PAYMENTREQUEST_0_AMT=10.00"
                . "&PAYMENTREQUEST_0_ITEMAMT=10.00"
                . "&AYMENTREQUEST_0_PAYMENTACTION=Sale"
                . "&L_PAYMENTREQUEST_0_ITEMCATEGORY0=Digital"
                . "&L_PAYMENTREQUEST_0_NAME0=test"
                . "&L_PAYMENTREQUEST_0_QTY0=1"
                . "&L_PAYMENTREQUEST_0_AMT0=10.00"
        ;
        //do paypal setECCheckout

        $paypal = $this->Paypal;
        if ($paypal->setExpressCheckout($nvpStr)) {
            //$paypal->setExpressCheckout($nvpStr);
            //echo $paypal->token;die;

            $result = $paypal->getPaypalUrl($paypal->token);
            //$paypal->setExpressCheckout($nvpStr)
            // echo 'Done';
            // $this->Session->setFlash('Done', 'success');
        } else {
            $this->log($paypal->errors);
            $result = false;
        }

        if (false !== $result) {
            $this->redirect($result);
        } else {
            $this->Session->setFlash('Error while connecting to PayPal, Please try again', 'error');
        }

        // }
    }

}
