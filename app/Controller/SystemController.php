<?php

App::uses('AppController', 'Controller');

class SystemController extends AppController {

    public $layout = 'admin';

    public function beforeFilter(){
        parent::beforeFilter();
    }

    public function index() {


        $this->set(compact('index'));
    }

    function profile($id = NULL) {
        $this->loadModel('User');
        $this->User->id = $this->Session->read('Ad.userId');
        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->User->save($this->request->data)) {

                $this->Session->setFlash('Profile has been saved', 'success');

                $this->redirect(array('action' => 'profile'));
            } else {

                $this->Session->setFlash('The user could not be saved. Please, try again.', 'error');
            }
        } else {

            $this->request->data = $this->User->read(null, $id);
        }

        $row = $this->User->find('first', array(
            'conditions' => array('type' => '3', 'status' => true, 'id' => $this->Session->read('Ad.userId'))));
        $breadcrumb = "Profile";
        $this->set(compact('row', 'breadcrumb'));
    }

    function changePassword() {
        $this->loadModel('User');
        if ($this->request->is('post')) {
            extract($_POST);
            $userId = $this->Session->read('Ad.userId');
            $row = $this->User->find('first', array(
                'conditions' => array('type' => '3', 'status' => true, 'id' => $userId)));

            if ($row['User']['password'] == md5($oldPass)) {
                if ($newPass == $conPass) {
                    $this->User->id = $userId;
                    $this->User->saveField('password', md5($newPass));
                    $this->Session->setFlash('Your password has been changed successfuly.', 'success');
                } else {
                    $this->Session->setFlash('New password and Confirm password did not matched with each other.', 'error');
                }
            } else {
                $this->Session->setFlash('Please insert correct old password.', 'error');
            }
        }
        $breadcrumb = "Change Password";
        $this->set(compact('breadcrumb'));
    }

   /* function beforeFilter() {
        $sess = $this->Session->read('Ad');
        if (!$sess['logedIn']) {
            $this->redirect("/");
        }
    }*/

}
