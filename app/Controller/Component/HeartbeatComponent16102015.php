<?php

class HeartbeatComponent extends Component {

    function deleteVariationsBeforeUpdate($productId, $userId) {
        $sql = "delete from variations where product_id='$productId' AND user_id = '$userId'";
        if (ClassRegistry::init('variations')->query($sql)) {
            return true;
        } else {
            return false;
        }
    }
function howDays($from, $to) {
        $first_date = strtotime($from);
        $second_date = strtotime($to);
        $offset = $second_date - $first_date;
        return floor($offset / 60 / 60 / 24);
    }
	public function getPerformanceMetrics($userId) {
        $result = ClassRegistry::init('Payments')->find('all', array(
            'conditions' => array('store_id' => $userId),
'joins' => array(
                        array(
                            'alias' => 'Orders',
                            'table' => 'orders',
                            'type' => 'LEFT',
                            'conditions' => 'Payments.id = Orders.order_id'
                        ),                      
                    ),
                    'fields' => array(
                        'Payments.picup_date','Payments.return_date','Payments.id','Orders.amount','Orders.buyer'

                    ),
		
        ));
if(!empty($result)){
$outSide = array();
$rDays = $revenue = $totalClient = '';
foreach($result as $row):
	$revenue += $row['Orders']['amount'];
	$rDays += $this->howDays($row['Payments']['picup_date'],$row['Payments']['return_date'])+1;
	$totalClient .= $row['Orders']['buyer'].',';
endforeach;  

$outSide['totalClient'] = count(array_unique(explode(',',rtrim($totalClient,','))));
$outSide['renterDays'] = $rDays;
$outSide['revenue'] = $revenue;
return $outSide;	
}
    }







	public function getLastPack($userId) {
        $row = ClassRegistry::init('Renters')->find('first', array(
            'conditions' => array('user_id' => $userId),
		'order' => array('id' => 'desc')
        ));
if(!empty($row['Renters']['order_id'])){
	$orderId = $row['Renters']['order_id'];
	$rowPay = ClassRegistry::init('Payments')->find('first', array(
            'conditions' => array('id' => $orderId),
		
        ));
	$pckId = $rowPay['Payments']['package'];

        $lastPack =  ClassRegistry::init('Packages')->find('first', array(
                    'conditions' => array('Packages.id' => $pckId),
                    'joins' => array(
                        array(
                            'alias' => 'SF',
                            'table' => 'skis_fors',
                            'type' => 'LEFT',
                            'conditions' => 'SF.id = Packages.skifor'
                        ),
                      
                    ),
                    'fields' => array(

                        'SF.name',

                    ),
                  
        ));
	return $lastPack['SF']['name'];
}

    }

    public function getStatusFlag($userId) {
        $results = ClassRegistry::init('Products')->find('all', array(
            'conditions' => array('user_id' => $userId)
        ));
    }

    public function getBusinessHoursRow($userId) {
        return ClassRegistry::init('BusinessHours')->find('first', array(
                    'conditions' => array('vid' => $userId)
        ));
    }

    public function getByFbId($fbId) {
        return ClassRegistry::init('User')->find('first', array(
                    'conditions' => array('fb_id' => $fbId)
        ));
    }

    public function getByEmail($email) {
        return ClassRegistry::init('User')->find('first', array(
                    'conditions' => array('email' => $email)
        ));
    }

    public function getCitiesByState($state) {
        return ClassRegistry::init('cities')->find('all', array(
                    'conditions' => array('state_code' => $state),
					'order' => array('cities.city ASC')
        ));
    }

    public function getStoreByCityState($state, $city) {
        return ClassRegistry::init('Users')->find('all', array(
                    'conditions' => array('b_city' => $city, 'b_state' => $state),
        ));
    }

    public function getStoreByCity($city) {
        return ClassRegistry::init('stores')->find('all', array(
                    'conditions' => array('city' => $city)
        ));
    }

    public function getPicsByRand($randNo) {
        return ClassRegistry::init('Picture')->find('all', array(
                    'conditions' => array('rand_no' => $randNo)
        ));
    }

    public function countPicsByRand($randNo) {
        return ClassRegistry::init('Picture')->find('count', array(
                    'conditions' => array('rand_no' => $randNo)
        ));
    }

    function tTables() {
        return array('packages', 'products', 'users');
    }

    function sendEmail($to, $sub, $username, $body) {
        $Email = new CakeEmail();
        $Email->from(array('info@skirentaldb.com' => 'SkiRentalDb'))
                ->to($to)
                ->subject($sub)
                ->emailFormat('html')
               ->template('fp')
                ->viewVars(array('header' => "Hello " . $username, 'body' => $body))
                ->send();
    }
	function sendVendorEmail($to, $sub, $username, $body) {
        $Email = new CakeEmail();
        $Email->from(array('info@skirentaldb.com' => 'SkiRentalDb'))
                ->to($to)
                ->subject($sub)
                ->emailFormat('html')
               ->template('fp')
                ->viewVars(array('body' => $body))
                ->send();
    }

    function getForStore($id) {
        return ClassRegistry::init('Products')->find('first', array(
                    'conditions' => array('Products.id' => $id, 'Products.status' => true),
                    'joins' => array(
                        array(
                            'alias' => 'S',
                            'table' => 'stores',
                            'type' => 'LEFT',
                            'conditions' => 'S.id = Products.store_id'
                        ),
                        array(
                            'alias' => 'U',
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' => 'U.id = Products.user_id'
                        ), array(
                            'alias' => 'ST',
                            'table' => 'skis_types',
                            'type' => 'LEFT',
                            'conditions' => 'ST.id = Products.skitype'
                        ), array(
                            'alias' => 'SF',
                            'table' => 'skis_fors',
                            'type' => 'LEFT',
                            'conditions' => 'SF.id = Products.skifor'
                        ),
                        array(
                            'alias' => 'PCK',
                            'table' => 'packages',
                            'type' => 'LEFT',
                            'conditions' => 'PCK.id = Products.package'
                        ),
                        array(
                            'alias' => 'PPIC',
                            'table' => 'pictures',
                            'type' => 'LEFT',
                            'conditions' => 'PPIC.product_id = Products.id',
                        ),
                        array(
                            'alias' => 'BH',
                            'table' => 'business_hours',
                            'type' => 'LEFT',
                            'conditions' => 'BH.vid = Products.user_id',
                        ),
                    ),
                    'fields' => array(
                        'ST.name',
                         'ST.id',
                        'SF.name',
                        'Products.id',
                        'U.b_name',
                        'U.b_address',
                        'U.b_phone',
                        'U.b_email',
                        'U.b_city',
                        'U.b_state',
                        'U.b_postalcode',
			'U.pic',
			'U.about_store',
'U.choose', //9/29/2015
'U.meetteam',//9/29/2015
                        'U.id',
                        'U.latitude',
                        'U.longitude',
                        'Products.pole',
			'Products.id',
                        'Products.boot_brand',
                        'Products.ski_brand',
                        'Products.discount',
                        'Products.walk_price',
                        'Products.srdb_price',
                        'Products.status',
                        'Products.description',
                        'Products.other_brand',
                        'Products.other_brand_avi',
			'Products.user_id',
                        'U.firstname',
                        'U.lastname',
                        'PPIC.pic',
                        'PCK.description',
                        'PCK.title',
                        'PCK.id',
                        'BH.open_time',
                        'BH.open_ampm',
                        'BH.close_time',
                        'BH.close_ampm',
                    ),
                    'order' => array('Products.title' => 'asc'),
                    'group' => array('PPIC.product_id')
        ));
    }
    
function historyByUserId($userId,$type='buyer'){
$cond = array();
if($type == 'buyer'){
$rType = "Orders.seller";
$cond['buyer'] = $userId;
}else{
$rType = "Orders.buyer";
$cond['seller'] = $userId;
}
   return ClassRegistry::init('Orders')->find('all', array(
            'conditions' => array($cond),

'joins' => array(
                        array(
                            'alias' => 'OrderHistory',
                            'table' => 'payments',
                            'type' => 'LEFT',
                            'conditions' => 'OrderHistory.id = Orders.order_id'
                        ),
                        array(
                            'alias' => 'U',
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' => "U.id = $rType"
                        ),
),
     'fields' => array(
                        'Orders.name','Orders.token','Orders.amount','Orders.created','Orders.id',
                        'OrderHistory.picup_date','OrderHistory.return_date',
                        'U.b_name','U.firstname','U.lastname','U.b_email','U.b_postalcode','U.b_phone',
                       
                       
                    ),
'order' => array('Orders.created' => 'desc'),

));
}


    function taxKarFiroti(){
        return "2.50";
    }

}
