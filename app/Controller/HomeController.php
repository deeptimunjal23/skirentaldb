<?php

App::uses('AppController', 'Controller');

class HomeController extends AppController {

    public $components = array('Heartbeat');
    public $uses = array('User');
	public function beforeFilter()
  	{
  		parent::beforeFilter();
	  $this->loadModel('Orders');
	 // echo strtotime("-1 week");
	  $orders = $this->Orders->find('all', array(
			'conditions' => array('Orders.mailsent' => '','Orders.feedbacklink != ' => '','Orders.created  < NOW() - INTERVAL 7 DAY AND NOW()')
			));
	 // print_r($orders);
	  foreach($orders as $odetail)
	  {
		  $storeid=explode('/',$odetail['Orders']['feedbacklink']);
		  $actualstid=$storeid[5];
		  $useractualid=$odetail['Orders']['buyer'];
		   $countRow = ClassRegistry::init('ratings')->query("SELECT COUNT(*) AS totalRow FROM ratings where pid = '$actualstid' AND user_id='$useractualid'");
		  if($countRow[0][0]['totalRow'] <= 0)
		  {
		  $user = $this->User->find('first', array(
			'conditions' => array('id' => $odetail['Orders']['buyer'],)
			));
		  $userEmail=$user['User']['email'];
		  $body="Hello ".$user['User']['firstname']."\r\n\r\n\r\n";
		  $body.="Please provide feedback for the store by visiting following link:\r\n\r\n";
		  $body.=$odetail['Orders']['feedbacklink'];
		  if(mail($userEmail,'Skirental Feedback Email',$body))
		  {
			  $data2=array('Orders'=>array(
				  'id'=>$odetail['Orders']['id'],
				  'mailsent'=>'sent',
				  ));
			  $this->Orders->save($data2);
		  }
		  }
	  }
  	}
    public function index() {
        $this->layout = "home";
        extract($_POST);
        if ($this->request->is('Post') || $this->request->is('Put')) {
            if (empty($setMyLoc)) {
                $this->redirect("/");
            } else {
                $exp = explode(',', $setMyLoc);
                $this->redirect(array(
                    'controller' => 'search',
                    //'action' => 'selectDate?city=' . current($exp) . '&state=' . end($exp)
					'action' => 'selectDate?resort=' . current($exp) . '&state=' . end($exp)
				));
            }
        }
    }

    function search() 
	{
        $this->layout = "ajax";
        if($this->request->is('Ajax')) 
		{
            extract($_GET);
            $result = $this->User->find('all', array(
              //  'conditions' => array('type != ' => '3', 'OR' => array('b_city LIKE' => "%". $term . "%",'full_state LIKE' => "%". $term . "%",'b_state LIKE' =>"%".  $term . "%",)),
				'conditions' => array('type != ' => '3', 'OR' => array('resort LIKE' => "%". $term . "%",'full_state LIKE' => "%". $term . "%",'b_state LIKE' =>"%".  $term . "%",)),
			//'fields'=>array('DISTINCT b_city','b_name','b_state'),
			//'group' => 'b_city',
				'group' => 'resort',
			 //  'group' => array('b_city')
            ));
            $filterData = array();
            $data = '';
            foreach ($result as $row):
               if (!empty($row['User']['b_name'])) {
             /*   $filterData['value'] = $row['User']['b_city'].', '.$row['User']['b_state'];
               // $filterData['label'] = $row['User']['b_name'];
                $filterData['desc'] = $row['User']['b_city'].', '.$row['User']['b_state'];
			*/
					$filterData['value'] = $row['User']['resort'].', '.$row['User']['b_state'];
	               $filterData['desc'] = $row['User']['resort'].', '.$row['User']['b_state'];	
			  $data .= json_encode($filterData).",";
            }
            endforeach;
            $this->set('mySearch', $data);
        }
    }


	function testCurl()
	{
		$this->layout = "ajax";
		$this->autoRender = false;
		//return "m from server";
		phpInfo();
	}

}
