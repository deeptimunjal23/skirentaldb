<?php

/* -----------------------------------------------------------------------------------------
  IdiotMinds - http://idiotminds.com
  -----------------------------------------------------------------------------------------
 */
App::uses('Controller', 'Controller');
App::import('Vendor', 'Facebook', array('file' => 'Facebook' . DS . 'facebook.php'));
App::uses('CakeEmail', 'Network/Email');

class FacebookCpsController extends AppController {

    public $name = 'FacebookCps';
    public $uses = array('User');
    public $components = array('Heartbeat');

    public function index() {
        $this->layout = false;
    }

    function login() {
        Configure::load('facebook');
        $appId = Configure::read('Facebook.appId');
        $app_secret = Configure::read('Facebook.secret');
        $facebook = new Facebook(array(
            'appId' => $appId,
            'secret' => $app_secret,
        ));
        $loginUrl = $facebook->getLoginUrl(array(
            'scope' => 'email, publish_actions, user_birthday, user_location, user_work_history, user_hometown, user_photos',
            'redirect_uri' => BASE_URL . 'facebookCps/facebookConnect',
            'display' => 'popup'
        ));
        $this->redirect($loginUrl);
    }

    function isExistFBID($fbId) {
        $this->autoRender = false;
        return $this->User->find('count', array(
                    'conditions' => array('fb_id' => $fbId)
        ));
    }

    function isExistEmail($email) {
        $this->autoRender = false;
        return $this->User->find('count', array(
                    'conditions' => array('email' => $email)
        ));
    }

    function facebookConnect() {
        Configure::load('facebook');
        $appId = Configure::read('Facebook.appId');
        $app_secret = Configure::read('Facebook.secret');

        $facebook = new Facebook(array(
            'appId' => $appId,
            'secret' => $app_secret,
        ));
        $accesstoken=$facebook->getAccessToken();
        $facebook->setAccessToken($accesstoken);
        $user = $facebook->getUser();
       
        if ($user) {
            try {
                $user_profile = $facebook->api('/me',array('fields' => 'id,email,first_name,last_name,picture')); 
                $params = array('next' => BASE_URL . 'faceboisExistEmailAndFBIDokCps/facebookLogout');
                $logout = $facebook->getLogoutUrl($params);
                /* Save and check */
                $isExistFBID = $this->isExistFBID($user_profile['id']);
                $isExistEmail = $this->isExistEmail($user_profile['email']);
                if ($isExistEmail > 0 AND $isExistFBID == 0) {
                    $row = $this->Heartbeat->getByEmail($user_profile['email']);
                    $this->User->id = $row['User']['id'];
                    $this->request->data['User']['fb_id'] = $user_profile['id'];
                    $file = 'http://graph.facebook.com/'.$user_profile['id'].'/picture?type=large';
                   $newfile = WWW_ROOT.'/img/avatars/'.$user_profile['id'].'fb.jpg';
                    if (copy($file, $newfile)) {
                        $this->request->data['User']['pic'] = $user_profile['id'].'fb.jpg';
                    }
                    if ($this->User->save($this->request->data)) {
                        $sess = array(
                            'fe.userId' => $row['User']['id'],
                            'fe.logedIn' => true,
                            'fe.email' => $row['User']['email'],
                            'fe.firstname' => $row['User']['firstname'],
                            'fe.lastname' => $row['User']['lastname'],
                            'fe.type' => $row['User']['type']
                        );
                        $this->Session->write($sess);
                        echo '<script type="text/javascript">
window.close();
</script>';
                    }
                } else if ($isExistFBID > 0 AND $isExistEmail > 0) {
                    $row = $this->Heartbeat->getByFbId($user_profile['id']);
                    $this->Session->write('logout', $logout);
                    $sess = array(
                        'fe.userId' => $row['User']['id'],
                        'fe.logedIn' => true,
                        'fe.email' => $row['User']['email'],
                        'fe.firstname' => $row['User']['firstname'],
                        'fe.lastname' => $row['User']['lastname'],
                        'fe.type' => $row['User']['type']
                    );
                    $this->Session->write($sess);  
                    echo '<script type="text/javascript">
window.close();
</script>';
                } else {
                    /* Register new entry */
                    $this->request->data['type'] = true;
                    $this->request->data['User']['status'] = true;
                    $this->request->data['User']['email'] = $user_profile['email'];
                    $this->request->data['User']['firstname'] = $user_profile['first_name'];
                    $this->request->data['User']['lastname'] = $user_profile['last_name'];
                    $this->request->data['User']['fb_id'] = $user_profile['id'];
                    $password = rand(9999999, 6);
                    $this->request->data['User']['password'] = md5($password);
			$file = 'http://graph.facebook.com/'.$user_profile['id'].'/picture?type=large';
                   $newfile = WWW_ROOT.'/img/avatars/'.$user_profile['id'].'fb.jpg';
                    if (copy($file, $newfile)) {
                        $this->request->data['User']['pic'] = $user_profile['id'].'fb.jpg';
                    }

                    if ($this->User->save($this->request->data)) {
                        $row = $this->User->read(null, $this->User->getLastInsertID());
                        $sess = array(
                            'fe.userId' => $row['User']['id'],
                            'fe.logedIn' => true,
                            'fe.email' => $row['User']['email'],
                            'fe.firstname' => $row['User']['firstname'],
                            'fe.lastname' => $row['User']['lastname'],
                            'fe.type' => $row['User']['type']
                        );
                        $this->Session->write($sess);
                        $body = "Thanks for register on our site through facebook, your password is: $password <br>We will recommend to change your password ASAP.";
                        $this->Heartbeat->sendEmail($user_profile['email'], "SkiRentalDB: Registration with facebook", $user_profile['first_name'], $body);
                        $this->Session->setFlash('Thanks for login into our site, your password has been sent in your email address.', 'success');

                        echo '<script type="text/javascript">
window.close();
</script>';
                    }
                }
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = NULL;
            }
        } else {
            $this->Session->setFlash('Opss!! There is some technical issue, please try again', 'error');
            $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
    }

    function facebookLogout() {

        $this->Session->delete('User');
        $this->Session->delete('logout');
        $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }

}
