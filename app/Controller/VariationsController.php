<?php

App::uses('AppController', 'Controller');

class VariationsController extends AppController {

    public $layout = 'inner';
    public $uses = array('VendorVariation');

    public function beforeFilter(){
        parent::beforeFilter();
    }

    public function index() {
        $result = $this->VendorVariation->find('all', array(
            'conditions' => array('user_id' => $this->Session->read('fe.userId'))
            , 'joins' => array(
                array(
                    'alias' => 'VT',
                    'table' => 'variation_types',
                    'type' => 'LEFT',
                    'conditions' => 'VT.id = VendorVariation.type'
                )
            ), 'fields' => array(
                'VendorVariation.name',
                'VendorVariation.id',
                'VendorVariation.price',
                'VendorVariation.description',
                'VT.name',
            ),
            'order' => array('VendorVariation.id' => 'desc')
        ));
        $this->set(compact('result'));
    }

    function add() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['VendorVariation']['user_id'] = $this->Session->read('fe.userId');
            if ($this->VendorVariation->save($this->request->data)) {
                $this->Session->setFlash('Variation has been saved successfuly', 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('The user could not be saved. Please, try again.', 'error');
            }
        }
    }

    function edit($id = NULL) {
        $this->VendorVariation->id = $id;
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->VendorVariation->save($this->request->data)) {
                $this->Session->setFlash('Variation has been updated successfuly', 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('The Variation could not be saved. Please, try again.', 'error');
            }
        } else {
            $this->request->data = $this->VendorVariation->read(null, $id);
        }
    }

    function delete($id = NULL) {
            $this->VendorVariation->id = $id;
            if ($this->VendorVariation->delete()) {
                $this->Session->setFlash('Variation has been deleted successfuly', 'success');
                $this->redirect(array('action' => 'index'));
            }
    }

    function beforeFilter() {
        $sess = $this->Session->read('fe');
        if (!$sess['logedIn']) {
            $this->redirect("/");
        }
    }

}
