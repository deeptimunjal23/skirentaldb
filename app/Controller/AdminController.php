<?php

App::uses('AppController', 'Controller');

class AdminController extends AppController {

    public function index() {
        $this->layout = 'login';
        $this->loadModel('User');
        extract($_POST);

        if ($_SERVER['REQUEST_METHOD'] == "POST" AND ! empty($_POST['password'])) {

            $count = $this->User->find('count', array(
                'conditions' => array('type' => '3', 'status' => true, 'email' => $email, 'password' => md5($password))
            ));
            if ($count === 1) {
                $row = $this->User->find('first', array(
                    'conditions' => array('type' => '3', 'status' => true, 'email' => $email, 'password' => md5($password))
                ));

                $sess = array(
                    'Ad.userId' => $row['User']['id'],
                    'Ad.logedIn' => true,
                    'Ad.email' => $row['User']['email'],
                    'Ad.firstname' => $row['User']['firstname'],
                    'Ad.lastname' => $row['User']['lastname'],
                    'Ad.type' => $row['User']['type']
                );
                $this->Session->write($sess);
                $this->redirect(array(
                    "controller" => "system",
                    "action" => "index"));
            } else {
                $this->Session->setFlash('Invalid credentials, please try again.', 'error');
                $this->redirect(array("controller" => "admin",
                    "action" => "index"));
            }
        }

        $this->set(compact('index'));
    }

}
