<?php

App::uses('AppController', 'Controller');

class ProductsController extends AppController {

    public $uses = array('Products', 'Picture', 'Variation', 'User', 'Packages');
    public $components = array('Heartbeat');
    
    public function beforeFilter(){
        parent::beforeFilter();
    }

    public function index() {
        $sess = $this->Session->read('fe');
        if (!empty($sess['logedIn']) AND $sess['type'] == 2) {
            $this->layout = 'inner';
            $this->Products->recursive = 0;
			$this->paginate = array(
                'limit' => 20,
                'conditions' => array('Products.user_id' => $this->Session->read('fe.userId')),
				'joins' => array(
                    array(
                        'alias' => 'S',
                        'table' => 'stores',
                        'type' => 'LEFT',
                        'conditions' => 'S.id = Products.store_id'
                    ),
                    array(
                        'alias' => 'U',
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' => 'U.id = Products.user_id'
                    ), array(
                        'alias' => 'ST',
                        'table' => 'skis_types',
                        'type' => 'LEFT',
                        'conditions' => 'ST.id = Products.skitype'
                    ), array(
                        'alias' => 'SF',
                        'table' => 'skis_fors',
                        'type' => 'LEFT',
                        'conditions' => 'SF.id = Products.skifor'
                    ),
                    array(
                        'alias' => 'PCK',
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'conditions' => 'PCK.id = Products.package'
                    ),
                    array(
                        'alias' => 'PPIC',
                        'table' => 'pictures',
                        'type' => 'LEFT',
                        'conditions' => 'PPIC.product_id = Products.id',
                    ),
                ),
				'fields' => array(
                    'ST.name',
                    'SF.name',
                    'Products.id',
                    'Products.title',
                    'Products.pole',
                    'Products.boot_brand',
                    'Products.ski_brand',
                    'Products.discount',
                    'Products.walk_price',
                    'Products.srdb_price',
                    'Products.status',
                    'Products.description',
                    'Products.other_brand',
                    'Products.other_brand_avi',
                    'U.firstname',
                    'U.lastname',
                    'PPIC.pic',
                    'PCK.description',
                ),
                'order' => array('Products.id' => 'desc'),
				//'group' => array('PPIC.product_id')
			);
           /* $this->paginate = array(
                'limit' => 20,
                'conditions' => array('Products.user_id' => $this->Session->read('fe.userId')),
                'joins' => array(
                    array(
                        'alias' => 'S',
                        'table' => 'stores',
                        'type' => 'LEFT',
                        'conditions' => 'S.id = Products.store_id'
                    ),
                    array(
                        'alias' => 'U',
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' => 'U.id = Products.user_id'
                    ), array(
                        'alias' => 'ST',
                        'table' => 'skis_types',
                        'type' => 'LEFT',
                        'conditions' => 'ST.id = Products.skitype'
                    ), array(
                        'alias' => 'SF',
                        'table' => 'skis_fors',
                        'type' => 'LEFT',
                        'conditions' => 'SF.id = Products.skifor'
                    ),
                    array(
                        'alias' => 'PCK',
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'conditions' => 'PCK.id = Products.package'
                    ),
                    array(
                        'alias' => 'PPIC',
                        'table' => 'pictures',
                        'type' => 'LEFT',
                        'conditions' => 'PPIC.product_id = Products.id',
                    ),
                ),
                'fields' => array(
                    'ST.name',
                    'SF.name',
                    'Products.id',
                    'Products.title',
                    'Products.pole',
                    'Products.boot_brand',
                    'Products.ski_brand',
                    'Products.discount',
                    'Products.walk_price',
                    'Products.srdb_price',
                    'Products.status',
                    'Products.description',
                    'Products.other_brand',
                    'Products.other_brand_avi',
                    'U.firstname',
                    'U.lastname',
                    'PPIC.pic',
                    'PCK.description',
                ),
                'order' => array('Products.id' => 'desc'),
                'group' => array('PPIC.product_id')
            );*/
            $result = $this->paginate();
            $this->set(compact('result'));
        } else {
            $this->redirect("/");
        }
    }

    function imageUpload() {
        $this->autoRender = false;
        if (!empty($_FILES['photoimg']['name'])) {
            $html = "";
            if ($this->Heartbeat->countPicsByRand($_REQUEST['randNo']) < 4) {
                $pic = $_FILES['photoimg'];
                $random = substr(md5(rand()), 0, 7);
                $picName = $random . '-' . $pic['name'];
                move_uploaded_file($pic['tmp_name'], "files/products/$picName");
                $this->Picture->saveField('pic', $picName);
                $this->Picture->saveField('user_id', $this->Session->read('fe.userId'));
                $this->Picture->saveField('rand_no', $_REQUEST['randNo']);
                $this->Picture->saveField('product_id', !empty($_REQUEST['productId']) ? $_REQUEST['productId'] : 0);
                $pics = $this->Heartbeat->getPicsByRand($_REQUEST['randNo']);
            }
            foreach ($pics as $row):
                $picId = $row['Picture']['id'];
                $html .= "<span id='mypic" . $row['Picture']['id'] . "' style='width: 110px; float: left; position: relative;cursor:pointer'><img onclick='javascript: getDelImg($picId)' src ='" . $this->webroot . "img/remove.png' style='z-index: 222222; position: absolute; top: -8px; right: 4px;' width='16'><img src='" . $this->webroot . 'files/products/' . $row['Picture']['pic'] . "' width='100' style='padding: 2px; margin-right: 2px; border: 1px solid;' class='hereCount'></span>";
            endforeach;
            echo $html;
        }
    }

    function productImageDelete() {
        extract($_REQUEST);
        if (!empty($id)) {
            $this->Picture->id = $id;
            $row = $this->Picture->read(null, $id);
            unlink(WWW_ROOT . "/files/products/" . $row['Picture']['pic']);
            $this->Picture->delete();
            return true;
        }
    }

    function add() {
        $sess = $this->Session->read('fe');
        if (!empty($sess['logedIn']) AND $sess['type'] == 2) {
            $this->layout = 'inner';
            if ($this->request->is('post') || $this->request->is('put')) {
                extract($_REQUEST);
                if (!empty($this->request->data['Products']['other_brand']) AND ! empty($this->request->data['Products']['other_brand_avi'])) {
                    $this->request->data['Products']['other_brand'] = serialize($this->request->data['Products']['other_brand']);
                    $this->request->data['Products']['other_brand_avi'] = serialize($this->request->data['Products']['other_brand_avi']);
                }
                $this->request->data['Products']['user_id'] = $this->Session->read('fe.userId');
                   $this->request->data['Products']['walk_price']=preg_replace('/&.*?;/', '', $this->request->data['Products']['walk_price']);
                 $this->request->data['Products']['srdb_price']=preg_replace('/&.*?;/', '', $this->request->data['Products']['srdb_price']);
                /* get User Row */
                $urow = $this->User->read(null, $this->Session->read('fe.userId'));
                $this->request->data['Products']['city'] = !empty($urow['b_User']['city']) ? $urow['b_User']['city'] : "";
                $this->request->data['Products']['state'] = !empty($urow['b_User']['state']) ? $urow['b_User']['state'] : "";
                if ($this->Products->save($this->request->data)) {
                    /* Ad variations */
                    if (!empty($variations)) {
                        foreach ($variations as $var):
                            $sql = "insert into variations (vid,user_id,product_id) values('$var','" . $this->Session->read('fe.userId') . "','" . $this->Products->id . "')";
                            $this->Variation->query($sql);
                        endforeach;
                    }
                    if ($this->Heartbeat->countPicsByRand($this->request->data['Products']['rand_no']) > 0) {
                        $sql = "update pictures set product_id='" . $this->Products->id . "' where rand_no='" . $this->request->data['Products']['rand_no'] . "'";
                        $this->Picture->query($sql);
                    }
                    $this->Session->setFlash('Product has been saved successfuly', 'success');
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash('The product could not be saved. Please, try again.', 'error');
                }
            }
        } else {
            $this->redirect("/");
        }
    }

    function edit($id = NULL) {
        $sess = $this->Session->read('fe');
        if (!empty($sess['logedIn']) AND $sess['type'] == 2) {
            $this->layout = 'inner';
            if ($this->request->is('post') || $this->request->is('put')) {
                extract($_REQUEST);
                $this->request->data['Products']['user_id'] = $this->Session->read('fe.userId');
                $this->request->data['Products']['walk_price']=str_replace('$','',$this->request->data['Products']['walk_price']);
                 $this->request->data['Products']['srdb_price']=str_replace('$','',$this->request->data['Products']['srdb_price']);
                $this->Products->id = $id;
                if (!empty($this->request->data['Products']['other_brand']) AND ! empty($this->request->data['Products']['other_brand_avi'])) {
                    $this->request->data['Products']['other_brand'] = !empty($this->request->data['Products']['other_brand'])?serialize($this->request->data['Products']['other_brand']):"";
                    $this->request->data['Products']['other_brand_avi'] = !empty($this->request->data['Products']['other_brand_avi'])?serialize($this->request->data['Products']['other_brand_avi']):"";
                }
                if ($this->Products->save($this->request->data)) {
                    /* Ad variations */
                    if (!empty($variations)) {
                        $this->Heartbeat->deleteVariationsBeforeUpdate($id, $this->Session->read('fe.userId'));
                        foreach ($variations as $var):
                            $sql = "insert into variations (vid,user_id,product_id) values('$var','" . $this->Session->read('fe.userId') . "','" . $id . "')";
                            $this->Variation->query($sql);
                        endforeach;
                    }else {
                        $this->Heartbeat->deleteVariationsBeforeUpdate($id, $this->Session->read('fe.userId'));
                    }
                    if ($this->Heartbeat->countPicsByRand($this->request->data['Products']['rand_no']) > 0) {
                        $sql = "update pictures set product_id='" . $id . "' where rand_no='" . $this->request->data['Products']['rand_no'] . "'";
                        $this->Picture->query($sql);
                    }
                    $this->Session->setFlash('Product has been saved successfuly', 'success');
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash('The user could not be saved. Please, try again.', 'error');
                }
            } else {
                $row = $this->request->data = $this->Products->read(null, $id);
            }
            $this->set(compact('row'));
        } else {
            $this->redirect("/");
        }
    }

    function adminProducts() {

        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $condition = array();
            $this->layout = "admin";
            if ($this->request->is('get')) {
                extract($_GET);
                if (!empty($title)) {
                    $condition[] = array("Products.title LIKE" => $title . "%");
                }
                if (!empty($skifor)) {
                    $condition[] = array("SF.id" => $skifor);
                }if (!empty($skitype)) {
                    $condition[] = array("ST.id" => $skitype);
                }if (!empty($state)) {
                    $condition[] = array("S.state" => $state);
                }if (!empty($city)) {
                    $condition[] = array("S.city" => $city);
                } else {
                    $city = "";
                }if (!empty($store)) {
                    $condition[] = array("Products.store_id" => $store);
                } else {
                    $store = "";
                }
            } else {
                $condition[] = array("Products.status" => "IN(0,1)");
            }
            $this->Products->recursive = 0;
            $this->paginate = array(
                'limit' => 20,
                'conditions' => $condition,
                'joins' => array(
                    array(
                        'alias' => 'S',
                        'table' => 'stores',
                        'type' => 'LEFT',
                        'conditions' => 'S.id = Products.store_id'
                    ),
                    array(
                        'alias' => 'U',
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' => 'U.id = Products.user_id'
                    ), array(
                        'alias' => 'ST',
                        'table' => 'skis_types',
                        'type' => 'LEFT',
                        'conditions' => 'ST.id = Products.skitype'
                    ), array(
                        'alias' => 'SF',
                        'table' => 'skis_fors',
                        'type' => 'LEFT',
                        'conditions' => 'SF.id = Products.skifor'
                    ),
                ),
                'fields' => array(
                    'SF.name',
                    'ST.name',
                    'Products.id',
                    'Products.title',
                    'Products.status',
                    'U.firstname',
                    'U.lastname',
                ),
                'order' => array('Products.id' => 'desc')
            );
            $result = $this->paginate();

            $breadcrumb = "Products";
            $this->set(compact('result', 'breadcrumb', 'city', 'store'));
        } else {
            $this->redirect('/');
        }
    }

    function delete($id) {
        $sess = $this->Session->read('fe');
        if (!empty($sess) AND ! empty($id)) {
            $ok = false;
            /* delete variations,picture and product */
            $sql = "delete from variations where product_id = '$id'";
            $this->Variation->query($sql);
            $sql3 = "select * from pictures where product_id = '$id'";
            $resultss = $this->Picture->query($sql3);
            foreach ($resultss as $pic):
                @unlink(WWW_ROOT . "/files/products/" . $pic['pictures']['pic']);
                $ok = true;
            endforeach;

            $sql2 = "delete from pictures where product_id = '$id'";
            if ($this->Picture->query($sql2)) {
                $ok = true;
            }

            $this->Products->id = $id;
            if ($this->Products->delete()) {

                $this->Session->setFlash('Product has been deleted successfuly', 'success');
                $this->redirect(array('action' => 'index'));
            }
        } else {
            $this->redirect('/');
        }
    }

    function changeStatus() {
        extract($_GET);
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $this->Products->id = $pid;
            $this->request->data['Products']['status'] = empty($status) ? 1 : 0;
            if ($this->Products->save($this->request->data)) {
                echo json_encode(array('changeStatus' => true));
            } else {
                echo json_encode(array('changeStatus' => false));
            }
        }
    }

    function getPackages() {
        extract($_POST);
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $cond['Packages.status'] = true;
            //$cond['Packages.boot'] = true;
            !empty($st) ? $cond['Packages.skitype'] = $st : "";
            !empty($sf) ? $cond['Packages.skifor'] = $sf : "";

            $result = $this->Packages->find('all', array(
                'conditions' => array($cond),
                'joins' => array(
                    array(
                        'alias' => 'ST',
                        'table' => 'skis_types',
                        'type' => 'LEFT',
                        'conditions' => 'ST.id = Packages.skitype'
                    ), array(
                        'alias' => 'SF',
                        'table' => 'skis_fors',
                        'type' => 'LEFT',
                        'conditions' => 'SF.id = Packages.skifor'
                    ),
                ),
                'fields' => array(
                    'SF.name',
                    'ST.name',
                    'Packages.title',
                    'Packages.pic',
                    'Packages.id',
                    'Packages.boot',
                    'Packages.description',
                    'Packages.status',
                ))
            );
            $html = "<option value=''> - Select Package - ";
            foreach ($result as $st):
                $id = $st['Packages']['id'];
                $boot = empty($st['Packages']['boot']) ? 'Without Boot' : 'With Boot';
                // $name = "" . $st['ST']['name'] . ' - ' . $st['SF']['name'] . ' - ' . $st['Packages']['title'];
               // $name = $st['SF']['name'] . ' - ' . $boot." - ".$st['Packages']['title'];
$name = $st['SF']['name'] ." - ".$st['Packages']['title'];
                if (isset($pckg) AND $pckg == $id) {
                    $html .= "<option value='$id' selected>$name</option>";
                } else {
                    $html .= "<option value='$id'>$name</option>";
                }
            endforeach;
            echo json_encode(array('html' => $html));
        }
    }

    function getPackagesDesc() {
        extract($_POST);
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $row = $urow = $this->Packages->read(null, $pid);
            $desc = $row['Packages']['description'];
            $title = $row['Packages']['title'];
            echo json_encode(array('desc' => $desc, 'title' => $title));
        }
    }
    
     function activate($pid) {
        $this->autoRender = false;
            $this->Products->id = $pid;
            $this->request->data['Products']['status'] =  1 ;
            if ($this->Products->save($this->request->data)) {
                 $this->Session->setFlash('Product Has Been Activated.', 'success');
                 
            } else {
               $this->Session->setFlash('Oops! Some Error Occurs.Please Try Again Later.', 'success');
        }
        $this->redirect(array(
            "controller" => "products",
            "action" => "index"));
    }
    
    
    function logout() {
        $this->Session->destroy();
        $this->Session->setFlash('You have successfuly loged out', 'success');
        $this->redirect(array(
            "controller" => "admin",
            "action" => "index"));
    }

}
