<?php

App::uses('AppController', 'Controller');

class PackagesController extends AppController {

    public $uses = 'Packages';

    public function beforeFilter(){
        parent::beforeFilter();
    }

    public function index() {
        $this->layout = 'admin';
        $this->set(compact('index', 'subpage', 'title_for_layout'));
    }

    function adminPackages() {

        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->layout = "admin";
            $result = $this->Packages->find('all', array('joins' => array(
                 array(
                        'alias' => 'ST',
                        'table' => 'skis_types',
                        'type' => 'LEFT',
                        'conditions' => 'ST.id = Packages.skitype'
                    ), array(
                        'alias' => 'SF',
                        'table' => 'skis_fors',
                        'type' => 'LEFT',
                        'conditions' => 'SF.id = Packages.skifor'
                    ),
               
                ),
                'fields' => array(
                    'SF.name',
                    'ST.name',
                    'Packages.title',
                    'Packages.pic',
                    'Packages.id',
                    'Packages.boot',
                    'Packages.description',
                    'Packages.status',
                ))
            );
            $breadcrumb = "Packages";
            $this->set(compact('result', 'breadcrumb'));
        } else {
            $this->redirect('/');
        }
    }

    function adminAdd() {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->layout = 'admin';
            if ($this->request->is('post') || $this->request->is('put')) {

                if (!empty($this->request->data['Packages']['pic'])) {
                    $pic = $this->request->data['Packages']['pic'];

                    $random = substr(md5(rand()), 0, 7);
                    $picName = $random . '-' . $pic['name'];
                    move_uploaded_file($pic['tmp_name'], "files/packages/$picName");
                    $this->request->data['Packages']['pic'] = isset($picName) ? $picName : '';
                }
                $this->request->data['Packages']['status'] = isset($this->request->data['Packages']['status']) ? 1 : 0;


                if ($this->Packages->save($this->request->data)) {
                    $this->Session->setFlash('Package has been saved successfuly', 'success');
                    $this->redirect(array('action' => 'adminPackages'));
                } else {
                    $this->Session->setFlash('Package could not be saved. Please, try again.', 'error');
                }
            }

            $breadcrumb = "Create New Package";
            $this->set(compact('breadcrumb'));
        } else {
            $this->redirect('/');
        }
    }

    function adminEdit($id = NULL) {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->layout = 'admin';
            $this->Packages->id = $id;
            $row2 = $this->Packages->read(null, $id);
            if ($this->request->is('post') || $this->request->is('put')) {
                if (!empty($_FILES['pic']['name'])) {
                    @unlink("files/packages/" . $row2['Packages']['pic']);
                    $pic = $_FILES['pic'];
                    $random = substr(md5(rand()), 0, 7);
                    $picName = $random . '-' . $pic['name'];
                    move_uploaded_file($pic['tmp_name'], "files/packages/$picName");
                    $this->request->data['Packages']['pic'] = isset($picName) ? $picName : '';
                }
                $this->request->data['Packages']['status'] = isset($this->request->data['Packages']['status']) ? 1 : 0;
                if ($this->Packages->save($this->request->data)) {
                    $this->Session->setFlash('Package has been updated successfuly', 'success');
                    $this->redirect(array('action' => 'adminPackages'));
                } else {
                    $this->Session->setFlash('Package could not be saved. Please, try again.', 'error');
                }
            }else{
                $this->request->data = $this->Packages->read(null, $id);
            }

            $breadcrumb = "Edit Package";
            $this->set(compact('row2', 'breadcrumb'));
        } else {
            $this->redirect('/');
        }
    }

    function adminDelete($id = NULL) {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->Packages->id = $id;
            if ($this->Packages->delete()) {
                $this->Session->setFlash('Package has been deleted successfuly', 'success');
                $this->redirect(array('action' => 'adminPackages'));
            }
        } else {
            $this->redirect('/');
        }
    }

    function logout() {
        $this->Session->destroy();
        $this->Session->setFlash('You have successfuly loged out', 'success');
        $this->redirect(array(
            "controller" => "admin",
            "action" => "index"));
    }

}
