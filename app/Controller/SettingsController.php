<?php

App::uses('AppController', 'Controller');

class SettingsController extends AppController {

    public $uses = 'Settings';

    public function beforeFilter(){
        parent::beforeFilter();
    }

    public function index() {
        redirect('/');
    }
    function adminSetting() {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->layout = 'admin';
            $this->Settings->id = 1;
            if ($this->request->is('post') || $this->request->is('put')) {
                
                if ($this->Settings->save($this->request->data)) {
                    $this->Session->setFlash('Your information has been saved successfuly', 'success');
                    $this->redirect(array('action' => 'adminSetting'));
                } else {
                    $this->Session->setFlash('Your information could not be saved. Please, try again.', 'error');
                }
            } else {
               $this->request->data = $this->Settings->read(null, 1);
            }
            $breadcrumb = "Settings";
            $this->set(compact('breadcrumb'));
        } else {
            $this->redirect('/');
        }
    }
  

}
