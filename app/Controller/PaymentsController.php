<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class PaymentsController extends AppController {

    public $uses = array('Products', 'Orders', 'Renter', 'User', 'Payment', 'Invoice', 'Setting');
    public $components = array('Heartbeat', 'Paypal');

    public function beforeFilter(){
        parent::beforeFilter();
    }

    //public $helper = array('Html');
    public function index() {
        $this->layout = 'admin';
        $this->set(compact('index', 'subpage', 'title_for_layout'));
    }

    function adminPayments() {

        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->layout = "admin";
            $result = array(); //$this->Products->find('all');
            $breadcrumb = "Payments";
            $this->set(compact('result', 'breadcrumb'));
        } else {
            $this->redirect('/');
        }
    }

    function adminDelete($id = NULL) {
        $sess = $this->Session->read('Ad');
        if (!empty($sess)) {
            $this->Packages->id = $id;
            if ($this->Packages->delete()) {
                $this->Session->setFlash('Package has been deleted successfuly', 'success');
                $this->redirect(array('action' => 'adminPackages'));
            }
        } else {
            $this->redirect('/');
        }
    }

    function logout() {
        $this->Session->destroy();
        $this->Session->setFlash('You have successfuly loged out', 'success');
        $this->redirect(array(
            "controller" => "admin",
            "action" => "index"));
    }

    function paymentProcess() {
        ob_start();
        $this->autoRender = false;
        $returnURL = RETURN_URL;
        $cancelURL = CANCEL_URL;
        //print_r($_POST);die;
        $resArray = $this->Paypal->CallShortcutExpressCheckout($_POST, $returnURL, $cancelURL);
        $ack = strtoupper($resArray["ACK"]);
        if($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") 
		{  
			//if SetExpressCheckout API call is successful
            $this->Paypal->RedirectToPayPal($resArray["TOKEN"]);
        } else 
		{
            //Display a user friendly Error on the page using any of the following error information returned by PayPal
            $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
            $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
            $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
            $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

            echo "SetExpressCheckout API call failed. ";
            echo "Detailed Error Message: " . $ErrorLongMsg;
            echo "Short Error Message: " . $ErrorShortMsg;
            echo "Error Code: " . $ErrorCode;
            echo "Error Severity Code: " . $ErrorSeverityCode;
        }
    }

    function paymentSuccess() {
        $this->layout = 'inner';
        extract($_GET);
        if (isset($token) AND ! empty($token)) {
            // remove cart if necessary
            $this->cart_reset();
            $returnArray = $this->Paypal->GetShippingDetails($token);

            $ackGetExpressCheckout = strtoupper($returnArray["ACK"]);
            if ($ackGetExpressCheckout == "SUCCESS" || $ackGetExpressCheckout == "SUCESSWITHWARNING") {
                $this->request->data['Orders']['name'] = $returnArray['SHIPTONAME'];
                $this->request->data['Orders']['amount'] = $returnArray['PAYMENTREQUEST_0_AMT'];
                $this->request->data['Orders']['email'] = $returnArray['EMAIL'];
                $this->request->data['Orders']['all_string'] = serialize($returnArray);
                $custom = explode('-', $returnArray['CUSTOM']); // first id for customer 2nd id for product 3rd id for seller
                $custRow = $this->User->read(null, $custom[0]);
                $vendRow = $this->User->read(null, $custom[2]);
                $this->request->data['Orders']['token'] = $token;
                $this->request->data['Orders']['payer_id'] = $PayerID;
                $this->request->data['Orders']['buyer'] = current($custom);
                $this->request->data['Orders']['seller'] = end($custom);
                $this->request->data['Orders']['order_id'] = $custom[1];
                $this->request->data['Orders']['order_id'] = $custom[1];
                $odr = $this->Orders;
                if ($this->Orders->save($this->request->data)) {
//$this->Session->write('newId', '');
$lastOrder = $this->Orders->getLastInsertId();

                    /* email Customer */

                    $topMessageCust = "Thank you for choosing skirentalDb.com! Here’s your rental reservation invoice for the equipment you just reserved! <br/> Have an awesome trip and we look forward to seeing you again soon! If you have any questions or concerns, please feel
free to email us at info@skirentalDb.com or call us direct at 913.353.5959 , we’ll be happy to help!.";
                    $invoiceBody = $this->getBody($custom, $token, $topMessageCust);
$this->Heartbeat->sendEmail($custRow['User']['email'], "SkiRentalDB: Reservation", $custRow['User']['firstname'], $invoiceBody,$topMessageCust);
                    /* email Vendor */
                    $topMessageVend = "Here’s another skirentalDb.com reservation for you!<br/>If for any reason you cannot fulfill this reservation, please contact skirentalDb.com at bustedrez@skirentalDb.com as
well as the customer at: <".$custRow['User']['email'].">";
                    $invoiceBody = $this->getBody($custom, $token, $topMessageVend);
					$topMessageSave='Save';
					 $invoiceToSave = $this->getBody($custom, $token, $topMessageSave);
$this->Heartbeat->sendEmail($vendRow['User']['b_email'], "SkiRentalDB: Reservation", $vendRow['User']['b_name'], $invoiceBody,$topMessageVend);
$this->Heartbeat->sendEmail($vendRow['User']['rr_email'], "SkiRentalDB: Reservation", $vendRow['User']['b_name'], $invoiceBody,$topMessageVend);

                    
                    /* save invoice */
                    $this->request->data['Invoice']['buyer'] = $custom[0];
                    $this->request->data['Invoice']['seller'] = $custom[2];
                    $this->request->data['Invoice']['product_id'] = $custom[1];
                    $this->request->data['Invoice']['invoice'] = $invoiceToSave;
			$this->request->data['Invoice']['order_no'] = $lastOrder;


                    if ($this->Invoice->save($this->request->data)) {
                        $this->Session->write('newId', '');
$this->redirect(array('controller' => 'payments','action'=>'paymentSuccess'));
                    }
                }
            }
        }

        if (isset($card_payment)) {
            // reset cart if necessary
            $this->cart_reset();
            $this->Session->write('newId', '');
            $this->redirect(array('controller' => 'payments','action'=>'paymentSuccess'));
        }
    }

    private function cart_reset() {
        // delete from cart if cart renters
        $this->loadModel('Cart');
        $payment_id = $this->Session->read('newId');
        $user_id = $this->Session->read('fe.userId');
        $cart_info = $this->Cart->findByUserId($user_id);
        if (!empty($cart_info)) { // if cart data exist
            if ($payment_id == $cart_info['Cart']['payment_id']) { // if payment for the same orders
                $this->Cart->deleteAll(array('Cart.user_id' => $user_id));
            }
        }
    }

    function getBody($custom, $token, $topMessage) {

        $custRow = $this->User->read(null, $custom[0]);
        $vendRow = $this->User->read(null, $custom[2]);

        $vendorStoreInfo = "<strong>" . $vendRow['User']['b_name'] . "</strong><br>" . $vendRow['User']['b_address'] . "<br>" . $vendRow['User']['b_city'] . ", " . $vendRow['User']['b_state'] . ", USA";

        $customerInfo = "<strong>" . $custRow['User']['firstname'] . " " . $custRow['User']['lastname'] . "</strong><br>" . $custRow['User']['address'] . "<br>" . $custRow['User']['city'] . ", " . $custRow['User']['state'];

        $logoPath = FULL_BASE_URL . $this->base . "/images/logo1.png";

// first id for customer 2nd id for product 3rd id for seller
        $settingRow = $this->Setting->read(null, 1);
        $results = $this->Renter->find('all', array(
            'conditions' => array('Renter.order_id' => $custom[1], 'Renter.user_id' => $custom[0]),
            'joins' => array(
                array(
                    'alias' => 'Pay',
                    'table' => 'payments',
                    'type' => 'LEFT',
                    'conditions' => 'Pay.id = Renter.order_id'
                ),
                array(
                    'alias' => 'PCK',
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'conditions' => 'PCK.id = Renter.package'
                ),
                array(
                    'alias' => 'Products',
                    'table' => 'products',
                    'type' => 'LEFT',
                    'conditions' => 'Products.id = Renter.product_id'
                )
            ),
            'fields' => array(
                'Pay.picup_date',
                'Pay.return_date',
                'Pay.package',
                'Renter.firstname',
                'Renter.lastname',
                'Renter.age',
                'Renter.gender',
				'Renter.height',
				'Renter.weight',
				'Renter.boot_size',
                'PCK.title',
                'Products.discount',
                'Products.walk_price',
                'Products.srdb_price',
                'Products.user_id',
            )
        ));








        $x = 1;$inTotal=0;$disc=0;$walkPrice=0;
        $bahrWalaArray = array();
        $totalDays = 0;
		  $renterTr = '<tr style="text-align:left;">
                                	<th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Name</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Sex</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Package</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Boots</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Age</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Height</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Weight</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Shoe Size</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Pickup/Return</th>
                                    <th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;">Days</th>
									<th style="border:solid 1px #e0e0e0; padding:5px; background-color:#eee;;text-align:right">Cost</th>
                                </tr>';

//print_r($results);die;
        if (!empty($results)) {

            //echo 'here';die;
            $bahrWalaArray = array();

            foreach ($results as $row):
                if (isset($row['Pay']['picup_date']) AND ! empty($row['Pay']['picup_date'])) {
                    $expF = explode(', ', $row['Pay']['picup_date']);
                    $expF2 = explode('/', end($expF));
                    $makeFromDate = $expF2[2] . '-' . $expF2[0] . '-' . $expF2[1];
                }
                if (isset($row['Pay']['return_date']) AND ! empty($row['Pay']['return_date'])) {
                    $expT = explode(', ', $row['Pay']['return_date']);
                    $expT2 = explode('/', end($expT));
                    $makeToDate = $expT2[2] . '-' . $expT2[0] . '-' . $expT2[1];
                }
                $daysDiff = $this->howDays($makeFromDate, $makeToDate) + 1;
			if($row['Renter']['boot_size']=='')
			{
				$boots='No';
			}
			else
			{
				$boots='Yes';
			}
			$renterTr.='<tr style="vertical-align:top;">
                                	<td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">'.$row['Renter']['firstname'] . " " . $row['Renter']['lastname'].'</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">'.$row['Renter']['gender'].'</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">' . $row['PCK']['title'] . '</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">'.$boots.'</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">'.$row['Renter']['age'].'</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">'.$row['Renter']['height'].'</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">'.$row['Renter']['weight'].' lbs</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">'.$row['Renter']['boot_size'].'</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">'.$row['Pay']['picup_date'].'<br/>'.$row['Pay']['return_date'].'</td>
                                    <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;">'.$daysDiff .'</td>
									 <td style="border:solid 1px #e0e0e0; border-bottom:0; border-top:0; padding:5px;text-align:right">$' . number_format($row['Products']['srdb_price'],2) . '</td>
                                    
                                </tr>';
			 
                $bahrWalaArray['discount'] = $row['Products']['discount'];
                $bahrWalaArray['WP'] = $row['Products']['walk_price'];
                $bahrWalaArray['SP'] = $row['Products']['srdb_price'];
                $bahrWalaArray['vendorId'] = $row['Products']['user_id'];
                $walkPrice=$walkPrice+ ($row['Products']['walk_price'] * $daysDiff);
                    $inTotal =$inTotal+ ($row['Products']['srdb_price'] * $daysDiff);
                    $disc=$disc+(($row['Products']['walk_price']-$row['Products']['srdb_price'])*$daysDiff);
                $bahrWalaArray['picupDate'] = $row['Pay']['picup_date'];
                $bahrWalaArray['returnDate'] = $row['Pay']['return_date'];
                
                $totalDays += $daysDiff;
                $x++;
            endforeach;
            $disperc=$disc/$walkPrice*100;
			 } 
            $pickupDateSet = !empty($bahrWalaArray['picupDate']) ? $bahrWalaArray['picupDate'] : "";
            $returnDateSet = !empty($bahrWalaArray['returnDate']) ? $bahrWalaArray['returnDate'] : "";
            $vendorRow = $this->User->read(null, $bahrWalaArray['vendorId']);
            $damageWaiverForDay = !empty($vendorRow['User']['damage']) ? $vendorRow['User']['damage'] : '0';
            $damageWaiver = $damageWaiverForDay * $totalDays;
           // $inTotal = $bahrWalaArray['SP'] * $totalDays;
            $aamAadmiTax = !empty($vendorRow['User']['tax']) ? $vendorRow['User']['tax'] : '0.00';
            $taxDiKullRakam = ($inTotal * $aamAadmiTax) / 100; //don't smile focus on your work :)
            $grandTotalWithTax = $inTotal + $taxDiKullRakam;
            $grandTotal = $grandTotalWithTax + $damageWaiver;

            /* Payable Amount percentage */
            $payDoler = !empty($settingRow['Setting']['due_today']) ? $settingRow['Setting']['due_today'] : "0.00";
            $payableTotalAmt = $payDoler * $totalDays;
            $payableBalance = $grandTotal - $payableTotalAmt;
			
			
			

$renterTr .= '</table><!--table7 end-->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; vertical-align:middle;"><!--table8 -->
                            	<tr>
                                	<td>
                                    	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; vertical-align:middle;"><!--table9-->
                                        	<tr>
                                            	<th style="text-align:center; border:solid 1px #e0e0e0;  padding:5px;border-bottom:none">Pickup Location</th>
                                            </tr>
                                            	<tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none">'.$vendRow['User']['b_name'].'</td></tr>
                                                <tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none">'.$vendRow['User']['b_address'].'</td></tr>
                                                <tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none">'.$vendRow['User']['b_city'] . ", " . $vendRow['User']['b_state'].' '.$vendRow['User']['b_postalcode'].'</td></tr>
                                                <tr><td style="border-left: 1px solid #e0e0e0; border-right: 1px solid #e0e0e0;padding:5px;border-bottom:none">&nbsp;</td></tr>
                                                <tr><td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none">Phone:  '.$vendRow['User']['b_phone'].'</td></tr>
												<tr><td style="border-left: 1px solid #e0e0e0; border-right: 1px solid #e0e0e0;padding:5px;border-bottom: 1px solid #e0e0e0;">&nbsp;</td></tr>
                                        </table><!--table9 end-->
                                    </td>
                                    
                                    <td>
                                    	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; vertical-align:middle;"><!--table10-->
                                        	<tr>
                                            	<td style="border:solid 1px #e0e0e0;  padding:5px;border-bottom:none;border-right:none">Subtotal</td>
                                                <td style="border:solid 1px #e0e0e0;  padding:5px;border-bottom:none;border-left:none;text-align:right">$'.number_format($inTotal, 2).'</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-right:none">Discount &nbsp;&nbsp;&nbsp;&nbsp;'.number_format($disperc,2) .'%</td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-left:none;text-align:right">$'.number_format($disc, 2).'</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-right:none">Tax ('.number_format($aamAadmiTax, 2).'%)</td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-left:none;text-align:right">$'.number_format($taxDiKullRakam, 2).'</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-right:none">Damage Waiver</td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-left:none;text-align:right">$'.number_format($damageWaiver, 2).'</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-right:none">Total</td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-left:none;text-align:right">$'.number_format($grandTotal, 2).'</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-right:none">Non Refundable Reservation Fee </td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px;border-bottom:none;border-left:none;text-align:right">$'.number_format($payableTotalAmt, 2).'</td>
                                            </tr>
                                            <tr>
                                            	<td style="border:solid 1px #e0e0e0; border-top:0; padding:5px 5px; font-weight:bold; color:#090;border-right:none">Balance Due at Equipment Pickup</td>
                                                <td style="border:solid 1px #e0e0e0; border-top:0; padding:5px; font-weight:bold; color:#090;border-left:none;text-align:right">$'.number_format($payableBalance, 2).'</td>
                                            </tr>
                                        </table><!--table10 end-->
                                    </td>
                                </tr>
                            </table><!--table8 end-->
                        </td>
                    </tr>
					<tr style="text-align:center;">
                    	<td style="border-top:0; padding:20px;">
                        	<strong>Thank you for choosing skirentalDb.com to rent your equipment! We sincerely appreciate your business!</strong><br />
                            <strong>The balance of <span style="font-weight:bold; color:#090">$'.number_format($payableBalance, 2).'</span> is due when you pick up your equipment at your selected ski shop.</strong>
                            <p>* Please note: skirentalDb.com discount is only available for advanced reservations and is not valid with any other discount or offer.<br/> * Cancellation Policy: You will receive a full refund if you cancel 48 hours prior to your arrival.</p><strong>
                           	<p style="font-size:13px;"><strong>Each customer renting equipment must be present for proper fitting and signature on rental agreement.</strong></p>

                        </td>
                    </tr>
                    
                </table><!--table2 end-->
            </td>
        </tr>
        <tr><td style="text-align:center;"><img src="' . $logoPath . '"  style="width:150px; height:auto;"></td></tr>
        
    </table>';
            if($topMessage!='Save')
			{
				$msg=$topMessage;
			}
			else
			{
				$msg='';
			}
                    
          
	return '<table style="max-width:900px; width:90%; margin:0px auto;  border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px;"><!--table1 start-->
   		<tr>
        	<td>
            	<p style="font-style:italic;"></p>
				<p style="font-style:italic;">'.$msg.'</p>
             </td>
        </tr>
        
        <tr>
        	<td>
            	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:100%;"><!--table2 start -->
                	<tr>
                    	<td style=" padding:5px;">
                        	<table border="0" style="border-collapse:collapse; width:100%;"><!--table3 start -->
                            	<tr><td><img src="' . $logoPath . '" style="width:180px; height:auto;"></td></tr>
                            </table><!--table3 end-->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; text-transform:uppercase; text-align:center; vertical-align:middle;"><!--table4 start -->
                            	<tr>
                                	<td style="border:solid 1px #e0e0e0; padding:5px 8px;">STORE No.<br/>'.$vendRow['User']['ssn'].'</td>
                                    <td style="border:solid 1px #e0e0e0; padding:5px 8px;">Group Name</td>
                                    <td style="border:solid 1px #e0e0e0; padding:5px 8px;">Date Out<br/>' . $pickupDateSet . '</td>
                                    <td style="border:solid 1px #e0e0e0; padding:5px 8px;">Date Due Back<br/>' . $returnDateSet . '</td>
                                    <td style="border:solid 1px #e0e0e0; padding:5px 8px;">Res#<br/>' . $token . '</td>
                                </tr>
                            </table><!--table4 end-->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; text-align:center; vertical-align:middle;"><!--table5 start -->
                            	<tr>
                                	<td style=" padding:5px;"><a href="www.skirentalDb.com" target="_blank";>www.skirentalDb.com</a><br/><a href="mailto:info@skirentalDb.com">info@skirentalDb.com</a></td>
                                    <td style="font-size:14px; font-weight:bold;  padding:5px;">Rental Reservation Summary<br/>www.skirentalDb.com</td>
                                    <td style=" padding:5px;">SRDB Contact: <span style="font-size:14px; font-weight:bold;">913-353-5959</span> <br/>Website Rental Reservation</td>
                                </tr>
                                
                            </table><!--table5 end -->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; vertical-align:middle;"><!--table6 start -->
                            	<tr>
                                	<td style=" padding:5px 10px;">' . $customerInfo . '</td>
                                    <td style=" padding:5px 10px;">Home Phone: <strong>' . $custRow['User']['telephone'] . '</strong><br/> Email: <a href="mailto:' . $custRow['User']['email'] . '">' . $custRow['User']['email'] . '</a></td>
                                </tr>
                                <tr><td colspan="2"style="padding:5px 10px;">Created: '.date("m-d-Y").'</td></tr>                            
                            </table><!--table6 -->
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; width:100%; vertical-align:top;"><!--table7 -->
                            	<tr><td colspan="12" style=" padding:5px 10px; text-align:center;">'.$vendRow['User']['b_name'].'- Rental Information</td></tr>
                               ' . $renterTr;                    
                    
    }

    function howDays($from, $to) {
        $first_date = strtotime($from);
        $second_date = strtotime($to);
        $offset = $second_date - $first_date;
        return floor($offset / 60 / 60 / 24);
    }

    function paymentCancel() {
        $this->layout = 'inner';
    }

}

