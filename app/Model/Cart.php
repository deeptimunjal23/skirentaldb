<?php
App::uses('AppModel', 'Model');
/**
 * Cart Model
 *
 */
class Cart extends AppModel {

    public $recursive = -1;
    
    public $actsAs = array('Containable');

/**
 * Display field
 *
 * @var string
 */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
        ),
        'Renter' => array(
            'className' => 'Renter',
            'foreignKey' => 'renter_id',
        )
    );
    
    public $hasMany = array(
        
    );
    
    public $hasOne = array(
        
    );
    
    public $hasAndBelongsToMany = array();
}
